<?php 

 return array (
  'yes' => '是的',
  'no' => '没有',
  'is_default' => '默认？',
  'proc_close_disabled_error' => '函数proc_close（）被禁用。请联系您的托管提供商以启用    它。或者您可以添加到.env：can_execute_command = false以禁用此功能。',
  'change_image' => '更改图像',
  'delete_image' => '删除图像',
  'preview_image' => '预览图像',
  'image' => '图像',
  'using_button' => '使用按钮',
  'select_image' => '选择图像',
  'to_add_more_image' => '添加更多图片',
  'add_image' => '添加图像',
  'tools' => '工具',
  'email_template' => 
  array (
    'header' => '电子邮件模板标题',
    'footer' => '电子邮件模板页脚',
    'site_title' => '网站标题',
    'site_url' => '网站URL',
    'site_logo' => '网站标志',
    'date_time' => '当前日期时间',
    'date_year' => '当年',
    'site_admin_email' => '网站管理电子邮件',
  ),
);
