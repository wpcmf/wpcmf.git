<?php 

 return array (
  'platform_admin' => '平台管理',
  'dashboard' => '仪表板',
  'widgets' => '小部件',
  'plugins' => '插件',
  'settings' => '设置',
  'setting_general' => '一般',
  'setting_email' => '电子邮件',
  'system_information' => '系统信息',
  'theme' => '主题',
  'copyright' => 'Copyright :year &copy; :company. 版本: <span>:version</span>',
  'profile' => '配置文件',
  'logout' => '注销',
  'no_search_result' => '没有找到结果，请尝试使用不同的关键字。',
  'home' => '家',
  'search' => '搜索',
  'add_new' => '添加新',
  'n_a' => 'n / a',
  'page_loaded_time' => '装入的页面',
  'view_website' => '查看网站',
);
