<?php 

 return array (
  'filter' => '过滤器',
  'everything' => '一切',
  'image' => '图像',
  'video' => '视频',
  'document' => '文档',
  'view_in' => '查看',
  'all_media' => '所有媒体',
  'trash' => '垃圾',
  'recent' => '最近',
  'favorites' => '收藏夹',
  'upload' => '上传',
  'create_folder' => '创建文件夹',
  'refresh' => '刷新',
  'empty_trash' => '空垃圾',
  'search_file_and_folder' => '在当前文件夹中搜索',
  'sort' => '排序',
  'file_name_asc' => '文件名 -  ASC',
  'file_name_desc' => '文件名 -  DESC',
  'uploaded_date_asc' => '上传日期 -  asc',
  'uploaded_date_desc' => '上传日期 -  DESC',
  'size_asc' => '大小 -  asc',
  'size_desc' => '大小 -  DESC',
  'actions' => '动作',
  'nothing_is_selected' => '没有选择',
  'insert' => '插入',
  'folder_name' => '文件夹名称',
  'create' => '创建',
  'rename' => '重命名',
  'close' => '关闭',
  'save_changes' => '保存更改',
  'move_to_trash' => '将物品移动到垃圾桶',
  'confirm_trash' => '您确定要将这些项目移动到垃圾吗？',
  'confirm' => '确认',
  'confirm_delete' => '删除项目',
  'confirm_delete_description' => '您的请求无法回滚。你确定你想删除这些物品吗？',
  'empty_trash_title' => '空垃圾',
  'empty_trash_description' => '您的请求无法回滚。你确定你想删除垃圾中的所有物品吗？',
  'up_level' => '向上一个级别',
  'upload_progress' => '上传进度',
  'folder_created' => '文件夹已成功创建！',
  'gallery' => '媒体画廊',
  'trash_error' => '删除所选项目时出错',
  'trash_success' => '将所选项目移动成功垃圾！',
  'restore_error' => '恢复所选项目时出错',
  'restore_success' => '成功恢复所选项目！',
  'copy_success' => '成功复制了所选项目！',
  'delete_success' => '成功删除了所选项目！',
  'favorite_success' => '最喜欢的选定项目成功！',
  'remove_favorite_success' => '成功地从收藏夹中删除所选项目！',
  'rename_success' => '成功重命名所选项目！',
  'empty_trash_success' => '成功空垃圾！',
  'invalid_action' => '无效操作！',
  'file_not_exists' => '文件不存在！',
  'missing_zip_archive_extension' => '请启用ZiParchive扩展以下载文件！',
  'can_not_download_file' => '无法下载此文件！',
  'invalid_request' => '无效的请求！',
  'add_success' => '成功添加项目！',
  'file_too_big' => '文件太大。Max文件上传是：大小字节',
  'can_not_detect_file_type' => '文件类型不允许或无法检测到文件类型！',
  'upload_failed' => '文件未完全上传。服务器允许最大上载文件大小为：大小。请检查您的文件大小或尝试再次上载网络错误',
  'menu_name' => '媒体',
  'add' => '添加媒体',
  'name_invalid' => '文件夹名称具有无效字符。',
  'url_invalid' => '请提供有效的URL',
  'path_invalid' => '请提供有效的路径',
  'download_link' => '下载',
  'url' => 'url',
  'download_explain' => '每行输入一个URL。',
  'downloading' => '下载......',
  'javascript' => 
  array (
    'name' => '名称',
    'url' => 'url',
    'full_url' => '完整网址',
    'size' => '大小',
    'mime_type' => '类型',
    'created_at' => '上传',
    'updated_at' => '修改过',
    'nothing_selected' => '没有选择',
    'visit_link' => '打开链接',
    'no_item' => 
    array (
      'all_media' => 
      array (
        'icon' => 'fas fa-cloud-upload-alt',
        'title' => '将文件和文件夹拖放到此处',
        'message' => '或者使用上面的上传按钮',
      ),
      'trash' => 
      array (
        'icon' => 'fas fa-trash-alt',
        'title' => '目前您的垃圾箱中没有任何内容',
        'message' => '删除文件以自动将它们移动到垃圾箱。 从垃圾箱中删除文件以永久删除它们',
      ),
      'favorites' => 
      array (
        'icon' => 'fas fa-star',
        'title' => '您还没有将任何内容添加到您的收藏夹',
        'message' => '将文件添加到收藏夹以便以后轻松找到它们',
      ),
      'recent' => 
      array (
        'icon' => 'far fa-clock',
        'title' => '你还没有打开任何东西',
        'message' => '您最近打开的所有文件都将显示在此处',
      ),
      'default' => 
      array (
        'icon' => 'fas fa-sync',
        'title' => '没有内容',
        'message' => '此目录没有内容',
      ),
    ),
    'clipboard' => 
    array (
      'success' => '这些文件链接已复制到剪贴板',
    ),
    'message' => 
    array (
      'error_header' => '错误',
      'success_header' => '成功',
    ),
    'download' => 
    array (
      'error' => '未选择文件或无法下载这些文件',
    ),
    'actions_list' => 
    array (
      'basic' => 
      array (
        'preview' => '预览',
      ),
      'file' => 
      array (
        'copy_link' => '复制链接',
        'rename' => '重命名',
        'make_copy' => '制作副本',
      ),
      'user' => 
      array (
        'favorite' => '添加到收藏夹',
        'remove_favorite' => '删除收藏夹',
      ),
      'other' => 
      array (
        'download' => '下载',
        'trash' => '移到垃圾桶',
        'delete' => '永久删除',
        'restore' => '恢复',
      ),
    ),
  ),
);
