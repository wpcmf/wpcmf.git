<?php 

 return array (
  'password_confirmation' => '密码确认',
  'failed' => '失败了',
  'throttle' => '油门',
  'not_member' => '不是会员？',
  'register_now' => '立即注册',
  'lost_your_password' => '丢失了密码？',
  'login_title' => 'admin',
  'login_via_social' => '与社交网络登录',
  'back_to_login' => '返回登录页面',
  'sign_in_below' => '在下面登录',
  'languages' => '语言',
  'reset_password' => '重置密码',
  'login' => 
  array (
    'username' => '邮箱/用户名',
    'email' => '邮箱',
    'password' => '密码',
    'title' => '用户登录',
    'remember' => '记住我?',
    'login' => '登录',
    'placeholder' => 
    array (
      'username' => '请输入用户名',
      'email' => '请输入邮箱',
    ),
    'success' => '登录成功!',
    'fail' => '用户名或密码错误.',
    'not_active' => '您的帐号未激活!',
    'banned' => '您的帐号被冻结.',
    'logout_success' => '退出成功!',
    'dont_have_account' => '您的帐号不存在，请联系系统管理员!',
  ),
  'forgot_password' => 
  array (
    'title' => '忘记密码',
    'message' => '<p>您忘记密码了?</p><p>请输入您的邮件帐户。 系统将发送一封带有活动链接的邮件以重置您的密码.</p>',
    'submit' => 'Submit',
  ),
  'reset' => 
  array (
    'new_password' => '新密码',
    'password_confirmation' => '确认密码',
    'email' => '邮箱',
    'title' => '重置密码',
    'update' => '更新',
    'wrong_token' => '此链接无效或已过期。 请再次尝试使用重置表单.',
    'user_not_found' => '帐号不存在.',
    'success' => '重置密码成功!',
    'fail' => '令牌无效，重置密码链接已过期!',
    'reset' => 
    array (
      'title' => '邮箱重置密码',
    ),
    'send' => 
    array (
      'success' => '一封电子邮件已发送到您的电子邮件帐户。 请检查并完成此操作.',
      'fail' => '此时无法发送电子邮件。 请稍后再试.',
    ),
    'new-password' => '新密码',
  ),
  'email' => 
  array (
    'reminder' => 
    array (
      'title' => '邮箱重置密码',
    ),
  ),
  'settings' => 
  array (
    'email' => 
    array (
      'title' => 'ACL',
      'description' => 'ACL 邮件配置',
    ),
  ),
);
