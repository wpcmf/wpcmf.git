<?php 

 return array (
  'update_position_success' => '成功更新小部件位置！',
  'hide_success' => '成功更新小部件！',
  'confirm_hide' => '你确定吗？',
  'hide_message' => '你真的想隐藏这个小部件吗？它会在仪表板上消失！',
  'confirm_hide_btn' => '是的，隐藏这个小部件',
  'cancel_hide_btn' => '取消',
  'collapse_expand' => '崩溃/展开',
  'hide' => '隐藏',
  'reload' => '重新加载',
  'save_setting_success' => '成功保存小部件设置！',
  'widget_not_exists' => '小部件没有退出！',
  'manage_widgets' => '管理小部件',
  'fullscreen' => '全屏',
  'title' => '仪表板',
  'predefined_ranges' => 
  array (
    'yesterday' => '昨天',
    'last_7_days' => '过去7天',
    'this_month' => '本月',
    'last_30_days' => '过去30天',
  ),
);
