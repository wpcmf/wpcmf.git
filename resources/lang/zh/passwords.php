<?php 

 return array (
  'reset' => '您的密码已重置！',
  'sent' => '我们通过电子邮件发送了密码重置链接！',
  'throttled' => '请在重试之前等待。',
  'token' => '此密码重置令牌无效。',
  'user' => '我们找不到具有该电子邮件地址的用户。',
);
