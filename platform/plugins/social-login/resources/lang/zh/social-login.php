<?php 

 return array (
  'menu' => '社交登录',
  'settings' => 
  array (
    'description' => '配置社交登录选项',
    'enable' => '启用？',
    'facebook' => 
    array (
      'title' => 'Facebook login settings',
      'description' => 'Enable/disable & configure app credentials for Facebook login',
      'app_id' => 'App ID',
      'app_secret' => 'App Secret',
      'helper' => 'Please go to https://developers.facebook.com to create new app update App ID, App Secret. Callback URL is :callback',
    ),
    'google' => 
    array (
      'title' => 'Google login settings',
      'description' => 'Enable/disable & configure app credentials for Google login',
      'app_id' => 'App ID',
      'app_secret' => 'App Secret',
      'helper' => 'Please go to https://console.developers.google.com/apis/dashboard to create new app update App ID, App Secret. Callback URL is :callback',
    ),
    'github' => 
    array (
      'title' => 'Github login settings',
      'description' => 'Enable/disable & configure app credentials for Github login',
      'app_id' => 'App ID',
      'app_secret' => 'App Secret',
      'helper' => 'Please go to https://github.com/settings/developers to create new app update App ID, App Secret. Callback URL is :callback',
    ),
    'linkedin' => 
    array (
      'title' => 'Linkedin login settings',
      'description' => 'Enable/disable & configure app credentials for Linkedin login',
      'app_id' => 'App ID',
      'app_secret' => 'App Secret',
      'helper' => 'Please go to https://www.linkedin.com/developers/apps/new to create new app update App ID, App Secret. Callback URL is :callback',
    ),
  ),
);
