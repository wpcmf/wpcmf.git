<?php

namespace Wpcmf\SocialLogin\Http\Requests;

use Wpcmf\Support\Http\Requests\Request;

class SocialLoginRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
