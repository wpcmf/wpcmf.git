<?php

use Wpcmf\Member\Models\Member;

return [
    'supported' => [
        Member::class,
    ],
];
