<?php

return [
    // List supported modules or plugins
    'supported' => [
        'Wpcmf\Gallery\Models\Gallery',
        'Wpcmf\Page\Models\Page',
        'Wpcmf\Blog\Models\Post',
    ],
];
