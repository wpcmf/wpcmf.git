<?php 

 return array (
  'create' => '创建新画廊',
  'edit' => '编辑库',
  'galleries' => '画廊',
  'item' => '图库项目',
  'select_image' => '选择图像',
  'reset' => '重置库',
  'update_photo_description' => '更新照片的描述',
  'update_photo_description_placeholder' => '照片的描述......',
  'delete_photo' => '删除这张照片',
  'gallery_box' => '画廊图片',
  'by' => 'by',
  'menu_name' => '画廊',
  'gallery_images' => '画廊图片',
  'add_gallery_short_code' => '添加一个图库',
  'shortcode_name' => '画廊图片',
  'limit_display' => '限制数字显示',
);
