<?php

namespace Wpcmf\Gallery\Providers;

use Wpcmf\Base\Events\CreatedContentEvent;
use Wpcmf\Base\Events\DeletedContentEvent;
use Wpcmf\Base\Events\UpdatedContentEvent;
use Wpcmf\Gallery\Listeners\CreatedContentListener;
use Wpcmf\Gallery\Listeners\DeletedContentListener;
use Wpcmf\Gallery\Listeners\RenderingSiteMapListener;
use Wpcmf\Gallery\Listeners\UpdatedContentListener;
use Wpcmf\Theme\Events\RenderingSiteMapEvent;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        RenderingSiteMapEvent::class => [
            RenderingSiteMapListener::class,
        ],
        UpdatedContentEvent::class   => [
            UpdatedContentListener::class,
        ],
        CreatedContentEvent::class   => [
            CreatedContentListener::class,
        ],
        DeletedContentEvent::class   => [
            DeletedContentListener::class,
        ],
    ];
}
