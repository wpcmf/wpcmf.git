<?php

namespace Wpcmf\Gallery\Repositories\Caches;

use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;
use Wpcmf\Gallery\Repositories\Interfaces\GalleryMetaInterface;

class GalleryMetaCacheDecorator extends CacheAbstractDecorator implements GalleryMetaInterface
{

}
