<?php

namespace Wpcmf\Gallery\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface GalleryMetaInterface extends RepositoryInterface
{
}
