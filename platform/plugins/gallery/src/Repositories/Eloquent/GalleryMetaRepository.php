<?php

namespace Wpcmf\Gallery\Repositories\Eloquent;

use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;
use Wpcmf\Gallery\Repositories\Interfaces\GalleryMetaInterface;

class GalleryMetaRepository extends RepositoriesAbstract implements GalleryMetaInterface
{
}
