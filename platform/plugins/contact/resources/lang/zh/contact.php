<?php 

 return array (
  'menu' => '联系',
  'edit' => '查看联系人',
  'contact_information' => '联系信息',
  'replies' => '回复',
  'contact_sent_from' => '发送的联系信息',
  'sender' => '发件人',
  'sender_email' => '电子邮件',
  'sender_address' => '地址',
  'sender_phone' => '手机',
  'message_content' => '消息内容',
  'sent_from' => '发送的电子邮件',
  'form_name' => '名称',
  'form_email' => '电子邮件',
  'form_address' => '地址',
  'form_subject' => '主题',
  'form_phone' => '手机',
  'form_message' => '消息',
  'required_field' => '使用（<span style ="color:red"> * </span>）是必需的。',
  'send_btn' => '发送消息',
  'new_msg_notice' => '您有<span class ="blod">：count </span>新消息',
  'view_all' => '查看全部',
  'phone' => '手机',
  'address' => '地址',
  'message' => '消息',
  'no_reply' => '还没有回复！',
  'reply' => '回复',
  'send' => '发送',
  'shortcode_name' => '联系表格',
  'shortcode_description' => '添加联系表格',
  'shortcode_content_description' => '添加短代码[contact-form] [/contact-form]到编辑？',
  'message_sent_success' => '消息成功发送！',
  'tables' => 
  array (
    'phone' => '手机',
    'email' => '电子邮件',
    'full_name' => '全名',
    'time' => '时间',
    'address' => '地址',
    'subject' => '主题',
    'content' => '内容',
  ),
  'email' => 
  array (
    'header' => '电子邮件',
    'title' => '您网站的新联系人',
  ),
  'statuses' => 
  array (
    'read' => '阅读',
    'unread' => '未读',
  ),
  'form' => 
  array (
    'name' => 
    array (
      'required' => '姓名为必填项',
    ),
    'email' => 
    array (
      'required' => '邮箱为必填项',
      'email' => '邮箱地址无效',
    ),
    'content' => 
    array (
      'required' => '内容为必填项',
    ),
  ),
  'settings' => 
  array (
    'email' => 
    array (
      'title' => '联系',
      'description' => '联系邮箱配置',
      'templates' => 
      array (
        'notice_title' => '向管理员发送通知',
        'notice_description' => '系统获得新联系人时向管理员发送通知的电子邮件模板',
      ),
    ),
  ),
);
