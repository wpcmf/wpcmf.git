<?php

namespace Wpcmf\Contact\Repositories\Caches;

use Wpcmf\Contact\Repositories\Interfaces\ContactReplyInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class ContactReplyCacheDecorator extends CacheAbstractDecorator implements ContactReplyInterface
{
}
