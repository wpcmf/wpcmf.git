<?php

namespace Wpcmf\Contact\Repositories\Eloquent;

use Wpcmf\Contact\Repositories\Interfaces\ContactReplyInterface;
use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;

class ContactReplyRepository extends RepositoriesAbstract implements ContactReplyInterface
{
}
