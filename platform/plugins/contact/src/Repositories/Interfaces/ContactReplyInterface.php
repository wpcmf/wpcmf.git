<?php

namespace Wpcmf\Contact\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface ContactReplyInterface extends RepositoryInterface
{
}
