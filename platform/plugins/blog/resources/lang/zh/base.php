<?php 

 return array (
  'menu_name' => '博客',
  'blog_page' => '博客页面',
  'select' => '- 选择 -',
  'blog_page_id' => '博客页面',
  'number_posts_per_page' => '每页号码',
  'write_some_tags' => '写一些标签',
  'short_code_name' => '博客帖子',
  'short_code_description' => '添加博客帖子',
  'number_posts_per_page_in_category' => '类别中每页的帖子数',
  'number_posts_per_page_in_tag' => '标签中每页的帖子数',
);
