<?php 

 return array (
  'create' => '创建新帖子',
  'edit' => '编辑帖子',
  'cannot_delete' => '帖子无法删除',
  'post_deleted' => '删除帖子',
  'posts' => '帖子',
  'post' => '帖子',
  'edit_this_post' => '编辑这篇文章',
  'no_new_post_now' => '现在没有新帖子！',
  'menu_name' => '帖子',
  'widget_posts_recent' => '最近的帖子',
  'categories' => '类别',
  'category' => '类别',
  'author' => '作者',
  'form' => 
  array (
    'name' => '名称',
    'name_placeholder' => '帖子的名称（最大：C字符）',
    'description' => '描述',
    'description_placeholder' => '帖子的简短描述（最大：C字符）',
    'categories' => '类别',
    'tags' => '标签',
    'tags_placeholder' => '标签',
    'content' => '内容',
    'is_featured' => '是特色的？',
    'note' => '注意内容',
    'format_type' => '格式',
  ),
);
