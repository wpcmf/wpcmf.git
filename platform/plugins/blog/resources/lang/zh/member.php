<?php 

 return array (
  'dob' => '出生：日期',
  'draft_posts' => '帖子',
  'pending_posts' => '待处理帖子',
  'published_posts' => '已发表的帖子',
  'posts' => '帖子',
  'write_post' => '写一个帖子',
);
