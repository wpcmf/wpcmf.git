<?php 

 return array (
  'create' => '创建新标签',
  'edit' => '编辑标记',
  'cannot_delete' => '标签无法删除',
  'deleted' => '删除了标签',
  'menu' => '标签',
  'edit_this_tag' => '编辑此标记',
  'menu_name' => '标签',
  'form' => 
  array (
    'name' => '名称',
    'name_placeholder' => '标签的名称（最多120个字符）',
    'description' => '描述',
    'description_placeholder' => '标签的简短描述（最多400个字符）',
    'categories' => '类别',
  ),
  'notices' => 
  array (
    'no_select' => '请选择至少一个标签来获取此操作！',
  ),
);
