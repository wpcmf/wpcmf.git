<?php 

 return array (
  'create' => '新会员',
  'edit' => '编辑成员',
  'menu_name' => '成员',
  'confirmation_subject' => '电子邮件验证',
  'confirmation_subject_title' => '验证您的电子邮件',
  'not_confirmed' => '尚未确认给定的电子邮件地址。<a href=":resend_link">重新发送确认链接。</a>',
  'confirmation_successful' => '您成功确认了您的电子邮件地址。',
  'confirmation_info' => '请确认您的电子邮件地址。',
  'confirmation_resent' => '我们给你发了另一封确认电子邮件。你应该很快收到它。',
  'forgot_password' => '忘了密码',
  'login' => '登录',
  'first_name' => '名字',
  'last_name' => '姓氏',
  'email_placeholder' => '如:example@gmail.com',
  'write_a_post' => '写一个帖子',
  'phone' => '手机',
  'phone_placeholder' => '手机',
  'confirmed_at' => '确认',
  'avatar' => '头像',
  'dob' => '出生日期',
  'form' => 
  array (
    'email' => '电子邮件',
    'password' => '密码',
    'password_confirmation' => '密码确认',
    'change_password' => '更改密码？',
  ),
  'settings' => 
  array (
    'email' => 
    array (
      'title' => '会员',
      'description' => '确认邮箱',
    ),
  ),
);
