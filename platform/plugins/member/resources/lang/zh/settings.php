<?php 

 return array (
  'title' => '会员',
  'description' => '成员设置',
  'verify_account_email' => '验证帐户的电子邮件吗？',
  'verify_account_email_description' => '需要在admin中配置电子邮件 - >设置 - >电子邮件发送电子邮件验证。',
);
