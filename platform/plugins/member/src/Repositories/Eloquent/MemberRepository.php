<?php

namespace Wpcmf\Member\Repositories\Eloquent;

use Wpcmf\Member\Repositories\Interfaces\MemberInterface;
use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;

class MemberRepository extends RepositoriesAbstract implements MemberInterface
{
}
