<?php

namespace Wpcmf\Member\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface MemberInterface extends RepositoryInterface
{
}
