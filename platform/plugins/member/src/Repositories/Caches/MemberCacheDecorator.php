<?php

namespace Wpcmf\Member\Repositories\Caches;

use Wpcmf\Member\Repositories\Interfaces\MemberInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class MemberCacheDecorator extends CacheAbstractDecorator implements MemberInterface
{

}
