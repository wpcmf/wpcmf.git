<?php

namespace Wpcmf\Member\Repositories\Caches;

use Wpcmf\Member\Repositories\Interfaces\MemberActivityLogInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class MemberActivityLogCacheDecorator extends CacheAbstractDecorator implements MemberActivityLogInterface
{
    /**
     * {@inheritDoc}
     */
    public function getAllLogs($memberId, $paginate = 10)
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}
