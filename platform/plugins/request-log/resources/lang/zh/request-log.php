<?php 

 return array (
  'name' => '请求日志',
  'status_code' => '状态代码',
  'no_request_error' => '现在没有请求错误！',
  'widget_request_errors' => '请求错误',
  'count' => '计数',
  'delete_all' => '删除所有记录',
);
