<?php

namespace Wpcmf\RequestLog\Repositories\Caches;

use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;
use Wpcmf\RequestLog\Repositories\Interfaces\RequestLogInterface;

class RequestLogCacheDecorator extends CacheAbstractDecorator implements RequestLogInterface
{
}
