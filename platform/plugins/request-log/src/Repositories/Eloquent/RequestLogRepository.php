<?php

namespace Wpcmf\RequestLog\Repositories\Eloquent;

use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;
use Wpcmf\RequestLog\Repositories\Interfaces\RequestLogInterface;

class RequestLogRepository extends RepositoriesAbstract implements RequestLogInterface
{
}
