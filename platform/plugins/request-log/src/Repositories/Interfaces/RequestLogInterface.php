<?php

namespace Wpcmf\RequestLog\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface RequestLogInterface extends RepositoryInterface
{
}
