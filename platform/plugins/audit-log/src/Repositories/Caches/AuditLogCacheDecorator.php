<?php

namespace Wpcmf\AuditLog\Repositories\Caches;

use Wpcmf\AuditLog\Repositories\Interfaces\AuditLogInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

/**
 * @since 16/09/2016 10:55 AM
 */
class AuditLogCacheDecorator extends CacheAbstractDecorator implements AuditLogInterface
{
}
