<?php

namespace Wpcmf\AuditLog\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface AuditLogInterface extends RepositoryInterface
{
}
