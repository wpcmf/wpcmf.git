<?php

namespace Wpcmf\AuditLog\Repositories\Eloquent;

use Wpcmf\AuditLog\Repositories\Interfaces\AuditLogInterface;
use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;

/**
 * @since 16/09/2016 10:55 AM
 */
class AuditLogRepository extends RepositoriesAbstract implements AuditLogInterface
{
}
