<?php

namespace Wpcmf\AuditLog\Providers;

use Wpcmf\AuditLog\Commands\ActivityLogClearCommand;
use Wpcmf\AuditLog\Commands\CleanOldLogsCommand;
use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                ActivityLogClearCommand::class,
                CleanOldLogsCommand::class,
            ]);
        }
    }
}
