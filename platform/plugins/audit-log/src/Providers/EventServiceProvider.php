<?php

namespace Wpcmf\AuditLog\Providers;

use Wpcmf\AuditLog\Events\AuditHandlerEvent;
use Wpcmf\AuditLog\Listeners\AuditHandlerListener;
use Wpcmf\AuditLog\Listeners\CreatedContentListener;
use Wpcmf\AuditLog\Listeners\DeletedContentListener;
use Wpcmf\AuditLog\Listeners\LoginListener;
use Wpcmf\AuditLog\Listeners\UpdatedContentListener;
use Wpcmf\Base\Events\CreatedContentEvent;
use Wpcmf\Base\Events\DeletedContentEvent;
use Wpcmf\Base\Events\UpdatedContentEvent;
use Illuminate\Auth\Events\Login;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        AuditHandlerEvent::class   => [
            AuditHandlerListener::class,
        ],
        Login::class               => [
            LoginListener::class,
        ],
        UpdatedContentEvent::class => [
            UpdatedContentListener::class,
        ],
        CreatedContentEvent::class => [
            CreatedContentListener::class,
        ],
        DeletedContentEvent::class => [
            DeletedContentListener::class,
        ],
    ];
}
