<?php

namespace Wpcmf\Backup\Providers;

use Wpcmf\Backup\Commands\BackupCreateCommand;
use Wpcmf\Backup\Commands\BackupListCommand;
use Wpcmf\Backup\Commands\BackupRemoveCommand;
use Wpcmf\Backup\Commands\BackupRestoreCommand;
use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                BackupCreateCommand::class,
                BackupRestoreCommand::class,
                BackupRemoveCommand::class,
                BackupListCommand::class,
            ]);
        }
    }
}
