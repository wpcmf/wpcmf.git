<?php

namespace Wpcmf\Language\Repositories\Caches;

use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;
use Wpcmf\Language\Repositories\Interfaces\LanguageMetaInterface;

class LanguageMetaCacheDecorator extends CacheAbstractDecorator implements LanguageMetaInterface
{

}
