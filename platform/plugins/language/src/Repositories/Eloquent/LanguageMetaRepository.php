<?php

namespace Wpcmf\Language\Repositories\Eloquent;

use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;
use Wpcmf\Language\Repositories\Interfaces\LanguageMetaInterface;

class LanguageMetaRepository extends RepositoriesAbstract implements LanguageMetaInterface
{
}
