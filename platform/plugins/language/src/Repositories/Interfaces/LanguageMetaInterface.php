<?php

namespace Wpcmf\Language\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface LanguageMetaInterface extends RepositoryInterface
{
}
