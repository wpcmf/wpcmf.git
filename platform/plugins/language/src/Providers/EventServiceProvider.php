<?php

namespace Wpcmf\Language\Providers;

use Artisan;
use Wpcmf\Base\Events\CreatedContentEvent;
use Wpcmf\Base\Events\DeletedContentEvent;
use Wpcmf\Base\Events\UpdatedContentEvent;
use Wpcmf\Language\Listeners\ActivatedPluginListener;
use Wpcmf\Language\Listeners\CreatedContentListener;
use Wpcmf\Language\Listeners\DeletedContentListener;
use Wpcmf\Language\Listeners\ThemeRemoveListener;
use Wpcmf\Language\Listeners\UpdatedContentListener;
use Wpcmf\PluginManagement\Events\ActivatedPluginEvent;
use Wpcmf\Theme\Events\ThemeRemoveEvent;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UpdatedContentEvent::class => [
            UpdatedContentListener::class,
        ],
        CreatedContentEvent::class => [
            CreatedContentListener::class,
        ],
        DeletedContentEvent::class => [
            DeletedContentListener::class,
        ],
        ThemeRemoveEvent::class    => [
            ThemeRemoveListener::class,
        ],
        ActivatedPluginEvent::class    => [
            ActivatedPluginListener::class,
        ],
    ];

    public function boot()
    {
        parent::boot();

        if (version_compare(get_cms_version(), '7.0') > 0) {
            Event::listen(['cache:cleared'], function () {
                Artisan::call('route:trans:clear');
            });
        }
    }
}
