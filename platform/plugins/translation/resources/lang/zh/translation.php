<?php 

 return array (
  'translations' => '翻译',
  'translations_description' => '在系统中翻译所有单词。',
  'export_warning' => '警告，在使用\'PHP Artisan CMS将其导出到Resources / LANG文件之前，翻译不可见，直到使用\'PHP Artisan CMS：Translations：Exporting\'命令或发布按钮。',
  'import_done' => '完成导入，处理：计数器物品！重新载入此页面刷新组！',
  'translation_manager' => '翻译经理',
  'done_publishing' => '完成了组的翻译',
  'append_translation' => '附加新翻译',
  'replace_translation' => '替换现有翻译',
  'import_group' => '导入组',
  'confirm_publish_group' => '您确定要发布翻译组“：group”？这将覆盖现有的语言文件。',
  'publish_translations' => '发布翻译',
  'back' => '回来',
  'edit_title' => '输入翻译',
  'choose_group_msg' => '选择一个组以显示组翻译。如果没有可见组，请确保已导入翻译。',
  'choose_a_group' => '选择一个组',
  'locales' => 'locales',
  'theme-translations' => '主题翻译',
  'admin-translations' => '其他翻译',
  'translate_from' => '翻译',
  'to' => '到',
  'no_other_languages' => '没有其他语言翻译！',
  'edit' => '编辑',
  'locale' => '语言环境',
  'locale_placeholder' => '前：en',
  'name' => '名称',
  'default_locale' => '默认语言环境？',
  'actions' => '动作',
  'choose_language' => '选择语言',
  'add_new_language' => '添加新语言',
  'select_language' => '选择语言',
  'flag' => '标志',
  'folder_is_not_writeable' => '无法写入文件！文件夹/资源/郎不可写。请Chmod让它变得可写！',
  'delete' => '删除',
  'confirm_delete_message' => '你真的想删除这个区域设置吗？它将删除此本地/资源/郎的所有文件/文件夹！',
  'download' => '下载',
  'select_locale' => '选择locale',
  'theme_translations_instruction' => '单击文本转换。不要翻译变量，例如。：用户名，：查询，：链接...',
);
