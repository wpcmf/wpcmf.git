<?php 

 return array (
  'message' => '您在此网站上的体验将通过允许cookie来改进。',
  'button_text' => '允许cookie',
  'theme_options' => 
  array (
    'name' => 'cookie同意',
    'description' => 'cookie同意设置',
    'enable' => '启用cookie同意？',
    'message' => '消息',
    'button_text' => '按钮文本',
    'max_width' => 'max宽度（px）',
    'background_color' => '背景颜色',
    'text_color' => '文本颜色',
    'learn_more_url' => '了解更多URL',
    'learn_more_text' => '了解更多文字',
    'style' => '风格',
    'full_width' => '全宽',
    'minimal' => '最小',
  ),
);
