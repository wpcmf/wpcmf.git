<?php

namespace Wpcmf\PluginManagement\Providers;

use Wpcmf\PluginManagement\Commands\PluginActivateAllCommand;
use Wpcmf\PluginManagement\Commands\PluginActivateCommand;
use Wpcmf\PluginManagement\Commands\PluginAssetsPublishCommand;
use Wpcmf\PluginManagement\Commands\PluginDeactivateCommand;
use Wpcmf\PluginManagement\Commands\PluginRemoveCommand;
use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                PluginAssetsPublishCommand::class,
            ]);
        }

        $this->commands([
            PluginActivateCommand::class,
            PluginDeactivateCommand::class,
            PluginRemoveCommand::class,
            PluginActivateAllCommand::class,
        ]);
    }
}
