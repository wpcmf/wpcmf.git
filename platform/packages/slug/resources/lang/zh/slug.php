<?php 

 return array (
  'permalink_settings' => 'permalink',
  'preview' => '预览',
  'prefix_for' => '前缀为:name',
  'settings' => 
  array (
    'title' => '永久链接设置',
    'description' => '管理所有模块的永久链接。',
    'preview' => '预览',
    'turn_off_automatic_url_translation_into_latin' => '关闭自动网址转换为拉丁语？',
  ),
);
