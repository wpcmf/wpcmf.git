<?php

return [
    'pattern'   => '--slug--',
    'supported' => [
        'Wpcmf\Page\Models\Page' => 'Pages',
    ],
    'prefixes'  => [

    ],
    'disable_preview' => [

    ],
];
