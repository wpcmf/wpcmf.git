<?php

namespace Wpcmf\Slug\Repositories\Caches;

use Wpcmf\Slug\Repositories\Interfaces\SlugInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class SlugCacheDecorator extends CacheAbstractDecorator implements SlugInterface
{

}
