<?php

namespace Wpcmf\Slug\Repositories\Eloquent;

use Wpcmf\Slug\Repositories\Interfaces\SlugInterface;
use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;

class SlugRepository extends RepositoriesAbstract implements SlugInterface
{
}
