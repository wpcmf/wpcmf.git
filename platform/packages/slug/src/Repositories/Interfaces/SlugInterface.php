<?php

namespace Wpcmf\Slug\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface SlugInterface extends RepositoryInterface
{
}
