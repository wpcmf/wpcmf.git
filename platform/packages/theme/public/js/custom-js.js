/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!******************************************************************!*\
  !*** ./platform/packages/theme/resources/assets/js/custom-js.js ***!
  \******************************************************************/
$(document).ready(function () {
  Wpcmf.initCodeEditor('header_js', 'javascript');
  Wpcmf.initCodeEditor('body_js', 'javascript');
  Wpcmf.initCodeEditor('footer_js', 'javascript');
});
/******/ })()
;