<?php

namespace Wpcmf\Theme\Exceptions;

use UnexpectedValueException;

class UnknownViewFileException extends UnexpectedValueException
{
}
