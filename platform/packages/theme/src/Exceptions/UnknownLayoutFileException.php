<?php

namespace Wpcmf\Theme\Exceptions;

use UnexpectedValueException;

class UnknownLayoutFileException extends UnexpectedValueException
{
}
