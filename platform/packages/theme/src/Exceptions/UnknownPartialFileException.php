<?php

namespace Wpcmf\Theme\Exceptions;

use UnexpectedValueException;

class UnknownPartialFileException extends UnexpectedValueException
{
}
