<?php

namespace Wpcmf\Theme\Exceptions;

use UnexpectedValueException;

class UnknownThemeException extends UnexpectedValueException
{
}
