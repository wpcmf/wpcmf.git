<?php

namespace Wpcmf\Theme\Http\Requests;

use Wpcmf\Support\Http\Requests\Request;

class CustomJsRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
