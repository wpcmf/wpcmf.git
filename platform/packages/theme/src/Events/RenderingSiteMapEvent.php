<?php

namespace Wpcmf\Theme\Events;

use Wpcmf\Base\Events\Event;
use Illuminate\Queue\SerializesModels;

class RenderingSiteMapEvent extends Event
{
    use SerializesModels;
}
