<?php

namespace Wpcmf\Theme\Events;

use Wpcmf\Base\Events\Event;
use Illuminate\Queue\SerializesModels;

class RenderingHomePageEvent extends Event
{
    use SerializesModels;
}