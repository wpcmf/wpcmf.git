<?php

namespace Wpcmf\Menu\Repositories\Eloquent;

use Wpcmf\Menu\Repositories\Interfaces\MenuLocationInterface;
use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;

class MenuLocationRepository extends RepositoriesAbstract implements MenuLocationInterface
{
}
