<?php

namespace Wpcmf\Menu\Repositories\Caches;

use Wpcmf\Menu\Repositories\Interfaces\MenuLocationInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class MenuLocationCacheDecorator extends CacheAbstractDecorator implements MenuLocationInterface
{
}
