<?php

namespace Wpcmf\Menu\Providers;

use Wpcmf\Base\Events\DeletedContentEvent;
use Wpcmf\Menu\Listeners\DeleteMenuNodeListener;
use Wpcmf\Menu\Listeners\UpdateMenuNodeUrlListener;
use Wpcmf\Slug\Events\UpdatedSlugEvent;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UpdatedSlugEvent::class    => [
            UpdateMenuNodeUrlListener::class,
        ],
        DeletedContentEvent::class => [
            DeleteMenuNodeListener::class,
        ],
    ];
}
