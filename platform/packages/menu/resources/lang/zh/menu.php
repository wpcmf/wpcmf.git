<?php 

 return array (
  'name' => '菜单',
  'key_name' => '菜单名称（键：键）',
  'basic_info' => '基本信息',
  'add_to_menu' => '添加到菜单',
  'custom_link' => '自定义链接',
  'add_link' => '添加链接',
  'structure' => '菜单结构',
  'remove' => '删除',
  'cancel' => '取消',
  'title' => '标题',
  'icon' => '图标',
  'url' => '网址',
  'target' => '目标',
  'css_class' => 'css类',
  'self_open_link' => '直接打开链接',
  'blank_open_link' => '在新选项卡中打开链接',
  'create' => '创建菜单',
  'edit' => '编辑菜单',
  'menu_settings' => '菜单设置',
  'display_location' => '显示位置',
);
