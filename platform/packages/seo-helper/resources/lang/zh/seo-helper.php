<?php 

 return array (
  'meta_box_header' => '搜索引擎优化',
  'edit_seo_meta' => '编辑SEO元',
  'default_description' => '设置元标题和描述，使您的网站易于在谷歌等搜索引擎上发现',
  'seo_title' => 'seo标题',
  'seo_description' => 'SEO描述',
);
