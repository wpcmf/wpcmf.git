<?php

namespace Wpcmf\SeoHelper\Listeners;

use Wpcmf\Base\Events\CreatedContentEvent;
use Exception;
use SeoHelper;

class CreatedContentListener
{

    /**
     * Handle the event.
     *
     * @param CreatedContentEvent $event
     * @return void
     */
    public function handle(CreatedContentEvent $event)
    {
        try {
            SeoHelper::saveMetaData($event->screen, $event->request, $event->data);
        } catch (Exception $exception) {
            info($exception->getMessage());
        }
    }
}
