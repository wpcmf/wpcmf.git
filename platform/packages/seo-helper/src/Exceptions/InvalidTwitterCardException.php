<?php

namespace Wpcmf\SeoHelper\Exceptions;

class InvalidTwitterCardException extends InvalidArgumentException
{
}
