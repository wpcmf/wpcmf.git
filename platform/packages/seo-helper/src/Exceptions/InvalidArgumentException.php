<?php

namespace Wpcmf\SeoHelper\Exceptions;

class InvalidArgumentException extends SeoHelperException
{
}
