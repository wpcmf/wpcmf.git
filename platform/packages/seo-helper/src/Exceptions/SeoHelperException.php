<?php

namespace Wpcmf\SeoHelper\Exceptions;

use Exception;

abstract class SeoHelperException extends Exception
{
}
