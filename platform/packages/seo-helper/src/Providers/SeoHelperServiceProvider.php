<?php

namespace Wpcmf\SeoHelper\Providers;

use Wpcmf\Base\Supports\Helper;
use Wpcmf\Base\Traits\LoadAndPublishDataTrait;
use Wpcmf\SeoHelper\Contracts\SeoHelperContract;
use Wpcmf\SeoHelper\Contracts\SeoMetaContract;
use Wpcmf\SeoHelper\Contracts\SeoOpenGraphContract;
use Wpcmf\SeoHelper\Contracts\SeoTwitterContract;
use Wpcmf\SeoHelper\SeoHelper;
use Wpcmf\SeoHelper\SeoMeta;
use Wpcmf\SeoHelper\SeoOpenGraph;
use Wpcmf\SeoHelper\SeoTwitter;
use Illuminate\Support\ServiceProvider;

/**
 * @since 02/12/2015 14:09 PM
 */
class SeoHelperServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    public function register()
    {
        $this->app->bind(SeoMetaContract::class, SeoMeta::class);
        $this->app->bind(SeoHelperContract::class, SeoHelper::class);
        $this->app->bind(SeoOpenGraphContract::class, SeoOpenGraph::class);
        $this->app->bind(SeoTwitterContract::class, SeoTwitter::class);

        Helper::autoload(__DIR__ . '/../../helpers');
    }

    public function boot()
    {
        $this->setNamespace('packages/seo-helper')
            ->loadAndPublishConfigurations(['general'])
            ->loadAndPublishViews()
            ->loadAndPublishTranslations()
            ->publishAssets();

        $this->app->register(EventServiceProvider::class);

        $this->app->booted(function () {
            $this->app->register(HookServiceProvider::class);
        });
    }
}
