<?php

namespace Wpcmf\SeoHelper\Entities\Twitter;

use Wpcmf\SeoHelper\Bases\MetaCollection as BaseMetaCollection;

class MetaCollection extends BaseMetaCollection
{

    /**
     * Meta tag prefix.
     *
     * @var string
     */
    protected $prefix = 'twitter:';
}
