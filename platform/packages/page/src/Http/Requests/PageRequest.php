<?php

namespace Wpcmf\Page\Http\Requests;

use Wpcmf\Base\Enums\BaseStatusEnum;
use Wpcmf\Page\Supports\Template;
use Wpcmf\Support\Http\Requests\Request;
use Illuminate\Validation\Rule;

class PageRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|max:120',
            'content'  => 'required',
            'template' => Rule::in(array_keys(Template::getPageTemplates())),
            'status'   => Rule::in(BaseStatusEnum::values()),
        ];
    }
}
