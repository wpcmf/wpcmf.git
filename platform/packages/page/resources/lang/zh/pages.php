<?php 

 return array (
  'create' => '创建新页面',
  'edit' => '编辑页面',
  'cannot_delete' => '页面无法删除',
  'deleted' => '页面已删除',
  'pages' => '页面',
  'menu' => '页面',
  'menu_name' => '页面',
  'edit_this_page' => '编辑此页面',
  'total_pages' => '总页数',
  'front_page' => '首页',
  'form' => 
  array (
    'name' => '名称',
    'name_placeholder' => '页面的名称（最多120个字符）',
    'content' => '内容',
    'note' => '注意内容',
  ),
  'notices' => 
  array (
    'no_select' => '请选择至少一个记录来获取此操作！',
    'update_success_message' => '成功更新',
  ),
  'settings' => 
  array (
    'show_on_front' => '您的主页显示',
    'select' => '- 选择 -',
  ),
);
