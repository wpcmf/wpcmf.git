<?php

namespace Wpcmf\Widget\Repositories\Eloquent;

use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;
use Wpcmf\Widget\Repositories\Interfaces\WidgetInterface;

class WidgetRepository extends RepositoriesAbstract implements WidgetInterface
{
    /**
     * {@inheritDoc}
     */
    public function getByTheme($theme)
    {
        $data = $this->model->where('theme', $theme)->get();
        $this->resetModel();

        return $data;
    }
}
