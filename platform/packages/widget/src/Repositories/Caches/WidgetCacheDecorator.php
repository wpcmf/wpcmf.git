<?php

namespace Wpcmf\Widget\Repositories\Caches;

use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;
use Wpcmf\Widget\Repositories\Interfaces\WidgetInterface;

class WidgetCacheDecorator extends CacheAbstractDecorator implements WidgetInterface
{
    /**
     * {@inheritDoc}
     */
    public function getByTheme($theme)
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}
