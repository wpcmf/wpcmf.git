<?php

namespace Wpcmf\Table\Http\Requests;

use Wpcmf\Support\Http\Requests\Request;

class FilterRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'class' => 'required',
        ];
    }
}
