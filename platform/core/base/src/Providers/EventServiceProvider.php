<?php

namespace Wpcmf\Base\Providers;

use Wpcmf\Base\Events\BeforeEditContentEvent;
use Wpcmf\Base\Events\CreatedContentEvent;
use Wpcmf\Base\Events\DeletedContentEvent;
use Wpcmf\Base\Events\SendMailEvent;
use Wpcmf\Base\Events\UpdatedContentEvent;
use Wpcmf\Base\Listeners\BeforeEditContentListener;
use Wpcmf\Base\Listeners\CreatedContentListener;
use Wpcmf\Base\Listeners\DeletedContentListener;
use Wpcmf\Base\Listeners\SendMailListener;
use Wpcmf\Base\Listeners\UpdatedContentListener;
use Illuminate\Support\Facades\Event;
use File;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        SendMailEvent::class          => [
            SendMailListener::class,
        ],
        CreatedContentEvent::class    => [
            CreatedContentListener::class,
        ],
        UpdatedContentEvent::class    => [
            UpdatedContentListener::class,
        ],
        DeletedContentEvent::class    => [
            DeletedContentListener::class,
        ],
        BeforeEditContentEvent::class => [
            BeforeEditContentListener::class,
        ],
    ];

    public function boot()
    {
        parent::boot();

        Event::listen(['cache:cleared'], function () {
            File::delete([storage_path('cache_keys.json'), storage_path('settings.json')]);
        });
    }
}
