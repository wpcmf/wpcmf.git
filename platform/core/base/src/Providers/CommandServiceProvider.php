<?php

namespace Wpcmf\Base\Providers;

use Wpcmf\Base\Commands\ClearLogCommand;
use Wpcmf\Base\Commands\InstallCommand;
use Wpcmf\Base\Commands\PublishAssetsCommand;
use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->commands([
            ClearLogCommand::class,
            InstallCommand::class,
            PublishAssetsCommand::class,
        ]);
    }
}
