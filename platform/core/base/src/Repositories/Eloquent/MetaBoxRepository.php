<?php

namespace Wpcmf\Base\Repositories\Eloquent;

use Wpcmf\Base\Repositories\Interfaces\MetaBoxInterface;
use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;

class MetaBoxRepository extends RepositoriesAbstract implements MetaBoxInterface
{
}
