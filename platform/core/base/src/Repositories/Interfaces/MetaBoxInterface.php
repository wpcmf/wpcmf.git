<?php

namespace Wpcmf\Base\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface MetaBoxInterface extends RepositoryInterface
{
}
