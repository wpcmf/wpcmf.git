<?php

namespace Wpcmf\Base\Repositories\Caches;

use Wpcmf\Base\Repositories\Interfaces\MetaBoxInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class MetaBoxCacheDecorator extends CacheAbstractDecorator implements MetaBoxInterface
{

}
