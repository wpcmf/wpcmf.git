<?php

namespace Wpcmf\Base\Facades;

use Wpcmf\Base\Supports\Assets;
use Illuminate\Support\Facades\Facade;

class AssetsFacade extends Facade
{

    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
       return Assets::class;
    }
}
