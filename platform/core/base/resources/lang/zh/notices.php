<?php 

 return array (
  'create_success_message' => '成功创建',
  'update_success_message' => '成功更新',
  'delete_success_message' => '成功删除',
  'success_header' => '成功！',
  'error_header' => '错误！',
  'no_select' => '请选择至少一个记录来执行此操作！',
  'processing_request' => '我们正在处理您的请求。',
  'error' => '错误！',
  'success' => '成功！',
  'info' => '信息！',
  'enum' => 
  array (
    'validate_message' => '：您输入的属性值无效。',
  ),
);
