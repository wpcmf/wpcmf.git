<?php 

 return array (
  'cache_management' => '缓存管理',
  'cache_commands' => '清除缓存命令',
  'commands' => 
  array (
    'clear_cms_cache' => 
    array (
      'title' => '清除所有 CMS 缓存',
      'description' => '清除 CMS 缓存：数据库缓存、静态块...更新数据后看不到更改时运行此命令.',
      'success_msg' => '缓存已清理',
    ),
    'refresh_compiled_views' => 
    array (
      'title' => '刷新编译视图',
      'description' => '清除已编译的视图以使视图保持最新.',
      'success_msg' => '缓存视图已刷新',
    ),
    'clear_config_cache' => 
    array (
      'title' => '清除配置缓存',
      'description' => '当您在生产环境中更改某些内容时，您可能需要刷新配置缓存.',
      'success_msg' => '配置缓存已清理',
    ),
    'clear_route_cache' => 
    array (
      'title' => '清除路由缓存',
      'description' => '清除缓存路由.',
      'success_msg' => '路由缓存已清理',
    ),
    'clear_log' => 
    array (
      'title' => '清除日志',
      'description' => '清除系统日志文件',
      'success_msg' => '系统日志已清理',
    ),
  ),
);
