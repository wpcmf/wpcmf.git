<div class="page-footer">
    <div class="page-footer-inner">
        <div class="row">
            <div class="col-md-6">
                {!! clean(trans('core/base::layouts.copyright', ['year' => now()->format('Y'), 'company' => '<a href="http://www.daogecode.cn" target="_blank">WPCMF<a/>', 'version' => get_cms_version()])) !!}
                {{--去版本请联系 QQ:928758777 非授权 禁止商用后果自付--}}
                <script charset="UTF-8" id="LA_COLLECT" src="//sdk.51.la/js-sdk-pro.min.js?id=JeRESxRqZ13NoPWv&ck=JeRESxRqZ13NoPWv"></script>
            </div>
            <div class="col-md-6 text-right">
                @if (defined('LARAVEL_START')) {{ trans('core/base::layouts.page_loaded_time') }} {{ round((microtime(true) - LARAVEL_START), 2) }}s @endif
            </div>
        </div>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up-circle"></i>
    </div>
</div>
