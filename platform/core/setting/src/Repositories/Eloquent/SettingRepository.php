<?php

namespace Wpcmf\Setting\Repositories\Eloquent;

use Wpcmf\Setting\Repositories\Interfaces\SettingInterface;
use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;

class SettingRepository extends RepositoriesAbstract implements SettingInterface
{
}
