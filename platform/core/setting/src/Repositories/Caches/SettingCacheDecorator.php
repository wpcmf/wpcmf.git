<?php

namespace Wpcmf\Setting\Repositories\Caches;

use Wpcmf\Setting\Repositories\Interfaces\SettingInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class SettingCacheDecorator extends CacheAbstractDecorator implements SettingInterface
{

}
