<?php

namespace Wpcmf\Setting\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface SettingInterface extends RepositoryInterface
{
}
