<?php

namespace Wpcmf\Media\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface MediaSettingInterface extends RepositoryInterface
{
}
