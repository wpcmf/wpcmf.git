<?php

namespace Wpcmf\Media\Repositories\Caches;

use Wpcmf\Media\Repositories\Interfaces\MediaSettingInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class MediaSettingCacheDecorator extends CacheAbstractDecorator implements MediaSettingInterface
{
}
