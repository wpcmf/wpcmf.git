<?php

namespace Wpcmf\Media\Repositories\Eloquent;

use Wpcmf\Media\Repositories\Interfaces\MediaSettingInterface;
use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;

class MediaSettingRepository extends RepositoriesAbstract implements MediaSettingInterface
{
}
