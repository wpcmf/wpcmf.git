<?php

namespace Wpcmf\Media\Chunks\Exceptions;

use Exception;

class ChunkSaveException extends Exception
{
}
