<?php 

 return array (
  'api_clients' => 'api客户端',
  'create_new_client' => '创建新客户',
  'create_new_client_success' => '成功创建新客户！',
  'edit_client' => '编辑客户端 ":name"',
  'edit_client_success' => '更新了客户端成功！',
  'delete_success' => '成功删除了客户！',
  'confirm_delete_title' => '确认删除客户端":name"',
  'confirm_delete_description' => '你真的想删除客户":name"？',
  'cancel_delete' => '没有',
  'continue_delete' => '是的，让我们删除它！',
  'name' => '名称',
  'cancel' => '取消',
  'save' => '保存',
  'edit' => '编辑',
  'delete' => '删除',
  'client_id' => '客户端ID',
  'secret' => '秘密',
);
