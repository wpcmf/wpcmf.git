<?php

namespace Wpcmf\ACL\Providers;

use Wpcmf\ACL\Events\RoleAssignmentEvent;
use Wpcmf\ACL\Events\RoleUpdateEvent;
use Wpcmf\ACL\Listeners\LoginListener;
use Wpcmf\ACL\Listeners\RoleAssignmentListener;
use Wpcmf\ACL\Listeners\RoleUpdateListener;
use Illuminate\Auth\Events\Login;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        RoleUpdateEvent::class     => [
            RoleUpdateListener::class,
        ],
        RoleAssignmentEvent::class => [
            RoleAssignmentListener::class,
        ],
        Login::class               => [
            LoginListener::class,
        ],
    ];
}
