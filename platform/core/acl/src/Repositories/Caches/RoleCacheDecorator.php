<?php

namespace Wpcmf\ACL\Repositories\Caches;

use Wpcmf\ACL\Repositories\Interfaces\RoleInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class RoleCacheDecorator extends CacheAbstractDecorator implements RoleInterface
{
    /**
     * {@inheritDoc}
     */
    public function createSlug($name, $id)
    {
        return $this->flushCacheAndUpdateData(__FUNCTION__, func_get_args());
    }
}
