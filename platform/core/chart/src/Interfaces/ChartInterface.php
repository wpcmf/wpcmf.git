<?php

namespace Wpcmf\Chart\Interfaces;

interface ChartInterface
{
    /**
     * @return mixed
     */
    public function init();
}
