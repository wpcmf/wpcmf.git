<?php

namespace Wpcmf\Dashboard\Repositories\Eloquent;

use Wpcmf\Dashboard\Repositories\Interfaces\DashboardWidgetInterface;
use Wpcmf\Support\Repositories\Eloquent\RepositoriesAbstract;

class DashboardWidgetRepository extends RepositoriesAbstract implements DashboardWidgetInterface
{
}
