<?php

namespace Wpcmf\Dashboard\Repositories\Caches;

use Wpcmf\Dashboard\Repositories\Interfaces\DashboardWidgetSettingInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class DashboardWidgetSettingCacheDecorator extends CacheAbstractDecorator implements DashboardWidgetSettingInterface
{
    /**
     * {@inheritDoc}
     */
    public function getListWidget()
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}
