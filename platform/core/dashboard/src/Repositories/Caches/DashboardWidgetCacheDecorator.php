<?php

namespace Wpcmf\Dashboard\Repositories\Caches;

use Wpcmf\Dashboard\Repositories\Interfaces\DashboardWidgetInterface;
use Wpcmf\Support\Repositories\Caches\CacheAbstractDecorator;

class DashboardWidgetCacheDecorator extends CacheAbstractDecorator implements DashboardWidgetInterface
{

}
