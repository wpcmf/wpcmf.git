<?php

namespace Wpcmf\Dashboard\Repositories\Interfaces;

use Wpcmf\Support\Repositories\Interfaces\RepositoryInterface;

interface DashboardWidgetInterface extends RepositoryInterface
{
}
