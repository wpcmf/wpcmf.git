<h1 align="center">Wpcmf内容管理系统</h1> 
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>

### 项目介绍

Wpcmf内容管理系统是基于Laravel8+Bootstrap+Vue开发,功能包含会员系统、博客系统、流量统计系统、多语言系统、自定义菜单、模板自定义、字段自定义、有效提高曝光、吸引流量、网络营销、品牌推广的一款应用，且更适合企业二次开发。

###前端页面展示
 ![](screeenshot/front/1.png)
 
 ![](screeenshot/front/2.png)
 
 ###后端页面展示
![](screeenshot/backend/1.png)
![](screeenshot/backend/2.png)
![](screeenshot/backend/3.png)
![](screeenshot/backend/4.png)
![](screeenshot/backend/5.png)
![](screeenshot/backend/6.png)
![](screeenshot/backend/7.png)
![](screeenshot/backend/8.png)
![](screeenshot/backend/9.png)

###  技术交流
**Wpcmf内容管理系统QQ交流群**：306944679 

###开发文档

**https://www.kancloud.cn/rorg/wpcmf/2721978

### 运行环境
* PHP  >= 7.3

* MYSQL  5.7

* PDO PHP Extension 

* MBstring PHP Extension 

* CURL PHP Extension 

* Mylsqi PHP Extension

###搭建
1、安装 comopser install

2、.env 配置数据库信息

3、导入 database.sql

4、后台登录：admin      123456

### 开源版使用须知
1.允许用于个人学习、毕业设计、教学案例、公益事业;

2.如果商用必须保留版权信息，请自觉遵守。开源版不适合商用，商用请购买商业版;

3.禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负。

     
### 版权信息

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2015-2022 by WPCMF 

All rights reserved。

WPCMF®     
    

