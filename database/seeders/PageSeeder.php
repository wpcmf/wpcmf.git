<?php

namespace Database\Seeders;

use Wpcmf\Base\Models\MetaBox as MetaBoxModel;
use Wpcmf\Base\Supports\BaseSeeder;
use Wpcmf\Language\Models\LanguageMeta;
use Wpcmf\Page\Models\Page;
use Wpcmf\Slug\Models\Slug;
use Html;
use Illuminate\Support\Str;
use SlugHelper;

class PageSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'zh_CN' => [
                [
                    'name'     => '首页',
                    'content'  =>
                        Html::tag('div', '[featured-posts][/featured-posts]') .
                        Html::tag('div', '[recent-posts title="最新动态"][/recent-posts]') .
                        Html::tag('div', '[featured-categories-posts title="推荐"][/featured-categories-posts]') .
                        Html::tag('div', '[all-galleries limit="8"][/all-galleries]')
                    ,
                    'template' => 'no-sidebar',
                ],
                [
                    'name'     => '博客',
                    'content'  => '---',
                    'template' => 'default',
                ],
                [
                    'name'     => '联系我们',
                    'content'  => Html::tag('p',
                            '地址:广州市海珠区保利广场100号1201') .
                        Html::tag('p', 'QQ: 928758777') .
                        Html::tag('p', 'Email: hoganmarry@gmail.com') .
                        Html::tag('p',
                            '[google-map]广州市海珠区保利广场100号1201[/google-map]') .
                        Html::tag('p', '为了最快的回复，请使用下面的联系表格.') .
                        Html::tag('p', '[contact-form][/contact-form]'),
                    'template' => 'default',
                ],
                [
                    'name'     => 'Cookie 政策',
                    'content'  => Html::tag('h3', 'Cookie 同意书') .
                        Html::tag('p',
                            '为了使用本网站，我们使用 Cookie 并收集一些数据。 我们让您选择是否允许我们使用某些 Cookie 并收集某些数据.') .
                        Html::tag('h4', '基本数据') .
                        Html::tag('p',
                            '运行您在技术上访问的站点需要基本数据。 您无法停用它们.') .
                        Html::tag('p',
                            '- 会话 Cookie：PHP 使用 Cookie 来识别用户会话。 没有此 Cookie，网站将无法运行.') .
                        Html::tag('p',
                            '- XSRF-Token Cookie: Laravel 会为应用程序管理的每个活动用户会话自动生成一个 CSRF “令牌”。 此令牌用于验证经过身份验证的用户是实际向应用程序发出请求的用户.'),
                    'template' => 'default',
                ],
            ],
            'en_US' => [
                [
                    'name'     => 'Homepage',
                    'content'  =>
                        Html::tag('div', '[featured-posts][/featured-posts]') .
                        Html::tag('div', '[recent-posts title="What\'s new?"][/recent-posts]') .
                        Html::tag('div', '[featured-categories-posts title="Best for you"][/featured-categories-posts]') .
                        Html::tag('div', '[all-galleries limit="8"][/all-galleries]')
                    ,
                    'template' => 'no-sidebar',
                ],
                [
                    'name'     => 'Blog',
                    'content'  => '---',
                    'template' => 'default',
                ],
                [
                    'name'     => 'Contact',
                    'content'  => Html::tag('p',
                            'Address: North Link Building, 10 Admiralty Street, 757695 Singapore') .
                        Html::tag('p', 'Hotline: 18006268') .
                        Html::tag('p', 'Email: contact@botble.com') .
                        Html::tag('p',
                            '[google-map]North Link Building, 10 Admiralty Street, 757695 Singapore[/google-map]') .
                        Html::tag('p', 'For the fastest reply, please use the contact form below.') .
                        Html::tag('p', '[contact-form][/contact-form]'),
                    'template' => 'default',
                ],
                [
                    'name'     => 'Cookie Policy',
                    'content'  => Html::tag('h3', 'EU Cookie Consent') .
                        Html::tag('p',
                            'To use this website we are using Cookies and collecting some Data. To be compliant with the EU GDPR we give you to choose if you allow us to use certain Cookies and to collect some Data.') .
                        Html::tag('h4', 'Essential Data') .
                        Html::tag('p',
                            'The Essential Data is needed to run the Site you are visiting technically. You can not deactivate them.') .
                        Html::tag('p',
                            '- Session Cookie: PHP uses a Cookie to identify user sessions. Without this Cookie the Website is not working.') .
                        Html::tag('p',
                            '- XSRF-Token Cookie: Laravel automatically generates a CSRF "token" for each active user session managed by the application. This token is used to verify that the authenticated user is the one actually making the requests to the application.'),
                    'template' => 'default',
                ],
            ],

        ];

        Page::truncate();
        Slug::where('reference_type', Page::class)->delete();
        MetaBoxModel::where('reference_type', Page::class)->delete();
        LanguageMeta::where('reference_type', Page::class)->delete();

        foreach ($data as $locale => $pages) {
            foreach ($pages as $index => $item) {
                $item['user_id'] = 1;
                $page = Page::create($item);

                Slug::create([
                    'reference_type' => Page::class,
                    'reference_id'   => $page->id,
                    'key'            => Str::slug($page->name,'-',$locale),
                    'prefix'         => SlugHelper::getPrefix(Page::class),
                ]);

                $originValue = null;

                if ($locale !== 'zh_CN') {
                    $originValue = LanguageMeta::where([
                        'reference_id'   => $index + 1,
                        'reference_type' => Page::class,
                    ])->value('lang_meta_origin');
                }

                LanguageMeta::saveMetaData($page, $locale, $originValue);
            }
        }
    }
}
