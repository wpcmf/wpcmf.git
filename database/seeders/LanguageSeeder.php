<?php

namespace Database\Seeders;

use Wpcmf\Base\Supports\BaseSeeder;
use Wpcmf\Language\Models\Language;
use Wpcmf\Language\Models\LanguageMeta;
use Wpcmf\Setting\Models\Setting as SettingModel;

class LanguageSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
            [
                'lang_name'       => '简体中文',
                'lang_locale'     => 'zh',
                'lang_is_default' => true,
                'lang_code'       => 'zh_CN',
                'lang_is_rtl'     => false,
                'lang_flag'       => 'cn',
                'lang_order'      => 0,
            ],
            [
                'lang_name'       => 'English',
                'lang_locale'     => 'en',
                'lang_is_default' => false,
                'lang_code'       => 'en_US',
                'lang_is_rtl'     => false,
                'lang_flag'       => 'us',
                'lang_order'      => 0,
            ],

        ];

        Language::truncate();
        LanguageMeta::truncate();

        foreach ($languages as $item) {
            Language::create($item);
        }

        SettingModel::whereIn('key', [
            'language_hide_default',
            'language_switcher_display',
            'language_display',
            'language_hide_languages',
        ])->delete();

        SettingModel::insertOrIgnore([
            [
                'key'   => 'language_hide_default',
                'value' => '1',
            ],
            [
                'key'   => 'language_switcher_display',
                'value' => 'list',
            ],
            [
                'key'   => 'language_display',
                'value' => 'all',
            ],
            [
                'key'   => 'language_hide_languages',
                'value' => '[]',
            ],
        ]);

    }
}
