<?php

namespace Database\Seeders;

use Wpcmf\ACL\Models\User;
use Wpcmf\ACL\Repositories\Interfaces\ActivationInterface;
use Wpcmf\Base\Supports\BaseSeeder;
use Illuminate\Support\Facades\Schema;

class UserSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        User::truncate();

        $user = new User;
        $user->first_name = '系统管理员';
        $user->last_name = 'Admin';
        $user->email = 'hoganmarry@gmail.com';
        $user->username = 'admin';
        $user->password = bcrypt('123456');
        $user->super_user = 1;
        $user->manage_supers = 1;
        $user->save();

        event('acl.activating', $user);

        $activationRepository = app(ActivationInterface::class);

        $activation = $activationRepository->createUser($user);

        event('acl.activated', [$user, $activation]);

        return $activationRepository->complete($user, $activation->code);
    }
}
