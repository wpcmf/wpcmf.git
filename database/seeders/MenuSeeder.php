<?php

namespace Database\Seeders;

use Wpcmf\Base\Supports\BaseSeeder;
use Wpcmf\Blog\Models\Category;
use Wpcmf\Language\Models\LanguageMeta;
use Wpcmf\Menu\Models\Menu as MenuModel;
use Wpcmf\Menu\Models\MenuLocation;
use Wpcmf\Menu\Models\MenuNode;
use Wpcmf\Page\Models\Page;
use Illuminate\Support\Arr;
use Menu;

class MenuSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'zh_CN' => [
                [
                    'name'     => '主菜单',
                    'slug'     => 'main-menu-zh',
                    'location' => 'main-menu',
                    'items'    => [
                        [
                            'title' => '首页',
                            'url'   => '/',
                        ],
                        [
                            'title'  => '购买',
                            'url'    => 'https://www.daogecode.cn',
                            'target' => '_blank',
                        ],

                        [
                            'title'          => '博客',
                            'reference_id'   => 2,
                            'reference_type' => Page::class,
                        ],
                        [
                            'title' => '相册 ',
                            'url'   => '/galleries',
                        ],
                        [
                            'title'          => '联系人',
                            'reference_id'   => 3,
                            'reference_type' => Page::class,
                        ],
                    ],
                ],

                [
                    'name'  => '收藏网站',
                    'slug'  => 'favorite-websites-zh',
                    'items' => [
                        [
                            'title' => '刀哥网',
                            'url'   => 'http://www.daogecode.cn',
                        ],
                        [
                            'title' => '刀哥网1',
                            'url'   => 'http://www.daogecode.cn',
                        ],
                        [
                            'title' => '博客',
                            'url'   => '#',
                        ],
                        [
                            'title' => '站点',
                            'url'   => '#',
                        ],
                        [
                            'title' => '博客',
                            'url'   => 'http://www.daogecode.cn',
                        ],
                        [
                            'title' => '技术',
                            'url'   => 'http://www.daogecode.cnn',
                        ],
                    ],
                ],

                [
                    'name'  => '我的链接',
                    'slug'  => 'my-links-zh',
                    'items' => [
                        [
                            'title' => '首页',
                            'url'   => '/',
                        ],
                        [
                            'title'          => '联系我们',
                            'reference_id'   => 3,
                            'reference_type' => Page::class,
                        ],
                        [
                            'title'          => '酒店',
                            'reference_id'   => 6,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title'          => '旅游指引',
                            'reference_id'   => 3,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title' => '相册',
                            'url'   => '/galleries',
                        ],
                    ],
                ],

                [
                    'name'  => '推荐分类',
                    'slug'  => 'featured-categories-zh',
                    'items' => [
                        [
                            'title'          => '生活方式',
                            'reference_id'   => 2,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title'          => '旅游指引',
                            'reference_id'   => 3,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title'          => '健康',
                            'reference_id'   => 4,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title'          => '酒店',
                            'reference_id'   => 6,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title'          => '自然',
                            'reference_id'   => 7,
                            'reference_type' => Category::class,
                        ],
                    ],
                ],

                [
                    'name'  => '社交',
                    'slug'  => 'social-zh',
                    'items' => [
                        [
                            'title'     => 'Facebook',
                            'url'       => 'https://facebook.com',
                            'icon_font' => 'fa fa-facebook',
                            'target'    => '_blank',
                        ],
                        [
                            'title'     => 'Twitter',
                            'url'       => 'https://twitter.com',
                            'icon_font' => 'fa fa-twitter',
                            'target'    => '_blank',
                        ],
                        [
                            'title'     => 'Github',
                            'url'       => 'https://github.com',
                            'icon_font' => 'fa fa-github',
                            'target'    => '_blank',
                        ],

                        [
                            'title'     => 'Linkedin',
                            'url'       => 'https://linkedin.com',
                            'icon_font' => 'fa fa-linkedin',
                            'target'    => '_blank',
                        ],
                        [
                            'title'     => 'Weixin',
                            'url'       => 'https://weixi.qq.com',
                            'icon_font' => 'fa fa-wechat',
                            'target'    => '_blank',
                        ],
                    ],
                ],
            ],
            'en_US' => [
                [
                    'name'     => 'Main menu',
                    'slug'     => 'main-menu',
                    'location' => 'main-menu',
                    'items'    => [
                        [
                            'title' => 'Home',
                            'url'   => '/',
                        ],
                        [
                            'title'  => 'Purchase',
                            'url'    => 'http://www.daogecode.cn',
                            'target' => '_blank',
                        ],
                        [
                            'title'          => 'Blog',
                            'reference_id'   => 2,
                            'reference_type' => Page::class,
                        ],
                        [
                            'title' => 'Galleries',
                            'url'   => '/galleries',
                        ],
                        [
                            'title'          => 'Contact',
                            'reference_id'   => 3,
                            'reference_type' => Page::class,
                        ],
                    ],
                ],

                [
                    'name'  => 'Favorite websites',
                    'slug'  => 'favorite-websites',
                    'items' => [
                        [
                            'title' => 'Speckyboy Magazine',
                            'url'   => 'http://speckyboy.com',
                        ],
                        [
                            'title' => 'Tympanus-Codrops',
                            'url'   => 'http://tympanus.com',
                        ],
                        [
                            'title' => 'Kipalog Blog',
                            'url'   => '#',
                        ],
                        [
                            'title' => 'SitePoint',
                            'url'   => 'http://www.sitepoint.com',
                        ],
                        [
                            'title' => 'CreativeBloq',
                            'url'   => 'http://www.creativebloq.com',
                        ],
                        [
                            'title' => 'Techtalk',
                            'url'   => 'http://techtalk.vn',
                        ],
                    ],
                ],

                [
                    'name'  => 'My links',
                    'slug'  => 'my-links',
                    'items' => [
                        [
                            'title' => 'Homepage',
                            'url'   => '/',
                        ],
                        [
                            'title'          => 'Contact',
                            'reference_id'   => 3,
                            'reference_type' => Page::class,
                        ],
                        [
                            'title'          => 'Hotel',
                            'reference_id'   => 6,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title'          => 'Travel Tips',
                            'reference_id'   => 3,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title' => 'Galleries',
                            'url'   => '/galleries',
                        ],
                    ],
                ],

                [
                    'name'  => 'Featured Categories',
                    'slug'  => 'featured-categories',
                    'items' => [
                        [
                            'title'          => 'Lifestyle',
                            'reference_id'   => 2,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title'          => 'Travel Tips',
                            'reference_id'   => 3,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title'          => 'Healthy',
                            'reference_id'   => 4,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title'          => 'Hotel',
                            'reference_id'   => 6,
                            'reference_type' => Category::class,
                        ],
                        [
                            'title'          => 'Nature',
                            'reference_id'   => 7,
                            'reference_type' => Category::class,
                        ],
                    ],
                ],

                [
                    'name'  => 'Social',
                    'slug'  => 'social',
                    'items' => [
                        [
                            'title'     => 'Facebook',
                            'url'       => 'https://facebook.com',
                            'icon_font' => 'fa fa-facebook',
                            'target'    => '_blank',
                        ],
                        [
                            'title'     => 'Twitter',
                            'url'       => 'https://twitter.com',
                            'icon_font' => 'fa fa-twitter',
                            'target'    => '_blank',
                        ],
                        [
                            'title'     => 'Github',
                            'url'       => 'https://github.com',
                            'icon_font' => 'fa fa-github',
                            'target'    => '_blank',
                        ],

                        [
                            'title'     => 'Linkedin',
                            'url'       => 'https://linkedin.com',
                            'icon_font' => 'fa fa-linkedin',
                            'target'    => '_blank',
                        ],
                    ],
                ],
            ],

        ];

        MenuModel::truncate();
        MenuLocation::truncate();
        MenuNode::truncate();
        LanguageMeta::where('reference_type', MenuModel::class)->delete();
        LanguageMeta::where('reference_type', MenuLocation::class)->delete();

        foreach ($data as $locale => $menus) {
            foreach ($menus as $index => $item) {
                $menu = MenuModel::create(Arr::except($item, ['items', 'location']));

                if (isset($item['location'])) {
                    $menuLocation = MenuLocation::create([
                        'menu_id'  => $menu->id,
                        'location' => $item['location'],
                    ]);

                    $originValue = LanguageMeta::where([
                        'reference_id'   => $locale == 'en_US' ? 1 : 2,
                        'reference_type' => MenuLocation::class,
                    ])->value('lang_meta_origin');

                    LanguageMeta::saveMetaData($menuLocation, $locale, $originValue);
                }

                foreach ($item['items'] as $menuNode) {
                    $this->createMenuNode($index, $menuNode, $locale, $menu->id);
                }

                $originValue = null;

                if ($locale !== 'zh_CN') {
                    $originValue = LanguageMeta::where([
                        'reference_id'   => $index + 1,
                        'reference_type' => MenuModel::class,
                    ])->value('lang_meta_origin');
                }

                LanguageMeta::saveMetaData($menu, $locale, $originValue);
            }
        }

        Menu::clearCacheMenuItems();
    }

    /**
     * @param int $index
     * @param array $menuNode
     * @param string $locale
     * @param int $menuId
     * @param int $parentId
     */
    protected function createMenuNode(int $index, array $menuNode, string $locale, int $menuId, int $parentId = 0): void
    {
        $menuNode['menu_id'] = $menuId;
        $menuNode['parent_id'] = $parentId;

        if (Arr::has($menuNode, 'children')) {
            $children = $menuNode['children'];
            $menuNode['has_child'] = true;

            unset($menuNode['children']);
        } else {
            $children = [];
            $menuNode['has_child'] = false;
        }

        $createdNode = MenuNode::create($menuNode);

        if ($children) {
            foreach ($children as $child) {
                $this->createMenuNode($index, $child, $locale, $menuId, $createdNode->id);
            }
        }
    }
}
