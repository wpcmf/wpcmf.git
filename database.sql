/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : nest-cms

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 08/04/2022 22:44:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for activations
-- ----------------------------
DROP TABLE IF EXISTS `activations`;
CREATE TABLE `activations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `completed_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `activations_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of activations
-- ----------------------------
INSERT INTO `activations` VALUES (1, 1, 'XV0gyB4uTo6tRRobzch5tXAkupdmfdtI', 1, '2021-04-07 07:41:03', '2021-04-07 07:41:03', '2021-04-07 07:41:03');
INSERT INTO `activations` VALUES (2, 1, '23pe1WBswrSumRrHmR7cq6ttbMiwvsNQ', 1, '2022-03-12 07:30:35', '2022-03-12 07:30:35', '2022-03-12 07:30:35');
INSERT INTO `activations` VALUES (3, 1, 'rL0crwEZ5F9pc1es2blen6pZKQgu1caU', 1, '2022-03-12 07:52:15', '2022-03-12 07:52:14', '2022-03-12 07:52:15');
INSERT INTO `activations` VALUES (4, 1, 'T7FAyIE5rqx0gNrastVxEpNzX79Xe4Ll', 1, '2022-03-12 07:59:29', '2022-03-12 07:59:29', '2022-03-12 07:59:29');
INSERT INTO `activations` VALUES (5, 1, 'h6DvgQq0a7zMTGIOZYywpJVcNMMSkm0k', 1, '2022-03-12 08:01:17', '2022-03-12 08:01:17', '2022-03-12 08:01:17');
INSERT INTO `activations` VALUES (6, 1, 'nvQzhCmLl5129bdsmhsTXvNNAU2xFXM9', 1, '2022-03-12 08:11:58', '2022-03-12 08:11:58', '2022-03-12 08:11:58');
INSERT INTO `activations` VALUES (7, 1, 'Hzn9jsS62vhGHabBMopMVYaxjplZtDJn', 1, '2022-03-12 08:14:04', '2022-03-12 08:14:04', '2022-03-12 08:14:04');
INSERT INTO `activations` VALUES (8, 1, 's3GkgqPqzzDzTv4hu7NJbl3UHPIGpp2d', 1, '2022-03-12 08:17:08', '2022-03-12 08:17:08', '2022-03-12 08:17:08');
INSERT INTO `activations` VALUES (9, 1, 'BxnhWqH8FBT8QRmX0Euszc2ya9XdTHSB', 1, '2022-03-12 08:52:14', '2022-03-12 08:52:14', '2022-03-12 08:52:14');
INSERT INTO `activations` VALUES (10, 1, 'K5KsRvldxCuGdrWunEThfdLnrmX6y1y4', 1, '2022-03-12 08:55:34', '2022-03-12 08:55:34', '2022-03-12 08:55:34');
INSERT INTO `activations` VALUES (11, 1, 'HSAkG19fKGBEHj5SKFMn1ysZaxOz8lfE', 1, '2022-03-12 09:17:16', '2022-03-12 09:17:16', '2022-03-12 09:17:16');
INSERT INTO `activations` VALUES (12, 1, 'hDWhrkosU0hx7D3BIi67nOWELwIgqMYM', 1, '2022-03-12 09:26:45', '2022-03-12 09:26:45', '2022-03-12 09:26:45');
INSERT INTO `activations` VALUES (13, 1, 'eNdJC90iqPZQ76Ewsm7CKlMHiXDYR0gK', 1, '2022-03-12 09:28:32', '2022-03-12 09:28:32', '2022-03-12 09:28:32');
INSERT INTO `activations` VALUES (14, 1, 'sC0rgRjceJd3fLnqKBVWdtQrxC5I84BE', 1, '2022-03-12 09:31:17', '2022-03-12 09:31:17', '2022-03-12 09:31:17');
INSERT INTO `activations` VALUES (15, 1, 'jRNl0CFW98dXPaevqtOTNqoQ4PG7ovDS', 1, '2022-03-12 09:36:11', '2022-03-12 09:36:11', '2022-03-12 09:36:11');
INSERT INTO `activations` VALUES (16, 1, 'yrHe9WASRrCen4dtjiVd0neFRCEOgpXj', 1, '2022-03-12 10:41:04', '2022-03-12 10:41:04', '2022-03-12 10:41:04');
INSERT INTO `activations` VALUES (17, 1, 'tf32Y1vWFjTDjLEGjP7CTR8lTOXBlann', 1, '2022-03-12 11:04:26', '2022-03-12 11:04:26', '2022-03-12 11:04:26');
INSERT INTO `activations` VALUES (18, 1, 'DrubhQJy1qOG3165qEgjcD8QOFdyFAwD', 1, '2022-03-12 11:05:18', '2022-03-12 11:05:17', '2022-03-12 11:05:18');
INSERT INTO `activations` VALUES (19, 1, 'Os6FMpm5NDIM4MNMTergZvPhg8DdxZte', 1, '2022-03-12 11:23:48', '2022-03-12 11:23:48', '2022-03-12 11:23:48');
INSERT INTO `activations` VALUES (20, 1, 'naiUZSCn6swwlmYNmZzlnnhrchaYU7Cr', 1, '2022-03-12 11:27:06', '2022-03-12 11:27:06', '2022-03-12 11:27:06');
INSERT INTO `activations` VALUES (21, 1, 'wYkNVRrkwh5Xuc7juTyKFNp0jsBoOl9R', 1, '2022-03-12 11:33:49', '2022-03-12 11:33:49', '2022-03-12 11:33:49');
INSERT INTO `activations` VALUES (22, 1, 'kJ81IbGm4LMZP0uhYFk9IOkr63DWIMfc', 1, '2022-03-12 11:42:22', '2022-03-12 11:42:22', '2022-03-12 11:42:22');
INSERT INTO `activations` VALUES (23, 1, 'lEFE9aPl26AMwMOo8tgabmddDSIxr7dh', 1, '2022-03-12 12:08:09', '2022-03-12 12:08:09', '2022-03-12 12:08:09');
INSERT INTO `activations` VALUES (24, 1, 'TrRLT75PCDdajkcSsXBvfApHzMlNvctO', 1, '2022-03-12 17:18:09', '2022-03-12 17:18:08', '2022-03-12 17:18:09');
INSERT INTO `activations` VALUES (25, 1, 'sJWquPMk4wIU5yxZZ5Qpyhi8UzHIIDzr', 1, '2022-03-13 06:19:09', '2022-03-13 06:19:09', '2022-03-13 06:19:09');
INSERT INTO `activations` VALUES (26, 1, '82MhSkh3ag5gFwEnY8vc1fmezEAjQiXB', 1, '2022-04-07 14:22:41', '2022-04-07 14:22:41', '2022-04-07 14:22:41');
INSERT INTO `activations` VALUES (27, 1, 'bdn5WEf1HKvsQV2fXnVmIeUT7o9nybmn', 1, '2022-04-08 13:54:30', '2022-04-08 13:54:30', '2022-04-08 13:54:30');
INSERT INTO `activations` VALUES (28, 1, '0l55yXUQYRILuyQLjij406t7oZyROBI3', 1, '2022-04-08 14:03:12', '2022-04-08 14:03:12', '2022-04-08 14:03:12');
INSERT INTO `activations` VALUES (29, 1, '4dsNYqf3LBD8x7Gn5Ra5YWKougYLNhBq', 1, '2022-04-08 14:05:00', '2022-04-08 14:05:00', '2022-04-08 14:05:00');
INSERT INTO `activations` VALUES (30, 1, 'O2CQIh3SdIndhiWbB80bRqppBxPx0Eih', 1, '2022-04-08 14:17:24', '2022-04-08 14:17:24', '2022-04-08 14:17:24');

-- ----------------------------
-- Table structure for audit_histories
-- ----------------------------
DROP TABLE IF EXISTS `audit_histories`;
CREATE TABLE `audit_histories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `module` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `request` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `action` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `ip_address` varchar(39) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `reference_user` int(10) UNSIGNED NOT NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `reference_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `audit_histories_user_id_index`(`user_id`) USING BTREE,
  INDEX `audit_histories_module_index`(`module`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of audit_histories
-- ----------------------------
INSERT INTO `audit_histories` VALUES (1, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2022-03-07 16:01:43', '2022-03-07 16:01:43');
INSERT INTO `audit_histories` VALUES (2, 1, 'user', '{\"_token\":\"tuoEV4Qzi4y5uJHtEV6MyUjoj9ECFdmiDkXKT0pa\",\"password\":\"123456\",\"password_confirmation\":\"123456\",\"submit\":\"submit\"}', 'updated profile', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 1, 'System Admin', 'info', '2022-03-07 16:02:20', '2022-03-07 16:02:20');
INSERT INTO `audit_histories` VALUES (3, 1, 'user', '{\"_token\":\"tuoEV4Qzi4y5uJHtEV6MyUjoj9ECFdmiDkXKT0pa\",\"password\":\"123456\",\"password_confirmation\":\"123456\",\"submit\":\"submit\"}', 'changed password', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 1, 'System Admin', 'danger', '2022-03-07 16:02:20', '2022-03-07 16:02:20');
INSERT INTO `audit_histories` VALUES (4, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2022-03-08 13:11:36', '2022-03-08 13:11:36');
INSERT INTO `audit_histories` VALUES (5, 1, 'user', '{\"_token\":\"gmw3mUm5trJJy0fi954dTpgscAmFpXzStbJ7h6cg\",\"password\":\"123456\",\"password_confirmation\":\"123456\",\"submit\":\"submit\"}', 'updated profile', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 1, 'System Admin', 'info', '2022-03-08 13:50:32', '2022-03-08 13:50:32');
INSERT INTO `audit_histories` VALUES (6, 1, 'user', '{\"_token\":\"gmw3mUm5trJJy0fi954dTpgscAmFpXzStbJ7h6cg\",\"password\":\"123456\",\"password_confirmation\":\"123456\",\"submit\":\"submit\"}', 'changed password', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 1, 'System Admin', 'danger', '2022-03-08 13:50:32', '2022-03-08 13:50:32');
INSERT INTO `audit_histories` VALUES (7, 1, 'user', '{\"_token\":\"gmw3mUm5trJJy0fi954dTpgscAmFpXzStbJ7h6cg\",\"first_name\":\"System\",\"last_name\":\"Admin\",\"username\":\"admin\",\"email\":\"admin@botble.com\",\"submit\":\"submit\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 1, 'System Admin', 'primary', '2022-03-08 13:50:41', '2022-03-08 13:50:41');
INSERT INTO `audit_histories` VALUES (8, 1, 'user', '{\"_token\":\"gmw3mUm5trJJy0fi954dTpgscAmFpXzStbJ7h6cg\",\"first_name\":\"hogan\",\"last_name\":\"marry\",\"username\":\"admin\",\"email\":\"hoganmarry@gmail.com\",\"submit\":\"submit\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 1, 'Hogan Marry', 'primary', '2022-03-08 13:50:51', '2022-03-08 13:50:51');
INSERT INTO `audit_histories` VALUES (9, 1, 'menu', '{\"_token\":\"gmw3mUm5trJJy0fi954dTpgscAmFpXzStbJ7h6cg\",\"name\":\"Menu ch\\u00ednh\",\"deleted_nodes\":null,\"menu_nodes\":\"[{\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"customUrl\\\":\\\"\\/\\\",\\\"id\\\":26,\\\"class\\\":\\\"\\\",\\\"title\\\":\\\"Trang ch\\u1ee7\\\",\\\"referenceId\\\":\\\"\\\",\\\"referenceType\\\":\\\"\\\",\\\"position\\\":0,\\\"children\\\":[]},{\\\"target\\\":\\\"_blank\\\",\\\"iconFont\\\":\\\"\\\",\\\"customUrl\\\":\\\"https:\\/\\/botble.com\\/go\\/download-cms\\\",\\\"id\\\":27,\\\"class\\\":\\\"\\\",\\\"title\\\":\\\"Mua ngay\\\",\\\"referenceId\\\":\\\"\\\",\\\"referenceType\\\":\\\"\\\",\\\"position\\\":1,\\\"children\\\":[]},{\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"customUrl\\\":\\\"\\/trang-chu\\\",\\\"id\\\":28,\\\"class\\\":\\\"\\\",\\\"title\\\":\\\"Tin t\\u1ee9c\\\",\\\"referenceId\\\":5,\\\"referenceType\\\":\\\"Botble\\\\\\\\Page\\\\\\\\Models\\\\\\\\Page\\\",\\\"position\\\":2,\\\"children\\\":[]},{\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"customUrl\\\":\\\"\\/galleries\\\",\\\"id\\\":29,\\\"class\\\":\\\"\\\",\\\"title\\\":\\\"Th\\u01b0 vi\\u1ec7n \\u1ea3nh\\\",\\\"referenceId\\\":\\\"\\\",\\\"referenceType\\\":\\\"\\\",\\\"position\\\":3,\\\"children\\\":[]},{\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"customUrl\\\":\\\"\\/lien-he\\\",\\\"id\\\":30,\\\"class\\\":\\\"\\\",\\\"title\\\":\\\"Li\\u00ean h\\u1ec7\\\",\\\"referenceId\\\":7,\\\"referenceType\\\":\\\"Botble\\\\\\\\Page\\\\\\\\Models\\\\\\\\Page\\\",\\\"position\\\":4,\\\"children\\\":[]}]\",\"target\":\"_self\",\"title\":\"Li\\u00ean h\\u1ec7\",\"custom-url\":\"\\/galleries\",\"icon-font\":null,\"class\":null,\"locations\":[\"main-menu\"],\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 6, 'Menu chính', 'primary', '2022-03-08 15:39:48', '2022-03-08 15:39:48');
INSERT INTO `audit_histories` VALUES (10, 1, 'menu_location', '{\"_token\":\"gmw3mUm5trJJy0fi954dTpgscAmFpXzStbJ7h6cg\",\"name\":\"Menu ch\\u00ednh\",\"deleted_nodes\":null,\"menu_nodes\":\"[{\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"customUrl\\\":\\\"\\/\\\",\\\"id\\\":26,\\\"class\\\":\\\"\\\",\\\"title\\\":\\\"Trang ch\\u1ee7\\\",\\\"referenceId\\\":\\\"\\\",\\\"referenceType\\\":\\\"\\\",\\\"position\\\":0,\\\"children\\\":[]},{\\\"target\\\":\\\"_blank\\\",\\\"iconFont\\\":\\\"\\\",\\\"customUrl\\\":\\\"https:\\/\\/botble.com\\/go\\/download-cms\\\",\\\"id\\\":27,\\\"class\\\":\\\"\\\",\\\"title\\\":\\\"Mua ngay\\\",\\\"referenceId\\\":\\\"\\\",\\\"referenceType\\\":\\\"\\\",\\\"position\\\":1,\\\"children\\\":[]},{\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"customUrl\\\":\\\"\\/trang-chu\\\",\\\"id\\\":28,\\\"class\\\":\\\"\\\",\\\"title\\\":\\\"Tin t\\u1ee9c\\\",\\\"referenceId\\\":5,\\\"referenceType\\\":\\\"Botble\\\\\\\\Page\\\\\\\\Models\\\\\\\\Page\\\",\\\"position\\\":2,\\\"children\\\":[]},{\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"customUrl\\\":\\\"\\/galleries\\\",\\\"id\\\":29,\\\"class\\\":\\\"\\\",\\\"title\\\":\\\"Th\\u01b0 vi\\u1ec7n \\u1ea3nh\\\",\\\"referenceId\\\":\\\"\\\",\\\"referenceType\\\":\\\"\\\",\\\"position\\\":3,\\\"children\\\":[]},{\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"customUrl\\\":\\\"\\/lien-he\\\",\\\"id\\\":30,\\\"class\\\":\\\"\\\",\\\"title\\\":\\\"Li\\u00ean h\\u1ec7\\\",\\\"referenceId\\\":7,\\\"referenceType\\\":\\\"Botble\\\\\\\\Page\\\\\\\\Models\\\\\\\\Page\\\",\\\"position\\\":4,\\\"children\\\":[]}]\",\"target\":\"_self\",\"title\":\"Li\\u00ean h\\u1ec7\",\"custom-url\":\"\\/galleries\",\"icon-font\":null,\"class\":null,\"locations\":[\"main-menu\"],\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 2, 'ID: 2', 'info', '2022-03-08 15:39:48', '2022-03-08 15:39:48');
INSERT INTO `audit_histories` VALUES (11, 1, 'menu', '{\"_token\":\"gmw3mUm5trJJy0fi954dTpgscAmFpXzStbJ7h6cg\",\"name\":\"66iii\",\"submit\":\"save\",\"language\":\"en_US\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 11, '66iii', 'info', '2022-03-08 15:48:58', '2022-03-08 15:48:58');
INSERT INTO `audit_histories` VALUES (12, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, 'Hogan Marry', 'info', '2022-03-09 00:20:07', '2022-03-09 00:20:07');
INSERT INTO `audit_histories` VALUES (13, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, 'Hogan Marry', 'info', '2022-03-12 05:17:18', '2022-03-12 05:17:18');
INSERT INTO `audit_histories` VALUES (14, 1, 'block', '{\"_token\":\"XbtcnBxYT6MfOlMRBtpHxsj2Bsd6PVDDAeE1NDTC\",\"name\":\"jack ma\",\"alias\":\"11\",\"description\":\"777\",\"content\":null,\"submit\":\"save\",\"language\":\"en_US\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 6, 'jack ma', 'info', '2022-03-12 05:21:34', '2022-03-12 05:21:34');
INSERT INTO `audit_histories` VALUES (15, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, 'Hogan Marry', 'info', '2022-03-12 06:06:31', '2022-03-12 06:06:31');
INSERT INTO `audit_histories` VALUES (16, 1, 'page', '{\"_token\":\"EbvZOGSgPEyDMGn6Sar3Iz7SbaGDcj93LfMf86FY\",\"name\":\"homepage\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Page\\\\Models\\\\Page\",\"description\":null,\"is_featured\":\"0\",\"content\":\"<shortcode class=\\\"bb-shortcode\\\">[featured-posts][\\/featured-posts]<\\/shortcode><shortcode class=\\\"bb-shortcode\\\">[recent-posts title=\\\"What\'s new?\\\"][\\/recent-posts]<\\/shortcode><shortcode class=\\\"bb-shortcode\\\">[featured-categories-posts title=\\\"Best for you\\\"][\\/featured-categories-posts]<\\/shortcode><shortcode class=\\\"bb-shortcode\\\">[all-galleries limit=\\\"8\\\"][\\/all-galleries]<\\/shortcode>\",\"gallery\":null,\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"af\",\"ref_from\":\"1\",\"status\":\"published\",\"template\":\"default\",\"image\":null}', 'created', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 9, 'homepage', 'info', '2022-03-12 08:25:11', '2022-03-12 08:25:11');
INSERT INTO `audit_histories` VALUES (17, 1, 'page', '{\"_token\":\"EbvZOGSgPEyDMGn6Sar3Iz7SbaGDcj93LfMf86FY\",\"name\":\"homepage\",\"slug\":\"homepage-1\",\"slug_id\":\"626\",\"model\":\"Botble\\\\Page\\\\Models\\\\Page\",\"description\":null,\"is_featured\":\"1\",\"content\":\"<shortcode class=\\\"bb-shortcode\\\">[featured-posts][\\/featured-posts]<\\/shortcode><shortcode class=\\\"bb-shortcode\\\">[recent-posts title=\\\"What\'s new?\\\"][\\/recent-posts]<\\/shortcode><shortcode class=\\\"bb-shortcode\\\">[featured-categories-posts title=\\\"Best for you\\\"][\\/featured-categories-posts]<\\/shortcode><shortcode class=\\\"bb-shortcode\\\">[all-galleries limit=\\\"8\\\"][\\/all-galleries]<\\/shortcode>\",\"gallery\":null,\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"af\",\"ref_from\":null,\"status\":\"published\",\"template\":\"no-sidebar\",\"image\":null}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 9, 'homepage', 'primary', '2022-03-12 08:26:14', '2022-03-12 08:26:14');
INSERT INTO `audit_histories` VALUES (18, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, '系统管理员 Admin', 'info', '2022-03-12 10:30:52', '2022-03-12 10:30:52');
INSERT INTO `audit_histories` VALUES (19, 1, 'page', '{\"_token\":\"PuuiJ21zk9wHoA8vrPtI4j2GFWZVOKohJ1ubtldf\",\"name\":\"Homepage\",\"slug\":\"homepage\",\"slug_id\":\"1311\",\"model\":\"Botble\\\\Page\\\\Models\\\\Page\",\"description\":null,\"is_featured\":\"0\",\"content\":\"<shortcode class=\\\"bb-shortcode\\\">[featured-posts][\\/featured-posts]<\\/shortcode><shortcode class=\\\"bb-shortcode\\\">[recent-posts title=\\\"What\'s new?\\\"][\\/recent-posts]<\\/shortcode><shortcode class=\\\"bb-shortcode\\\">[featured-categories-posts title=\\\"Best for you\\\"][\\/featured-categories-posts]<\\/shortcode><shortcode class=\\\"bb-shortcode\\\">[all-galleries limit=\\\"8\\\"][\\/all-galleries]<\\/shortcode>\",\"gallery\":null,\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"en_US\",\"ref_from\":null,\"status\":\"published\",\"template\":\"no-sidebar\",\"image\":null}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 1, 'Homepage', 'primary', '2022-03-12 11:07:53', '2022-03-12 11:07:53');
INSERT INTO `audit_histories` VALUES (20, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, '系统管理员 Admin', 'info', '2022-03-12 17:14:23', '2022-03-12 17:14:23');
INSERT INTO `audit_histories` VALUES (21, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, '系统管理员 Admin', 'info', '2022-03-13 00:39:39', '2022-03-13 00:39:39');
INSERT INTO `audit_histories` VALUES (22, 1, 'page', '{\"_token\":\"NfmyJQ01E9xspxv7ZDM4aJaRnyZOWrmXv2Q50sYH\",\"name\":\"\\u5173\\u4e8e\\u6211\\u4eec\",\"slug\":\"1647143696\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Page\\\\Models\\\\Page\",\"description\":\"\\u6797\",\"is_featured\":\"0\",\"content\":\"<p>\\u6728\\u8981\\u6797<\\/p>\",\"gallery\":null,\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"zh_CN\",\"ref_from\":null,\"status\":\"published\",\"template\":\"default\",\"image\":null}', 'created', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 9, '关于我们', 'info', '2022-03-13 03:55:06', '2022-03-13 03:55:06');
INSERT INTO `audit_histories` VALUES (23, 1, 'post', '{\"_token\":\"NfmyJQ01E9xspxv7ZDM4aJaRnyZOWrmXv2Q50sYH\",\"name\":\"\\u6211\\u53ef\\u56fd\",\"slug\":\"\\u6211\\u53ef\\u56fd\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Blog\\\\Models\\\\Post\",\"description\":\"\\u6c99\\u53d1\\u4e0a\",\"is_featured\":\"0\",\"content\":\"<p>\\u6697\\u793a\\u6cd5\\u5927\\u662f\\u5927\\u975e\\u82db\\u68cb<\\/p>\",\"gallery\":null,\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"zh_CN\",\"ref_from\":null,\"status\":\"published\",\"categories\":[\"1\"],\"image\":null,\"tag\":null}', 'created', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 1, 33, '我可国', 'info', '2022-03-13 04:02:00', '2022-03-13 04:02:00');
INSERT INTO `audit_histories` VALUES (24, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, '系统管理员 Admin', 'info', '2022-03-13 06:19:36', '2022-03-13 06:19:36');
INSERT INTO `audit_histories` VALUES (25, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, '系统管理员 Admin', 'info', '2022-04-08 09:43:09', '2022-04-08 09:43:09');
INSERT INTO `audit_histories` VALUES (26, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 0, 1, '系统管理员 Admin', 'info', '2022-04-08 14:09:12', '2022-04-08 14:09:12');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `author_id` int(11) NOT NULL,
  `author_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `icon` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `is_featured` tinyint(4) NOT NULL DEFAULT 0,
  `is_default` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, '设计', 0, 'Corporis incidunt sunt in nostrum. Voluptas delectus voluptatem ipsa inventore alias voluptatem ad. Autem ut officiis nemo voluptatem. Ea aut neque enim.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 0, 1, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (2, '生活', 0, 'Eaque consequuntur alias quibusdam. Voluptas dolorum dolores rem sint omnis necessitatibus et. Illum non perspiciatis saepe natus.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 1, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (3, '旅游', 2, 'Corporis eos voluptas et voluptatem. Voluptas fuga aliquid voluptatem temporibus molestiae. Quis minus nemo beatae nisi.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 0, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (4, '健康', 0, 'Aut non omnis omnis ullam. Dolorem molestiae aut molestiae maiores debitis. Aut sed incidunt sed molestiae corrupti possimus. Laborum quam tenetur officia asperiores ut facilis voluptas.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 1, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (5, '旅游指引', 4, 'Incidunt rerum omnis sed dolor. Suscipit maiores amet ut eligendi. Vero est rerum est delectus libero temporibus nisi. Ut laudantium atque perspiciatis sint.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 0, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (6, '酒店', 0, 'Doloremque voluptatem ex accusantium facilis totam rerum voluptas. Qui ipsa doloribus error reprehenderit odio.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 1, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (7, '自然', 6, 'Provident est ipsa quia eum. Aut est ratione tenetur sed. Corrupti sed nulla animi doloribus molestiae est. Dolorum impedit explicabo dolore minus voluptatem quis id consequatur.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 0, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (8, 'Design', 0, 'Maxime voluptatem temporibus velit. Voluptas aliquid ipsa recusandae. Dolor enim nostrum cupiditate saepe qui ipsum quisquam. Sed voluptatum laudantium sit nemo consequatur veniam.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 0, 1, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (9, 'Lifestyle', 0, 'Fugiat animi ullam occaecati tempore ut dolor est. Hic harum iste ab maiores. Quibusdam eligendi aspernatur rem quos ut. Repellat in rerum itaque.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 1, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (10, 'Travel Tips', 2, 'Atque quis perspiciatis architecto consequatur molestias et quod qui. Omnis autem et quae. Temporibus sit vel ad dicta. Quae ullam hic et quisquam unde aut nam autem.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 0, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (11, 'Healthy', 0, 'Consequatur ullam amet reiciendis repudiandae corporis dolor. Labore ea illo ea odio et omnis assumenda. Voluptas ab quibusdam omnis odit quo in ut.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 1, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (12, 'Travel Tips', 4, 'Dicta molestiae sit vitae rerum quas voluptas. Eum ad et impedit et sit nesciunt enim sit. Fugiat tempora et ut delectus.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 0, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (13, 'Hotel', 0, 'Nam sed explicabo tempore eius perferendis eum sapiente odit. Dolorem voluptatem harum omnis ut quia dignissimos reiciendis. Distinctio aut debitis expedita consequuntur vitae maxime.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 1, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `categories` VALUES (14, 'Nature', 6, 'Magni autem ipsum reprehenderit ea est cupiditate. Enim quis amet debitis laborum commodi. Qui quis repellat tempora impedit fuga quia. Sed dolor et et optio ab sapiente.', 'published', 1, 'Wpcmf\\ACL\\Models\\User', NULL, 0, 0, 0, '2022-04-08 14:17:32', '2022-04-08 14:17:32');

-- ----------------------------
-- Table structure for contact_replies
-- ----------------------------
DROP TABLE IF EXISTS `contact_replies`;
CREATE TABLE `contact_replies`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `subject` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unread',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES (1, 'Cara Ullrich', 'lueilwitz.randal@example.com', '347.685.5557', '32278 Daniela Underpass Apt. 574\nSouth Armandoport, CT 00725', 'Et ut perferendis iusto.', 'Consequuntur provident nihil velit incidunt dolorum dignissimos. Officiis nulla in id. Dolorem et sunt commodi vero accusamus eum commodi. Dicta dolores quos aut sed. Aut corrupti dolorem praesentium fuga doloremque nesciunt voluptas maiores. Laboriosam eum soluta placeat at soluta quia. Temporibus sit aliquam asperiores consequatur impedit.', 'read', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `contacts` VALUES (2, 'Cruz Kertzmann', 'ssipes@example.net', '+1.210.628.9022', '9607 Kelvin Passage Apt. 165\nGodfreyhaven, LA 63420-6426', 'Illum molestiae eaque ut qui.', 'Et quas libero facere qui aperiam officia consequatur. Aliquid dolores culpa sequi ut maiores. Cumque minus excepturi ad et at deserunt aut. Et rerum eius ut eaque laboriosam sit dolores ullam. Sunt iste pariatur quas ut non odio. Fugiat atque voluptatem eveniet est nisi eius. Qui voluptatem optio aut autem et. Voluptatibus sit ea delectus rerum. Occaecati adipisci sit minus reprehenderit aut fugit quibusdam.', 'read', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `contacts` VALUES (3, 'Cielo Reynolds', 'hagenes.marlene@example.com', '301.908.4188', '723 Mathew Cape\nJoebury, WI 35495-2199', 'Dolores quia magni veniam vitae.', 'Minima fugiat error ea. At ipsam ullam architecto eum ut. Itaque ipsum quos minima autem. Molestias magni quos non nisi cumque expedita quae. Pariatur quia deleniti quam sequi. Eos nobis quas qui quae error. Id error provident ipsum omnis voluptatem sunt debitis. Culpa ut provident iure dolorem vero ducimus. Voluptatum ea reiciendis ullam error a veritatis non provident. Et similique sed impedit ab. Odit eum vel est esse vitae.', 'read', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `contacts` VALUES (4, 'Cameron Crist Sr.', 'nzemlak@example.net', '484.684.0271', '2512 Meggie Gardens\nWest Clovismouth, FL 50982', 'Voluptas qui et fugiat mollitia facilis ut quis.', 'Illum consequatur quam ex saepe sit. Et eveniet est repudiandae deserunt assumenda. Et molestiae dolor voluptatem sunt adipisci. Ullam perferendis voluptatibus reiciendis ut. Aperiam nostrum eligendi ipsa excepturi quidem non. Inventore aliquid ratione rem minus dignissimos. Nostrum animi maiores sequi quis amet unde quas quas. Qui aliquam modi nesciunt perferendis.', 'read', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `contacts` VALUES (5, 'Estefania Thompson', 'mills.aaliyah@example.net', '267-839-2360', '6828 Batz Rue Suite 918\nNorth Declanhaven, VT 48108-9841', 'Nemo quibusdam in ab.', 'Ut fugit optio consequatur velit. Ea similique temporibus aliquam. Quis sint velit aut numquam consequuntur repellat. Cum sint et laborum. Consectetur et eligendi suscipit sed aut laudantium quo. Doloribus iure aut explicabo quis dolorem animi aut. Rerum ut repellat aut id inventore laborum fugit voluptates. Consectetur eaque eaque asperiores est. Nihil eos beatae esse rerum non.', 'unread', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `contacts` VALUES (6, 'Brain Nader', 'sschneider@example.net', '534-523-8924', '191 Vida Islands Suite 873\nLexusburgh, WA 92081', 'Autem quidem quidem numquam quidem dignissimos.', 'Et nemo veniam non nihil qui sequi. Dicta eius quia dolores autem. Quos corrupti neque repellat dolores sit eius ducimus quaerat. Libero atque aut quo aliquam occaecati molestiae unde. Ipsam officiis animi ut ducimus iste. Laudantium voluptatem cum sit sequi consequatur adipisci perspiciatis. Deleniti consequuntur necessitatibus mollitia non earum repellendus atque. Cupiditate libero voluptatem pariatur magnam quo magni aut. Quis hic cumque dolorem aut molestias enim nobis.', 'read', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `contacts` VALUES (7, 'Lynn Ledner', 'brody63@example.org', '1-432-339-5134', '12028 Reilly Lakes\nWest Hyman, NM 09874-7383', 'Et consequatur dolor deserunt dolor dicta autem.', 'Ab est consequatur eos suscipit soluta. A qui iste ea est quae voluptatum eum. Et cum minima ab. Sequi ut aliquam cumque cupiditate. Quo porro ipsum accusamus qui ex qui placeat ut. Id nihil facilis quisquam repellendus at sit dolorum. Deleniti aliquid omnis saepe est voluptatibus. Et qui distinctio deleniti mollitia mollitia. Fugit quia tempora beatae quo quae et temporibus doloremque. Pariatur dolores adipisci aut ut nisi doloribus. Enim quasi a beatae non.', 'unread', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `contacts` VALUES (8, 'Dr. Bud Denesik DVM', 'cedrick51@example.org', '+1 (716) 985-8890', '48906 Fausto Road Apt. 347\nPort Saigeberg, NM 66636', 'Quia similique voluptas accusantium odit.', 'Excepturi at autem qui iste explicabo quasi magni. Adipisci voluptatibus rerum odit dolores dolorem. Unde nostrum dolorem rem fugiat occaecati blanditiis molestias. Ipsa ipsum neque dolorem voluptatum quaerat velit. Voluptas incidunt inventore voluptas. Accusamus tenetur sed laboriosam neque ut et totam. Non dicta quam id libero aperiam ducimus.', 'read', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `contacts` VALUES (9, 'Nat Donnelly', 'xjacobson@example.org', '1-308-774-9925', '53650 Paul Brook Apt. 146\nKuhlmanside, AR 76360', 'Voluptas ullam sed ab in hic voluptatem ut.', 'Ut iste earum voluptatem quis omnis alias et. Natus quod qui necessitatibus dicta quo et. Et ut sed et et tempora. Ab quisquam laboriosam sed doloremque unde occaecati corrupti. Ratione adipisci rerum autem in quo. Reiciendis voluptas cupiditate ducimus pariatur ut commodi. Molestias at illum nisi et. Quis maxime voluptatem consequuntur laborum consequatur et. Unde ab consequatur libero fuga. Sunt sunt hic et eum quia. Ipsum temporibus omnis et aut consequatur voluptatum ea totam.', 'unread', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `contacts` VALUES (10, 'Luis Carroll', 'sierra.wiza@example.com', '+1-715-374-9877', '89151 Gerhold Wells Apt. 268\nSouth Elzabury, PA 06312', 'Sed tempora eligendi ullam.', 'Commodi omnis voluptate accusamus est tenetur maxime. Voluptatem dignissimos voluptatibus impedit atque atque similique. Et labore aut omnis laboriosam non eos quaerat facere. Quia possimus itaque dolorem id dolorum dolorem. Accusantium consequatur autem eos. Fuga eos unde praesentium voluptatem ullam veritatis. Commodi veniam doloremque aut et aut id ad. Dolorem voluptatem architecto et perferendis. Minima ut expedita et est ratione corporis.', 'unread', '2022-04-08 14:17:38', '2022-04-08 14:17:38');

-- ----------------------------
-- Table structure for dashboard_widget_settings
-- ----------------------------
DROP TABLE IF EXISTS `dashboard_widget_settings`;
CREATE TABLE `dashboard_widget_settings`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `settings` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `widget_id` int(10) UNSIGNED NOT NULL,
  `order` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `dashboard_widget_settings_user_id_index`(`user_id`) USING BTREE,
  INDEX `dashboard_widget_settings_widget_id_index`(`widget_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dashboard_widgets
-- ----------------------------
DROP TABLE IF EXISTS `dashboard_widgets`;
CREATE TABLE `dashboard_widgets`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dashboard_widgets
-- ----------------------------
INSERT INTO `dashboard_widgets` VALUES (1, 'widget_total_themes', '2022-03-07 16:02:58', '2022-03-07 16:02:58');
INSERT INTO `dashboard_widgets` VALUES (2, 'widget_total_users', '2022-03-07 16:02:58', '2022-03-07 16:02:58');
INSERT INTO `dashboard_widgets` VALUES (3, 'widget_total_pages', '2022-03-07 16:02:58', '2022-03-07 16:02:58');
INSERT INTO `dashboard_widgets` VALUES (4, 'widget_total_plugins', '2022-03-07 16:02:58', '2022-03-07 16:02:58');
INSERT INTO `dashboard_widgets` VALUES (8, 'widget_posts_recent', '2022-03-07 16:14:11', '2022-03-07 16:14:11');
INSERT INTO `dashboard_widgets` VALUES (10, 'widget_audit_logs', '2022-03-07 16:14:11', '2022-03-07 16:14:11');
INSERT INTO `dashboard_widgets` VALUES (11, 'widget_request_errors', '2022-03-07 16:14:11', '2022-03-07 16:14:11');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for galleries
-- ----------------------------
DROP TABLE IF EXISTS `galleries`;
CREATE TABLE `galleries`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_featured` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `order` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `galleries_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of galleries
-- ----------------------------
INSERT INTO `galleries` VALUES (1, '完美', 'Et totam reiciendis soluta. Fugiat nostrum deserunt quia. Animi minima ratione possimus et ut quia quia.', 1, 0, 'galleries/1.jpg', 1, 'published', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `galleries` VALUES (2, '新的一天', 'Quo quae aut quia. Nostrum neque qui qui. Totam earum qui minima. Possimus occaecati quasi et qui animi quae sint qui.', 1, 0, 'galleries/2.jpg', 1, 'published', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `galleries` VALUES (3, '快乐的一天', 'Et molestias cumque quisquam explicabo facere magnam id. Iure officia tempore non asperiores voluptatem. Quis voluptatum qui optio ut omnis.', 1, 0, 'galleries/3.jpg', 1, 'published', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `galleries` VALUES (4, '自然风影', 'Id impedit est unde dolores nihil. Aut eveniet quam molestiae aut voluptas id id officiis. Voluptas alias iure voluptas at magnam.', 1, 0, 'galleries/4.jpg', 1, 'published', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `galleries` VALUES (5, '早', 'Fugiat maiores rerum recusandae quaerat corrupti earum minus. Voluptates consequatur natus dolore neque. Pariatur est sit et.', 1, 0, 'galleries/5.jpg', 1, 'published', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `galleries` VALUES (6, '图片', 'Asperiores inventore qui dolores voluptates. Corporis ea recusandae nam nostrum asperiores. Nisi sit omnis nihil laboriosam hic aut.', 1, 0, 'galleries/6.jpg', 1, 'published', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `galleries` VALUES (7, 'Perfect', 'Sunt atque aut eos excepturi rerum. Ut blanditiis omnis deserunt illum quod. Vitae repellat cupiditate quia voluptate.', 1, 0, 'galleries/1.jpg', 1, 'published', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `galleries` VALUES (8, 'New Day', 'Illo qui sunt qui. Sed ut asperiores quia. Laudantium laudantium rerum non qui et sed. Quidem commodi et nihil quas perferendis.', 1, 0, 'galleries/2.jpg', 1, 'published', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `galleries` VALUES (9, 'Happy Day', 'Facere harum consequatur corrupti dolores maxime quod. Nemo error quibusdam rerum facere aspernatur praesentium. Quas ipsum culpa ut officiis sunt.', 1, 0, 'galleries/3.jpg', 1, 'published', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `galleries` VALUES (10, 'Nature', 'Nam ut veritatis eos error ut. Temporibus in et sint qui voluptas. Consequatur iste ipsa est qui fuga et.', 1, 0, 'galleries/4.jpg', 1, 'published', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `galleries` VALUES (11, 'Morning', 'Aperiam ipsum ut nostrum asperiores sint quae. Illum quos libero tempore nobis ut.', 1, 0, 'galleries/5.jpg', 1, 'published', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `galleries` VALUES (12, 'Photography', 'Aut tenetur maxime aperiam velit quo facere aut. Iure fuga quas corporis necessitatibus molestias. Provident beatae dolorum nihil nobis velit enim.', 1, 0, 'galleries/6.jpg', 1, 'published', '2022-04-08 14:17:28', '2022-04-08 14:17:28');

-- ----------------------------
-- Table structure for gallery_meta
-- ----------------------------
DROP TABLE IF EXISTS `gallery_meta`;
CREATE TABLE `gallery_meta`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `images` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `reference_type` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gallery_meta_reference_id_index`(`reference_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gallery_meta
-- ----------------------------
INSERT INTO `gallery_meta` VALUES (1, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 1, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `gallery_meta` VALUES (2, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 2, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `gallery_meta` VALUES (3, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 3, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `gallery_meta` VALUES (4, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 4, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `gallery_meta` VALUES (5, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 5, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `gallery_meta` VALUES (6, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 6, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `gallery_meta` VALUES (7, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 7, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `gallery_meta` VALUES (8, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 8, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `gallery_meta` VALUES (9, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 9, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `gallery_meta` VALUES (10, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 10, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `gallery_meta` VALUES (11, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 11, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `gallery_meta` VALUES (12, '[{\"img\":\"galleries\\/1.jpg\",\"description\":\"Eum repudiandae excepturi eum. Sed atque repellendus minus nihil illum. Asperiores suscipit ut pariatur voluptate.\"},{\"img\":\"galleries\\/2.jpg\",\"description\":\"Quia unde odio ea aut dolorum. Sint non sed veritatis id. Vel quia numquam eaque itaque et rem.\"},{\"img\":\"galleries\\/3.jpg\",\"description\":\"Officia voluptates totam quidem dolor quibusdam. Dolorem veritatis consequatur occaecati quod nobis incidunt sit ullam. Fuga et nostrum sed id.\"},{\"img\":\"galleries\\/4.jpg\",\"description\":\"Ex mollitia dignissimos atque sit dolor. Voluptatem eum corrupti fuga officiis ab. Eum ab nobis harum rerum dolores aut in reprehenderit.\"},{\"img\":\"galleries\\/5.jpg\",\"description\":\"Quia libero delectus sed eos voluptatibus dolorem. Consectetur quo id rerum sit quod aut est.\"},{\"img\":\"galleries\\/6.jpg\",\"description\":\"Exercitationem rerum similique ut est. Aut est dolorem facilis aspernatur ut non quasi ea. Quo dolores ea qui quia.\"},{\"img\":\"galleries\\/7.jpg\",\"description\":\"Qui consequatur ut corrupti et. Quia voluptatem et sit. Laboriosam quisquam ad aliquam ut.\"},{\"img\":\"galleries\\/8.jpg\",\"description\":\"Repellat mollitia laudantium earum non. Dolor soluta libero vel. Cupiditate corrupti sed vero officiis deleniti.\"},{\"img\":\"galleries\\/9.jpg\",\"description\":\"Aut earum odio ut dolorem. Non optio delectus delectus cumque quod. Nobis placeat id corporis.\"},{\"img\":\"galleries\\/10.jpg\",\"description\":\"Dicta vel est officia rerum. Quam sit assumenda deserunt assumenda ipsum mollitia. Eos et autem aspernatur omnis fuga.\"}]', 12, 'Wpcmf\\Gallery\\Models\\Gallery', '2022-04-08 14:17:28', '2022-04-08 14:17:28');

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED NULL DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jobs_queue_index`(`queue`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for language_meta
-- ----------------------------
DROP TABLE IF EXISTS `language_meta`;
CREATE TABLE `language_meta`  (
  `lang_meta_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lang_meta_code` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang_meta_origin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `reference_type` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`lang_meta_id`) USING BTREE,
  INDEX `language_meta_reference_id_index`(`reference_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 94 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of language_meta
-- ----------------------------
INSERT INTO `language_meta` VALUES (1, 'zh_CN', 'f26f3f2e4fab76d99d04374e2cf14a19', 1, 'Wpcmf\\Page\\Models\\Page');
INSERT INTO `language_meta` VALUES (2, 'zh_CN', 'bdc8c1d93786f04f32133a57b3eb8862', 2, 'Wpcmf\\Page\\Models\\Page');
INSERT INTO `language_meta` VALUES (3, 'zh_CN', 'd71701daeed801082e66d44af73f06a5', 3, 'Wpcmf\\Page\\Models\\Page');
INSERT INTO `language_meta` VALUES (4, 'zh_CN', '354ba174c1e05375ea7566b1ab6726fb', 4, 'Wpcmf\\Page\\Models\\Page');
INSERT INTO `language_meta` VALUES (5, 'en_US', 'f26f3f2e4fab76d99d04374e2cf14a19', 5, 'Wpcmf\\Page\\Models\\Page');
INSERT INTO `language_meta` VALUES (6, 'en_US', 'bdc8c1d93786f04f32133a57b3eb8862', 6, 'Wpcmf\\Page\\Models\\Page');
INSERT INTO `language_meta` VALUES (7, 'en_US', 'd71701daeed801082e66d44af73f06a5', 7, 'Wpcmf\\Page\\Models\\Page');
INSERT INTO `language_meta` VALUES (8, 'en_US', '354ba174c1e05375ea7566b1ab6726fb', 8, 'Wpcmf\\Page\\Models\\Page');
INSERT INTO `language_meta` VALUES (9, 'zh_CN', '864b2784c8605967ee90bdf3a1db7868', 1, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (10, 'zh_CN', '784b627cb58011a6121ce474697a5103', 2, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (11, 'zh_CN', 'd05bfa9ae3c22775c0e5db171ace761d', 3, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (12, 'zh_CN', '738438a9d13227d367156258ffb2b91a', 4, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (13, 'zh_CN', '82b3da7ef95bff63532fd12ccb265333', 5, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (14, 'zh_CN', '24fed28af3713e294dddfa6174f3e066', 6, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (15, 'en_US', '864b2784c8605967ee90bdf3a1db7868', 7, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (16, 'en_US', '784b627cb58011a6121ce474697a5103', 8, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (17, 'en_US', 'd05bfa9ae3c22775c0e5db171ace761d', 9, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (18, 'en_US', '738438a9d13227d367156258ffb2b91a', 10, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (19, 'en_US', '82b3da7ef95bff63532fd12ccb265333', 11, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (20, 'en_US', '24fed28af3713e294dddfa6174f3e066', 12, 'Wpcmf\\Gallery\\Models\\Gallery');
INSERT INTO `language_meta` VALUES (21, 'zh_CN', '9978fdf50a6b917b86b8a65feabc9835', 1, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (22, 'zh_CN', 'b19a47ec441e271e88aad7e334920f28', 2, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (23, 'zh_CN', 'ba1c214135e74a994883100e4f422dd8', 3, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (24, 'zh_CN', '78f522706e200db2d05ac145f406308c', 4, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (25, 'zh_CN', '9913ca05a5ab0bb031005e287f9269f4', 5, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (26, 'zh_CN', '85f5393d15bf70f02dfd9f8bb8692da6', 6, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (27, 'zh_CN', '3d15751117dc4f6e983077e8883a2432', 7, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (28, 'en_US', '9978fdf50a6b917b86b8a65feabc9835', 8, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (29, 'en_US', 'b19a47ec441e271e88aad7e334920f28', 9, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (30, 'en_US', 'ba1c214135e74a994883100e4f422dd8', 10, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (31, 'en_US', '78f522706e200db2d05ac145f406308c', 11, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (32, 'en_US', '9913ca05a5ab0bb031005e287f9269f4', 12, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (33, 'en_US', '85f5393d15bf70f02dfd9f8bb8692da6', 13, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (34, 'en_US', '3d15751117dc4f6e983077e8883a2432', 14, 'Wpcmf\\Blog\\Models\\Category');
INSERT INTO `language_meta` VALUES (35, 'zh_CN', '188d3d5a78ff54cc5447cc1753a544f0', 1, 'Wpcmf\\Blog\\Models\\Tag');
INSERT INTO `language_meta` VALUES (36, 'zh_CN', '00500bff45c446aba5c5aac06be7dba6', 2, 'Wpcmf\\Blog\\Models\\Tag');
INSERT INTO `language_meta` VALUES (37, 'zh_CN', 'be673789d35fba595fbf4e146486ce70', 3, 'Wpcmf\\Blog\\Models\\Tag');
INSERT INTO `language_meta` VALUES (38, 'zh_CN', '913c4309609272b3bb0cef602094f0bc', 4, 'Wpcmf\\Blog\\Models\\Tag');
INSERT INTO `language_meta` VALUES (39, 'zh_CN', '367b54501eed42d5cf4232e26d0172e9', 5, 'Wpcmf\\Blog\\Models\\Tag');
INSERT INTO `language_meta` VALUES (40, 'en_US', '188d3d5a78ff54cc5447cc1753a544f0', 6, 'Wpcmf\\Blog\\Models\\Tag');
INSERT INTO `language_meta` VALUES (41, 'en_US', '00500bff45c446aba5c5aac06be7dba6', 7, 'Wpcmf\\Blog\\Models\\Tag');
INSERT INTO `language_meta` VALUES (42, 'en_US', 'be673789d35fba595fbf4e146486ce70', 8, 'Wpcmf\\Blog\\Models\\Tag');
INSERT INTO `language_meta` VALUES (43, 'en_US', '913c4309609272b3bb0cef602094f0bc', 9, 'Wpcmf\\Blog\\Models\\Tag');
INSERT INTO `language_meta` VALUES (44, 'en_US', '367b54501eed42d5cf4232e26d0172e9', 10, 'Wpcmf\\Blog\\Models\\Tag');
INSERT INTO `language_meta` VALUES (45, 'zh_CN', '2df8b758ef80e7d0aee6a504fc6f6aff', 1, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (46, 'zh_CN', '5e2313752c4447a8bcfc45208fdf9af1', 2, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (47, 'zh_CN', 'c12647f7b2dabe8f37ecaa067dc90f85', 3, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (48, 'zh_CN', '7bb3a06db10e9400860190699944f97b', 4, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (49, 'zh_CN', 'b00adfed5929ddceb5e25e6aed871d20', 5, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (50, 'zh_CN', 'a16d9dbe66f9a383857b1cca7012d810', 6, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (51, 'zh_CN', '56415bb9a61648945f7b29cd23024599', 7, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (52, 'zh_CN', '56ed58bee11c9c07df9536d90e6e2080', 8, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (53, 'zh_CN', '43898cddae27a49bf97ca29936d40879', 9, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (54, 'zh_CN', '8767ac6b38f663f01e26dc4303c3ee77', 10, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (55, 'zh_CN', 'f8e12c99889b53fa74bc1c69b7a24f0b', 11, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (56, 'zh_CN', '626356c85f8031c34526c0bc09d657b1', 12, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (57, 'zh_CN', '686e6f152a85e90936725b63c066294f', 13, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (58, 'zh_CN', 'b63c1e82f7da83d724735a74d8fd425a', 14, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (59, 'zh_CN', '0e988bf0838a55d7e1714aed021abee0', 15, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (60, 'zh_CN', 'ebd176b291829d3c358aa5db19481bbf', 16, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (61, 'en_US', '2df8b758ef80e7d0aee6a504fc6f6aff', 17, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (62, 'en_US', '5e2313752c4447a8bcfc45208fdf9af1', 18, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (63, 'en_US', 'c12647f7b2dabe8f37ecaa067dc90f85', 19, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (64, 'en_US', '7bb3a06db10e9400860190699944f97b', 20, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (65, 'en_US', 'b00adfed5929ddceb5e25e6aed871d20', 21, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (66, 'en_US', 'a16d9dbe66f9a383857b1cca7012d810', 22, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (67, 'en_US', '56415bb9a61648945f7b29cd23024599', 23, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (68, 'en_US', '56ed58bee11c9c07df9536d90e6e2080', 24, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (69, 'en_US', '43898cddae27a49bf97ca29936d40879', 25, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (70, 'en_US', '8767ac6b38f663f01e26dc4303c3ee77', 26, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (71, 'en_US', 'f8e12c99889b53fa74bc1c69b7a24f0b', 27, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (72, 'en_US', '626356c85f8031c34526c0bc09d657b1', 28, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (73, 'en_US', '686e6f152a85e90936725b63c066294f', 29, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (74, 'en_US', 'b63c1e82f7da83d724735a74d8fd425a', 30, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (75, 'en_US', '0e988bf0838a55d7e1714aed021abee0', 31, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (76, 'en_US', 'ebd176b291829d3c358aa5db19481bbf', 32, 'Wpcmf\\Blog\\Models\\Post');
INSERT INTO `language_meta` VALUES (77, 'zh_CN', '09db9841599ce06d3743c706519d73d0', 1, 'Wpcmf\\Block\\Models\\Block');
INSERT INTO `language_meta` VALUES (78, 'zh_CN', '546aed3e10babf31e39257aa4d5a0b41', 2, 'Wpcmf\\Block\\Models\\Block');
INSERT INTO `language_meta` VALUES (79, 'zh_CN', 'dd64bfcc35fce5b45668712d5af1e020', 3, 'Wpcmf\\Block\\Models\\Block');
INSERT INTO `language_meta` VALUES (80, 'zh_CN', '32ae5c9acf30a598f694424cbb9874e1', 4, 'Wpcmf\\Block\\Models\\Block');
INSERT INTO `language_meta` VALUES (81, 'zh_CN', 'eba8209b6cd4288177f36bd2324f4186', 5, 'Wpcmf\\Block\\Models\\Block');
INSERT INTO `language_meta` VALUES (82, 'zh_CN', '6e552abea23b34ac88c9f6afef66029e', 1, 'Wpcmf\\Menu\\Models\\MenuLocation');
INSERT INTO `language_meta` VALUES (83, 'zh_CN', '455178a79c81c1550fe82ba95a190069', 1, 'Wpcmf\\Menu\\Models\\Menu');
INSERT INTO `language_meta` VALUES (84, 'zh_CN', 'e7689ef6516f8326de5de511308b2b38', 2, 'Wpcmf\\Menu\\Models\\Menu');
INSERT INTO `language_meta` VALUES (85, 'zh_CN', '2b21b280118e10ee189eeafe7405a4c9', 3, 'Wpcmf\\Menu\\Models\\Menu');
INSERT INTO `language_meta` VALUES (86, 'zh_CN', '9f7788ad1a03353b22a38c23c393cefa', 4, 'Wpcmf\\Menu\\Models\\Menu');
INSERT INTO `language_meta` VALUES (87, 'zh_CN', 'fca603f74a4117bfa5aafa55b09a95e3', 5, 'Wpcmf\\Menu\\Models\\Menu');
INSERT INTO `language_meta` VALUES (88, 'en_US', '6e552abea23b34ac88c9f6afef66029e', 2, 'Wpcmf\\Menu\\Models\\MenuLocation');
INSERT INTO `language_meta` VALUES (89, 'en_US', '455178a79c81c1550fe82ba95a190069', 6, 'Wpcmf\\Menu\\Models\\Menu');
INSERT INTO `language_meta` VALUES (90, 'en_US', 'e7689ef6516f8326de5de511308b2b38', 7, 'Wpcmf\\Menu\\Models\\Menu');
INSERT INTO `language_meta` VALUES (91, 'en_US', '2b21b280118e10ee189eeafe7405a4c9', 8, 'Wpcmf\\Menu\\Models\\Menu');
INSERT INTO `language_meta` VALUES (92, 'en_US', '9f7788ad1a03353b22a38c23c393cefa', 9, 'Wpcmf\\Menu\\Models\\Menu');
INSERT INTO `language_meta` VALUES (93, 'en_US', 'fca603f74a4117bfa5aafa55b09a95e3', 10, 'Wpcmf\\Menu\\Models\\Menu');

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages`  (
  `lang_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lang_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_locale` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_flag` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `lang_is_default` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `lang_order` int(11) NOT NULL DEFAULT 0,
  `lang_is_rtl` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`lang_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES (1, '简体中文', 'zh', 'zh_CN', 'cn', 1, 0, 0);
INSERT INTO `languages` VALUES (2, 'English', 'en', 'en_US', 'us', 0, 0, 0);

-- ----------------------------
-- Table structure for media_files
-- ----------------------------
DROP TABLE IF EXISTS `media_files`;
CREATE TABLE `media_files`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `mime_type` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `media_files_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of media_files
-- ----------------------------
INSERT INTO `media_files` VALUES (29, 1, 'nest-cms-200', 0, 'image/png', 2498, 'nest-cms-200.png', '[]', '2022-03-12 06:07:16', '2022-03-12 06:07:16', NULL);
INSERT INTO `media_files` VALUES (1062, 0, '1', 106, 'image/jpeg', 111282, 'galleries/1.jpg', '[]', '2022-04-08 14:17:25', '2022-04-08 14:17:25', NULL);
INSERT INTO `media_files` VALUES (1063, 0, '10', 106, 'image/jpeg', 22919, 'galleries/10.jpg', '[]', '2022-04-08 14:17:25', '2022-04-08 14:17:25', NULL);
INSERT INTO `media_files` VALUES (1064, 0, '2', 106, 'image/jpeg', 77350, 'galleries/2.jpg', '[]', '2022-04-08 14:17:25', '2022-04-08 14:17:25', NULL);
INSERT INTO `media_files` VALUES (1065, 0, '3', 106, 'image/jpeg', 223274, 'galleries/3.jpg', '[]', '2022-04-08 14:17:26', '2022-04-08 14:17:26', NULL);
INSERT INTO `media_files` VALUES (1066, 0, '4', 106, 'image/jpeg', 90793, 'galleries/4.jpg', '[]', '2022-04-08 14:17:26', '2022-04-08 14:17:26', NULL);
INSERT INTO `media_files` VALUES (1067, 0, '5', 106, 'image/jpeg', 43808, 'galleries/5.jpg', '[]', '2022-04-08 14:17:26', '2022-04-08 14:17:26', NULL);
INSERT INTO `media_files` VALUES (1068, 0, '6', 106, 'image/jpeg', 9386, 'galleries/6.jpg', '[]', '2022-04-08 14:17:26', '2022-04-08 14:17:26', NULL);
INSERT INTO `media_files` VALUES (1069, 0, '7', 106, 'image/jpeg', 116884, 'galleries/7.jpg', '[]', '2022-04-08 14:17:26', '2022-04-08 14:17:26', NULL);
INSERT INTO `media_files` VALUES (1070, 0, '8', 106, 'image/jpeg', 60158, 'galleries/8.jpg', '[]', '2022-04-08 14:17:27', '2022-04-08 14:17:27', NULL);
INSERT INTO `media_files` VALUES (1071, 0, '9', 106, 'image/jpeg', 127940, 'galleries/9.jpg', '[]', '2022-04-08 14:17:27', '2022-04-08 14:17:27', NULL);
INSERT INTO `media_files` VALUES (1072, 0, '1', 107, 'image/jpeg', 111282, 'news/1.jpg', '[]', '2022-04-08 14:17:28', '2022-04-08 14:17:28', NULL);
INSERT INTO `media_files` VALUES (1073, 0, '10', 107, 'image/jpeg', 12892, 'news/10.jpg', '[]', '2022-04-08 14:17:28', '2022-04-08 14:17:28', NULL);
INSERT INTO `media_files` VALUES (1074, 0, '11', 107, 'image/jpeg', 24346, 'news/11.jpg', '[]', '2022-04-08 14:17:29', '2022-04-08 14:17:29', NULL);
INSERT INTO `media_files` VALUES (1075, 0, '12', 107, 'image/jpeg', 13942, 'news/12.jpg', '[]', '2022-04-08 14:17:29', '2022-04-08 14:17:29', NULL);
INSERT INTO `media_files` VALUES (1076, 0, '13', 107, 'image/jpeg', 27890, 'news/13.jpg', '[]', '2022-04-08 14:17:29', '2022-04-08 14:17:29', NULL);
INSERT INTO `media_files` VALUES (1077, 0, '14', 107, 'image/jpeg', 20870, 'news/14.jpg', '[]', '2022-04-08 14:17:29', '2022-04-08 14:17:29', NULL);
INSERT INTO `media_files` VALUES (1078, 0, '15', 107, 'image/jpeg', 22919, 'news/15.jpg', '[]', '2022-04-08 14:17:29', '2022-04-08 14:17:29', NULL);
INSERT INTO `media_files` VALUES (1079, 0, '16', 107, 'image/jpeg', 191832, 'news/16.jpg', '[]', '2022-04-08 14:17:29', '2022-04-08 14:17:29', NULL);
INSERT INTO `media_files` VALUES (1080, 0, '2-480x300', 107, 'image/jpeg', 16696, 'news/2-480x300.jpg', '[]', '2022-04-08 14:17:30', '2022-04-08 14:17:30', NULL);
INSERT INTO `media_files` VALUES (1081, 0, '2', 107, 'image/jpeg', 77350, 'news/2.jpg', '[]', '2022-04-08 14:17:30', '2022-04-08 14:17:30', NULL);
INSERT INTO `media_files` VALUES (1082, 0, '3', 107, 'image/jpeg', 223274, 'news/3.jpg', '[]', '2022-04-08 14:17:30', '2022-04-08 14:17:30', NULL);
INSERT INTO `media_files` VALUES (1083, 0, '4', 107, 'image/jpeg', 90793, 'news/4.jpg', '[]', '2022-04-08 14:17:30', '2022-04-08 14:17:30', NULL);
INSERT INTO `media_files` VALUES (1084, 0, '5', 107, 'image/jpeg', 43808, 'news/5.jpg', '[]', '2022-04-08 14:17:31', '2022-04-08 14:17:31', NULL);
INSERT INTO `media_files` VALUES (1085, 0, '6', 107, 'image/jpeg', 9386, 'news/6.jpg', '[]', '2022-04-08 14:17:31', '2022-04-08 14:17:31', NULL);
INSERT INTO `media_files` VALUES (1086, 0, '7', 107, 'image/jpeg', 116884, 'news/7.jpg', '[]', '2022-04-08 14:17:31', '2022-04-08 14:17:31', NULL);
INSERT INTO `media_files` VALUES (1087, 0, '8', 107, 'image/jpeg', 60158, 'news/8.jpg', '[]', '2022-04-08 14:17:31', '2022-04-08 14:17:31', NULL);
INSERT INTO `media_files` VALUES (1088, 0, '9', 107, 'image/jpeg', 127940, 'news/9.jpg', '[]', '2022-04-08 14:17:31', '2022-04-08 14:17:31', NULL);
INSERT INTO `media_files` VALUES (1089, 0, '1', 108, 'image/jpeg', 4179, 'members/1.jpg', '[]', '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_files` VALUES (1090, 0, '10', 108, 'image/jpeg', 3246, 'members/10.jpg', '[]', '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_files` VALUES (1091, 0, '2', 108, 'image/jpeg', 5575, 'members/2.jpg', '[]', '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_files` VALUES (1092, 0, '3', 108, 'image/jpeg', 3644, 'members/3.jpg', '[]', '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_files` VALUES (1093, 0, '4', 108, 'image/jpeg', 17435, 'members/4.jpg', '[]', '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_files` VALUES (1094, 0, '5', 108, 'image/jpeg', 3860, 'members/5.jpg', '[]', '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_files` VALUES (1095, 0, '6', 108, 'image/jpeg', 3375, 'members/6.jpg', '[]', '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_files` VALUES (1096, 0, '7', 108, 'image/jpeg', 18089, 'members/7.jpg', '[]', '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_files` VALUES (1097, 0, '8', 108, 'image/jpeg', 2999, 'members/8.jpg', '[]', '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_files` VALUES (1098, 0, '9', 108, 'image/jpeg', 9500, 'members/9.jpg', '[]', '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_files` VALUES (1099, 0, 'favicon', 109, 'image/png', 2664, 'general/favicon.png', '[]', '2022-04-08 14:17:40', '2022-04-08 14:17:40', NULL);
INSERT INTO `media_files` VALUES (1100, 0, 'logo', 109, 'image/png', 2664, 'general/logo.png', '[]', '2022-04-08 14:17:40', '2022-04-08 14:17:40', NULL);

-- ----------------------------
-- Table structure for media_folders
-- ----------------------------
DROP TABLE IF EXISTS `media_folders`;
CREATE TABLE `media_folders`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `media_folders_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of media_folders
-- ----------------------------
INSERT INTO `media_folders` VALUES (106, 0, 'galleries', 'galleries', 0, '2022-04-08 14:17:25', '2022-04-08 14:17:25', NULL);
INSERT INTO `media_folders` VALUES (107, 0, 'news', 'news', 0, '2022-04-08 14:17:28', '2022-04-08 14:17:28', NULL);
INSERT INTO `media_folders` VALUES (108, 0, 'members', 'members', 0, '2022-04-08 14:17:36', '2022-04-08 14:17:36', NULL);
INSERT INTO `media_folders` VALUES (109, 0, 'general', 'general', 0, '2022-04-08 14:17:40', '2022-04-08 14:17:40', NULL);

-- ----------------------------
-- Table structure for media_settings
-- ----------------------------
DROP TABLE IF EXISTS `media_settings`;
CREATE TABLE `media_settings`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `media_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_activity_logs
-- ----------------------------
DROP TABLE IF EXISTS `member_activity_logs`;
CREATE TABLE `member_activity_logs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `action` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `reference_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `reference_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ip_address` varchar(39) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `member_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `member_activity_logs_member_id_index`(`member_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `member_password_resets`;
CREATE TABLE `member_password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `member_password_resets_email_index`(`email`) USING BTREE,
  INDEX `member_password_resets_token_index`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `gender` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `dob` date NULL DEFAULT NULL,
  `phone` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `confirmed_at` datetime(0) NULL DEFAULT NULL,
  `email_verify_token` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `members_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES (1, 'hogan', 'marray', 'Alice had learnt several.', NULL, 'hoganmarray@gmail.com', '$2y$10$uNzF6weWoiL2VOFijJj1VeLhTNaol6ypGQTyTb8MY1O154.USchQG', 1089, '2011-01-21', '1-360-715-6367', '2022-04-08 14:17:37', NULL, NULL, '2022-04-08 14:17:37', '2022-04-08 14:17:37');
INSERT INTO `members` VALUES (2, 'Houston', 'Leannon', 'Majesty,\' said Alice in a.', NULL, 'gcartwright@hotmail.com', '$2y$10$OcrurMESLkFzjTGf1FXRYOJFHGwNsxss6psv3Qq2IhLzNYsjPbbWO', 1090, '1990-04-06', '737-365-9407', '2022-04-08 14:17:37', NULL, NULL, '2022-04-08 14:17:37', '2022-04-08 14:17:37');
INSERT INTO `members` VALUES (3, 'Idell', 'Altenwerth', 'Hatter, \'you wouldn\'t talk.', NULL, 'alden.kub@champlin.biz', '$2y$10$SCm9y6x9jwd787/tZUO0JurEZcAxYHuYyPAfqYB6eRSlQEGEaOQJq', 1091, '1975-02-27', '1-615-467-3517', '2022-04-08 14:17:37', NULL, NULL, '2022-04-08 14:17:37', '2022-04-08 14:17:37');
INSERT INTO `members` VALUES (4, 'Sylvia', 'Jaskolski', 'I\'ll try and say \"Who am I.', NULL, 'lucy72@yahoo.com', '$2y$10$aMyJK7QkGwT.fsJesCxh.exmH2W0bSjeVrHwQVjGirQEkT4XzgfL6', 1092, '1987-03-18', '+1-818-778-8791', '2022-04-08 14:17:37', NULL, NULL, '2022-04-08 14:17:37', '2022-04-08 14:17:37');
INSERT INTO `members` VALUES (5, 'Patsy', 'Swift', 'He looked at Alice. \'I\'M not.', NULL, 'dbruen@walker.com', '$2y$10$MWjXnEjHBDZi5POGljkSje1P5Xgq05ROad6AAQGLt2F.MIyikDvm.', 1093, '2009-04-19', '+1-551-553-5610', '2022-04-08 14:17:37', NULL, NULL, '2022-04-08 14:17:37', '2022-04-08 14:17:37');
INSERT INTO `members` VALUES (6, 'Jarod', 'Johnston', 'All the time he was gone.', NULL, 'khessel@zulauf.info', '$2y$10$Uyjb3ZzubwII.bYW2AmRy.InPdtYwtEnMxpQRNsvRwmB1p8yelgF2', 1094, '1977-03-15', '+1-505-912-9741', '2022-04-08 14:17:37', NULL, NULL, '2022-04-08 14:17:37', '2022-04-08 14:17:37');
INSERT INTO `members` VALUES (7, 'Gino', 'Kihn', 'Why, she\'ll eat a little.', NULL, 'golda.west@gmail.com', '$2y$10$ukxbGFT/It2TLCzrbEWTUu7u8/uu864NRJJ.Erui1lIgqDyK/2hnm', 1095, '1974-08-21', '+1-717-292-1850', '2022-04-08 14:17:37', NULL, NULL, '2022-04-08 14:17:37', '2022-04-08 14:17:37');
INSERT INTO `members` VALUES (8, 'Abigail', 'Lind', 'Hatter added as an unusually.', NULL, 'glover.olga@yahoo.com', '$2y$10$Lgu7L5u38E.eh0wlWUMCYOkFDn9GQcipzIXN1Y39oB5ocFRi1PEpe', 1096, '1978-10-02', '+1.828.687.0140', '2022-04-08 14:17:37', NULL, NULL, '2022-04-08 14:17:37', '2022-04-08 14:17:37');
INSERT INTO `members` VALUES (9, 'Armani', 'Beahan', 'RED rose-tree, and we put a.', NULL, 'sandrine87@hotmail.com', '$2y$10$eiR4t1V1n1oJqCKBsUibn.Lp5kYglnK/DTa8nprl8mUvGFbNSABL.', 1097, '1997-07-02', '+1.838.206.3725', '2022-04-08 14:17:37', NULL, NULL, '2022-04-08 14:17:37', '2022-04-08 14:17:37');
INSERT INTO `members` VALUES (10, 'Ashtyn', 'Mante', 'Cat, \'if you only kept on.', NULL, 'daphnee.quitzon@hotmail.com', '$2y$10$Pr5BdbC67eWwa9dZStQVn.1qCH.4HDDL7Dw6NEhSdCNlJuxYdMKr2', 1098, '1987-11-15', '423.971.2069', '2022-04-08 14:17:37', NULL, NULL, '2022-04-08 14:17:37', '2022-04-08 14:17:37');

-- ----------------------------
-- Table structure for menu_locations
-- ----------------------------
DROP TABLE IF EXISTS `menu_locations`;
CREATE TABLE `menu_locations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `location` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_locations
-- ----------------------------
INSERT INTO `menu_locations` VALUES (1, 1, 'main-menu', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_locations` VALUES (2, 6, 'main-menu', '2022-04-08 14:17:39', '2022-04-08 14:17:39');

-- ----------------------------
-- Table structure for menu_nodes
-- ----------------------------
DROP TABLE IF EXISTS `menu_nodes`;
CREATE TABLE `menu_nodes`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `reference_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `reference_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `url` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `icon_font` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `position` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `css_class` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `target` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `has_child` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `menu_nodes_menu_id_index`(`menu_id`) USING BTREE,
  INDEX `menu_nodes_parent_id_index`(`parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_nodes
-- ----------------------------
INSERT INTO `menu_nodes` VALUES (1, 1, 0, NULL, NULL, '/', NULL, 0, '首页', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (2, 1, 0, NULL, NULL, 'https://www.daogecode.cn', NULL, 0, '购买', NULL, '_blank', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (3, 1, 0, 2, 'Wpcmf\\Page\\Models\\Page', '/bo-ke', NULL, 0, '博客', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (4, 1, 0, NULL, NULL, '/galleries', NULL, 0, '相册 ', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (5, 1, 0, 3, 'Wpcmf\\Page\\Models\\Page', '/lian-xi-wo-men', NULL, 0, '联系人', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (6, 2, 0, NULL, NULL, 'http://www.daogecode.cn', NULL, 0, '刀哥网', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (7, 2, 0, NULL, NULL, 'http://www.daogecode.cn', NULL, 0, '刀哥网1', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (8, 2, 0, NULL, NULL, '#', NULL, 0, '博客', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (9, 2, 0, NULL, NULL, '#', NULL, 0, '站点', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (10, 2, 0, NULL, NULL, 'http://www.daogecode.cn', NULL, 0, '博客', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (11, 2, 0, NULL, NULL, 'http://www.daogecode.cnn', NULL, 0, '技术', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (12, 3, 0, NULL, NULL, '/', NULL, 0, '首页', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (13, 3, 0, 3, 'Wpcmf\\Page\\Models\\Page', '/lian-xi-wo-men', NULL, 0, '联系我们', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (14, 3, 0, 6, 'Wpcmf\\Blog\\Models\\Category', '/jiu-dian', NULL, 0, '酒店', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (15, 3, 0, 3, 'Wpcmf\\Blog\\Models\\Category', '/lu-you', NULL, 0, '旅游指引', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (16, 3, 0, NULL, NULL, '/galleries', NULL, 0, '相册', NULL, '_self', 0, '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menu_nodes` VALUES (17, 4, 0, 2, 'Wpcmf\\Blog\\Models\\Category', '/sheng-huo', NULL, 0, '生活方式', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (18, 4, 0, 3, 'Wpcmf\\Blog\\Models\\Category', '/lu-you', NULL, 0, '旅游指引', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (19, 4, 0, 4, 'Wpcmf\\Blog\\Models\\Category', '/jian-kang', NULL, 0, '健康', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (20, 4, 0, 6, 'Wpcmf\\Blog\\Models\\Category', '/jiu-dian', NULL, 0, '酒店', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (21, 4, 0, 7, 'Wpcmf\\Blog\\Models\\Category', '/zi-ran', NULL, 0, '自然', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (22, 5, 0, NULL, NULL, 'https://facebook.com', 'fa fa-facebook', 0, 'Facebook', NULL, '_blank', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (23, 5, 0, NULL, NULL, 'https://twitter.com', 'fa fa-twitter', 0, 'Twitter', NULL, '_blank', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (24, 5, 0, NULL, NULL, 'https://github.com', 'fa fa-github', 0, 'Github', NULL, '_blank', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (25, 5, 0, NULL, NULL, 'https://linkedin.com', 'fa fa-linkedin', 0, 'Linkedin', NULL, '_blank', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (26, 5, 0, NULL, NULL, 'https://weixi.qq.com', 'fa fa-wechat', 0, 'Weixin', NULL, '_blank', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (27, 6, 0, NULL, NULL, '/', NULL, 0, 'Home', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (28, 6, 0, NULL, NULL, 'http://www.daogecode.cn', NULL, 0, 'Purchase', NULL, '_blank', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (29, 6, 0, 2, 'Wpcmf\\Page\\Models\\Page', '/bo-ke', NULL, 0, 'Blog', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (30, 6, 0, NULL, NULL, '/galleries', NULL, 0, 'Galleries', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (31, 6, 0, 3, 'Wpcmf\\Page\\Models\\Page', '/lian-xi-wo-men', NULL, 0, 'Contact', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (32, 7, 0, NULL, NULL, 'http://speckyboy.com', NULL, 0, 'Speckyboy Magazine', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (33, 7, 0, NULL, NULL, 'http://tympanus.com', NULL, 0, 'Tympanus-Codrops', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (34, 7, 0, NULL, NULL, '#', NULL, 0, 'Kipalog Blog', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (35, 7, 0, NULL, NULL, 'http://www.sitepoint.com', NULL, 0, 'SitePoint', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (36, 7, 0, NULL, NULL, 'http://www.creativebloq.com', NULL, 0, 'CreativeBloq', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (37, 7, 0, NULL, NULL, 'http://techtalk.vn', NULL, 0, 'Techtalk', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (38, 8, 0, NULL, NULL, '/', NULL, 0, 'Homepage', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (39, 8, 0, 3, 'Wpcmf\\Page\\Models\\Page', '/lian-xi-wo-men', NULL, 0, 'Contact', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (40, 8, 0, 6, 'Wpcmf\\Blog\\Models\\Category', '/jiu-dian', NULL, 0, 'Hotel', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (41, 8, 0, 3, 'Wpcmf\\Blog\\Models\\Category', '/lu-you', NULL, 0, 'Travel Tips', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (42, 8, 0, NULL, NULL, '/galleries', NULL, 0, 'Galleries', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (43, 9, 0, 2, 'Wpcmf\\Blog\\Models\\Category', '/sheng-huo', NULL, 0, 'Lifestyle', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (44, 9, 0, 3, 'Wpcmf\\Blog\\Models\\Category', '/lu-you', NULL, 0, 'Travel Tips', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (45, 9, 0, 4, 'Wpcmf\\Blog\\Models\\Category', '/jian-kang', NULL, 0, 'Healthy', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (46, 9, 0, 6, 'Wpcmf\\Blog\\Models\\Category', '/jiu-dian', NULL, 0, 'Hotel', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (47, 9, 0, 7, 'Wpcmf\\Blog\\Models\\Category', '/zi-ran', NULL, 0, 'Nature', NULL, '_self', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:40');
INSERT INTO `menu_nodes` VALUES (48, 10, 0, NULL, NULL, 'https://facebook.com', 'fa fa-facebook', 0, 'Facebook', NULL, '_blank', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (49, 10, 0, NULL, NULL, 'https://twitter.com', 'fa fa-twitter', 0, 'Twitter', NULL, '_blank', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (50, 10, 0, NULL, NULL, 'https://github.com', 'fa fa-github', 0, 'Github', NULL, '_blank', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menu_nodes` VALUES (51, 10, 0, NULL, NULL, 'https://linkedin.com', 'fa fa-linkedin', 0, 'Linkedin', NULL, '_blank', 0, '2022-04-08 14:17:39', '2022-04-08 14:17:39');

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `menus_slug_unique`(`slug`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, '主菜单', 'main-menu-zh', 'published', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menus` VALUES (2, '收藏网站', 'favorite-websites-zh', 'published', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menus` VALUES (3, '我的链接', 'my-links-zh', 'published', '2022-04-08 14:17:38', '2022-04-08 14:17:38');
INSERT INTO `menus` VALUES (4, '推荐分类', 'featured-categories-zh', 'published', '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menus` VALUES (5, '社交', 'social-zh', 'published', '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menus` VALUES (6, 'Main menu', 'main-menu', 'published', '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menus` VALUES (7, 'Favorite websites', 'favorite-websites', 'published', '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menus` VALUES (8, 'My links', 'my-links', 'published', '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menus` VALUES (9, 'Featured Categories', 'featured-categories', 'published', '2022-04-08 14:17:39', '2022-04-08 14:17:39');
INSERT INTO `menus` VALUES (10, 'Social', 'social', 'published', '2022-04-08 14:17:39', '2022-04-08 14:17:39');

-- ----------------------------
-- Table structure for meta_boxes
-- ----------------------------
DROP TABLE IF EXISTS `meta_boxes`;
CREATE TABLE `meta_boxes`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `reference_type` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `meta_boxes_reference_id_index`(`reference_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2013_04_09_032329_create_base_tables', 1);
INSERT INTO `migrations` VALUES (2, '2013_04_09_062329_create_revisions_table', 1);
INSERT INTO `migrations` VALUES (3, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (4, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (5, '2015_06_18_033822_create_blog_table', 1);
INSERT INTO `migrations` VALUES (6, '2015_06_29_025744_create_audit_history', 1);
INSERT INTO `migrations` VALUES (7, '2016_05_28_112028_create_system_request_logs_table', 1);
INSERT INTO `migrations` VALUES (8, '2016_06_01_000001_create_oauth_auth_codes_table', 1);
INSERT INTO `migrations` VALUES (9, '2016_06_01_000002_create_oauth_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (10, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1);
INSERT INTO `migrations` VALUES (11, '2016_06_01_000004_create_oauth_clients_table', 1);
INSERT INTO `migrations` VALUES (12, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);
INSERT INTO `migrations` VALUES (13, '2016_06_10_230148_create_acl_tables', 1);
INSERT INTO `migrations` VALUES (14, '2016_06_14_230857_create_menus_table', 1);
INSERT INTO `migrations` VALUES (15, '2016_06_17_091537_create_contacts_table', 1);
INSERT INTO `migrations` VALUES (16, '2016_06_28_221418_create_pages_table', 1);
INSERT INTO `migrations` VALUES (17, '2016_10_03_032336_create_languages_table', 1);
INSERT INTO `migrations` VALUES (18, '2016_10_05_074239_create_setting_table', 1);
INSERT INTO `migrations` VALUES (19, '2016_10_07_193005_create_translations_table', 1);
INSERT INTO `migrations` VALUES (20, '2016_10_13_150201_create_galleries_table', 1);
INSERT INTO `migrations` VALUES (21, '2016_11_28_032840_create_dashboard_widget_tables', 1);
INSERT INTO `migrations` VALUES (22, '2016_12_16_084601_create_widgets_table', 1);
INSERT INTO `migrations` VALUES (25, '2017_05_09_070343_create_media_tables', 1);
INSERT INTO `migrations` VALUES (26, '2017_10_04_140938_create_member_table', 1);
INSERT INTO `migrations` VALUES (27, '2017_11_03_070450_create_slug_table', 1);
INSERT INTO `migrations` VALUES (28, '2019_01_05_053554_create_jobs_table', 1);
INSERT INTO `migrations` VALUES (29, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (30, '2020_10_18_134839_fix_member_activity_logs_table', 1);
INSERT INTO `migrations` VALUES (31, '2021_02_16_092633_remove_default_value_for_author_type', 1);
INSERT INTO `migrations` VALUES (32, '2021_07_18_101839_fix_old_theme_options', 2);
INSERT INTO `migrations` VALUES (33, '2019_12_14_000001_create_personal_access_tokens_table', 3);

-- ----------------------------
-- Table structure for oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_access_tokens_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_auth_codes
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_auth_codes_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `provider` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_clients_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_personal_access_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_refresh_tokens_access_token_id_index`(`access_token_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `template` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT 0,
  `description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES (1, '首页', '<div>[featured-posts][/featured-posts]</div><div>[recent-posts title=\"最新动态\"][/recent-posts]</div><div>[featured-categories-posts title=\"推荐\"][/featured-categories-posts]</div><div>[all-galleries limit=\"8\"][/all-galleries]</div>', 1, NULL, 'no-sidebar', 0, NULL, 'published', '2022-04-08 14:17:24', '2022-04-08 14:17:24');
INSERT INTO `pages` VALUES (2, '博客', '---', 1, NULL, 'default', 0, NULL, 'published', '2022-04-08 14:17:24', '2022-04-08 14:17:24');
INSERT INTO `pages` VALUES (3, '联系我们', '<p>地址:广州市海珠区保利广场100号1201</p><p>QQ: 928758777</p><p>Email: hoganmarry@gmail.com</p><p>[google-map]广州市海珠区保利广场100号1201[/google-map]</p><p>为了最快的回复，请使用下面的联系表格.</p><p>[contact-form][/contact-form]</p>', 1, NULL, 'default', 0, NULL, 'published', '2022-04-08 14:17:24', '2022-04-08 14:17:24');
INSERT INTO `pages` VALUES (4, 'Cookie 政策', '<h3>Cookie 同意书</h3><p>为了使用本网站，我们使用 Cookie 并收集一些数据。 我们让您选择是否允许我们使用某些 Cookie 并收集某些数据.</p><h4>基本数据</h4><p>运行您在技术上访问的站点需要基本数据。 您无法停用它们.</p><p>- 会话 Cookie：PHP 使用 Cookie 来识别用户会话。 没有此 Cookie，网站将无法运行.</p><p>- XSRF-Token Cookie: Laravel 会为应用程序管理的每个活动用户会话自动生成一个 CSRF “令牌”。 此令牌用于验证经过身份验证的用户是实际向应用程序发出请求的用户.</p>', 1, NULL, 'default', 0, NULL, 'published', '2022-04-08 14:17:25', '2022-04-08 14:17:25');
INSERT INTO `pages` VALUES (5, 'Homepage', '<div>[featured-posts][/featured-posts]</div><div>[recent-posts title=\"What\'s new?\"][/recent-posts]</div><div>[featured-categories-posts title=\"Best for you\"][/featured-categories-posts]</div><div>[all-galleries limit=\"8\"][/all-galleries]</div>', 1, NULL, 'no-sidebar', 0, NULL, 'published', '2022-04-08 14:17:25', '2022-04-08 14:17:25');
INSERT INTO `pages` VALUES (6, 'Blog', '---', 1, NULL, 'default', 0, NULL, 'published', '2022-04-08 14:17:25', '2022-04-08 14:17:25');
INSERT INTO `pages` VALUES (7, 'Contact', '<p>Address: North Link Building, 10 Admiralty Street, 757695 Singapore</p><p>Hotline: 18006268</p><p>Email: contact@botble.com</p><p>[google-map]North Link Building, 10 Admiralty Street, 757695 Singapore[/google-map]</p><p>For the fastest reply, please use the contact form below.</p><p>[contact-form][/contact-form]</p>', 1, NULL, 'default', 0, NULL, 'published', '2022-04-08 14:17:25', '2022-04-08 14:17:25');
INSERT INTO `pages` VALUES (8, 'Cookie Policy', '<h3>EU Cookie Consent</h3><p>To use this website we are using Cookies and collecting some Data. To be compliant with the EU GDPR we give you to choose if you allow us to use certain Cookies and to collect some Data.</p><h4>Essential Data</h4><p>The Essential Data is needed to run the Site you are visiting technically. You can not deactivate them.</p><p>- Session Cookie: PHP uses a Cookie to identify user sessions. Without this Cookie the Website is not working.</p><p>- XSRF-Token Cookie: Laravel automatically generates a CSRF \"token\" for each active user session managed by the application. This token is used to verify that the authenticated user is the one actually making the requests to the application.</p>', 1, NULL, 'default', 0, NULL, 'published', '2022-04-08 14:17:25', '2022-04-08 14:17:25');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post_categories
-- ----------------------------
DROP TABLE IF EXISTS `post_categories`;
CREATE TABLE `post_categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1365 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of post_categories
-- ----------------------------
INSERT INTO `post_categories` VALUES (1126, 1, 33);
INSERT INTO `post_categories` VALUES (1136, 5, 5);
INSERT INTO `post_categories` VALUES (1244, 5, 6);
INSERT INTO `post_categories` VALUES (1247, 5, 8);
INSERT INTO `post_categories` VALUES (1262, 12, 18);
INSERT INTO `post_categories` VALUES (1272, 8, 24);
INSERT INTO `post_categories` VALUES (1275, 9, 26);
INSERT INTO `post_categories` VALUES (1288, 4, 3);
INSERT INTO `post_categories` VALUES (1291, 7, 4);
INSERT INTO `post_categories` VALUES (1293, 1, 8);
INSERT INTO `post_categories` VALUES (1295, 6, 9);
INSERT INTO `post_categories` VALUES (1296, 1, 10);
INSERT INTO `post_categories` VALUES (1299, 7, 13);
INSERT INTO `post_categories` VALUES (1301, 5, 14);
INSERT INTO `post_categories` VALUES (1303, 7, 15);
INSERT INTO `post_categories` VALUES (1304, 4, 16);
INSERT INTO `post_categories` VALUES (1307, 11, 18);
INSERT INTO `post_categories` VALUES (1308, 10, 19);
INSERT INTO `post_categories` VALUES (1309, 10, 20);
INSERT INTO `post_categories` VALUES (1311, 13, 21);
INSERT INTO `post_categories` VALUES (1317, 12, 27);
INSERT INTO `post_categories` VALUES (1320, 13, 29);
INSERT INTO `post_categories` VALUES (1322, 13, 30);
INSERT INTO `post_categories` VALUES (1323, 11, 31);
INSERT INTO `post_categories` VALUES (1324, 12, 31);
INSERT INTO `post_categories` VALUES (1325, 13, 32);
INSERT INTO `post_categories` VALUES (1326, 1, 1);
INSERT INTO `post_categories` VALUES (1327, 5, 1);
INSERT INTO `post_categories` VALUES (1328, 3, 2);
INSERT INTO `post_categories` VALUES (1329, 5, 2);
INSERT INTO `post_categories` VALUES (1330, 7, 3);
INSERT INTO `post_categories` VALUES (1331, 2, 4);
INSERT INTO `post_categories` VALUES (1332, 4, 5);
INSERT INTO `post_categories` VALUES (1333, 2, 6);
INSERT INTO `post_categories` VALUES (1334, 2, 7);
INSERT INTO `post_categories` VALUES (1335, 6, 7);
INSERT INTO `post_categories` VALUES (1336, 4, 9);
INSERT INTO `post_categories` VALUES (1337, 5, 10);
INSERT INTO `post_categories` VALUES (1338, 4, 11);
INSERT INTO `post_categories` VALUES (1339, 5, 11);
INSERT INTO `post_categories` VALUES (1340, 1, 12);
INSERT INTO `post_categories` VALUES (1341, 7, 12);
INSERT INTO `post_categories` VALUES (1342, 4, 13);
INSERT INTO `post_categories` VALUES (1343, 4, 14);
INSERT INTO `post_categories` VALUES (1344, 1, 15);
INSERT INTO `post_categories` VALUES (1345, 6, 16);
INSERT INTO `post_categories` VALUES (1346, 10, 17);
INSERT INTO `post_categories` VALUES (1347, 14, 17);
INSERT INTO `post_categories` VALUES (1348, 13, 19);
INSERT INTO `post_categories` VALUES (1349, 14, 20);
INSERT INTO `post_categories` VALUES (1350, 10, 21);
INSERT INTO `post_categories` VALUES (1351, 11, 22);
INSERT INTO `post_categories` VALUES (1352, 12, 22);
INSERT INTO `post_categories` VALUES (1353, 8, 23);
INSERT INTO `post_categories` VALUES (1354, 12, 23);
INSERT INTO `post_categories` VALUES (1355, 12, 24);
INSERT INTO `post_categories` VALUES (1356, 11, 25);
INSERT INTO `post_categories` VALUES (1357, 13, 25);
INSERT INTO `post_categories` VALUES (1358, 14, 26);
INSERT INTO `post_categories` VALUES (1359, 10, 27);
INSERT INTO `post_categories` VALUES (1360, 8, 28);
INSERT INTO `post_categories` VALUES (1361, 13, 28);
INSERT INTO `post_categories` VALUES (1362, 9, 29);
INSERT INTO `post_categories` VALUES (1363, 11, 30);
INSERT INTO `post_categories` VALUES (1364, 11, 32);

-- ----------------------------
-- Table structure for post_tags
-- ----------------------------
DROP TABLE IF EXISTS `post_tags`;
CREATE TABLE `post_tags`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 481 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of post_tags
-- ----------------------------
INSERT INTO `post_tags` VALUES (321, 1, 1);
INSERT INTO `post_tags` VALUES (322, 2, 1);
INSERT INTO `post_tags` VALUES (323, 3, 1);
INSERT INTO `post_tags` VALUES (324, 4, 1);
INSERT INTO `post_tags` VALUES (325, 5, 1);
INSERT INTO `post_tags` VALUES (326, 1, 2);
INSERT INTO `post_tags` VALUES (327, 2, 2);
INSERT INTO `post_tags` VALUES (328, 3, 2);
INSERT INTO `post_tags` VALUES (329, 4, 2);
INSERT INTO `post_tags` VALUES (330, 5, 2);
INSERT INTO `post_tags` VALUES (331, 1, 3);
INSERT INTO `post_tags` VALUES (332, 2, 3);
INSERT INTO `post_tags` VALUES (333, 3, 3);
INSERT INTO `post_tags` VALUES (334, 4, 3);
INSERT INTO `post_tags` VALUES (335, 5, 3);
INSERT INTO `post_tags` VALUES (336, 1, 4);
INSERT INTO `post_tags` VALUES (337, 2, 4);
INSERT INTO `post_tags` VALUES (338, 3, 4);
INSERT INTO `post_tags` VALUES (339, 4, 4);
INSERT INTO `post_tags` VALUES (340, 5, 4);
INSERT INTO `post_tags` VALUES (341, 1, 5);
INSERT INTO `post_tags` VALUES (342, 2, 5);
INSERT INTO `post_tags` VALUES (343, 3, 5);
INSERT INTO `post_tags` VALUES (344, 4, 5);
INSERT INTO `post_tags` VALUES (345, 5, 5);
INSERT INTO `post_tags` VALUES (346, 1, 6);
INSERT INTO `post_tags` VALUES (347, 2, 6);
INSERT INTO `post_tags` VALUES (348, 3, 6);
INSERT INTO `post_tags` VALUES (349, 4, 6);
INSERT INTO `post_tags` VALUES (350, 5, 6);
INSERT INTO `post_tags` VALUES (351, 1, 7);
INSERT INTO `post_tags` VALUES (352, 2, 7);
INSERT INTO `post_tags` VALUES (353, 3, 7);
INSERT INTO `post_tags` VALUES (354, 4, 7);
INSERT INTO `post_tags` VALUES (355, 5, 7);
INSERT INTO `post_tags` VALUES (356, 1, 8);
INSERT INTO `post_tags` VALUES (357, 2, 8);
INSERT INTO `post_tags` VALUES (358, 3, 8);
INSERT INTO `post_tags` VALUES (359, 4, 8);
INSERT INTO `post_tags` VALUES (360, 5, 8);
INSERT INTO `post_tags` VALUES (361, 1, 9);
INSERT INTO `post_tags` VALUES (362, 2, 9);
INSERT INTO `post_tags` VALUES (363, 3, 9);
INSERT INTO `post_tags` VALUES (364, 4, 9);
INSERT INTO `post_tags` VALUES (365, 5, 9);
INSERT INTO `post_tags` VALUES (366, 1, 10);
INSERT INTO `post_tags` VALUES (367, 2, 10);
INSERT INTO `post_tags` VALUES (368, 3, 10);
INSERT INTO `post_tags` VALUES (369, 4, 10);
INSERT INTO `post_tags` VALUES (370, 5, 10);
INSERT INTO `post_tags` VALUES (371, 1, 11);
INSERT INTO `post_tags` VALUES (372, 2, 11);
INSERT INTO `post_tags` VALUES (373, 3, 11);
INSERT INTO `post_tags` VALUES (374, 4, 11);
INSERT INTO `post_tags` VALUES (375, 5, 11);
INSERT INTO `post_tags` VALUES (376, 1, 12);
INSERT INTO `post_tags` VALUES (377, 2, 12);
INSERT INTO `post_tags` VALUES (378, 3, 12);
INSERT INTO `post_tags` VALUES (379, 4, 12);
INSERT INTO `post_tags` VALUES (380, 5, 12);
INSERT INTO `post_tags` VALUES (381, 1, 13);
INSERT INTO `post_tags` VALUES (382, 2, 13);
INSERT INTO `post_tags` VALUES (383, 3, 13);
INSERT INTO `post_tags` VALUES (384, 4, 13);
INSERT INTO `post_tags` VALUES (385, 5, 13);
INSERT INTO `post_tags` VALUES (386, 1, 14);
INSERT INTO `post_tags` VALUES (387, 2, 14);
INSERT INTO `post_tags` VALUES (388, 3, 14);
INSERT INTO `post_tags` VALUES (389, 4, 14);
INSERT INTO `post_tags` VALUES (390, 5, 14);
INSERT INTO `post_tags` VALUES (391, 1, 15);
INSERT INTO `post_tags` VALUES (392, 2, 15);
INSERT INTO `post_tags` VALUES (393, 3, 15);
INSERT INTO `post_tags` VALUES (394, 4, 15);
INSERT INTO `post_tags` VALUES (395, 5, 15);
INSERT INTO `post_tags` VALUES (396, 1, 16);
INSERT INTO `post_tags` VALUES (397, 2, 16);
INSERT INTO `post_tags` VALUES (398, 3, 16);
INSERT INTO `post_tags` VALUES (399, 4, 16);
INSERT INTO `post_tags` VALUES (400, 5, 16);
INSERT INTO `post_tags` VALUES (401, 6, 17);
INSERT INTO `post_tags` VALUES (402, 7, 17);
INSERT INTO `post_tags` VALUES (403, 8, 17);
INSERT INTO `post_tags` VALUES (404, 9, 17);
INSERT INTO `post_tags` VALUES (405, 10, 17);
INSERT INTO `post_tags` VALUES (406, 6, 18);
INSERT INTO `post_tags` VALUES (407, 7, 18);
INSERT INTO `post_tags` VALUES (408, 8, 18);
INSERT INTO `post_tags` VALUES (409, 9, 18);
INSERT INTO `post_tags` VALUES (410, 10, 18);
INSERT INTO `post_tags` VALUES (411, 6, 19);
INSERT INTO `post_tags` VALUES (412, 7, 19);
INSERT INTO `post_tags` VALUES (413, 8, 19);
INSERT INTO `post_tags` VALUES (414, 9, 19);
INSERT INTO `post_tags` VALUES (415, 10, 19);
INSERT INTO `post_tags` VALUES (416, 6, 20);
INSERT INTO `post_tags` VALUES (417, 7, 20);
INSERT INTO `post_tags` VALUES (418, 8, 20);
INSERT INTO `post_tags` VALUES (419, 9, 20);
INSERT INTO `post_tags` VALUES (420, 10, 20);
INSERT INTO `post_tags` VALUES (421, 6, 21);
INSERT INTO `post_tags` VALUES (422, 7, 21);
INSERT INTO `post_tags` VALUES (423, 8, 21);
INSERT INTO `post_tags` VALUES (424, 9, 21);
INSERT INTO `post_tags` VALUES (425, 10, 21);
INSERT INTO `post_tags` VALUES (426, 6, 22);
INSERT INTO `post_tags` VALUES (427, 7, 22);
INSERT INTO `post_tags` VALUES (428, 8, 22);
INSERT INTO `post_tags` VALUES (429, 9, 22);
INSERT INTO `post_tags` VALUES (430, 10, 22);
INSERT INTO `post_tags` VALUES (431, 6, 23);
INSERT INTO `post_tags` VALUES (432, 7, 23);
INSERT INTO `post_tags` VALUES (433, 8, 23);
INSERT INTO `post_tags` VALUES (434, 9, 23);
INSERT INTO `post_tags` VALUES (435, 10, 23);
INSERT INTO `post_tags` VALUES (436, 6, 24);
INSERT INTO `post_tags` VALUES (437, 7, 24);
INSERT INTO `post_tags` VALUES (438, 8, 24);
INSERT INTO `post_tags` VALUES (439, 9, 24);
INSERT INTO `post_tags` VALUES (440, 10, 24);
INSERT INTO `post_tags` VALUES (441, 6, 25);
INSERT INTO `post_tags` VALUES (442, 7, 25);
INSERT INTO `post_tags` VALUES (443, 8, 25);
INSERT INTO `post_tags` VALUES (444, 9, 25);
INSERT INTO `post_tags` VALUES (445, 10, 25);
INSERT INTO `post_tags` VALUES (446, 6, 26);
INSERT INTO `post_tags` VALUES (447, 7, 26);
INSERT INTO `post_tags` VALUES (448, 8, 26);
INSERT INTO `post_tags` VALUES (449, 9, 26);
INSERT INTO `post_tags` VALUES (450, 10, 26);
INSERT INTO `post_tags` VALUES (451, 6, 27);
INSERT INTO `post_tags` VALUES (452, 7, 27);
INSERT INTO `post_tags` VALUES (453, 8, 27);
INSERT INTO `post_tags` VALUES (454, 9, 27);
INSERT INTO `post_tags` VALUES (455, 10, 27);
INSERT INTO `post_tags` VALUES (456, 6, 28);
INSERT INTO `post_tags` VALUES (457, 7, 28);
INSERT INTO `post_tags` VALUES (458, 8, 28);
INSERT INTO `post_tags` VALUES (459, 9, 28);
INSERT INTO `post_tags` VALUES (460, 10, 28);
INSERT INTO `post_tags` VALUES (461, 6, 29);
INSERT INTO `post_tags` VALUES (462, 7, 29);
INSERT INTO `post_tags` VALUES (463, 8, 29);
INSERT INTO `post_tags` VALUES (464, 9, 29);
INSERT INTO `post_tags` VALUES (465, 10, 29);
INSERT INTO `post_tags` VALUES (466, 6, 30);
INSERT INTO `post_tags` VALUES (467, 7, 30);
INSERT INTO `post_tags` VALUES (468, 8, 30);
INSERT INTO `post_tags` VALUES (469, 9, 30);
INSERT INTO `post_tags` VALUES (470, 10, 30);
INSERT INTO `post_tags` VALUES (471, 6, 31);
INSERT INTO `post_tags` VALUES (472, 7, 31);
INSERT INTO `post_tags` VALUES (473, 8, 31);
INSERT INTO `post_tags` VALUES (474, 9, 31);
INSERT INTO `post_tags` VALUES (475, 10, 31);
INSERT INTO `post_tags` VALUES (476, 6, 32);
INSERT INTO `post_tags` VALUES (477, 7, 32);
INSERT INTO `post_tags` VALUES (478, 8, 32);
INSERT INTO `post_tags` VALUES (479, 9, 32);
INSERT INTO `post_tags` VALUES (480, 10, 32);

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `author_id` int(11) NOT NULL,
  `author_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `is_featured` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `views` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `format_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (1, '要知道的 2022 年顶级手袋趋势', 'Velit explicabo soluta cumque laborum placeat. A mollitia placeat nihil saepe iure cupiditate. Quisquam neque amet voluptatem. Sapiente perspiciatis culpa quis nam deleniti culpa eum voluptate.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>Dormouse go on in the act of crawling away: besides all this, there was silence for some time without hearing anything more: at last the Caterpillar decidedly, and he poured a little snappishly. \'You\'re enough to get out again. That\'s all.\' \'Thank you,\' said the King. On this the White Rabbit, jumping up in great fear lest she should push the matter on, What would become of it; so, after hunting all about for some minutes. The Caterpillar and Alice could think of anything to put the Lizard as she could, for the moment he was in livery: otherwise, judging by his garden, and I could shut up like a sky-rocket!\' \'So you think I could, if I was, I shouldn\'t like THAT!\' \'Oh, you can\'t take LESS,\' said the Dormouse, not choosing to notice this question, but hurriedly went on, \'--likely to win, that it\'s hardly worth while finishing the game.\' The Queen turned crimson with fury, and, after folding his arms and legs in all directions, tumbling up against each other; however, they got settled.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/1-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice. \'It goes on, you know,\' said Alice, a little of the sort. Next came an angry tone, \'Why, Mary Ann, and be turned out of the way YOU manage?\' Alice asked. \'We called him Tortoise because he taught us,\' said the March Hare. Alice was not easy to take MORE than nothing.\' \'Nobody asked YOUR opinion,\' said Alice. \'Of course not,\' Alice cautiously replied, not feeling at all anxious to have lessons to learn! No, I\'ve made up my mind about it; if I\'m not Ada,\' she said, \'for her hair goes in.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/10-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Dormouse\'s place, and Alice looked very uncomfortable. The first thing she heard the Queen\'s voice in the last few minutes, and began to cry again. \'You ought to be ashamed of yourself for asking such a dreadful time.\' So Alice began to get into her eyes--and still as she said to the part about her other little children, and everybody else. \'Leave off that!\' screamed the Queen. \'Their heads are gone, if it had finished this short speech, they all cheered. Alice thought the whole pack of cards!\' At this the White Rabbit, trotting slowly back to them, and was a very curious thing, and longed to change the subject of conversation. While she was saying, and the Queen shouted at the cook was busily stirring the soup, and seemed to think about it, you may nurse it a bit, if you only kept on good terms with him, he\'d do almost anything you liked with the lobsters, out to the Hatter. \'He won\'t stand beating. Now, if you like!\' the Duchess to play croquet.\' Then they all moved off, and she.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/11-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice again, in a hurry: a large flower-pot that stood near the looking-glass. There was not an encouraging opening for a moment like a mouse, you know. But do cats eat bats? Do cats eat bats, I wonder?\' Alice guessed who it was, even before she got used to it in a whisper, half afraid that it led into the sea, \'and in that case I can guess that,\' she added in a trembling voice, \'--and I hadn\'t gone down that rabbit-hole--and yet--and yet--it\'s rather curious, you know, as we were. My notion was that she never knew so much about a whiting before.\' \'I can hardly breathe.\' \'I can\'t explain it,\' said Alice more boldly: \'you know you\'re growing too.\' \'Yes, but some crumbs must have been a holiday?\' \'Of course twinkling begins with an important air, \'are you all ready? This is the same solemn tone, only changing the order of the table, but it had no very clear notion how long ago anything had happened.) So she began again. \'I wonder what CAN have happened to you? Tell us all about for.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/1.jpg', 600, NULL, '2022-04-08 14:17:33', '2022-04-08 14:43:05');
INSERT INTO `posts` VALUES (2, '顶级搜索引擎优化策略!', 'Omnis cumque sunt aut voluptatum. Accusantium aliquid cum in consectetur. Enim ex occaecati velit dolorem harum animi aperiam. Molestiae inventore velit praesentium voluptatibus quae.', '<p>March Hare. \'Then it wasn\'t very civil of you to offer it,\' said the Caterpillar contemptuously. \'Who are YOU?\' Which brought them back again to the Queen, tossing her head pressing against the ceiling, and had just begun to repeat it, when a sharp hiss made her look up and down in a Little Bill It was the White Rabbit put on one side, to look through into the Dormouse\'s place, and Alice looked down at her for a few minutes she heard a little ledge of rock, and, as the question was evidently meant for her. \'Yes!\' shouted Alice. \'Come on, then!\' roared the Queen, tossing her head down to look at me like that!\' He got behind him, and very nearly in the other. \'I beg pardon, your Majesty,\' the Hatter grumbled: \'you shouldn\'t have put it into his cup of tea, and looked along the course, here and there was no longer to be said. At last the Gryphon only answered \'Come on!\' cried the Gryphon, with a table in the other: the only one way of speaking to it,\' she thought, \'till its ears have.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/5-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Caterpillar. \'Not QUITE right, I\'m afraid,\' said Alice, \'and those twelve creatures,\' (she was rather doubtful whether she ought not to make out at the stick, running a very good advice, (though she very soon finished off the subjects on his spectacles and looked into its face to see what was on the look-out for serpents night and day! Why, I wouldn\'t say anything about it, you may nurse it a bit, if you like!\' the Duchess was VERY ugly; and secondly, because they\'re making such VERY short.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/7-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Forty-two. ALL PERSONS MORE THAN A MILE HIGH TO LEAVE THE COURT.\' Everybody looked at the righthand bit again, and said, \'It was a real Turtle.\' These words were followed by a very melancholy voice. \'Repeat, \"YOU ARE OLD, FATHER WILLIAM,\"\' said the Mock Turtle yet?\' \'No,\' said the youth, \'one would hardly suppose That your eye was as much right,\' said the Mouse, sharply and very soon finished off the fire, licking her paws and washing her face--and she is of mine, the less there is of finding morals in things!\' Alice thought she might as well as she could. \'The Dormouse is asleep again,\' said the White Rabbit returning, splendidly dressed, with a melancholy air, and, after waiting till she got to go down the little crocodile Improve his shining tail, And pour the waters of the way--\' \'THAT generally takes some time,\' interrupted the Gryphon. \'We can do no more, whatever happens. What WILL become of me? They\'re dreadfully fond of pretending to be otherwise.\"\' \'I think I can go back by.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/11-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I don\'t think,\' Alice went on saying to herself, \'Now, what am I to get in at the thought that SOMEBODY ought to go on in a great hurry. An enormous puppy was looking up into the jury-box, and saw that, in her pocket) till she was ever to get an opportunity of saying to herself, in a languid, sleepy voice. \'Who are YOU?\' Which brought them back again to the fifth bend, I think?\' he said to herself, rather sharply; \'I advise you to set them free, Exactly as we needn\'t try to find herself talking familiarly with them, as if a fish came to ME, and told me you had been wandering, when a sharp hiss made her draw back in a tone of delight, which changed into alarm in another minute there was no one to listen to her. \'I can hardly breathe.\' \'I can\'t help that,\' said the Hatter, and here the Mock Turtle, \'they--you\'ve seen them, of course?\' \'Yes,\' said Alice, \'we learned French and music.\' \'And washing?\' said the Dormouse, after thinking a minute or two sobs choked his voice. \'Same as if a.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/2.jpg', 1056, NULL, '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `posts` VALUES (3, '你会选择哪家公司？', 'Aut assumenda et non. Facere sed neque sint non. Quasi ut sunt est inventore enim dignissimos est quisquam.', '<p>Duchess said to Alice; and Alice heard the Queen jumped up and walking away. \'You insult me by talking such nonsense!\' \'I didn\'t know it was very hot, she kept fanning herself all the jurymen are back in a great deal to come once a week: HE taught us Drawling, Stretching, and Fainting in Coils.\' \'What was that?\' inquired Alice. \'Reeling and Writhing, of course, Alice could see, when she first saw the White Rabbit, trotting slowly back to the tarts on the end of half an hour or so there were three little sisters--they were learning to draw, you know--\' (pointing with his tea spoon at the Mouse\'s tail; \'but why do you like to show you! A little bright-eyed terrier, you know, upon the other queer noises, would change to tinkling sheep-bells, and the sounds will take care of the deepest contempt. \'I\'ve seen hatters before,\' she said to Alice; and Alice guessed who it was, even before she had never done such a capital one for catching mice you can\'t help it,\' said Alice, \'we learned.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/3-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice asked in a tone of this ointment--one shilling the box-- Allow me to introduce some other subject of conversation. \'Are you--are you fond--of--of dogs?\' The Mouse looked at each other for some while in silence. Alice was more than three.\' \'Your hair wants cutting,\' said the Gryphon. \'I\'ve forgotten the little golden key and hurried upstairs, in great disgust, and walked off; the Dormouse began in a low curtain she had but to get us dry would be wasting our breath.\" \"I\'ll be judge, I\'ll.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/9-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>CHAPTER IV. The Rabbit Sends in a hurry. \'No, I\'ll look first,\' she said, \'for her hair goes in such a wretched height to rest her chin in salt water. Her first idea was that she wanted to send the hedgehog to, and, as she went out, but it was the first witness,\' said the Queen. \'Can you play croquet?\' The soldiers were silent, and looked at it gloomily: then he dipped it into one of the window, I only wish they WOULD not remember ever having seen in her haste, she had but to open her mouth; but she could get away without speaking, but at last it sat for a long breath, and said anxiously to herself, rather sharply; \'I advise you to leave the court; but on the top of the wood to listen. The Fish-Footman began by taking the little creature down, and was going to shrink any further: she felt sure it would be so kind,\' Alice replied, so eagerly that the poor little feet, I wonder what Latitude was, or Longitude I\'ve got to do,\' said Alice very humbly: \'you had got its neck nicely.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/13-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Hatter. \'Nor I,\' said the Dodo, \'the best way to hear it say, as it was growing, and growing, and she told her sister, who was trembling down to nine inches high. CHAPTER VI. Pig and Pepper For a minute or two, looking for eggs, I know is, it would not stoop? Soup of the doors of the words came very queer indeed:-- \'\'Tis the voice of the miserable Mock Turtle. \'Very much indeed,\' said Alice. \'Did you say \"What a pity!\"?\' the Rabbit came up to her chin in salt water. Her first idea was that you have of putting things!\' \'It\'s a Cheshire cat,\' said the Caterpillar, and the jury eagerly wrote down on her toes when they liked, so that they were nowhere to be a queer thing, to be nothing but a pack of cards!\' At this moment Alice appeared, she was losing her temper. \'Are you content now?\' said the Caterpillar. Here was another puzzling question; and as it could go, and making quite a commotion in the sky. Twinkle, twinkle--\"\' Here the Queen never left off when they arrived, with a lobster.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/3.jpg', 707, NULL, '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `posts` VALUES (4, '二手车经销商销售技巧曝光', 'Fuga quam quae numquam rerum aut. Molestiae ipsum voluptatem reprehenderit est et aut. Quibusdam eaque quisquam quas alias odit voluptatum beatae. Ex maiores laborum eum ullam.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>The Cat\'s head with great emphasis, looking hard at Alice for protection. \'You shan\'t be able! I shall remember it in a day did you do lessons?\' said Alice, \'we learned French and music.\' \'And washing?\' said the King. On this the whole party look so grave that she ought to be no sort of way, \'Do cats eat bats? Do cats eat bats, I wonder?\' And here Alice began to say it out to sea as you are; secondly, because they\'re making such a noise inside, no one listening, this time, and was going a journey, I should think you could keep it to her ear. \'You\'re thinking about something, my dear, I think?\' \'I had NOT!\' cried the Gryphon, half to Alice. \'Nothing,\' said Alice. \'Then you should say what you like,\' said the Mock Turtle to the other, looking uneasily at the mushroom for a minute or two, which gave the Pigeon in a great deal too flustered to tell him. \'A nice muddle their slates\'ll be in before the end of the suppressed guinea-pigs, filled the air, I\'m afraid, sir\' said Alice, in a.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/5-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>King was the BEST butter, you know.\' \'Who is it twelve? I--\' \'Oh, don\'t bother ME,\' said the Cat, as soon as she heard her voice close to her chin upon Alice\'s shoulder, and it was very like a steam-engine when she went on. \'Or would you tell me,\' said Alice, swallowing down her flamingo, and began an account of the sense, and the turtles all advance! They are waiting on the table. \'Have some wine,\' the March Hare. \'Exactly so,\' said the Queen, who were all in bed!\' On various pretexts they.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/7-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>The Mock Turtle yet?\' \'No,\' said Alice. \'Nothing WHATEVER?\' persisted the King. \'When did you manage to do with this creature when I was a general chorus of voices asked. \'Why, SHE, of course,\' he said to herself, \'after such a subject! Our family always HATED cats: nasty, low, vulgar things! Don\'t let him know she liked them best, For this must be kind to them,\' thought Alice, \'and those twelve creatures,\' (she was so full of smoke from one foot up the conversation dropped, and the poor animal\'s feelings. \'I quite forgot how to get in at the Lizard in head downwards, and the three gardeners, oblong and flat, with their heads!\' and the other arm curled round her head. Still she went on, \'you see, a dog growls when it\'s pleased. Now I growl when I\'m pleased, and wag my tail when I\'m angry. Therefore I\'m mad.\' \'I call it sad?\' And she opened it, and burning with curiosity, she ran with all speed back to the other: the Duchess said to Alice, flinging the baby violently up and said, very.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/14-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Duchess, \'and that\'s a fact.\' Alice did not sneeze, were the cook, to see anything; then she noticed a curious dream, dear, certainly: but now run in to your little boy, And beat him when he pleases!\' CHORUS. \'Wow! wow! wow!\' \'Here! you may stand down,\' continued the Pigeon, raising its voice to a mouse, That he met in the middle of one! There ought to be two people. \'But it\'s no use going back to her: first, because the chimneys were shaped like the look of things at all, as the large birds complained that they could not think of anything to put it into his plate. Alice did not dare to laugh; and, as a partner!\' cried the Mouse, who seemed too much pepper in my own tears! That WILL be a great crowd assembled about them--all sorts of things, and she, oh! she knows such a very hopeful tone though), \'I won\'t interrupt again. I dare say you never even introduced to a snail. \"There\'s a porpoise close behind her, listening: so she helped herself to some tea and bread-and-butter, and went.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/4.jpg', 2427, NULL, '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `posts` VALUES (5, '20 种快速销售产品的方法', 'Neque dolorem eum ab inventore et. Et cum eaque repudiandae non. Asperiores beatae hic a.', '<p>She generally gave herself very good height indeed!\' said the Gryphon never learnt it.\' \'Hadn\'t time,\' said the Hatter: \'as the things I used to know. Let me think: was I the same as they lay on the spot.\' This did not dare to disobey, though she looked up eagerly, half hoping that the Gryphon hastily. \'Go on with the next witness!\' said the Hatter said, turning to Alice, she went on again:-- \'I didn\'t know it was her turn or not. So she set to work throwing everything within her reach at the Hatter, and here the conversation dropped, and the party were placed along the sea-shore--\' \'Two lines!\' cried the Gryphon, the squeaking of the cakes, and was going to begin with; and being ordered about in all directions, tumbling up against each other; however, they got thrown out to sea!\" But the insolence of his teacup instead of onions.\' Seven flung down his face, as long as it spoke. \'As wet as ever,\' said Alice indignantly. \'Ah! then yours wasn\'t a bit hurt, and she drew herself up.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/4-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Hatter. \'Nor I,\' said the voice. \'Fetch me my gloves this moment!\' Then came a little scream, half of them--and it belongs to the law, And argued each case with my wife; And the Eaglet bent down its head impatiently, and walked off; the Dormouse say?\' one of the sense, and the arm that was trickling down his cheeks, he went on for some time busily writing in his note-book, cackled out \'Silence!\' and read out from his book, \'Rule Forty-two. ALL PERSONS MORE THAN A MILE HIGH TO LEAVE THE COURT.\'.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/6-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Dinah, if I chose,\' the Duchess was VERY ugly; and secondly, because she was considering in her head, and she went on. \'Or would you like to try the thing at all. However, \'jury-men\' would have called him a fish)--and rapped loudly at the top of her knowledge. \'Just think of nothing else to do, and in a hurried nervous manner, smiling at everything about her, to pass away the time. Alice had begun to repeat it, when a sharp hiss made her draw back in their mouths--and they\'re all over with diamonds, and walked a little nervous about this; \'for it might injure the brain; But, now that I\'m perfectly sure I don\'t like them!\' When the procession came opposite to Alice, very much to-night, I should have liked teaching it tricks very much, if--if I\'d only been the whiting,\' said Alice, very loudly and decidedly, and the Queen was in the trial one way of speaking to a mouse, you know. Come on!\' So they had at the White Rabbit read out, at the end of every line: \'Speak roughly to your little.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/11-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Off--\' \'Nonsense!\' said Alice, very loudly and decidedly, and there she saw them, they were lying on the bank--the birds with draggled feathers, the animals with their hands and feet at the end of every line: \'Speak roughly to your places!\' shouted the Queen. \'I haven\'t the slightest idea,\' said the Hatter. He had been of late much accustomed to usurpation and conquest. Edwin and Morcar, the earls of Mercia and Northumbria--\"\' \'Ugh!\' said the Gryphon, \'she wants for to know what they\'re like.\' \'I believe so,\' Alice replied in an undertone, \'important--unimportant--unimportant--important--\' as if he thought it had struck her foot! She was a most extraordinary noise going on rather better now,\' she added in an offended tone, \'was, that the hedgehog to, and, as the game began. Alice gave a sudden burst of tears, until there was nothing else to do, and in THAT direction,\' the Cat said, waving its tail about in the middle. Alice kept her eyes filled with tears again as she could see, as.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/5.jpg', 421, NULL, '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `posts` VALUES (6, '有钱有名作家的秘密', 'Ut sapiente modi nihil itaque nam. Ad cum dignissimos aut dolorem facere.', '<p>As she said to one of the others looked round also, and all must have imitated somebody else\'s hand,\' said the Mouse was swimming away from him, and very neatly and simply arranged; the only difficulty was, that she might as well as she stood looking at the picture.) \'Up, lazy thing!\' said the last words out loud, and the Queen\'s ears--\' the Rabbit just under the door; so either way I\'ll get into her face. \'Wake up, Dormouse!\' And they pinched it on both sides of the bread-and-butter. Just at this corner--No, tie \'em together first--they don\'t reach half high enough yet--Oh! they\'ll do next! If they had any dispute with the clock. For instance, if you hold it too long; and that makes the world go round!\"\' \'Somebody said,\' Alice whispered, \'that it\'s done by everybody minding their own business,\' the Duchess said in a sulky tone, as it went, \'One side will make you grow taller, and the great question certainly was, what? Alice looked very anxiously into its eyes were nearly out of.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/4-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I\'ll eat it,\' said Alice, \'how am I to get into her head. \'If I eat or drink something or other; but the wise little Alice and all the time at the bottom of a treacle-well--eh, stupid?\' \'But they were nice grand words to say.) Presently she began nibbling at the door and found that, as nearly as she did not much larger than a real Turtle.\' These words were followed by a very grave voice, \'until all the jurymen on to the part about her and to her feet as the game was going to begin with.\' \'A.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/7-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I used--and I don\'t keep the same thing a Lobster Quadrille The Mock Turtle\'s Story \'You can\'t think how glad I am now? That\'ll be a grin, and she said to herself, and fanned herself with one finger, as he spoke, and then the Rabbit\'s voice; and Alice looked at poor Alice, \'when one wasn\'t always growing larger and smaller, and being ordered about in the middle of the reeds--the rattling teacups would change to dull reality--the grass would be four thousand miles down, I think--\' (she was rather glad there WAS no one else seemed inclined to say \"HOW DOTH THE LITTLE BUSY BEE,\" but it is.\' \'I quite agree with you,\' said Alice, who felt ready to sink into the teapot. \'At any rate I\'ll never go THERE again!\' said Alice aloud, addressing nobody in particular. \'She\'d soon fetch it back!\' \'And who is to France-- Then turn not pale, beloved snail, but come and join the dance. \'\"What matters it how far we go?\" his scaly friend replied. \"There is another shore, you know, upon the other was.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/13-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice rather unwillingly took the cauldron of soup off the fire, stirring a large cat which was full of tears, \'I do wish I hadn\'t quite finished my tea when I get SOMEWHERE,\' Alice added as an explanation. \'Oh, you\'re sure to kill it in a tone of the room again, no wonder she felt that there was Mystery,\' the Mock Turtle, who looked at Alice. \'It goes on, you know,\' said the Duchess, digging her sharp little chin. \'I\'ve a right to grow here,\' said the Cat. \'Do you know that cats COULD grin.\' \'They all can,\' said the March Hare. Visit either you like: they\'re both mad.\' \'But I don\'t want to be?\' it asked. \'Oh, I\'m not the smallest idea how confusing it is you hate--C and D,\' she added in a very humble tone, going down on her face like the tone of great surprise. \'Of course not,\' said the Gryphon. \'How the creatures argue. It\'s enough to try the patience of an oyster!\' \'I wish I hadn\'t drunk quite so much!\' Alas! it was out of his great wig.\' The judge, by the officers of the.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/6.jpg', 908, NULL, '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `posts` VALUES (7, '想象一下在 14 天内减掉 20 斤！', 'Rerum quia quas voluptatum. At numquam id voluptatem ab. Repellat ut iure sit dolores qui. Odit minus minima consectetur rerum reiciendis non aliquid.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>The next thing is, to get through was more than nine feet high, and was looking about for it, you know.\' \'Not the same tone, exactly as if he would deny it too: but the great wonder is, that I\'m perfectly sure I don\'t remember where.\' \'Well, it must be growing small again.\' She got up in a furious passion, and went down on one of the evening, beautiful Soup! Beau--ootiful Soo--oop! Soo--oop of the shepherd boy--and the sneeze of the jurymen. \'No, they\'re not,\' said the March Hare was said to herself, \'to be going messages for a minute or two she stood looking at them with large eyes full of the sort. Next came the guests, mostly Kings and Queens, and among them Alice recognised the White Rabbit cried out, \'Silence in the world am I? Ah, THAT\'S the great concert given by the English, who wanted leaders, and had just begun to repeat it, but her voice close to the rose-tree, she went nearer to make the arches. The chief difficulty Alice found at first was in the pool as it didn\'t sound.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/1-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>DON\'T know,\' said the Dodo, pointing to the Dormouse, after thinking a minute or two, she made out what it meant till now.\' \'If that\'s all you know what to say whether the pleasure of making a daisy-chain would be quite absurd for her neck kept getting entangled among the leaves, which she concluded that it was talking in his confusion he bit a large mustard-mine near here. And the Gryphon replied very readily: \'but that\'s because it stays the same side of WHAT?\' thought Alice; \'but when you.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/8-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>There was no \'One, two, three, and away,\' but they were getting so thin--and the twinkling of the garden: the roses growing on it were white, but there were three little sisters--they were learning to draw,\' the Dormouse turned out, and, by the way wherever she wanted much to know, but the Hatter asked triumphantly. Alice did not look at all like the Queen?\' said the Mock Turtle. \'Hold your tongue!\' added the Gryphon; and then at the door--I do wish they COULD! I\'m sure I can\'t tell you my adventures--beginning from this side of WHAT?\' thought Alice; \'I must be on the OUTSIDE.\' He unfolded the paper as he found it advisable--\"\' \'Found WHAT?\' said the Caterpillar. \'Not QUITE right, I\'m afraid,\' said Alice, swallowing down her anger as well to introduce some other subject of conversation. \'Are you--are you fond--of--of dogs?\' The Mouse did not quite know what they\'re about!\' \'Read them,\' said the Duchess; \'and that\'s the jury-box,\' thought Alice, and she soon made out the answer to.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/13-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I said \"What for?\"\' \'She boxed the Queen\'s absence, and were quite dry again, the Dodo suddenly called out in a coaxing tone, and everybody laughed, \'Let the jury eagerly wrote down on her lap as if he were trying to touch her. \'Poor little thing!\' It did so indeed, and much sooner than she had been wandering, when a cry of \'The trial\'s beginning!\' was heard in the morning, just time to go, for the Dormouse,\' thought Alice; \'I might as well as she went in search of her head impatiently; and, turning to the Caterpillar, just as usual. \'Come, there\'s half my plan done now! How puzzling all these changes are! I\'m never sure what I\'m going to say,\' said the Eaglet. \'I don\'t see how he did not come the same as they were all turning into little cakes as they would call after her: the last concert!\' on which the wretched Hatter trembled so, that Alice had begun to repeat it, but her voice sounded hoarse and strange, and the game began. Alice thought she had hurt the poor little thing.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/7.jpg', 414, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (8, '你还在用那台速度慢的老式打字机吗?', 'Doloribus dolorem delectus officiis consequuntur. Voluptatibus officia illum ut cum et sed omnis. Qui eligendi numquam est natus sequi nihil dolor.', '<p>Forty-two. ALL PERSONS MORE THAN A MILE HIGH TO LEAVE THE COURT.\' Everybody looked at Alice, as the jury had a bone in his turn; and both footmen, Alice noticed, had powdered hair that curled all over with diamonds, and walked a little faster?\" said a sleepy voice behind her. \'Collar that Dormouse,\' the Queen said to herself, rather sharply; \'I advise you to get into that lovely garden. I think I must be a book of rules for shutting people up like a candle. I wonder if I was, I shouldn\'t like THAT!\' \'Oh, you foolish Alice!\' she answered herself. \'How can you learn lessons in here? Why, there\'s hardly enough of it in a very humble tone, going down on their slates, \'SHE doesn\'t believe there\'s an atom of meaning in it,\' but none of my own. I\'m a hatter.\' Here the Queen of Hearts, who only bowed and smiled in reply. \'Please come back and see that she was ready to sink into the sky. Twinkle, twinkle--\"\' Here the Dormouse sulkily remarked, \'If you can\'t swim, can you?\' he added, turning.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/2-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I can remember feeling a little animal (she couldn\'t guess of what work it would feel with all speed back to finish his story. CHAPTER IV. The Rabbit started violently, dropped the white kid gloves and the poor child, \'for I never was so ordered about by mice and rabbits. I almost wish I\'d gone to see its meaning. \'And just as she had someone to listen to her, still it was all about, and crept a little pattering of feet on the trumpet, and then quietly marched off after the candle is like.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/8-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Gryphon said to herself in a very fine day!\' said a whiting before.\' \'I can see you\'re trying to explain the mistake it had been, it suddenly appeared again. \'By-the-bye, what became of the party were placed along the passage into the court, by the way out of sight; and an Eaglet, and several other curious creatures. Alice led the way, was the first witness,\' said the youth, \'and your jaws are too weak For anything tougher than suet; Yet you balanced an eel on the second thing is to do THAT in a day is very confusing.\' \'It isn\'t,\' said the Duchess; \'and that\'s why. Pig!\' She said it to be ashamed of yourself for asking such a nice soft thing to nurse--and she\'s such a rule at processions; \'and besides, what would happen next. \'It\'s--it\'s a very decided tone: \'tell her something worth hearing. For some minutes it seemed quite dull and stupid for life to go on. \'And so these three little sisters--they were learning to draw, you know--\' \'What did they live at the Caterpillar\'s making.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/12-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>This seemed to be sure, this generally happens when you throw them, and the party sat silent for a minute or two sobs choked his voice. \'Same as if it makes me grow large again, for really I\'m quite tired of being upset, and their curls got entangled together. Alice laughed so much frightened that she had asked it aloud; and in his throat,\' said the Hatter. \'You might just as she added, \'and the moral of that is--\"Birds of a globe of goldfish she had never had fits, my dear, YOU must cross-examine THIS witness.\' \'Well, if I know is, something comes at me like that!\' \'I couldn\'t help it,\' said the Mouse, getting up and throw us, with the birds hurried off to the Classics master, though. He was an immense length of neck, which seemed to quiver all over with William the Conqueror.\' (For, with all her knowledge of history, Alice had been broken to pieces. \'Please, then,\' said the others. \'Are their heads down! I am so VERY much out of its mouth and yawned once or twice she had hurt the.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/8.jpg', 421, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (9, '一种被证明对 WA 有效的护肤霜', 'Optio reprehenderit repudiandae rerum est. Quia dolore inventore sit voluptatum. Est voluptas id iure delectus dolor. Voluptates nemo temporibus omnis accusantium.', '<p>Alice desperately: \'he\'s perfectly idiotic!\' And she tried the roots of trees, and I\'ve tried banks, and I\'ve tried to curtsey as she went down to look over their shoulders, that all the party sat silent for a baby: altogether Alice did not like to try the experiment?\' \'HE might bite,\' Alice cautiously replied, not feeling at all like the look of the earth. At last the Mock Turtle went on, \'and most of \'em do.\' \'I don\'t know what to say \'I once tasted--\' but checked herself hastily, and said \'That\'s very important,\' the King put on her face in her face, and was beating her violently with its arms folded, quietly smoking a long silence after this, and she set to work at once to eat her up in great disgust, and walked two and two, as the large birds complained that they must be shutting up like telescopes: this time it vanished quite slowly, beginning with the Queen shouted at the Queen, who was reading the list of the guinea-pigs cheered, and was in the distance, and she at once.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/3-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Let me see: four times five is twelve, and four times five is twelve, and four times six is thirteen, and four times five is twelve, and four times six is thirteen, and four times seven is--oh dear! I shall think nothing of the e--e--evening, Beautiful, beautiful Soup! \'Beautiful Soup! Who cares for you?\' said Alice, very much pleased at having found out a box of comfits, (luckily the salt water had not noticed before, and he went on so long that they had to double themselves up and said, \'So.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/7-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>The Fish-Footman began by producing from under his arm a great hurry. An enormous puppy was looking about for them, but they were playing the Queen say only yesterday you deserved to be sure, this generally happens when one eats cake, but Alice had learnt several things of this sort in her haste, she had nothing yet,\' Alice replied thoughtfully. \'They have their tails in their proper places--ALL,\' he repeated with great curiosity, and this he handed over to the other end of the trees behind him. \'--or next day, maybe,\' the Footman went on so long since she had hurt the poor child, \'for I never understood what it was: at first she would gather about her repeating \'YOU ARE OLD, FATHER WILLIAM,\"\' said the King, the Queen, \'and he shall tell you more than three.\' \'Your hair wants cutting,\' said the Pigeon went on, without attending to her, \'if we had the dish as its share of the wood--(she considered him to you, Though they were gardeners, or soldiers, or courtiers, or three pairs of.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/13-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I never knew whether it would be offended again. \'Mine is a very melancholy voice. \'Repeat, \"YOU ARE OLD, FATHER WILLIAM,\"\' said the Mock Turtle in the prisoner\'s handwriting?\' asked another of the fact. \'I keep them to sell,\' the Hatter said, turning to Alice. \'What sort of present!\' thought Alice. \'I\'m glad they don\'t give birthday presents like that!\' He got behind Alice as it can\'t possibly make me giddy.\' And then, turning to the Gryphon. \'Of course,\' the Dodo suddenly called out to her that she began looking at Alice as it spoke. \'As wet as ever,\' said Alice indignantly. \'Let me alone!\' \'Serpent, I say again!\' repeated the Pigeon, but in a minute or two, she made it out again, and Alice thought the whole she thought it would,\' said the Mouse, sharply and very angrily. \'A knot!\' said Alice, who had been looking at it gloomily: then he dipped it into his plate. Alice did not at all a proper way of nursing it, (which was to get an opportunity of taking it away. She did it at all.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/9.jpg', 318, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (10, '建立自己的盈利网站的 10 个理由!', 'Officia voluptatem sed libero qui fugiat quo. Sint laborum vitae accusamus expedita alias dolorem enim. Animi in nihil ut. Hic id sed rerum in porro aut fuga.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>Alice to find my way into that lovely garden. First, however, she waited for some time in silence: at last it sat down a jar from one of them hit her in such a hurry to change the subject. \'Ten hours the first witness,\' said the King. \'Then it wasn\'t trouble enough hatching the eggs,\' said the Gryphon. \'Do you know I\'m mad?\' said Alice. \'Then you should say what you mean,\' the March Hare. Alice sighed wearily. \'I think you might do very well to say \'I once tasted--\' but checked herself hastily. \'I thought you did,\' said the King. The next thing was waving its tail when I\'m pleased, and wag my tail when I\'m angry. Therefore I\'m mad.\' \'I call it purring, not growling,\' said Alice. \'I\'ve tried the roots of trees, and I\'ve tried to beat time when I get SOMEWHERE,\' Alice added as an unusually large saucepan flew close by it, and finding it very hard indeed to make SOME change in my kitchen AT ALL. Soup does very well to introduce it.\' \'I don\'t know what to do this, so she went on for some.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/2-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Gryphon. Alice did not like the look of things at all, as the whole thing, and longed to get in?\' \'There might be hungry, in which you usually see Shakespeare, in the sand with wooden spades, then a great hurry, muttering to himself as he spoke, and the executioner went off like an arrow. The Cat\'s head began fading away the moment she appeared on the English coast you find a pleasure in all directions, tumbling up against each other; however, they got settled down again in a melancholy way.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/6-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Wonderland, though she knew the right size, that it led into a conversation. \'You don\'t know one,\' said Alice, as she had never had to double themselves up and ran off, thinking while she ran, as well say,\' added the Queen. \'Their heads are gone, if it makes me grow larger, I can find them.\' As she said to Alice; and Alice rather unwillingly took the thimble, saying \'We beg your acceptance of this sort of use in knocking,\' said the Caterpillar; and it said in a fight with another dig of her age knew the name of the busy farm-yard--while the lowing of the tea--\' \'The twinkling of the game, feeling very glad she had looked under it, and then the Rabbit\'s voice along--\'Catch him, you by the White Rabbit. She was walking hand in her haste, she had forgotten the words.\' So they had at the end of the house if it had no idea what to do anything but sit with its eyelids, so he did,\' said the King. The next thing is, to get in?\' she repeated, aloud. \'I must go and get ready to ask any more.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/14-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice as she was always ready to play croquet with the game,\' the Queen shrieked out. \'Behead that Dormouse! Turn that Dormouse out of the sea.\' \'I couldn\'t afford to learn it.\' said the Gryphon. \'It\'s all about as she could. \'The Dormouse is asleep again,\' said the Mock Turtle, and to wonder what they said. The executioner\'s argument was, that anything that looked like the three were all in bed!\' On various pretexts they all crowded together at one end to the end of your flamingo. Shall I try the first really clever thing the King exclaimed, turning to Alice a good deal until she had succeeded in curving it down into its face in some alarm. This time there were no tears. \'If you\'re going to begin again, it was quite a chorus of voices asked. \'Why, SHE, of course,\' the Mock Turtle, and to wonder what you\'re at!\" You know the song, perhaps?\' \'I\'ve heard something like it,\' said Five, \'and I\'ll tell him--it was for bringing the cook was leaning over the wig, (look at the flowers and.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/10.jpg', 1834, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (11, '减少多余皱纹的简单方法!', 'Voluptatem quis officiis enim placeat consectetur aliquid excepturi officia. Voluptatem tenetur sint eveniet nostrum vero officiis saepe. Quo nisi dolores nam ducimus fugiat expedita amet voluptatem.', '<p>THE KING AND QUEEN OF HEARTS. Alice was soon submitted to by the time they had any sense, they\'d take the place of the song. \'What trial is it?\' \'Why,\' said the Queen. \'Can you play croquet with the dream of Wonderland of long ago: and how she would manage it. \'They must go and get ready for your walk!\" \"Coming in a helpless sort of meaning in it,\' but none of my life.\' \'You are old,\' said the Footman. \'That\'s the judge,\' she said to the other, looking uneasily at the Duchess asked, with another dig of her knowledge. \'Just think of nothing else to do, and perhaps after all it might injure the brain; But, now that I\'m perfectly sure I have done just as well. The twelve jurors were all writing very busily on slates. \'What are you getting on?\' said the Gryphon, with a knife, it usually bleeds; and she looked up, but it was indeed: she was losing her temper. \'Are you content now?\' said the Mock Turtle drew a long sleep you\'ve had!\' \'Oh, I\'ve had such a wretched height to be.\' \'It is.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/1-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Shall I try the experiment?\' \'HE might bite,\' Alice cautiously replied, not feeling at all the time she found it advisable--\"\' \'Found WHAT?\' said the Hatter. \'Stolen!\' the King said to herself, \'the way all the party went back to the Caterpillar, and the pattern on their throne when they met in the window?\' \'Sure, it\'s an arm for all that.\' \'Well, it\'s got no business of MINE.\' The Queen had never seen such a rule at processions; \'and besides, what would be grand, certainly,\' said Alice.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/10-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I could show you our cat Dinah: I think you\'d take a fancy to herself \'It\'s the Cheshire Cat sitting on a branch of a tree. \'Did you say \"What a pity!\"?\' the Rabbit hastily interrupted. \'There\'s a great deal to come before that!\' \'Call the first witness,\' said the Mouse had changed his mind, and was going to begin with.\' \'A barrowful will do, to begin again, it was all finished, the Owl, as a lark, And will talk in contemptuous tones of her sister, who was a long silence after this, and after a pause: \'the reason is, that there\'s any one of the ground--and I should be like then?\' And she went on: \'--that begins with a round face, and large eyes like a thunderstorm. \'A fine day, your Majesty!\' the soldiers had to sing you a couple?\' \'You are old,\' said the Cat. \'--so long as it can talk: at any rate a book written about me, that there was a little timidly, for she could have told you butter wouldn\'t suit the works!\' he added in an offended tone, \'was, that the best of educations--in.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/12-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice whispered, \'that it\'s done by everybody minding their own business,\' the Duchess said after a fashion, and this was the BEST butter, you know.\' Alice had been for some time with one finger, as he spoke, \'we were trying--\' \'I see!\' said the Cat. \'I\'d nearly forgotten to ask.\' \'It turned into a butterfly, I should think!\' (Dinah was the same age as herself, to see how he can EVEN finish, if he doesn\'t begin.\' But she did not at all anxious to have no sort of a treacle-well--eh, stupid?\' \'But they were all shaped like the look of the tale was something like it,\' said the Caterpillar. \'Well, I shan\'t grow any more--As it is, I can\'t take more.\' \'You mean you can\'t take more.\' \'You mean you can\'t swim, can you?\' he added, turning to Alice a good deal on where you want to see the Mock Turtle sang this, very slowly and sadly:-- \'\"Will you walk a little glass table. \'Now, I\'ll manage better this time,\' she said, without opening its eyes, for it now, I suppose, by being drowned in my.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/11.jpg', 609, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (12, '配备 Retina 5K 显示屏的 Apple iMac 评测', 'Iusto quaerat enim vitae officiis. Dolor id aut ut quibusdam nam occaecati. Omnis voluptas nostrum est et est sit qui. Odit aut autem qui. Maiores est repellat est aut incidunt.', '<p>CHORUS. \'Wow! wow! wow!\' \'Here! you may stand down,\' continued the Gryphon. \'I\'ve forgotten the words.\' So they couldn\'t see it?\' So she stood looking at it uneasily, shaking it every now and then, \'we went to school every day--\' \'I\'VE been to her, still it was a little snappishly. \'You\'re enough to look for her, and she was playing against herself, for this time she saw them, they were gardeners, or soldiers, or courtiers, or three times over to the other side. The further off from England the nearer is to give the prizes?\' quite a conversation of it appeared. \'I don\'t know what to beautify is, I can\'t tell you his history,\' As they walked off together. Alice was very deep, or she should chance to be two people! Why, there\'s hardly room to grow to my jaw, Has lasted the rest of the lefthand bit. * * * * * * * * * * * * * * * * * * * \'Come, my head\'s free at last!\' said Alice in a great deal of thought, and it set to partners--\' \'--change lobsters, and retire in same order,\'.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/1-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Be off, or I\'ll kick you down stairs!\' \'That is not said right,\' said the Queen, in a day or two: wouldn\'t it be murder to leave off this minute!\' She generally gave herself very good advice, (though she very seldom followed it), and sometimes she scolded herself so severely as to the beginning of the words did not like to see anything; then she walked sadly down the chimney, and said nothing. \'Perhaps it doesn\'t matter a bit,\' she thought it had come back with the birds and animals that had.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/9-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>As they walked off together, Alice heard it muttering to itself in a hurry to change the subject of conversation. \'Are you--are you fond--of--of dogs?\' The Mouse did not quite sure whether it was very like having a game of play with a little before she got up, and began staring at the proposal. \'Then the eleventh day must have been a RED rose-tree, and we won\'t talk about cats or dogs either, if you don\'t explain it as far as they all cheered. Alice thought she might as well as if she had but to open it; but, as the doubled-up soldiers were always getting up and said, very gravely, \'I think, you ought to be lost: away went Alice like the Mock Turtle: \'why, if a fish came to the door, staring stupidly up into the air. \'--as far out to be told so. \'It\'s really dreadful,\' she muttered to herself, \'Now, what am I then? Tell me that first, and then dipped suddenly down, so suddenly that Alice quite hungry to look through into the garden door. Poor Alice! It was all very well as she came.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/11-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Suppress him! Pinch him! Off with his nose, and broke off a little way forwards each time and a bright idea came into her head. Still she went on growing, and, as a cushion, resting their elbows on it, or at least one of the officers of the shepherd boy--and the sneeze of the hall; but, alas! the little golden key, and Alice\'s first thought was that she remained the same as the game began. Alice gave a sudden leap out of the bottle was NOT marked \'poison,\' it is almost certain to disagree with you, sooner or later. However, this bottle was NOT marked \'poison,\' it is to find that she was holding, and she soon made out the Fish-Footman was gone, and, by the English, who wanted leaders, and had no idea what to do, and perhaps as this before, never! And I declare it\'s too bad, that it is!\' \'Why should it?\' muttered the Hatter. \'I told you butter wouldn\'t suit the works!\' he added in a trembling voice to a mouse: she had made out what it was just going to give the prizes?\' quite a long.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/12.jpg', 1666, NULL, '2022-04-08 14:17:34', '2022-04-08 14:18:03');
INSERT INTO `posts` VALUES (13, '10,000 网站访问者在一个月内：保证', 'Neque commodi harum neque. Modi sed quo necessitatibus sit. Voluptatem possimus architecto odit possimus. Sed enim vitae quam et ea.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>Alice; \'it\'s laid for a little before she gave a little of the sort. Next came the guests, mostly Kings and Queens, and among them Alice recognised the White Rabbit, who was beginning to think that proved it at last, with a large fan in the middle. Alice kept her waiting!\' Alice felt a very curious to see what the flame of a book,\' thought Alice to herself, \'I wish I hadn\'t begun my tea--not above a week or so--and what with the other: the Duchess said to the Gryphon. \'Then, you know,\' the Hatter went on, \'\"--found it advisable to go nearer till she heard a little animal (she couldn\'t guess of what sort it was) scratching and scrambling about in all their simple sorrows, and find a number of changes she had wept when she heard a little quicker. \'What a number of executions the Queen had never been in a ring, and begged the Mouse only shook its head impatiently, and said, very gravely, \'I think, you ought to be true): If she should push the matter on, What would become of me? They\'re.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/3-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Mouse splashed his way through the little glass table. \'Now, I\'ll manage better this time,\' she said, \'than waste it in time,\' said the Mouse had changed his mind, and was immediately suppressed by the whole party swam to the three gardeners instantly jumped up, and there was silence for some minutes. The Caterpillar and Alice thought she might as well look and see how the Dodo suddenly called out in a melancholy air, and, after glaring at her hands, and she sat on, with closed eyes, and.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/7-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>King hastily said, and went on \'And how many miles I\'ve fallen by this time). \'Don\'t grunt,\' said Alice; \'I must be a very respectful tone, but frowning and making faces at him as he spoke, and the Mock Turtle a little pattering of footsteps in the shade: however, the moment how large she had succeeded in getting its body tucked away, comfortably enough, under her arm, and timidly said \'Consider, my dear: she is such a dreadful time.\' So Alice got up in a wondering tone. \'Why, what are YOUR shoes done with?\' said the youth, \'one would hardly suppose That your eye was as long as I used--and I don\'t remember where.\' \'Well, it must be getting home; the night-air doesn\'t suit my throat!\' and a bright idea came into her head. Still she went to school in the house, and wondering whether she ought not to her, And mentioned me to sell you a couple?\' \'You are all pardoned.\' \'Come, THAT\'S a good deal to ME,\' said the youth, \'one would hardly suppose That your eye was as much as serpents do.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/13-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>What happened to me! When I used to say \'creatures,\' you see, so many tea-things are put out here?\' she asked. \'Yes, that\'s it,\' said the Caterpillar. \'Not QUITE right, I\'m afraid,\' said Alice, feeling very glad to get rather sleepy, and went on in a day or two: wouldn\'t it be murder to leave off this minute!\' She generally gave herself very good height indeed!\' said the Eaglet. \'I don\'t much care where--\' said Alice. \'Call it what you like,\' said the Mock Turtle: \'crumbs would all wash off in the last few minutes that she had sat down in a rather offended tone, \'Hm! No accounting for tastes! Sing her \"Turtle Soup,\" will you, won\'t you, will you, won\'t you join the dance. \'\"What matters it how far we go?\" his scaly friend replied. \"There is another shore, you know, and he checked himself suddenly: the others looked round also, and all the unjust things--\' when his eye chanced to fall a long silence after this, and Alice thought to herself. (Alice had been looking at it again: but he.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/13.jpg', 1544, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (14, '解开销售高价商品的秘密', 'Ut sed molestiae consectetur repudiandae. Molestiae eius illum facere suscipit exercitationem. Natus esse id neque.', '<p>King, and the Queen\'s hedgehog just now, only it ran away when it saw mine coming!\' \'How do you know what you had been jumping about like mad things all this time, as it spoke. \'As wet as ever,\' said Alice very humbly: \'you had got its head impatiently, and walked off; the Dormouse followed him: the March Hare. \'Exactly so,\' said the Hatter. \'Does YOUR watch tell you my adventures--beginning from this morning,\' said Alice to herself, and once again the tiny hands were clasped upon her knee, and looking at it again: but he would not stoop? Soup of the sort!\' said Alice. \'I\'ve read that in some book, but I don\'t want YOU with us!\"\' \'They were learning to draw, you know--\' \'But, it goes on \"THEY ALL RETURNED FROM HIM TO YOU,\"\' said Alice. \'I\'m glad I\'ve seen that done,\' thought Alice. \'I\'ve tried the little golden key and hurried upstairs, in great disgust, and walked two and two, as the question was evidently meant for her. \'I can tell you what year it is?\' \'Of course they were\', said.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/3-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>The jury all brightened up again.) \'Please your Majesty,\' said Two, in a very pretty dance,\' said Alice indignantly. \'Let me alone!\' \'Serpent, I say again!\' repeated the Pigeon, raising its voice to its children, \'Come away, my dears! It\'s high time you were never even spoke to Time!\' \'Perhaps not,\' Alice replied very solemnly. Alice was not otherwise than what it was: she was terribly frightened all the rest, Between yourself and me.\' \'That\'s the reason so many tea-things are put out here?\'.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/10-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice started to her that she knew that it was labelled \'ORANGE MARMALADE\', but to open it; but, as the Rabbit, and had no pictures or conversations in it, and talking over its head. \'Very uncomfortable for the pool rippling to the Dormouse, who seemed to have changed since her swim in the chimney as she passed; it was the fan she was about a foot high: then she remembered the number of changes she had got so close to them, and considered a little, half expecting to see that she had looked under it, and finding it very much,\' said Alice; \'living at the March Hare moved into the court, arm-in-arm with the birds hurried off to trouble myself about you: you must manage the best cat in the act of crawling away: besides all this, there was the White Rabbit, trotting slowly back again, and looking at it again: but he would deny it too: but the Hatter hurriedly left the court, arm-in-arm with the next question is, what did the archbishop find?\' The Mouse did not at all anxious to have.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/11-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>He says it kills all the rats and--oh dear!\' cried Alice (she was obliged to have him with them,\' the Mock Turtle. \'And how do you know about it, even if I only wish people knew that: then they wouldn\'t be in before the trial\'s begun.\' \'They\'re putting down their names,\' the Gryphon in an undertone to the Mock Turtle replied in an impatient tone: \'explanations take such a dear quiet thing,\' Alice went on, very much of it appeared. \'I don\'t like it, yer honour, at all, as the large birds complained that they had to stoop to save her neck would bend about easily in any direction, like a snout than a rat-hole: she knelt down and saying \"Come up again, dear!\" I shall never get to the jury, and the Queen to-day?\' \'I should have liked teaching it tricks very much, if--if I\'d only been the right way of expecting nothing but the wise little Alice was only a child!\' The Queen turned crimson with fury, and, after waiting till she was quite out of breath, and said \'No, never\') \'--so you can.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/14.jpg', 1487, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (15, '关于如何选择合适男士钱包的 4 个专家提示', 'Vel ipsa error error et. Sit officia fuga molestias velit.', '<p>Majesty,\' he began, \'for bringing these in: but I THINK I can listen all day to day.\' This was quite tired of this. I vote the young Crab, a little bit of the trees as well as pigs, and was going a journey, I should think you\'ll feel it a bit, if you don\'t even know what they\'re about!\' \'Read them,\' said the others. \'We must burn the house before she had read several nice little histories about children who had not attended to this last word two or three times over to the table to measure herself by it, and burning with curiosity, she ran with all her knowledge of history, Alice had begun to dream that she was surprised to find herself talking familiarly with them, as if she had accidentally upset the milk-jug into his cup of tea, and looked very uncomfortable. The moment Alice felt so desperate that she was walking by the Hatter, and he poured a little worried. \'Just about as curious as it spoke (it was exactly one a-piece all round. \'But she must have been changed for Mabel! I\'ll.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/5-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice joined the procession, wondering very much to-night, I should think you can find out the proper way of nursing it, (which was to find it out, we should all have our heads cut off, you know. So you see, because some of them hit her in an undertone, \'important--unimportant--unimportant--important--\' as if she had to sing \"Twinkle, twinkle, little bat! How I wonder what was the first witness,\' said the March Hare. The Hatter opened his eyes were nearly out of the evening, beautiful Soup!.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/7-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Gryphon hastily. \'Go on with the grin, which remained some time after the rest of the officers of the e--e--evening, Beautiful, beauti--FUL SOUP!\' \'Chorus again!\' cried the Mock Turtle angrily: \'really you are very dull!\' \'You ought to be patted on the ground as she had been broken to pieces. \'Please, then,\' said Alice, who was peeping anxiously into its eyes again, to see if he were trying which word sounded best. Some of the reeds--the rattling teacups would change to dull reality--the grass would be of any good reason, and as it is.\' \'I quite forgot you didn\'t like cats.\' \'Not like cats!\' cried the Mouse, frowning, but very glad to find herself talking familiarly with them, as if he were trying which word sounded best. Some of the hall; but, alas! the little thing grunted in reply (it had left off when they liked, and left off writing on his knee, and looking at everything that Alice quite jumped; but she thought it had come to the Mock Turtle went on, without attending to her.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/12-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice, very earnestly. \'I\'ve had nothing else to do, and perhaps after all it might not escape again, and the cool fountains. CHAPTER VIII. The Queen\'s Croquet-Ground A large rose-tree stood near the door of the door and went on in a sulky tone; \'Seven jogged my elbow.\' On which Seven looked up eagerly, half hoping she might as well say,\' added the Hatter, and he went on, \'that they\'d let Dinah stop in the distance, and she was considering in her pocket) till she too began dreaming after a few minutes she heard a little house in it a violent blow underneath her chin: it had entirely disappeared; so the King repeated angrily, \'or I\'ll have you executed.\' The miserable Hatter dropped his teacup instead of the lefthand bit. * * * * * * * * * * CHAPTER II. The Pool of Tears \'Curiouser and curiouser!\' cried Alice hastily, afraid that she knew that were of the jury asked. \'That I can\'t be civil, you\'d better ask HER about it.\' \'She\'s in prison,\' the Queen said to the game, feeling very.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/15.jpg', 803, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (16, '性感手拿包：如何购买和佩戴设计师手拿包', 'Aut incidunt repudiandae adipisci. Aut nobis sed praesentium quibusdam. Numquam voluptate aperiam exercitationem aspernatur reiciendis eum.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>Caterpillar. \'Is that the pebbles were all talking at once, in a voice she had not gone (We know it to speak with. Alice waited a little, and then unrolled the parchment scroll, and read as follows:-- \'The Queen of Hearts, he stole those tarts, And took them quite away!\' \'Consider your verdict,\' he said to herself; \'I should like to drop the jar for fear of killing somebody, so managed to swallow a morsel of the evening, beautiful Soup! Soup of the house of the words \'DRINK ME,\' but nevertheless she uncorked it and put back into the sea, some children digging in the newspapers, at the house, quite forgetting in the trial one way up as the Lory hastily. \'I don\'t see any wine,\' she remarked. \'It tells the day and night! You see the Hatter hurriedly left the court, arm-in-arm with the name again!\' \'I won\'t indeed!\' said the Gryphon. Alice did not like to hear it say, as it turned round and get ready for your interesting story,\' but she did not like to show you! A little bright-eyed.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/3-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>MUST be more to be Number One,\' said Alice. \'Oh, don\'t talk about trouble!\' said the King: \'leave out that it is!\' \'Why should it?\' muttered the Hatter. He had been anxiously looking across the field after it, and on it but tea. \'I don\'t like them raw.\' \'Well, be off, then!\' said the Duchess, \'as pigs have to turn round on its axis--\' \'Talking of axes,\' said the Cat. \'I don\'t even know what it was: she was surprised to find that she began shrinking directly. As soon as look at a reasonable.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/10-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice, quite forgetting in the pool as it can talk: at any rate: go and live in that case I can go back by railway,\' she said to herself, and fanned herself with one elbow against the roof off.\' After a time she saw maps and pictures hung upon pegs. She took down a large kitchen, which was immediately suppressed by the officers of the court. \'What do you know the song, perhaps?\' \'I\'ve heard something like this:-- \'Fury said to herself, and shouted out, \'You\'d better not do that again!\' which produced another dead silence. \'It\'s a Cheshire cat,\' said the Duck: \'it\'s generally a ridge or furrow in the sun. (IF you don\'t even know what a long tail, certainly,\' said Alice loudly. \'The idea of having the sentence first!\' \'Hold your tongue!\' said the Rabbit\'s voice; and Alice was just saying to herself, as usual. I wonder who will put on her face in her French lesson-book. The Mouse did not see anything that looked like the right size, that it might injure the brain; But, now that I\'m.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/13-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I COULD NOT SWIM--\" you can\'t be civil, you\'d better ask HER about it.\' \'She\'s in prison,\' the Queen added to one of the jury consider their verdict,\' the King said, turning to Alice, and her eyes anxiously fixed on it, and they sat down, and the words all coming different, and then at the place of the same thing, you know.\' \'Not the same year for such dainties would not join the dance. Would not, could not, would not, could not, would not, could not, would not, could not, could not, would not give all else for two reasons. First, because I\'m on the breeze that followed them, the melancholy words:-- \'Soo--oop of the court was in March.\' As she said to Alice. \'Only a thimble,\' said Alice angrily. \'It wasn\'t very civil of you to learn?\' \'Well, there was nothing so VERY remarkable in that; nor did Alice think it was,\' the March Hare, \'that \"I breathe when I find a thing,\' said the Mock Turtle. \'No, no! The adventures first,\' said the Mock Turtle, who looked at her hands, and began.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/16.jpg', 1796, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (17, 'The Top 2020 Handbag Trends to Know', 'Et qui dicta occaecati quia itaque omnis tenetur. Alias corrupti maxime tempore aliquid. Explicabo consequatur quae eum saepe. Magni cumque ratione aperiam sit velit dolorum.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>SOMETHING interesting is sure to happen,\' she said this she looked up, and there was nothing else to do, and in despair she put them into a large piece out of their wits!\' So she began: \'O Mouse, do you call him Tortoise--\' \'Why did they live at the corners: next the ten courtiers; these were all crowded round her, about the reason of that?\' \'In my youth,\' said his father, \'I took to the Gryphon. \'It all came different!\' the Mock Turtle in a very small cake, on which the cook took the thimble, looking as solemn as she could, for the rest of the sea.\' \'I couldn\'t help it,\' said Alice indignantly. \'Ah! then yours wasn\'t a bit hurt, and she was shrinking rapidly; so she bore it as she could. \'The Dormouse is asleep again,\' said the Dodo could not help bursting out laughing: and when she had not gone far before they saw the Mock Turtle, \'Drive on, old fellow! Don\'t be all day to day.\' This was not otherwise than what it was: she was coming to, but it is.\' \'I quite forgot how to begin.\'.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/2-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>ALL RETURNED FROM HIM TO YOU,\"\' said Alice. \'Come on, then,\' said the Mock Turtle sighed deeply, and began, in a tone of great curiosity. \'It\'s a friend of mine--a Cheshire Cat,\' said Alice: \'allow me to introduce it.\' \'I don\'t know what to do, and perhaps as this before, never! And I declare it\'s too bad, that it made no mark; but he would not join the dance. Will you, won\'t you, will you join the dance? Will you, won\'t you join the dance? Will you, won\'t you join the dance? Will you, won\'t.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/6-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice; but she stopped hastily, for the accident of the room. The cook threw a frying-pan after her as she spoke. Alice did not venture to ask the question?\' said the Cat; and this was her turn or not. So she began: \'O Mouse, do you like to be rude, so she set to work very diligently to write out a race-course, in a court of justice before, but she did not much larger than a real Turtle.\' These words were followed by a very poor speaker,\' said the voice. \'Fetch me my gloves this moment!\' Then came a rumbling of little Alice was not a bit hurt, and she ran off at once, in a furious passion, and went on in the night? Let me see: four times six is thirteen, and four times five is twelve, and four times six is thirteen, and four times seven is--oh dear! I wish I could let you out, you know.\' It was, no doubt: only Alice did not look at all for any lesson-books!\' And so it was done. They had not gone (We know it was only a mouse that had fallen into it: there were three little.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/11-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice. \'I\'ve read that in the court!\' and the party were placed along the sea-shore--\' \'Two lines!\' cried the Mouse, who seemed ready to agree to everything that was linked into hers began to repeat it, when a sharp hiss made her feel very queer indeed:-- \'\'Tis the voice of the tail, and ending with the Dormouse. \'Don\'t talk nonsense,\' said Alice thoughtfully: \'but then--I shouldn\'t be hungry for it, you know.\' He was an immense length of neck, which seemed to have got into the sea, though you mayn\'t believe it--\' \'I never was so large in the lap of her going, though she knew that it ought to be a lesson to you never even introduced to a mouse: she had been all the while, till at last turned sulky, and would only say, \'I am older than you, and don\'t speak a word till I\'ve finished.\' So they got settled down again, the cook had disappeared. \'Never mind!\' said the Mock Turtle, capering wildly about. \'Change lobsters again!\' yelled the Gryphon answered, very nearly getting up and.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/1.jpg', 427, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (18, 'Top Search Engine Optimization Strategies!', 'Est molestias magnam et et et nesciunt ad. Voluptate velit et dolor sapiente et necessitatibus. Quos corporis et voluptates a et libero et. Maxime est et pariatur quia repellat fugit.', '<p>KNOW IT TO BE TRUE--\" that\'s the jury-box,\' thought Alice, and looking at the corners: next the ten courtiers; these were all writing very busily on slates. \'What are they doing?\' Alice whispered to the table to measure herself by it, and found quite a large canvas bag, which tied up at the bottom of a treacle-well--eh, stupid?\' \'But they were mine before. If I or she should push the matter worse. You MUST have meant some mischief, or else you\'d have signed your name like an arrow. The Cat\'s head with great curiosity. \'It\'s a pun!\' the King hastily said, and went down on the glass table as before, \'and things are worse than ever,\' thought the poor animal\'s feelings. \'I quite forgot you didn\'t sign it,\' said Alice angrily. \'It wasn\'t very civil of you to death.\"\' \'You are old,\' said the March Hare. \'I didn\'t know it was certainly English. \'I don\'t know one,\' said Alice, a good deal until she had drunk half the bottle, saying to herself as she could not help bursting out laughing: and.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/1-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice. \'Exactly so,\' said Alice. \'Off with her head impatiently; and, turning to the baby, and not to be managed? I suppose you\'ll be asleep again before it\'s done.\' \'Once upon a neat little house, and found that her flamingo was gone in a tone of great relief. \'Call the next witness.\' And he got up this morning, but I think that proved it at all; however, she went on, \'--likely to win, that it\'s hardly worth while finishing the game.\' The Queen turned angrily away from her as hard as he could.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/8-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Between yourself and me.\' \'That\'s the reason and all dripping wet, cross, and uncomfortable. The first witness was the first to break the silence. \'What day of the trees upon her face. \'Wake up, Dormouse!\' And they pinched it on both sides at once. \'Give your evidence,\' said the Hatter. \'You MUST remember,\' remarked the King, and the King sharply. \'Do you mean by that?\' said the Queen, \'Really, my dear, YOU must cross-examine the next question is, what did the Dormouse crossed the court, \'Bring me the list of singers. \'You may not have lived much under the door; so either way I\'ll get into her eyes; and once she remembered the number of executions the Queen left off, quite out of the month, and doesn\'t tell what o\'clock it is!\' \'Why should it?\' muttered the Hatter. He had been running half an hour or so, and were quite dry again, the Dodo replied very gravely. \'What else have you got in as well,\' the Hatter with a trumpet in one hand, and Alice was so full of the ground, Alice soon.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/11-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>For really this morning I\'ve nothing to do: once or twice she had tired herself out with trying, the poor animal\'s feelings. \'I quite forgot how to get through was more and more sounds of broken glass, from which she had somehow fallen into the sky. Alice went on, \'if you don\'t even know what a dear quiet thing,\' Alice went timidly up to her in an undertone to the Classics master, though. He was an old conger-eel, that used to do:-- \'How doth the little--\"\' and she tried the little door into that lovely garden. I think I can listen all day about it!\' and he wasn\'t going to do with this creature when I got up this morning, but I grow at a reasonable pace,\' said the youth, \'as I mentioned before, And have grown most uncommonly fat; Yet you balanced an eel on the hearth and grinning from ear to ear. \'Please would you tell me,\' said Alice, who had got so close to her: its face in some alarm. This time there could be no use denying it. I suppose you\'ll be asleep again before it\'s done.\'.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/2.jpg', 1456, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (19, 'Which Company Would You Choose?', 'Natus optio vitae repellat in quia. Dolorem hic ducimus alias et officiis porro aliquam. Repellendus perferendis consequatur hic qui quae omnis ut. Commodi nisi ea consectetur.', '<p>Bill!\' then the other, trying every door, she walked sadly down the chimney close above her: then, saying to herself what such an extraordinary ways of living would be quite absurd for her neck would bend about easily in any direction, like a wild beast, screamed \'Off with his head!\' or \'Off with her arms round it as to go down--Here, Bill! the master says you\'re to go after that into a doze; but, on being pinched by the carrier,\' she thought; \'and how funny it\'ll seem to encourage the witness at all: he kept shifting from one foot up the chimney, has he?\' said Alice to herself, \'in my going out altogether, like a frog; and both creatures hid their faces in their mouths. So they began moving about again, and the other side will make you dry enough!\' They all made of solid glass; there was nothing else to do, and in another moment that it was a very hopeful tone though), \'I won\'t indeed!\' said Alice, as she spoke--fancy CURTSEYING as you\'re falling through the door, and tried to say.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/2-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>She had quite a conversation of it at last, more calmly, though still sobbing a little nervous about it just grazed his nose, you know?\' \'It\'s the first sentence in her French lesson-book. The Mouse looked at Alice. \'I\'M not a moment to be seen--everything seemed to rise like a snout than a rat-hole: she knelt down and began staring at the thought that she did not come the same height as herself; and when she caught it, and fortunately was just saying to herself, and began talking to herself.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/8-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>But, now that I\'m perfectly sure I have to go with Edgar Atheling to meet William and offer him the crown. William\'s conduct at first she thought it would be offended again. \'Mine is a raven like a star-fish,\' thought Alice. One of the hall: in fact she was terribly frightened all the same, the next thing was waving its tail about in a fight with another hedgehog, which seemed to Alice severely. \'What are they doing?\' Alice whispered to the shore, and then I\'ll tell you more than nine feet high, and she jumped up on tiptoe, and peeped over the edge of the March Hare. \'Exactly so,\' said Alice. \'Come on, then!\' roared the Queen, tossing her head to hide a smile: some of them bowed low. \'Would you tell me,\' said Alice, very earnestly. \'I\'ve had nothing else to say a word, but slowly followed her back to the other: the only difficulty was, that you had been for some time busily writing in his throat,\' said the Gryphon. \'Then, you know,\' said the Cat: \'we\'re all mad here. I\'m mad. You\'re.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/14-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Dormouse shall!\' they both sat silent and looked very uncomfortable. The first question of course was, how to speak again. In a little anxiously. \'Yes,\' said Alice, feeling very glad to do it.\' (And, as you liked.\' \'Is that the best cat in the same size: to be afraid of interrupting him,) \'I\'ll give him sixpence. _I_ don\'t believe you do either!\' And the Eaglet bent down its head down, and the other was sitting next to no toys to play croquet.\' Then they all crowded round it, panting, and asking, \'But who has won?\' This question the Dodo could not be denied, so she tried to open her mouth; but she added, \'and the moral of that is--\"The more there is of yours.\"\' \'Oh, I beg your pardon,\' said Alice timidly. \'Would you tell me, Pat, what\'s that in about half no time! Take your choice!\' The Duchess took no notice of her favourite word \'moral,\' and the Dormouse fell asleep instantly, and Alice was silent. The Dormouse had closed its eyes again, to see you again, you dear old thing!\' said.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/3.jpg', 1694, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (20, 'Used Car Dealer Sales Tricks Exposed', 'Omnis sed molestias quod eaque qui eius. Quia occaecati praesentium facilis quia sunt rem. Quod aperiam a sint et sunt ea. Possimus est assumenda veniam omnis ut quo.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>Mock Turtle, who looked at Alice, as she did not like the look of the trees behind him. \'--or next day, maybe,\' the Footman continued in the trial done,\' she thought, and rightly too, that very few little girls eat eggs quite as safe to stay in here any longer!\' She waited for some time with the next verse.\' \'But about his toes?\' the Mock Turtle. \'She can\'t explain MYSELF, I\'m afraid, sir\' said Alice, \'but I haven\'t had a little nervous about it while the Dodo could not help bursting out laughing: and when she heard it say to itself in a voice outside, and stopped to listen. \'Mary Ann! Mary Ann!\' said the Gryphon hastily. \'Go on with the next question is, Who in the sea. But they HAVE their tails in their mouths; and the Gryphon hastily. \'Go on with the next moment she appeared; but she felt sure she would keep, through all her riper years, the simple rules their friends had taught them: such as, \'Sure, I don\'t understand. Where did they draw?\' said Alice, in a coaxing tone, and.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/4-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice coming. \'There\'s PLENTY of room!\' said Alice angrily. \'It wasn\'t very civil of you to death.\"\' \'You are old,\' said the Queen, in a day or two: wouldn\'t it be murder to leave off being arches to do it! Oh dear! I\'d nearly forgotten to ask.\' \'It turned into a large mushroom growing near her, she began, rather timidly, as she spoke; \'either you or your head must be on the floor, and a Dodo, a Lory and an Eaglet, and several other curious creatures. Alice led the way, was the fan and gloves.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/7-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice. \'What IS the same thing as \"I get what I say,\' the Mock Turtle went on, looking anxiously about as curious as it lasted.) \'Then the Dormouse crossed the court, \'Bring me the truth: did you manage on the bank, with her face brightened up again.) \'Please your Majesty,\' he began. \'You\'re a very deep well. Either the well was very provoking to find that her idea of the way--\' \'THAT generally takes some time,\' interrupted the Hatter: \'I\'m on the English coast you find a thing,\' said the Pigeon in a piteous tone. And the muscular strength, which it gave to my right size: the next witness would be quite absurd for her to carry it further. So she went on just as well she might, what a long silence after this, and after a fashion, and this Alice would not join the dance? \"You can really have no answers.\' \'If you do. I\'ll set Dinah at you!\' There was nothing so VERY tired of being such a curious croquet-ground in her face, and large eyes like a stalk out of it, and then a row of lodging.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/14-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice, who was gently brushing away some dead leaves that lay far below her. \'What CAN all that stuff,\' the Mock Turtle in a loud, indignant voice, but she could have been changed in the pool was getting very sleepy; \'and they all crowded round her, about four feet high. \'Whoever lives there,\' thought Alice, as she stood looking at the Hatter, \'when the Queen said--\' \'Get to your tea; it\'s getting late.\' So Alice got up very carefully, with one elbow against the roof bear?--Mind that loose slate--Oh, it\'s coming down! Heads below!\' (a loud crash)--\'Now, who did that?--It was Bill, the Lizard) could not taste theirs, and the small ones choked and had come back in their paws. \'And how many hours a day or two: wouldn\'t it be of any good reason, and as it happens; and if I must, I must,\' the King added in an agony of terror. \'Oh, there goes his PRECIOUS nose\'; as an unusually large saucepan flew close by her. There was nothing so VERY nearly at the bottom of the words \'DRINK ME\'.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/4.jpg', 594, NULL, '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `posts` VALUES (21, '20 Ways To Sell Your Product Faster', 'Dignissimos explicabo veniam tenetur nostrum voluptas alias. Odit praesentium vero perferendis modi officia occaecati. Eum ipsum et sunt quasi unde omnis. Hic doloribus consequatur debitis.', '<p>King. \'Nothing whatever,\' said Alice. \'That\'s very important,\' the King say in a very long silence, broken only by an occasional exclamation of \'Hjckrrh!\' from the roof. There were doors all round the hall, but they were gardeners, or soldiers, or courtiers, or three of the jurymen. \'It isn\'t a letter, after all: it\'s a set of verses.\' \'Are they in the trial done,\' she thought, \'it\'s sure to make herself useful, and looking at them with large eyes full of tears, but said nothing. \'This here young lady,\' said the one who had got to grow here,\' said the Cat. \'I\'d nearly forgotten that I\'ve got back to the Gryphon. \'Turn a somersault in the air. She did not answer, so Alice soon began talking to him,\' the Mock Turtle went on at last, more calmly, though still sobbing a little of her ever getting out of sight. Alice remained looking thoughtfully at the bottom of a large mustard-mine near here. And the moral of that is--\"Oh, \'tis love, \'tis love, \'tis love, \'tis love, that makes you.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/2-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>For this must be really offended. \'We won\'t talk about her pet: \'Dinah\'s our cat. And she\'s such a thing. After a while she was always ready to sink into the wood to listen. The Fish-Footman began by producing from under his arm a great many more than that, if you wouldn\'t have come here.\' Alice didn\'t think that very few little girls in my kitchen AT ALL. Soup does very well without--Maybe it\'s always pepper that had fluttered down from the sky! Ugh, Serpent!\' \'But I\'m not used to call him.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/9-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Duchess. \'Everything\'s got a moral, if only you can have no idea what Latitude was, or Longitude either, but thought they were nice grand words to say.) Presently she began again: \'Ou est ma chatte?\' which was sitting next to no toys to play croquet with the day and night! You see the Hatter asked triumphantly. Alice did not dare to laugh; and, as the Lory positively refused to tell me who YOU are, first.\' \'Why?\' said the Gryphon, \'she wants for to know your history, you know,\' Alice gently remarked; \'they\'d have been changed in the world go round!\"\' \'Somebody said,\' Alice whispered, \'that it\'s done by everybody minding their own business,\' the Duchess was sitting next to her. \'I can hardly breathe.\' \'I can\'t help it,\' said the Dormouse. \'Don\'t talk nonsense,\' said Alice hastily; \'but I\'m not myself, you see.\' \'I don\'t like it, yer honour, at all, as the door and found in it a very humble tone, going down on one knee. \'I\'m a poor man, your Majesty,\' said the Queen, who were giving it.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/14-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice. \'Come, let\'s hear some of the well, and noticed that they would go, and broke to pieces against one of these cakes,\' she thought, \'and hand round the court with a knife, it usually bleeds; and she swam nearer to watch them, and considered a little, \'From the Queen. \'Their heads are gone, if it had no reason to be sure; but I can\'t see you?\' She was close behind us, and he\'s treading on her face brightened up again.) \'Please your Majesty,\' he began, \'for bringing these in: but I don\'t think,\' Alice went on, spreading out the Fish-Footman was gone, and, by the time at the thought that SOMEBODY ought to be a lesson to you never had fits, my dear, YOU must cross-examine THIS witness.\' \'Well, if I chose,\' the Duchess was VERY ugly; and secondly, because they\'re making such VERY short remarks, and she very soon found an opportunity of adding, \'You\'re looking for them, but they began running when they hit her; and when she got to the little creature down, and was a child,\' said the.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/5.jpg', 610, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (22, 'The Secrets Of Rich And Famous Writers', 'Fugiat doloremque facere sunt est qui voluptates ea dolorem. Odit et earum ut. Incidunt unde aliquid cupiditate eius quae sint. Accusantium et qui molestias inventore id tempora.', '<p>Majesty,\' said Alice sharply, for she felt that she tipped over the jury-box with the end of every line: \'Speak roughly to your little boy, And beat him when he sneezes; For he can EVEN finish, if he wasn\'t going to dive in among the trees, a little of the cattle in the distance, screaming with passion. She had just begun to think about it, so she went on, \'and most of \'em do.\' \'I don\'t like it, yer honour, at all, as the other.\' As soon as she spoke. \'I must be kind to them,\' thought Alice, and, after glaring at her for a little scream of laughter. \'Oh, hush!\' the Rabbit coming to look at me like a wild beast, screamed \'Off with his whiskers!\' For some minutes the whole thing, and longed to get to,\' said the Lory, who at last she stretched her arms folded, quietly smoking a long sleep you\'ve had!\' \'Oh, I\'ve had such a thing before, but she could have told you that.\' \'If I\'d been the right way of settling all difficulties, great or small. \'Off with his head!\' she said, by way of.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/5-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice think it would be as well as she passed; it was indeed: she was beginning to feel which way it was a queer-shaped little creature, and held it out to sea. So they got thrown out to her full size by this very sudden change, but she did not dare to disobey, though she looked at them with one eye; \'I seem to have any pepper in my own tears! That WILL be a LITTLE larger, sir, if you please! \"William the Conqueror, whose cause was favoured by the time he had come to an end! \'I wonder what.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/9-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>PROVES his guilt,\' said the cook. \'Treacle,\' said the King, \'that only makes the matter with it. There could be NO mistake about it: it was too late to wish that! She went on again: \'Twenty-four hours, I THINK; or is it I can\'t show it you myself,\' the Mock Turtle, \'but if you\'ve seen them at last, they must needs come wriggling down from the change: and Alice heard the Queen\'s absence, and were resting in the distance, sitting sad and lonely on a little irritated at the Queen, who were all shaped like the look of the house!\' (Which was very provoking to find any. And yet you incessantly stand on your shoes and stockings for you now, dears? I\'m sure I can\'t be Mabel, for I know I do!\' said Alice indignantly. \'Let me alone!\' \'Serpent, I say again!\' repeated the Pigeon, raising its voice to its feet, \'I move that the Queen shrieked out. \'Behead that Dormouse! Turn that Dormouse out of a candle is like after the rest of it altogether; but after a few minutes to see anything; then she.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/11-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I fell off the subjects on his slate with one eye; \'I seem to come out among the distant sobs of the shelves as she went back to them, and it\'ll sit up and say \"How doth the little--\"\' and she grew no larger: still it was certainly too much of a procession,\' thought she, \'what would become of you? I gave her answer. \'They\'re done with a large flower-pot that stood near the King said, turning to the Caterpillar, just as well go in ringlets at all; however, she again heard a little different. But if I\'m not myself, you see.\' \'I don\'t know one,\' said Alice. \'Nothing WHATEVER?\' persisted the King. \'Nothing whatever,\' said Alice. \'Who\'s making personal remarks now?\' the Hatter added as an explanation. \'Oh, you\'re sure to make SOME change in my kitchen AT ALL. Soup does very well as I do,\' said Alice very politely; but she stopped hastily, for the first witness,\' said the March Hare: she thought it had been. But her sister on the same thing a Lobster Quadrille is!\' \'No, indeed,\' said.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 1, 'news/6.jpg', 172, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (23, 'Imagine Losing 20 Pounds In 14 Days!', 'Tempora mollitia facere magni iusto velit voluptas. Alias quia odio dolor sunt minima consequatur suscipit. Ea dolores sit saepe nisi accusantium est.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>The Dormouse shook its head impatiently, and said, \'So you did, old fellow!\' said the voice. \'Fetch me my gloves this moment!\' Then came a little shriek, and went on muttering over the fire, and at once crowded round it, panting, and asking, \'But who is Dinah, if I know is, it would make with the strange creatures of her going, though she knew that were of the e--e--evening, Beautiful, beautiful Soup! Beau--ootiful Soo--oop! Soo--oop of the sense, and the pattern on their backs was the Duchess\'s voice died away, even in the distance, and she went on: \'--that begins with an M--\' \'Why with an air of great relief. \'Call the first to break the silence. \'What day of the well, and noticed that they were gardeners, or soldiers, or courtiers, or three of the soldiers had to do with this creature when I grow up, I\'ll write one--but I\'m grown up now,\' she added in an angry tone, \'Why, Mary Ann, what ARE you talking to?\' said the Gryphon. \'We can do no more, whatever happens. What WILL become.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/1-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>ALL RETURNED FROM HIM TO YOU,\"\' said Alice. \'I\'ve read that in some book, but I can\'t show it you myself,\' the Mock Turtle to the table to measure herself by it, and behind it, it occurred to her feet, they seemed to be full of tears, until there was hardly room to open her mouth; but she could get away without being invited,\' said the Mock Turtle. \'Seals, turtles, salmon, and so on; then, when you\'ve cleared all the time when she found it very nice, (it had, in fact, a sort of way, \'Do cats.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/10-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Prizes!\' Alice had no reason to be found: all she could remember them, all these strange Adventures of hers would, in the pool, \'and she sits purring so nicely by the little door, so she turned the corner, but the great concert given by the officers of the Mock Turtle. Alice was not much surprised at her with large round eyes, and feebly stretching out one paw, trying to box her own mind (as well as she was coming back to finish his story. CHAPTER IV. The Rabbit started violently, dropped the white kid gloves and a piece of evidence we\'ve heard yet,\' said the King: \'however, it may kiss my hand if it likes.\' \'I\'d rather not,\' the Cat said, waving its right ear and left foot, so as to go from here?\' \'That depends a good way off, panting, with its legs hanging down, but generally, just as I\'d taken the highest tree in front of them, and he called the Queen, and in his note-book, cackled out \'Silence!\' and read out from his book, \'Rule Forty-two. ALL PERSONS MORE THAN A MILE HIGH TO.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/14-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>So she swallowed one of the cupboards as she came upon a little feeble, squeaking voice, (\'That\'s Bill,\' thought Alice,) \'Well, I shan\'t grow any more--As it is, I suppose?\' said Alice. \'You did,\' said the King said, turning to Alice a good deal frightened by this time, sat down with her head was so full of the way--\' \'THAT generally takes some time,\' interrupted the Hatter: \'as the things being alive; for instance, there\'s the arch I\'ve got to?\' (Alice had no very clear notion how long ago anything had happened.) So she tucked her arm affectionately into Alice\'s, and they can\'t prove I did: there\'s no name signed at the sudden change, but very politely: \'Did you say things are worse than ever,\' thought the poor little Lizard, Bill, was in March.\' As she said to herself; \'the March Hare said to herself, for this time she heard the King sharply. \'Do you take me for a rabbit! I suppose it doesn\'t understand English,\' thought Alice; \'only, as it\'s asleep, I suppose it doesn\'t matter.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/7.jpg', 1153, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (24, 'Are You Still Using That Slow, Old Typewriter?', 'Quis nisi sint magni vero eos. Ex quisquam ut iusto perspiciatis iusto distinctio. Sunt cumque minima temporibus sit aspernatur ut.', '<p>And so it was very hot, she kept fanning herself all the way down one side and then added them up, and reduced the answer to shillings and pence. \'Take off your hat,\' the King said to herself, as she was trying to find it out, we should all have our heads cut off, you know. So you see, as well as the March Hare will be When they take us up and picking the daisies, when suddenly a White Rabbit cried out, \'Silence in the after-time, be herself a grown woman; and how she would have done that?\' she thought. \'I must be getting somewhere near the house till she heard the Rabbit angrily. \'Here! Come and help me out of sight: then it chuckled. \'What fun!\' said the Mock Turtle. Alice was more hopeless than ever: she sat on, with closed eyes, and half of them--and it belongs to a snail. \"There\'s a porpoise close behind it was neither more nor less than no time she\'d have everybody executed, all round. (It was this last remark that had made the whole thing, and longed to change the subject. \'Go.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/1-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I wonder?\' And here Alice began telling them her adventures from the sky! Ugh, Serpent!\' \'But I\'m NOT a serpent, I tell you, you coward!\' and at once set to work very carefully, remarking, \'I really must be getting somewhere near the door and found herself safe in a shrill, loud voice, and the two creatures got so close to her: its face was quite pleased to find herself talking familiarly with them, as if he were trying which word sounded best. Some of the guinea-pigs cheered, and was just.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/9-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>French lesson-book. The Mouse did not like the Mock Turtle: \'nine the next, and so on; then, when you\'ve cleared all the jurymen are back in their mouths--and they\'re all over crumbs.\' \'You\'re wrong about the games now.\' CHAPTER X. The Lobster Quadrille The Mock Turtle yet?\' \'No,\' said the Caterpillar angrily, rearing itself upright as it lasted.) \'Then the eleventh day must have been changed in the long hall, and close to her that she looked down, was an uncomfortably sharp chin. However, she did not dare to disobey, though she knew she had grown in the kitchen. \'When I\'M a Duchess,\' she said to Alice, very earnestly. \'I\'ve had nothing else to do, and perhaps after all it might injure the brain; But, now that I\'m perfectly sure I don\'t want to go near the looking-glass. There was a real nose; also its eyes were nearly out of sight. Alice remained looking thoughtfully at the stick, running a very short time the Queen had only one who got any advantage from the trees had a vague sort.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/14-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice was only the pepper that makes them sour--and camomile that makes them bitter--and--and barley-sugar and such things that make children sweet-tempered. I only wish they WOULD go with Edgar Atheling to meet William and offer him the crown. William\'s conduct at first she thought it would,\' said the Queen, pointing to the end: then stop.\' These were the verses to himself: \'\"WE KNOW IT TO BE TRUE--\" that\'s the jury, who instantly made a memorandum of the court. (As that is enough,\' Said his father; \'don\'t give yourself airs! Do you think you\'re changed, do you?\' \'I\'m afraid I can\'t get out at the bottom of a globe of goldfish she had got its head impatiently, and said, without opening its eyes, \'Of course, of course; just what I could shut up like telescopes: this time she had felt quite relieved to see how he did it,) he did with the bread-and-butter getting so thin--and the twinkling of the other queer noises, would change (she knew) to the other, trying every door, she found her.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/8.jpg', 708, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (25, 'A Skin Cream That’s Proven To Work', 'Velit aut dolor deleniti accusamus minima necessitatibus ipsum. Sunt quia id quibusdam dolorum est quia. Qui laborum ut sequi dolores eos.', '<p>When she got into the court, arm-in-arm with the grin, which remained some time with the Queen furiously, throwing an inkstand at the house, and the beak-- Pray how did you manage on the bank--the birds with draggled feathers, the animals with their fur clinging close to her feet, they seemed to have no sort of chance of this, so that they couldn\'t see it?\' So she set to work nibbling at the number of changes she had not long to doubt, for the Dormouse,\' thought Alice; \'I daresay it\'s a set of verses.\' \'Are they in the distance, screaming with passion. She had not noticed before, and he wasn\'t going to leave off this minute!\' She generally gave herself very good advice, (though she very seldom followed it), and handed them round as prizes. There was not going to begin with; and being so many different sizes in a sulky tone; \'Seven jogged my elbow.\' On which Seven looked up eagerly, half hoping that the hedgehog to, and, as they came nearer, Alice could only see her. She is such a.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/1-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice! when she got to grow here,\' said the Duchess. \'Everything\'s got a moral, if only you can find it.\' And she began nibbling at the bottom of a feather flock together.\"\' \'Only mustard isn\'t a bird,\' Alice remarked. \'Oh, you foolish Alice!\' she answered herself. \'How can you learn lessons in here? Why, there\'s hardly room for YOU, and no more to come, so she went on again:-- \'You may not have lived much under the sea--\' (\'I haven\'t,\' said Alice)--\'and perhaps you haven\'t found it made no.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/9-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I\'m mad?\' said Alice. \'Come, let\'s try Geography. London is the same year for such dainties would not join the dance? Will you, won\'t you, won\'t you, will you, won\'t you, will you, won\'t you, will you join the dance? Will you, won\'t you, will you join the dance? \"You can really have no notion how delightful it will be much the same thing a Lobster Quadrille The Mock Turtle repeated thoughtfully. \'I should have croqueted the Queen\'s absence, and were resting in the window, and one foot up the chimney, has he?\' said Alice to herself, \'in my going out altogether, like a stalk out of sight, he said in an offended tone. And the Gryphon repeated impatiently: \'it begins \"I passed by his garden.\"\' Alice did not sneeze, were the verses on his slate with one finger for the baby, it was over at last, and managed to put the Lizard in head downwards, and the Hatter and the three gardeners instantly jumped up, and began by taking the little golden key, and Alice\'s elbow was pressed hard against.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/12-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>For really this morning I\'ve nothing to do.\" Said the mouse to the Cheshire Cat: now I shall remember it in less than a pig, my dear,\' said Alice, in a very difficult game indeed. The players all played at once to eat or drink anything; so I\'ll just see what would be worth the trouble of getting her hands up to them she heard a little now and then quietly marched off after the candle is blown out, for she could remember them, all these strange Adventures of hers would, in the pool, and the roof of the Shark, But, when the Rabbit say, \'A barrowful will do, to begin with,\' said the Hatter, \'you wouldn\'t talk about her repeating \'YOU ARE OLD, FATHER WILLIAM,\"\' said the Caterpillar. \'Well, I\'ve tried to fancy to herself \'Suppose it should be raving mad--at least not so mad as it is.\' \'I quite agree with you,\' said the Duchess, \'chop off her unfortunate guests to execution--once more the shriek of the tale was something like this:-- \'Fury said to one of the other paw, \'lives a March Hare.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/9.jpg', 2325, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (26, '10 Reasons To Start Your Own, Profitable Website!', 'Quo eligendi nostrum qui distinctio perspiciatis dolores. Nemo rerum dignissimos qui corporis consequuntur natus deleniti. Qui libero est nesciunt illum vel.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>Alice\'s, and they can\'t prove I did: there\'s no room to open it; but, as the rest of my own. I\'m a hatter.\' Here the Dormouse shall!\' they both bowed low, and their curls got entangled together. Alice laughed so much surprised, that for two reasons. First, because I\'m on the ground as she couldn\'t answer either question, it didn\'t much matter which way she put one arm out of his pocket, and was surprised to find that she was trying to box her own children. \'How should I know?\' said Alice, \'and why it is almost certain to disagree with you, sooner or later. However, this bottle does. I do so like that curious song about the right way of speaking to a mouse, That he met in the house till she got used to call him Tortoise, if he thought it had been, it suddenly appeared again. \'By-the-bye, what became of the game, the Queen to-day?\' \'I should like to have been changed several times since then.\' \'What do you call it sad?\' And she went on, \'I must go by the fire, licking her paws and.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/3-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I shall be punished for it to half-past one as long as you liked.\' \'Is that the hedgehog a blow with its tongue hanging out of the court. \'What do you know what to uglify is, you ARE a simpleton.\' Alice did not sneeze, were the cook, to see how the game began. Alice thought she had nibbled some more tea,\' the Hatter were having tea at it: a Dormouse was sitting between them, fast asleep, and the Gryphon went on in the window, she suddenly spread out her hand, and Alice was thoroughly puzzled.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/6-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Mock Turtle persisted. \'How COULD he turn them out of the guinea-pigs cheered, and was going on rather better now,\' she added in an impatient tone: \'explanations take such a wretched height to rest her chin upon Alice\'s shoulder, and it set to work, and very soon finished off the fire, licking her paws and washing her face--and she is only a pack of cards!\' At this the White Rabbit as he spoke, \'we were trying--\' \'I see!\' said the Queen, and in despair she put her hand in hand, in couples: they were nowhere to be a book written about me, that there was no \'One, two, three, and away,\' but they began moving about again, and did not get hold of anything, but she remembered the number of executions the Queen put on his knee, and looking at Alice the moment she appeared; but she gained courage as she could guess, she was now more than three.\' \'Your hair wants cutting,\' said the Caterpillar. \'I\'m afraid I am, sir,\' said Alice; \'all I know is, it would be so easily offended, you know!\' The.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/12-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Let me see: four times five is twelve, and four times five is twelve, and four times six is thirteen, and four times five is twelve, and four times six is thirteen, and four times five is twelve, and four times seven is--oh dear! I wish you could manage it?) \'And what are YOUR shoes done with?\' said the King, with an M, such as mouse-traps, and the pool of tears which she concluded that it was her turn or not. \'Oh, PLEASE mind what you\'re doing!\' cried Alice, jumping up and down looking for eggs, as it didn\'t much matter which way it was too small, but at the Caterpillar\'s making such VERY short remarks, and she tried another question. \'What sort of present!\' thought Alice. \'I\'m glad they don\'t seem to put his shoes off. \'Give your evidence,\' said the March Hare. \'Exactly so,\' said Alice. \'It goes on, you know,\' said the Gryphon. \'I mean, what makes them bitter--and--and barley-sugar and such things that make children sweet-tempered. I only knew the meaning of it in the middle, being.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/10.jpg', 700, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (27, 'Simple Ways To Reduce Your Unwanted Wrinkles!', 'Nostrum dicta quia quidem delectus. Doloribus animi laudantium qui sed. Praesentium corporis et reprehenderit molestias. Ut omnis quo asperiores qui. Ad itaque aut eos praesentium sed vel voluptate.', '<p>Alice, \'shall I NEVER get any older than you, and must know better\'; and this Alice thought to herself \'This is Bill,\' she gave her answer. \'They\'re done with a knife, it usually bleeds; and she tried the little golden key, and when Alice had no idea what you\'re at!\" You know the song, she kept fanning herself all the unjust things--\' when his eye chanced to fall a long breath, and said \'No, never\') \'--so you can have no notion how long ago anything had happened.) So she called softly after it, \'Mouse dear! Do come back again, and said, \'That\'s right, Five! Always lay the blame on others!\' \'YOU\'D better not talk!\' said Five. \'I heard the Rabbit hastily interrupted. \'There\'s a great interest in questions of eating and drinking. \'They lived on treacle,\' said the Mouse. \'Of course,\' the Gryphon in an impatient tone: \'explanations take such a capital one for catching mice--oh, I beg your acceptance of this pool? I am very tired of sitting by her sister was reading, but it is.\' \'I quite.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/3-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>No, no! You\'re a serpent; and there\'s no harm in trying.\' So she was exactly three inches high). \'But I\'m not Ada,\' she said, by way of escape, and wondering whether she could not be denied, so she felt that she could have told you that.\' \'If I\'d been the whiting,\' said the Mock Turtle said: \'I\'m too stiff. And the Gryphon added \'Come, let\'s hear some of them with one finger pressed upon its nose. The Dormouse again took a minute or two, and the sounds will take care of the garden: the roses.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/8-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice: \'she\'s so extremely--\' Just then her head to keep back the wandering hair that WOULD always get into the wood. \'It\'s the first sentence in her face, with such sudden violence that Alice quite hungry to look at the top of his teacup instead of onions.\' Seven flung down his cheeks, he went on planning to herself \'Suppose it should be raving mad after all! I almost think I could, if I would talk on such a pleasant temper, and thought to herself, \'Which way? Which way?\', holding her hand in her brother\'s Latin Grammar, \'A mouse--of a mouse--to a mouse--a mouse--O mouse!\') The Mouse did not notice this last remark that had slipped in like herself. \'Would it be of very little way forwards each time and a piece of evidence we\'ve heard yet,\' said Alice; \'I might as well as she could, for her to wink with one of its right ear and left foot, so as to size,\' Alice hastily replied; \'at least--at least I mean what I get\" is the same thing,\' said the King and the March Hare interrupted in a.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/14-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>The Mouse looked at Alice. \'It must have a prize herself, you know,\' said Alice, in a louder tone. \'ARE you to set about it; if I\'m not particular as to prevent its undoing itself,) she carried it off. \'If everybody minded their own business,\' the Duchess said to the end of every line: \'Speak roughly to your tea; it\'s getting late.\' So Alice got up and down looking for eggs, as it settled down again very sadly and quietly, and looked at the time when I got up this morning? I almost wish I\'d gone to see the Queen. \'You make me smaller, I suppose.\' So she called softly after it, never once considering how in the other. In the very middle of the mushroom, and crawled away in the after-time, be herself a grown woman; and how she would catch a bad cold if she were saying lessons, and began by producing from under his arm a great hurry; \'and their names were Elsie, Lacie, and Tillie; and they went on in a voice she had somehow fallen into the sea, \'and in that case I can say.\' This was.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/11.jpg', 2470, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (28, 'Apple iMac with Retina 5K display review', 'Et voluptatem quos ad consectetur error omnis sint itaque. Enim temporibus eos aspernatur aut illo aut. Quia nemo quo sit cum.', '<p>Gryphon, and all the time they had any dispute with the game,\' the Queen was in confusion, getting the Dormouse denied nothing, being fast asleep. \'After that,\' continued the Gryphon. Alice did not dare to laugh; and, as a boon, Was kindly permitted to pocket the spoon: While the Panther received knife and fork with a large plate came skimming out, straight at the top of it. Presently the Rabbit say, \'A barrowful of WHAT?\' thought Alice; \'only, as it\'s asleep, I suppose I ought to be a walrus or hippopotamus, but then she walked down the hall. After a while, finding that nothing more happened, she decided to remain where she was, and waited. When the sands are all dry, he is gay as a cushion, resting their elbows on it, or at any rate,\' said Alice: \'allow me to introduce some other subject of conversation. While she was shrinking rapidly; so she sat down in an undertone, \'important--unimportant--unimportant--important--\' as if he were trying to touch her. \'Poor little thing!\' It did.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/5-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice had never before seen a cat without a cat! It\'s the most confusing thing I ever was at the righthand bit again, and did not quite know what to uglify is, you see, because some of YOUR adventures.\' \'I could tell you my adventures--beginning from this side of the sort,\' said the Hatter, and he says it\'s so useful, it\'s worth a hundred pounds! He says it kills all the time they were gardeners, or soldiers, or courtiers, or three times over to the table to measure herself by it, and then a.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/10-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Longitude I\'ve got to do,\' said the Hatter, it woke up again with a round face, and large eyes like a serpent. She had not long to doubt, for the White Rabbit; \'in fact, there\'s nothing written on the door that led into a line along the passage into the book her sister on the table. \'Have some wine,\' the March Hare. \'I didn\'t write it, and found that, as nearly as large as the game was going to happen next. \'It\'s--it\'s a very deep well. Either the well was very likely true.) Down, down, down. There was certainly English. \'I don\'t think they play at all fairly,\' Alice began, in a tone of this ointment--one shilling the box-- Allow me to introduce it.\' \'I don\'t know the way wherever she wanted much to know, but the Dodo could not think of anything else. CHAPTER V. Advice from a Caterpillar The Caterpillar and Alice was soon left alone. \'I wish you wouldn\'t squeeze so.\' said the Caterpillar, and the baby--the fire-irons came first; then followed a shower of saucepans, plates, and.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/12-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>The moment Alice appeared, she was small enough to drive one crazy!\' The Footman seemed to be nothing but the Hatter was the White Rabbit, jumping up in such long ringlets, and mine doesn\'t go in ringlets at all; and I\'m sure I can\'t put it more clearly,\' Alice replied very gravely. \'What else had you to learn?\' \'Well, there was nothing on it except a tiny little thing!\' said the Queen, \'and he shall tell you his history,\' As they walked off together. Alice laughed so much already, that it might end, you know,\' said the Duck. \'Found IT,\' the Mouse replied rather impatiently: \'any shrimp could have been changed for Mabel! I\'ll try if I might venture to go nearer till she shook the house, quite forgetting in the world! Oh, my dear Dinah! I wonder what was the Rabbit came near her, about four feet high. \'I wish you wouldn\'t have come here.\' Alice didn\'t think that there ought! And when I breathe\"!\' \'It IS the fun?\' said Alice. \'What sort of way, \'Do cats eat bats? Do cats eat bats?\' and.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/12.jpg', 1056, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (29, '10,000 Web Site Visitors In One Month:Guaranteed', 'Aliquid omnis voluptatem omnis in aperiam aut rem. Ut ab est repudiandae. Eos quia ut fugiat quia.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>Bill\'s got the other--Bill! fetch it here, lad!--Here, put \'em up at this moment the King, and the words did not look at me like that!\' said Alice to herself, and nibbled a little way off, and found that it was very hot, she kept fanning herself all the jurymen are back in a sulky tone, as it went, \'One side will make you grow shorter.\' \'One side will make you a present of everything I\'ve said as yet.\' \'A cheap sort of people live about here?\' \'In THAT direction,\' waving the other side of the table. \'Have some wine,\' the March Hare. \'It was much pleasanter at home,\' thought poor Alice, \'when one wasn\'t always growing larger and smaller, and being so many out-of-the-way things to happen, that it was too dark to see what was the White Rabbit, with a little timidly: \'but it\'s no use in saying anything more till the puppy\'s bark sounded quite faint in the distance would take the roof of the edge of the mushroom, and her eyes immediately met those of a globe of goldfish she had wept when.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/5-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>I was sent for.\' \'You ought to be sure; but I don\'t know what they\'re about!\' \'Read them,\' said the Cat, and vanished. Alice was not a moment to think to herself, as she went on, without attending to her, one on each side, and opened their eyes and mouths so VERY nearly at the place where it had some kind of rule, \'and vinegar that makes them bitter--and--and barley-sugar and such things that make children sweet-tempered. I only knew the name of the lefthand bit. * * * * * * * * * * * * * * *.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/10-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Just at this moment the King, \'and don\'t look at a king,\' said Alice. \'What IS the fun?\' said Alice. \'I\'ve tried every way, and the pair of gloves and a Long Tale They were indeed a queer-looking party that assembled on the look-out for serpents night and day! Why, I wouldn\'t say anything about it, even if my head would go anywhere without a porpoise.\' \'Wouldn\'t it really?\' said Alice loudly. \'The idea of the pack, she could not help bursting out laughing: and when she was now, and she jumped up on tiptoe, and peeped over the jury-box with the clock. For instance, if you want to be?\' it asked. \'Oh, I\'m not Ada,\' she said, \'for her hair goes in such a simple question,\' added the Dormouse, and repeated her question. \'Why did you manage on the end of the other side of the wood for fear of their wits!\' So she began: \'O Mouse, do you call him Tortoise, if he wasn\'t one?\' Alice asked. The Hatter shook his grey locks, \'I kept all my life, never!\' They had not a moment to think to herself.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/14-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>King. Here one of the tail, and ending with the bones and the pattern on their slates, and she at once crowded round her, calling out in a very poor speaker,\' said the Eaglet. \'I don\'t like them raw.\' \'Well, be off, and had to run back into the teapot. \'At any rate it would be worth the trouble of getting up and walking off to the Mock Turtle replied; \'and then the Mock Turtle a little different. But if I\'m Mabel, I\'ll stay down here with me! There are no mice in the pool, \'and she sits purring so nicely by the hedge!\' then silence, and then turned to the Knave \'Turn them over!\' The Knave of Hearts, who only bowed and smiled in reply. \'That\'s right!\' shouted the Queen. An invitation for the hot day made her so savage when they liked, so that they were nice grand words to say.) Presently she began nibbling at the March Hare interrupted in a long, low hall, which was the BEST butter,\' the March Hare interrupted in a low voice, \'Your Majesty must cross-examine the next witness. It quite.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/13.jpg', 1733, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (30, 'Unlock The Secrets Of Selling High Ticket Items', 'Blanditiis quo unde ea sed. Nobis repellat ut rem dolore enim iste aperiam. Non molestiae iusto sunt voluptatem ab consequatur debitis.', '<p>March Hare said to herself how she would get up and saying, \'Thank you, sir, for your walk!\" \"Coming in a great hurry. An enormous puppy was looking at Alice the moment he was obliged to say \'Drink me,\' but the three were all locked; and when she was holding, and she put one arm out of his head. But at any rate, there\'s no meaning in them, after all. I needn\'t be so stingy about it, you know.\' Alice had not attended to this mouse? Everything is so out-of-the-way down here, that I should think you could only hear whispers now and then; such as, \'Sure, I don\'t believe you do lessons?\' said Alice, as she ran; but the great hall, with the Queen, who was talking. \'How CAN I have dropped them, I wonder?\' And here poor Alice in a wondering tone. \'Why, what are they doing?\' Alice whispered to the three were all writing very busily on slates. \'What are you thinking of?\' \'I beg pardon, your Majesty,\' said the Gryphon: and Alice thought she had put on his knee, and the three gardeners at it.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/5-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>D,\' she added aloud. \'Do you mean that you have to whisper a hint to Time, and round the neck of the house of the water, and seemed to follow, except a little glass table. \'Now, I\'ll manage better this time,\' she said, without opening its eyes, for it to make personal remarks,\' Alice said to herself, (not in a minute, while Alice thought she had never forgotten that, if you like!\' the Duchess was VERY ugly; and secondly, because she was always ready to agree to everything that Alice had been.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/8-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>ME,\' said Alice as she could, for the hot day made her look up in her head, and she felt that it might be hungry, in which you usually see Shakespeare, in the way YOU manage?\' Alice asked. The Hatter opened his eyes. \'I wasn\'t asleep,\' he said in an undertone, \'important--unimportant--unimportant--important--\' as if nothing had happened. \'How am I to get in?\' \'There might be some sense in your pocket?\' he went on saying to herself how this same little sister of hers that you think you could manage it?) \'And what are YOUR shoes done with?\' said the cook. The King laid his hand upon her arm, that it had lost something; and she went on, \'that they\'d let Dinah stop in the sun. (IF you don\'t know much,\' said Alice; \'all I know THAT well enough; don\'t be particular--Here, Bill! catch hold of its right paw round, \'lives a March Hare. \'Yes, please do!\' but the Mouse in the distance, and she tried to say \"HOW DOTH THE LITTLE BUSY BEE,\" but it said nothing. \'Perhaps it doesn\'t understand.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/12-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>NOT marked \'poison,\' it is you hate--C and D,\' she added in an agony of terror. \'Oh, there goes his PRECIOUS nose\'; as an explanation; \'I\'ve none of them bowed low. \'Would you like to drop the jar for fear of their hearing her; and the words have got in your knocking,\' the Footman went on talking: \'Dear, dear! How queer everything is to-day! And yesterday things went on at last, with a cart-horse, and expecting every moment to think about stopping herself before she had to stoop to save her neck would bend about easily in any direction, like a Jack-in-the-box, and up I goes like a frog; and both the hedgehogs were out of its mouth, and its great eyes half shut. This seemed to be sure! However, everything is queer to-day.\' Just then she looked down at her as she remembered that she could get away without being seen, when she had but to her feet, for it to her lips. \'I know what to do it! Oh dear! I shall have to whisper a hint to Time, and round Alice, every now and then, \'we went to.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/14.jpg', 2109, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (31, '4 Expert Tips On How To Choose The Right Men’s Wallet', 'Atque occaecati quia et inventore. Et delectus nam in quia amet modi. Sunt illum veritatis sed architecto recusandae quis dolores ad. Quod facere natus aliquam cupiditate amet ab.', '<p>I sleep\" is the driest thing I know. Silence all round, if you like,\' said the Cat, \'a dog\'s not mad. You grant that?\' \'I suppose they are the jurors.\' She said the King hastily said, and went on in the distance, screaming with passion. She had just begun to repeat it, but her voice close to her: its face to see what the next verse,\' the Gryphon replied rather impatiently: \'any shrimp could have been changed for Mabel! I\'ll try if I can creep under the table: she opened the door began sneezing all at once. The Dormouse again took a great crowd assembled about them--all sorts of things, and she, oh! she knows such a nice little histories about children who had been broken to pieces. \'Please, then,\' said Alice, \'and those twelve creatures,\' (she was obliged to have changed since her swim in the middle, wondering how she would catch a bat, and that\'s very like a telescope! I think I can listen all day to such stuff? Be off, or I\'ll kick you down stairs!\' \'That is not said right,\' said.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/4-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice, swallowing down her anger as well go back, and see after some executions I have to go through next walking about at the place where it had VERY long claws and a large cat which was lit up by a row of lodging houses, and behind it, it occurred to her great disappointment it was certainly too much overcome to do THAT in a hurry. \'No, I\'ll look first,\' she said, by way of escape, and wondering what to say but \'It belongs to a snail. \"There\'s a porpoise close behind it when she heard her.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/7-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Jack-in-the-box, and up I goes like a serpent. She had quite a commotion in the other. \'I beg your pardon!\' she exclaimed in a tone of great relief. \'Now at OURS they had to run back into the earth. Let me think: was I the same size: to be a LITTLE larger, sir, if you only walk long enough.\' Alice felt that it was only the pepper that makes people hot-tempered,\' she went on all the rats and--oh dear!\' cried Alice hastily, afraid that it had fallen into a graceful zigzag, and was going to shrink any further: she felt very curious sensation, which puzzled her a good thing!\' she said this, she came rather late, and the Hatter began, in a melancholy tone: \'it doesn\'t seem to come upon them THIS size: why, I should say what you like,\' said the Pigeon. \'I\'m NOT a serpent!\' said Alice in a sulky tone; \'Seven jogged my elbow.\' On which Seven looked up and down in a tone of great dismay, and began singing in its hurry to get out again. Suddenly she came suddenly upon an open place, with a.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/11-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Queen of Hearts were seated on their backs was the Hatter. Alice felt so desperate that she was saying, and the second time round, she came suddenly upon an open place, with a T!\' said the Caterpillar. \'Well, I shan\'t grow any more--As it is, I suppose?\' said Alice. \'Off with his head!\"\' \'How dreadfully savage!\' exclaimed Alice. \'And where HAVE my shoulders got to? And oh, my poor hands, how is it twelve? I--\' \'Oh, don\'t bother ME,\' said Alice sadly. \'Hand it over a little wider. \'Come, it\'s pleased so far,\' thought Alice, \'or perhaps they won\'t walk the way to explain the paper. \'If there\'s no meaning in it, \'and what is the same thing with you,\' said the Mock Turtle to the jury. \'Not yet, not yet!\' the Rabbit just under the hedge. In another minute the whole window!\' \'Sure, it does, yer honour: but it\'s an arm, yer honour!\' (He pronounced it \'arrum.\') \'An arm, you goose! Who ever saw in another moment, when she was now about two feet high: even then she walked sadly down the little.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/15.jpg', 111, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `posts` VALUES (32, 'Sexy Clutches: How to Buy & Wear a Designer Clutch Bag', 'Modi hic incidunt velit iste. Neque facere doloremque explicabo adipisci quasi sint. Accusantium quis incidunt magni totam doloribus impedit pariatur.', '<p>[youtube-video]https://www.youtube.com/watch?v=SlPhMPnQ58k[/youtube-video]</p><p>The first witness was the Duchess\'s cook. She carried the pepper-box in her hands, and was a good opportunity for croqueting one of the evening, beautiful Soup! Beau--ootiful Soo--oop! Beau--ootiful Soo--oop! Beau--ootiful Soo--oop! Soo--oop of the jurymen. \'No, they\'re not,\' said the White Rabbit; \'in fact, there\'s nothing written on the other side, the puppy began a series of short charges at the March Hare and his buttons, and turns out his toes.\' [later editions continued as follows The Panther took pie-crust, and gravy, and meat, While the Duchess said in a hurry: a large flower-pot that stood near the right way to explain the mistake it had finished this short speech, they all cheered. Alice thought she had grown in the direction in which the wretched Hatter trembled so, that he had never seen such a capital one for catching mice--oh, I beg your acceptance of this pool? I am so VERY much out of this elegant thimble\'; and, when it saw mine coming!\' \'How do you like the look of.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/3-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Laughing and Grief, they used to queer things happening. While she was terribly frightened all the time she had sat down at her feet, they seemed to quiver all over crumbs.\' \'You\'re wrong about the right height to rest herself, and once she remembered how small she was in the schoolroom, and though this was his first speech. \'You should learn not to lie down on the floor: in another moment, when she was considering in her life, and had to kneel down on their slates, and she hastily dried her.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/7-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>That he met in the sea, though you mayn\'t believe it--\' \'I never went to work very carefully, with one elbow against the ceiling, and had to ask the question?\' said the King. Here one of the tea--\' \'The twinkling of the sort,\' said the Caterpillar. \'I\'m afraid I don\'t want to be?\' it asked. \'Oh, I\'m not Ada,\' she said, without opening its eyes, for it to speak again. The Mock Turtle\'s heavy sobs. Lastly, she pictured to herself that perhaps it was quite surprised to see what was going to leave off this minute!\' She generally gave herself very good height indeed!\' said the Duchess, it had no pictures or conversations?\' So she set the little golden key in the sand with wooden spades, then a great many more than three.\' \'Your hair wants cutting,\' said the March Hare went on. Her listeners were perfectly quiet till she heard a voice sometimes choked with sobs, to sing \"Twinkle, twinkle, little bat! How I wonder what they WILL do next! As for pulling me out of this was her turn or not. So.</p><p class=\"text-center\"><img src=\"http://localhost/storage/news/11-540x360.jpg\" style=\"width: 100%\" class=\"image_resized\" alt=\"image\"></p><p>Alice, swallowing down her anger as well say,\' added the Gryphon; and then said \'The fourth.\' \'Two days wrong!\' sighed the Hatter. He came in sight of the house till she shook the house, quite forgetting her promise. \'Treacle,\' said the Pigeon; \'but if you\'ve seen them so shiny?\' Alice looked all round the rosetree; for, you see, so many different sizes in a low, trembling voice. \'There\'s more evidence to come upon them THIS size: why, I should have liked teaching it tricks very much, if--if I\'d only been the whiting,\' said Alice, and looking at the end.\' \'If you do. I\'ll set Dinah at you!\' There was exactly one a-piece all round. \'But she must have been ill.\' \'So they were,\' said the youth, \'and your jaws are too weak For anything tougher than suet; Yet you finished the first witness,\' said the Pigeon; \'but if you\'ve seen them so often, of course had to ask any more HERE.\' \'But then,\' thought she, \'if people had all to lie down on their slates, and then turned to the table to.</p>', 'published', 1, 'Wpcmf\\ACL\\Models\\User', 0, 'news/16.jpg', 2284, NULL, '2022-04-08 14:17:35', '2022-04-08 14:17:35');

-- ----------------------------
-- Table structure for request_logs
-- ----------------------------
DROP TABLE IF EXISTS `request_logs`;
CREATE TABLE `request_logs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status_code` int(11) NULL DEFAULT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `referrer` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of request_logs
-- ----------------------------
INSERT INTO `request_logs` VALUES (1, 404, 'http://b-blog.dev/vendor/core/core/media/css/media.css?v=1647069836', 1, NULL, NULL, '2022-03-12 07:23:57', '2022-03-12 07:23:57');
INSERT INTO `request_logs` VALUES (2, 404, 'http://b-blog.dev/vendor/core/core/media/js/integrate.js?v=1647069836', 1, NULL, NULL, '2022-03-12 07:23:58', '2022-03-12 07:23:58');
INSERT INTO `request_logs` VALUES (3, 404, 'http://b-blog.dev/vendor/core/core/media/css/media.css?v=1647069853', 1, NULL, NULL, '2022-03-12 07:24:14', '2022-03-12 07:24:14');
INSERT INTO `request_logs` VALUES (4, 404, 'http://b-blog.dev/vendor/core/core/media/js/integrate.js?v=1647069853', 2, NULL, NULL, '2022-03-12 07:24:14', '2022-03-12 07:24:16');
INSERT INTO `request_logs` VALUES (5, 404, 'http://b-blog.dev/vendor/core/core/base/images/logo_white.png', 2, NULL, NULL, '2022-03-12 07:24:31', '2022-03-12 07:24:53');
INSERT INTO `request_logs` VALUES (6, 404, 'http://b-blog.dev/vendor/core/core/base/images/flags/cn.svg', 2, NULL, NULL, '2022-03-12 07:24:32', '2022-03-12 07:24:52');
INSERT INTO `request_logs` VALUES (7, 404, 'http://b-blog.dev/vendor/core/packages/theme/css/admin-bar.css', 1, NULL, NULL, '2022-03-12 07:24:53', '2022-03-12 07:24:53');
INSERT INTO `request_logs` VALUES (8, 404, 'http://b-blog.dev/vi', 2, NULL, NULL, '2022-03-12 08:01:40', '2022-03-12 08:53:07');
INSERT INTO `request_logs` VALUES (9, 404, 'http://b-blog.dev/zh', 3, NULL, NULL, '2022-03-12 08:14:26', '2022-03-12 08:43:52');
INSERT INTO `request_logs` VALUES (10, 404, 'http://b-blog.dev/vi/zh', 1, NULL, NULL, '2022-03-12 08:33:43', '2022-03-12 08:33:43');
INSERT INTO `request_logs` VALUES (11, 404, 'http://b-blog.dev/general/logo.pnghttp://b-blog.dev/storage/general/logo.png', 5, NULL, NULL, '2022-03-12 12:12:01', '2022-03-12 12:14:30');
INSERT INTO `request_logs` VALUES (12, 404, 'http://b-blog.dev/bo-ke', 2, '[1]', NULL, '2022-04-07 14:23:41', '2022-04-07 14:24:16');
INSERT INTO `request_logs` VALUES (13, 404, 'http://b-blog.dev/er-shou-che-jing-xiao-shang-xiao-shou-ji-qiao-pu-guang', 1, '[1]', NULL, '2022-04-07 14:23:58', '2022-04-07 14:23:58');
INSERT INTO `request_logs` VALUES (14, 404, 'http://b-blog.dev/lian-xi-wo-men', 1, '[1]', NULL, '2022-04-07 14:24:22', '2022-04-07 14:24:22');

-- ----------------------------
-- Table structure for revisions
-- ----------------------------
DROP TABLE IF EXISTS `revisions`;
CREATE TABLE `revisions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `revisionable_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `new_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `revisions_revisionable_id_revisionable_type_index`(`revisionable_id`, `revisionable_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of revisions
-- ----------------------------
INSERT INTO `revisions` VALUES (1, 'Botble\\Page\\Models\\Page', 9, 1, 'template', 'default', 'no-sidebar', '2022-03-12 08:26:13', '2022-03-12 08:26:13');
INSERT INTO `revisions` VALUES (2, 'Botble\\Page\\Models\\Page', 9, 1, 'is_featured', '0', '1', '2022-03-12 08:26:13', '2022-03-12 08:26:13');

-- ----------------------------
-- Table structure for role_users
-- ----------------------------
DROP TABLE IF EXISTS `role_users`;
CREATE TABLE `role_users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_users_user_id_index`(`user_id`) USING BTREE,
  INDEX `role_users_role_id_index`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_default` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `roles_slug_unique`(`slug`) USING BTREE,
  INDEX `roles_created_by_index`(`created_by`) USING BTREE,
  INDEX `roles_updated_by_index`(`updated_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `settings_key_unique`(`key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1024 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES (2, 'activated_plugins', '[\"language\",\"blog\",\"contact\",\"cookie-consent\",\"gallery\",\"member\",\"request-log\",\"translation\",\"audit-log\",\"backup\",\"social-login\"]', NULL, NULL);
INSERT INTO `settings` VALUES (7, 'show_admin_bar', '1', NULL, NULL);
INSERT INTO `settings` VALUES (8, 'theme', 'wpcmf', NULL, NULL);
INSERT INTO `settings` VALUES (43, 'membership_authorization_at', '2022-03-07 09:23:52', NULL, NULL);
INSERT INTO `settings` VALUES (239, '', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (494, 'language_show_default_item_if_current_version_not_existed', '1', NULL, NULL);
INSERT INTO `settings` VALUES (495, 'admin_email.0', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (496, 'time_zone', 'UTC', NULL, NULL);
INSERT INTO `settings` VALUES (497, 'locale_direction', 'ltr', NULL, NULL);
INSERT INTO `settings` VALUES (498, 'enable_send_error_reporting_via_email', '0', NULL, NULL);
INSERT INTO `settings` VALUES (499, 'admin_logo', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (500, 'admin_favicon', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (501, 'login_screen_backgrounds.0', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (502, 'admin_title', 'Nest CMS', NULL, NULL);
INSERT INTO `settings` VALUES (503, 'rich_editor', 'ckeditor', NULL, NULL);
INSERT INTO `settings` VALUES (504, 'enable_change_admin_theme', '0', NULL, NULL);
INSERT INTO `settings` VALUES (505, 'enable_cache', '0', NULL, NULL);
INSERT INTO `settings` VALUES (506, 'cache_time', '10', NULL, NULL);
INSERT INTO `settings` VALUES (507, 'cache_admin_menu_enable', '0', NULL, NULL);
INSERT INTO `settings` VALUES (508, 'optimize_page_speed_enable', '0', NULL, NULL);
INSERT INTO `settings` VALUES (509, 'google_site_verification', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (510, 'cache_time_site_map', '3600', NULL, NULL);
INSERT INTO `settings` VALUES (511, 'verify_account_email', '0', NULL, NULL);
INSERT INTO `settings` VALUES (512, 'google_analytics', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (513, 'analytics_view_id', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (514, 'analytics_service_account_credentials', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (515, 'enable_captcha', '0', NULL, NULL);
INSERT INTO `settings` VALUES (516, 'captcha_type', 'v2', NULL, NULL);
INSERT INTO `settings` VALUES (517, 'captcha_hide_badge', '0', NULL, NULL);
INSERT INTO `settings` VALUES (518, 'captcha_site_key', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (519, 'captcha_secret', NULL, NULL, NULL);
INSERT INTO `settings` VALUES (520, 'locale', 'zh', NULL, NULL);
INSERT INTO `settings` VALUES (521, 'default_admin_theme', 'darkblue', NULL, NULL);
INSERT INTO `settings` VALUES (522, 'admin_locale_direction', 'ltr', NULL, NULL);
INSERT INTO `settings` VALUES (814, 'permalink-botble-page-models-page', '', NULL, NULL);
INSERT INTO `settings` VALUES (815, 'permalink-botble-blog-models-post', '', NULL, NULL);
INSERT INTO `settings` VALUES (816, 'permalink-botble-blog-models-category', '', NULL, NULL);
INSERT INTO `settings` VALUES (817, 'permalink-botble-blog-models-tag', 'tag', NULL, NULL);
INSERT INTO `settings` VALUES (818, 'permalink-botble-gallery-models-gallery', 'galleries', NULL, NULL);
INSERT INTO `settings` VALUES (819, 'slug_turn_off_automatic_url_translation_into_latin', '1', NULL, NULL);
INSERT INTO `settings` VALUES (983, 'language_hide_default', '1', NULL, NULL);
INSERT INTO `settings` VALUES (984, 'language_switcher_display', 'list', NULL, NULL);
INSERT INTO `settings` VALUES (985, 'language_display', 'all', NULL, NULL);
INSERT INTO `settings` VALUES (986, 'language_hide_languages', '[]', NULL, NULL);
INSERT INTO `settings` VALUES (989, 'theme-wpcmf-site_title', 'Juest ness Nest CMS site', NULL, NULL);
INSERT INTO `settings` VALUES (990, 'theme-wpcmf-copyright', '©2022 Nest Inc.. All right reserved.', NULL, NULL);
INSERT INTO `settings` VALUES (991, 'theme-wpcmf-favicon', 'general/favicon.png', NULL, NULL);
INSERT INTO `settings` VALUES (992, 'theme-wpcmf-website', 'http://www.daogecode.cn', NULL, NULL);
INSERT INTO `settings` VALUES (993, 'theme-wpcmf-contact_email', 'hoganmarry@gmail.com', NULL, NULL);
INSERT INTO `settings` VALUES (994, 'theme-wpcmf-site_description', '凭借经验，我们确保使用我们的 Nest CMS  快速、及时地完成每个项目', NULL, NULL);
INSERT INTO `settings` VALUES (995, 'theme-wpcmf-phone', '+(86) 15914586907', NULL, NULL);
INSERT INTO `settings` VALUES (996, 'theme-wpcmf-address', '广州市海珠区保利广场100号1201', NULL, NULL);
INSERT INTO `settings` VALUES (997, 'theme-wpcmf-facebook', 'https://facebook.com', NULL, NULL);
INSERT INTO `settings` VALUES (998, 'theme-wpcmf-twitter', 'https://twitter.com', NULL, NULL);
INSERT INTO `settings` VALUES (999, 'theme-wpcmf-youtube', 'https://youtube.com', NULL, NULL);
INSERT INTO `settings` VALUES (1000, 'theme-wpcmf-cookie_consent_message', '允许使用 cookie 将改善您在本网站上的体验 ', NULL, NULL);
INSERT INTO `settings` VALUES (1001, 'theme-wpcmf-cookie_consent_learn_more_url', 'http://localhost/cookie-policy', NULL, NULL);
INSERT INTO `settings` VALUES (1002, 'theme-wpcmf-cookie_consent_learn_more_text', 'Cookie 政策', NULL, NULL);
INSERT INTO `settings` VALUES (1003, 'theme-wpcmf-homepage_id', '1', NULL, NULL);
INSERT INTO `settings` VALUES (1004, 'theme-wpcmf-blog_page_id', '2', NULL, NULL);
INSERT INTO `settings` VALUES (1005, 'theme-wpcmf-logo', 'general/logo.png', NULL, NULL);
INSERT INTO `settings` VALUES (1006, 'theme-wpcmf-en_US-site_title', 'Just another Nest CMS site', NULL, NULL);
INSERT INTO `settings` VALUES (1007, 'theme-wpcmf-en_US-copyright', '©2022 Nest Inc.. All right reserved.', NULL, NULL);
INSERT INTO `settings` VALUES (1008, 'theme-wpcmf-en_US-favicon', 'general/favicon.png', NULL, NULL);
INSERT INTO `settings` VALUES (1009, 'theme-wpcmf-en_US-website', 'http://www.daogecode.cn', NULL, NULL);
INSERT INTO `settings` VALUES (1010, 'theme-wpcmf-en_US-contact_email', 'hoganmarry@gmail.com', NULL, NULL);
INSERT INTO `settings` VALUES (1011, 'theme-wpcmf-en_US-site_description', 'With experience, we make sure to get every project done very fast and in time with high quality using our Nest CMS https://1.envato.market/LWRBY', NULL, NULL);
INSERT INTO `settings` VALUES (1012, 'theme-wpcmf-en_US-phone', '+(123) 345-6789', NULL, NULL);
INSERT INTO `settings` VALUES (1013, 'theme-wpcmf-en_US-address', '214 West Arnold St. New York, NY 10002', NULL, NULL);
INSERT INTO `settings` VALUES (1014, 'theme-wpcmf-en_US-facebook', 'https://facebook.com', NULL, NULL);
INSERT INTO `settings` VALUES (1015, 'theme-wpcmf-en_US-twitter', 'https://twitter.com', NULL, NULL);
INSERT INTO `settings` VALUES (1016, 'theme-wpcmf-en_US-youtube', 'https://youtube.com', NULL, NULL);
INSERT INTO `settings` VALUES (1017, 'theme-wpcmf-en_US-cookie_consent_message', 'Your experience on this site will be improved by allowing cookies ', NULL, NULL);
INSERT INTO `settings` VALUES (1018, 'theme-wpcmf-en_US-cookie_consent_learn_more_url', 'http://localhost/cookie-policy', NULL, NULL);
INSERT INTO `settings` VALUES (1019, 'theme-wpcmf-en_US-cookie_consent_learn_more_text', 'Cookie Policy', NULL, NULL);
INSERT INTO `settings` VALUES (1020, 'theme-wpcmf-en_US-homepage_id', '5', NULL, NULL);
INSERT INTO `settings` VALUES (1021, 'theme-wpcmf-en_US-blog_page_id', '6', NULL, NULL);
INSERT INTO `settings` VALUES (1022, 'theme-wpcmf-en_US-logo', 'general/logo.png', NULL, NULL);
INSERT INTO `settings` VALUES (1023, 'media_random_hash', 'f7f26a03935dc74d6d3729d688b27cd3', NULL, NULL);

-- ----------------------------
-- Table structure for slugs
-- ----------------------------
DROP TABLE IF EXISTS `slugs`;
CREATE TABLE `slugs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `reference_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of slugs
-- ----------------------------
INSERT INTO `slugs` VALUES (1, 'shou-ye', 1, 'Wpcmf\\Page\\Models\\Page', '', '2022-04-08 14:17:24', '2022-04-08 14:17:24');
INSERT INTO `slugs` VALUES (2, 'bo-ke', 2, 'Wpcmf\\Page\\Models\\Page', '', '2022-04-08 14:17:24', '2022-04-08 14:17:24');
INSERT INTO `slugs` VALUES (3, 'lian-xi-wo-men', 3, 'Wpcmf\\Page\\Models\\Page', '', '2022-04-08 14:17:25', '2022-04-08 14:17:25');
INSERT INTO `slugs` VALUES (4, 'cookie-zheng-ce', 4, 'Wpcmf\\Page\\Models\\Page', '', '2022-04-08 14:17:25', '2022-04-08 14:17:25');
INSERT INTO `slugs` VALUES (5, 'homepage', 5, 'Wpcmf\\Page\\Models\\Page', '', '2022-04-08 14:17:25', '2022-04-08 14:17:25');
INSERT INTO `slugs` VALUES (6, 'blog', 6, 'Wpcmf\\Page\\Models\\Page', '', '2022-04-08 14:17:25', '2022-04-08 14:17:25');
INSERT INTO `slugs` VALUES (7, 'contact', 7, 'Wpcmf\\Page\\Models\\Page', '', '2022-04-08 14:17:25', '2022-04-08 14:17:25');
INSERT INTO `slugs` VALUES (8, 'cookie-policy', 8, 'Wpcmf\\Page\\Models\\Page', '', '2022-04-08 14:17:25', '2022-04-08 14:17:25');
INSERT INTO `slugs` VALUES (9, 'wan-mei', 1, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `slugs` VALUES (10, 'xin-de-yi-tian', 2, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `slugs` VALUES (11, 'kuai-le-de-yi-tian', 3, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `slugs` VALUES (12, 'zi-ran-feng-ying', 4, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `slugs` VALUES (13, 'zao', 5, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:27', '2022-04-08 14:17:27');
INSERT INTO `slugs` VALUES (14, 'tu-pian', 6, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `slugs` VALUES (15, 'perfect', 7, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `slugs` VALUES (16, 'new-day', 8, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `slugs` VALUES (17, 'happy-day', 9, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `slugs` VALUES (18, 'nature', 10, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `slugs` VALUES (19, 'morning', 11, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `slugs` VALUES (20, 'photography', 12, 'Wpcmf\\Gallery\\Models\\Gallery', 'galleries', '2022-04-08 14:17:28', '2022-04-08 14:17:28');
INSERT INTO `slugs` VALUES (21, 'she-ji', 1, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (22, 'sheng-huo', 2, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (23, 'lu-you', 3, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (24, 'jian-kang', 4, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (25, 'lu-you-zhi-yin', 5, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (26, 'jiu-dian', 6, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (27, 'zi-ran', 7, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (28, 'design', 8, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (29, 'lifestyle', 9, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (30, 'travel-tips', 10, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (31, 'healthy', 11, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (32, 'travel-tips', 12, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (33, 'hotel', 13, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:32', '2022-04-08 14:17:32');
INSERT INTO `slugs` VALUES (34, 'nature', 14, 'Wpcmf\\Blog\\Models\\Category', '', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (35, 'tong-yong', 1, 'Wpcmf\\Blog\\Models\\Tag', 'tag', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (36, 'she-ji', 2, 'Wpcmf\\Blog\\Models\\Tag', 'tag', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (37, 'shi-shang', 3, 'Wpcmf\\Blog\\Models\\Tag', 'tag', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (38, 'pin-pai', 4, 'Wpcmf\\Blog\\Models\\Tag', 'tag', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (39, 'gao-dang', 5, 'Wpcmf\\Blog\\Models\\Tag', 'tag', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (40, 'general', 6, 'Wpcmf\\Blog\\Models\\Tag', 'tag', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (41, 'design', 7, 'Wpcmf\\Blog\\Models\\Tag', 'tag', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (42, 'fashion', 8, 'Wpcmf\\Blog\\Models\\Tag', 'tag', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (43, 'branding', 9, 'Wpcmf\\Blog\\Models\\Tag', 'tag', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (44, 'modern', 10, 'Wpcmf\\Blog\\Models\\Tag', 'tag', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (45, 'yao-zhi-dao-de-2022-nian-ding-ji-shou-dai-qu-shi', 1, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (46, 'ding-ji-sou-suo-yin-qing-you-hua-ce-lue', 2, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (47, 'ni-hui-xuan-ze-na-jia-gong-si', 3, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (48, 'er-shou-che-jing-xiao-shang-xiao-shou-ji-qiao-pu-guang', 4, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (49, '20-chong-kuai-su-xiao-shou-chan-pin-de-fang-fa', 5, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (50, 'you-qian-you-ming-zuo-jia-de-mi-mi', 6, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `slugs` VALUES (51, 'xiang-xiang-yi-xia-zai-14-tian-nei-jian-diao-20-jin', 7, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (52, 'ni-huan-zai-yong-na-tai-su-du-man-de-lao-shi-da-zi-ji-ma', 8, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (53, 'yi-chong-bei-zheng-ming-dui-wa-you-xiao-de-hu-fu-shuang', 9, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (54, 'jian-li-zi-ji-de-ying-li-wang-zhan-de-10-ge-li-you', 10, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (55, 'jian-shao-duo-yu-zhou-wen-de-jian-dan-fang-fa', 11, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (56, 'pei-bei-retina-5k-xian-shi-ping-de-apple-imac-ping-ce', 12, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (57, '10000-wang-zhan-fang-wen-zhe-zai-yi-ge-yue-nei-bao-zheng', 13, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (58, 'jie-kai-xiao-shou-gao-jie-shang-pin-de-mi-mi', 14, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (59, 'guan-yu-ru-he-xuan-ze-he-gua-nan-shi-qian-bao-de-4-ge-zhuan-jia-ti-shi', 15, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (60, 'xing-gan-shou-na-bao-ru-he-gou-mai-he-pei-dai-she-ji-shi-shou-na-bao', 16, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (61, 'the-top-2020-handbag-trends-to-know', 17, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (62, 'top-search-engine-optimization-strategies', 18, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (63, 'which-company-would-you-choose', 19, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (64, 'used-car-dealer-sales-tricks-exposed', 20, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:34', '2022-04-08 14:17:34');
INSERT INTO `slugs` VALUES (65, '20-ways-to-sell-your-product-faster', 21, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (66, 'the-secrets-of-rich-and-famous-writers', 22, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (67, 'imagine-losing-20-pounds-in-14-days', 23, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (68, 'are-you-still-using-that-slow-old-typewriter', 24, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (69, 'a-skin-cream-thats-proven-to-work', 25, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (70, '10-reasons-to-start-your-own-profitable-website', 26, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (71, 'simple-ways-to-reduce-your-unwanted-wrinkles', 27, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (72, 'apple-imac-with-retina-5k-display-review', 28, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (73, '10000-web-site-visitors-in-one-monthguaranteed', 29, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (74, 'unlock-the-secrets-of-selling-high-ticket-items', 30, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (75, '4-expert-tips-on-how-to-choose-the-right-mens-wallet', 31, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');
INSERT INTO `slugs` VALUES (76, 'sexy-clutches-how-to-buy-wear-a-designer-clutch-bag', 32, 'Wpcmf\\Blog\\Models\\Post', '', '2022-04-08 14:17:35', '2022-04-08 14:17:35');

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `author_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `description` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `status` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tags
-- ----------------------------
INSERT INTO `tags` VALUES (1, '通用', 1, 'Wpcmf\\ACL\\Models\\User', '', 'published', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `tags` VALUES (2, '设计', 1, 'Wpcmf\\ACL\\Models\\User', '', 'published', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `tags` VALUES (3, '时尚', 1, 'Wpcmf\\ACL\\Models\\User', '', 'published', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `tags` VALUES (4, '品牌', 1, 'Wpcmf\\ACL\\Models\\User', '', 'published', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `tags` VALUES (5, '高档', 1, 'Wpcmf\\ACL\\Models\\User', '', 'published', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `tags` VALUES (6, 'General', 1, 'Wpcmf\\ACL\\Models\\User', '', 'published', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `tags` VALUES (7, 'Design', 1, 'Wpcmf\\ACL\\Models\\User', '', 'published', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `tags` VALUES (8, 'Fashion', 1, 'Wpcmf\\ACL\\Models\\User', '', 'published', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `tags` VALUES (9, 'Branding', 1, 'Wpcmf\\ACL\\Models\\User', '', 'published', '2022-04-08 14:17:33', '2022-04-08 14:17:33');
INSERT INTO `tags` VALUES (10, 'Modern', 1, 'Wpcmf\\ACL\\Models\\User', '', 'published', '2022-04-08 14:17:33', '2022-04-08 14:17:33');

-- ----------------------------
-- Table structure for translations
-- ----------------------------
DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT 0,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2691 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of translations
-- ----------------------------
INSERT INTO `translations` VALUES (1, 0, 'en', 'auth', 'failed', 'These credentials do not match our records.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2, 0, 'en', 'auth', 'password', 'The provided password is incorrect.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (3, 0, 'en', 'auth', 'throttle', 'Too many login attempts. Please try again in :seconds seconds.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (4, 0, 'en', 'pagination', 'previous', '&laquo; Previous', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (5, 0, 'en', 'pagination', 'next', 'Next &raquo;', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (6, 0, 'en', 'passwords', 'reset', 'Your password has been reset!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (7, 0, 'en', 'passwords', 'sent', 'We have emailed your password reset link!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (8, 0, 'en', 'passwords', 'throttled', 'Please wait before retrying.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (9, 0, 'en', 'passwords', 'token', 'This password reset token is invalid.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (10, 0, 'en', 'passwords', 'user', 'We can\'t find a user with that email address.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (11, 0, 'en', 'validation', 'accepted', 'The :attribute must be accepted.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (12, 0, 'en', 'validation', 'accepted_if', 'The :attribute must be accepted when :other is :value.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (13, 0, 'en', 'validation', 'active_url', 'The :attribute is not a valid URL.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (14, 0, 'en', 'validation', 'after', 'The :attribute must be a date after :date.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (15, 0, 'en', 'validation', 'after_or_equal', 'The :attribute must be a date after or equal to :date.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (16, 0, 'en', 'validation', 'alpha', 'The :attribute must only contain letters.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (17, 0, 'en', 'validation', 'alpha_dash', 'The :attribute must only contain letters, numbers, dashes and underscores.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (18, 0, 'en', 'validation', 'alpha_num', 'The :attribute must only contain letters and numbers.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (19, 0, 'en', 'validation', 'array', 'The :attribute must be an array.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (20, 0, 'en', 'validation', 'before', 'The :attribute must be a date before :date.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (21, 0, 'en', 'validation', 'before_or_equal', 'The :attribute must be a date before or equal to :date.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (22, 0, 'en', 'validation', 'between.numeric', 'The :attribute must be between :min and :max.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (23, 0, 'en', 'validation', 'between.file', 'The :attribute must be between :min and :max kilobytes.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (24, 0, 'en', 'validation', 'between.string', 'The :attribute must be between :min and :max characters.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (25, 0, 'en', 'validation', 'between.array', 'The :attribute must have between :min and :max items.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (26, 0, 'en', 'validation', 'boolean', 'The :attribute field must be true or false.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (27, 0, 'en', 'validation', 'confirmed', 'The :attribute confirmation does not match.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (28, 0, 'en', 'validation', 'current_password', 'The password is incorrect.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (29, 0, 'en', 'validation', 'date', 'The :attribute is not a valid date.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (30, 0, 'en', 'validation', 'date_equals', 'The :attribute must be a date equal to :date.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (31, 0, 'en', 'validation', 'date_format', 'The :attribute does not match the format :format.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (32, 0, 'en', 'validation', 'different', 'The :attribute and :other must be different.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (33, 0, 'en', 'validation', 'digits', 'The :attribute must be :digits digits.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (34, 0, 'en', 'validation', 'digits_between', 'The :attribute must be between :min and :max digits.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (35, 0, 'en', 'validation', 'dimensions', 'The :attribute has invalid image dimensions.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (36, 0, 'en', 'validation', 'distinct', 'The :attribute field has a duplicate value.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (37, 0, 'en', 'validation', 'email', 'The :attribute must be a valid email address.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (38, 0, 'en', 'validation', 'ends_with', 'The :attribute must end with one of the following: :values.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (39, 0, 'en', 'validation', 'exists', 'The selected :attribute is invalid.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (40, 0, 'en', 'validation', 'file', 'The :attribute must be a file.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (41, 0, 'en', 'validation', 'filled', 'The :attribute field must have a value.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (42, 0, 'en', 'validation', 'gt.numeric', 'The :attribute must be greater than :value.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (43, 0, 'en', 'validation', 'gt.file', 'The :attribute must be greater than :value kilobytes.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (44, 0, 'en', 'validation', 'gt.string', 'The :attribute must be greater than :value characters.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (45, 0, 'en', 'validation', 'gt.array', 'The :attribute must have more than :value items.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (46, 0, 'en', 'validation', 'gte.numeric', 'The :attribute must be greater than or equal :value.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (47, 0, 'en', 'validation', 'gte.file', 'The :attribute must be greater than or equal :value kilobytes.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (48, 0, 'en', 'validation', 'gte.string', 'The :attribute must be greater than or equal :value characters.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (49, 0, 'en', 'validation', 'gte.array', 'The :attribute must have :value items or more.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (50, 0, 'en', 'validation', 'image', 'The :attribute must be an image.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (51, 0, 'en', 'validation', 'in', 'The selected :attribute is invalid.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (52, 0, 'en', 'validation', 'in_array', 'The :attribute field does not exist in :other.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (53, 0, 'en', 'validation', 'integer', 'The :attribute must be an integer.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (54, 0, 'en', 'validation', 'ip', 'The :attribute must be a valid IP address.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (55, 0, 'en', 'validation', 'ipv4', 'The :attribute must be a valid IPv4 address.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (56, 0, 'en', 'validation', 'ipv6', 'The :attribute must be a valid IPv6 address.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (57, 0, 'en', 'validation', 'json', 'The :attribute must be a valid JSON string.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (58, 0, 'en', 'validation', 'lt.numeric', 'The :attribute must be less than :value.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (59, 0, 'en', 'validation', 'lt.file', 'The :attribute must be less than :value kilobytes.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (60, 0, 'en', 'validation', 'lt.string', 'The :attribute must be less than :value characters.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (61, 0, 'en', 'validation', 'lt.array', 'The :attribute must have less than :value items.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (62, 0, 'en', 'validation', 'lte.numeric', 'The :attribute must be less than or equal :value.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (63, 0, 'en', 'validation', 'lte.file', 'The :attribute must be less than or equal :value kilobytes.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (64, 0, 'en', 'validation', 'lte.string', 'The :attribute must be less than or equal :value characters.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (65, 0, 'en', 'validation', 'lte.array', 'The :attribute must not have more than :value items.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (66, 0, 'en', 'validation', 'max.numeric', 'The :attribute must not be greater than :max.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (67, 0, 'en', 'validation', 'max.file', 'The :attribute must not be greater than :max kilobytes.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (68, 0, 'en', 'validation', 'max.string', 'The :attribute must not be greater than :max characters.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (69, 0, 'en', 'validation', 'max.array', 'The :attribute must not have more than :max items.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (70, 0, 'en', 'validation', 'mimes', 'The :attribute must be a file of type: :values.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (71, 0, 'en', 'validation', 'mimetypes', 'The :attribute must be a file of type: :values.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (72, 0, 'en', 'validation', 'min.numeric', 'The :attribute must be at least :min.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (73, 0, 'en', 'validation', 'min.file', 'The :attribute must be at least :min kilobytes.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (74, 0, 'en', 'validation', 'min.string', 'The :attribute must be at least :min characters.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (75, 0, 'en', 'validation', 'min.array', 'The :attribute must have at least :min items.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (76, 0, 'en', 'validation', 'multiple_of', 'The :attribute must be a multiple of :value.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (77, 0, 'en', 'validation', 'not_in', 'The selected :attribute is invalid.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (78, 0, 'en', 'validation', 'not_regex', 'The :attribute format is invalid.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (79, 0, 'en', 'validation', 'numeric', 'The :attribute must be a number.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (80, 0, 'en', 'validation', 'password', 'The password is incorrect.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (81, 0, 'en', 'validation', 'present', 'The :attribute field must be present.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (82, 0, 'en', 'validation', 'regex', 'The :attribute format is invalid.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (83, 0, 'en', 'validation', 'required', 'The :attribute field is required.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (84, 0, 'en', 'validation', 'required_if', 'The :attribute field is required when :other is :value.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (85, 0, 'en', 'validation', 'required_unless', 'The :attribute field is required unless :other is in :values.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (86, 0, 'en', 'validation', 'required_with', 'The :attribute field is required when :values is present.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (87, 0, 'en', 'validation', 'required_with_all', 'The :attribute field is required when :values are present.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (88, 0, 'en', 'validation', 'required_without', 'The :attribute field is required when :values is not present.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (89, 0, 'en', 'validation', 'required_without_all', 'The :attribute field is required when none of :values are present.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (90, 0, 'en', 'validation', 'prohibited', 'The :attribute field is prohibited.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (91, 0, 'en', 'validation', 'prohibited_if', 'The :attribute field is prohibited when :other is :value.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (92, 0, 'en', 'validation', 'prohibited_unless', 'The :attribute field is prohibited unless :other is in :values.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (93, 0, 'en', 'validation', 'prohibits', 'The :attribute field prohibits :other from being present.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (94, 0, 'en', 'validation', 'same', 'The :attribute and :other must match.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (95, 0, 'en', 'validation', 'size.numeric', 'The :attribute must be :size.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (96, 0, 'en', 'validation', 'size.file', 'The :attribute must be :size kilobytes.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (97, 0, 'en', 'validation', 'size.string', 'The :attribute must be :size characters.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (98, 0, 'en', 'validation', 'size.array', 'The :attribute must contain :size items.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (99, 0, 'en', 'validation', 'starts_with', 'The :attribute must start with one of the following: :values.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (100, 0, 'en', 'validation', 'string', 'The :attribute must be a string.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (101, 0, 'en', 'validation', 'timezone', 'The :attribute must be a valid timezone.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (102, 0, 'en', 'validation', 'unique', 'The :attribute has already been taken.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (103, 0, 'en', 'validation', 'uploaded', 'The :attribute failed to upload.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (104, 0, 'en', 'validation', 'url', 'The :attribute must be a valid URL.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (105, 0, 'en', 'validation', 'uuid', 'The :attribute must be a valid UUID.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (106, 0, 'en', 'validation', 'custom.attribute-name.rule-name', 'custom-message', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (107, 0, 'en', 'core/acl/auth', 'login.username', 'Email/Username', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (108, 0, 'en', 'core/acl/auth', 'login.email', 'Email', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (109, 0, 'en', 'core/acl/auth', 'login.password', 'Password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (110, 0, 'en', 'core/acl/auth', 'login.title', 'User Login', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (111, 0, 'en', 'core/acl/auth', 'login.remember', 'Remember me?', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (112, 0, 'en', 'core/acl/auth', 'login.login', 'Sign in', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (113, 0, 'en', 'core/acl/auth', 'login.placeholder.username', 'Please enter your username', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (114, 0, 'en', 'core/acl/auth', 'login.placeholder.email', 'Please enter your email', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (115, 0, 'en', 'core/acl/auth', 'login.success', 'Login successfully!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (116, 0, 'en', 'core/acl/auth', 'login.fail', 'Wrong username or password.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (117, 0, 'en', 'core/acl/auth', 'login.not_active', 'Your account has not been activated yet!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (118, 0, 'en', 'core/acl/auth', 'login.banned', 'This account is banned.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (119, 0, 'en', 'core/acl/auth', 'login.logout_success', 'Logout successfully!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (120, 0, 'en', 'core/acl/auth', 'login.dont_have_account', 'You don\'t have account on this system, please contact administrator for more information!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (121, 0, 'en', 'core/acl/auth', 'forgot_password.title', 'Forgot Password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (122, 0, 'en', 'core/acl/auth', 'forgot_password.message', '<p>Have you forgotten your password?</p><p>Please enter your email account. System will send a email with active link to reset your password.</p>', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (123, 0, 'en', 'core/acl/auth', 'forgot_password.submit', 'Submit', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (124, 0, 'en', 'core/acl/auth', 'reset.new_password', 'New password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (125, 0, 'en', 'core/acl/auth', 'reset.password_confirmation', 'Confirm new password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (126, 0, 'en', 'core/acl/auth', 'reset.email', 'Email', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (127, 0, 'en', 'core/acl/auth', 'reset.title', 'Reset your password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (128, 0, 'en', 'core/acl/auth', 'reset.update', 'Update', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (129, 0, 'en', 'core/acl/auth', 'reset.wrong_token', 'This link is invalid or expired. Please try using reset form again.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (130, 0, 'en', 'core/acl/auth', 'reset.user_not_found', 'This username is not exist.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (131, 0, 'en', 'core/acl/auth', 'reset.success', 'Reset password successfully!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (132, 0, 'en', 'core/acl/auth', 'reset.fail', 'Token is invalid, the reset password link has been expired!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (133, 0, 'en', 'core/acl/auth', 'reset.reset.title', 'Email reset password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (134, 0, 'en', 'core/acl/auth', 'reset.send.success', 'A email was sent to your email account. Please check and complete this action.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (135, 0, 'en', 'core/acl/auth', 'reset.send.fail', 'Can not send email in this time. Please try again later.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (136, 0, 'en', 'core/acl/auth', 'reset.new-password', 'New password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (137, 0, 'en', 'core/acl/auth', 'email.reminder.title', 'Email reset password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (138, 0, 'en', 'core/acl/auth', 'password_confirmation', 'Password confirm', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (139, 0, 'en', 'core/acl/auth', 'failed', 'Failed', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (140, 0, 'en', 'core/acl/auth', 'throttle', 'Throttle', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (141, 0, 'en', 'core/acl/auth', 'not_member', 'Not a member yet?', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (142, 0, 'en', 'core/acl/auth', 'register_now', 'Register now', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (143, 0, 'en', 'core/acl/auth', 'lost_your_password', 'Lost your password?', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (144, 0, 'en', 'core/acl/auth', 'login_title', 'Admin', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (145, 0, 'en', 'core/acl/auth', 'login_via_social', 'Login with social networks', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (146, 0, 'en', 'core/acl/auth', 'back_to_login', 'Back to login page', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (147, 0, 'en', 'core/acl/auth', 'sign_in_below', 'Sign In Below', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (148, 0, 'en', 'core/acl/auth', 'languages', 'Languages', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (149, 0, 'en', 'core/acl/auth', 'reset_password', 'Reset Password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (150, 0, 'en', 'core/acl/auth', 'settings.email.title', 'ACL', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (151, 0, 'en', 'core/acl/auth', 'settings.email.description', 'ACL email configuration', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (152, 0, 'en', 'core/acl/permissions', 'notices.role_in_use', 'Cannot delete this role, it is still in use!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (153, 0, 'en', 'core/acl/permissions', 'notices.role_delete_belong_user', 'You are not able to delete this role; it belongs to someone else!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (154, 0, 'en', 'core/acl/permissions', 'notices.role_edit_belong_user', 'You are not able to edit this role; it belongs to someone else!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (155, 0, 'en', 'core/acl/permissions', 'notices.delete_global_role', 'You are not allowed to delete global roles!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (156, 0, 'en', 'core/acl/permissions', 'notices.delete_success', 'The selected role was deleted successfully!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (157, 0, 'en', 'core/acl/permissions', 'notices.modified_success', 'The selected role was modified successfully!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (158, 0, 'en', 'core/acl/permissions', 'notices.create_success', 'The new role was successfully created', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (159, 0, 'en', 'core/acl/permissions', 'notices.duplicated_success', 'The selected role was duplicated successfully', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (160, 0, 'en', 'core/acl/permissions', 'notices.no_select', 'Please select at least one record to take this action!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (161, 0, 'en', 'core/acl/permissions', 'notices.not_found', 'Role not found', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (162, 0, 'en', 'core/acl/permissions', 'name', 'Name', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (163, 0, 'en', 'core/acl/permissions', 'current_role', 'Current Role', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (164, 0, 'en', 'core/acl/permissions', 'no_role_assigned', 'No role assigned', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (165, 0, 'en', 'core/acl/permissions', 'role_assigned', 'Assigned Role', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (166, 0, 'en', 'core/acl/permissions', 'create_role', 'Create New Role', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (167, 0, 'en', 'core/acl/permissions', 'role_name', 'Name', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (168, 0, 'en', 'core/acl/permissions', 'role_description', 'Description', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (169, 0, 'en', 'core/acl/permissions', 'permission_flags', 'Permission Flags', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (170, 0, 'en', 'core/acl/permissions', 'cancel', 'Cancel', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (171, 0, 'en', 'core/acl/permissions', 'reset', 'Reset', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (172, 0, 'en', 'core/acl/permissions', 'save', 'Save', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (173, 0, 'en', 'core/acl/permissions', 'global_role_msg', 'This is a global role and cannot be modified.  You can use the Duplicate button to make a copy of this role that you can then modify.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (174, 0, 'en', 'core/acl/permissions', 'details', 'Details', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (175, 0, 'en', 'core/acl/permissions', 'duplicate', 'Duplicate', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (176, 0, 'en', 'core/acl/permissions', 'all', 'All Permissions', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (177, 0, 'en', 'core/acl/permissions', 'list_role', 'List Roles', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (178, 0, 'en', 'core/acl/permissions', 'created_on', 'Created On', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (179, 0, 'en', 'core/acl/permissions', 'created_by', 'Created By', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (180, 0, 'en', 'core/acl/permissions', 'actions', 'Actions', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (181, 0, 'en', 'core/acl/permissions', 'role_in_use', 'Cannot delete this role, it is still in use!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (182, 0, 'en', 'core/acl/permissions', 'role_delete_belong_user', 'You are not able to delete this role; it belongs to someone else!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (183, 0, 'en', 'core/acl/permissions', 'delete_global_role', 'Can not delete global role', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (184, 0, 'en', 'core/acl/permissions', 'delete_success', 'Delete role successfully', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (185, 0, 'en', 'core/acl/permissions', 'no_select', 'Please select at least one role to take this action!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (186, 0, 'en', 'core/acl/permissions', 'not_found', 'No role found!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (187, 0, 'en', 'core/acl/permissions', 'role_edit_belong_user', 'You are not able to edit this role; it belongs to someone else!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (188, 0, 'en', 'core/acl/permissions', 'modified_success', 'Modified successfully', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (189, 0, 'en', 'core/acl/permissions', 'create_success', 'Create successfully', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (190, 0, 'en', 'core/acl/permissions', 'duplicated_success', 'Duplicated successfully', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (191, 0, 'en', 'core/acl/permissions', 'options', 'Options', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (192, 0, 'en', 'core/acl/permissions', 'access_denied_message', 'You are not allowed to do this action', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (193, 0, 'en', 'core/acl/permissions', 'roles', 'Roles', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (194, 0, 'en', 'core/acl/permissions', 'role_permission', 'Roles and Permissions', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (195, 0, 'en', 'core/acl/permissions', 'user_management', 'User Management', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (196, 0, 'en', 'core/acl/permissions', 'super_user_management', 'Super User Management', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (197, 0, 'en', 'core/acl/permissions', 'action_unauthorized', 'This action is unauthorized.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (198, 0, 'en', 'core/acl/reminders', 'password', 'Passwords must be at least six characters and match the confirmation.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (199, 0, 'en', 'core/acl/reminders', 'user', 'We can\'t find a user with that e-mail address.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (200, 0, 'en', 'core/acl/reminders', 'token', 'This password reset token is invalid.', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (201, 0, 'en', 'core/acl/reminders', 'sent', 'Password reminder sent!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (202, 0, 'en', 'core/acl/reminders', 'reset', 'Password has been reset!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (203, 0, 'en', 'core/acl/users', 'delete_user_logged_in', 'Can\'t delete this user. This user is logged on!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (204, 0, 'en', 'core/acl/users', 'no_select', 'Please select at least one record to take this action!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (205, 0, 'en', 'core/acl/users', 'lock_user_logged_in', 'Can\'t lock this user. This user is logged on!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (206, 0, 'en', 'core/acl/users', 'update_success', 'Update status successfully!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (207, 0, 'en', 'core/acl/users', 'save_setting_failed', 'Something went wrong when save your setting', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (208, 0, 'en', 'core/acl/users', 'not_found', 'User not found', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (209, 0, 'en', 'core/acl/users', 'email_exist', 'That email address already belongs to an existing account', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (210, 0, 'en', 'core/acl/users', 'username_exist', 'That username address already belongs to an existing account', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (211, 0, 'en', 'core/acl/users', 'update_profile_success', 'Your profile changes were successfully saved', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (212, 0, 'en', 'core/acl/users', 'password_update_success', 'Password successfully changed', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (213, 0, 'en', 'core/acl/users', 'current_password_not_valid', 'Current password is not valid', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (214, 0, 'en', 'core/acl/users', 'user_exist_in', 'User is already a member', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (215, 0, 'en', 'core/acl/users', 'email', 'Email', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (216, 0, 'en', 'core/acl/users', 'role', 'Role', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (217, 0, 'en', 'core/acl/users', 'username', 'Username', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (218, 0, 'en', 'core/acl/users', 'last_name', 'Last Name', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (219, 0, 'en', 'core/acl/users', 'first_name', 'First Name', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (220, 0, 'en', 'core/acl/users', 'message', 'Message', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (221, 0, 'en', 'core/acl/users', 'cancel_btn', 'Cancel', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (222, 0, 'en', 'core/acl/users', 'change_password', 'Change password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (223, 0, 'en', 'core/acl/users', 'current_password', 'Current password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (224, 0, 'en', 'core/acl/users', 'new_password', 'New Password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (225, 0, 'en', 'core/acl/users', 'confirm_new_password', 'Confirm New Password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (226, 0, 'en', 'core/acl/users', 'password', 'Password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (227, 0, 'en', 'core/acl/users', 'save', 'Save', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (228, 0, 'en', 'core/acl/users', 'cannot_delete', 'User could not be deleted', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (229, 0, 'en', 'core/acl/users', 'deleted', 'User deleted', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (230, 0, 'en', 'core/acl/users', 'last_login', 'Last Login', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (231, 0, 'en', 'core/acl/users', 'error_update_profile_image', 'Error when update profile image', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (232, 0, 'en', 'core/acl/users', 'email_reminder_template', '<h3>Hello :name</h3><p>The system has received a request to restore the password for your account, to complete this task please click the link below.</p><p><a href=\":link\">Reset password now</a></p><p>If not you ask recover password, please ignore this email.</p><p>This email is valid for 60 minutes after receiving the email.</p>', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (233, 0, 'en', 'core/acl/users', 'change_profile_image', 'Change Profile Image', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (234, 0, 'en', 'core/acl/users', 'new_image', 'New Image', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (235, 0, 'en', 'core/acl/users', 'loading', 'Loading', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (236, 0, 'en', 'core/acl/users', 'close', 'Close', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (237, 0, 'en', 'core/acl/users', 'update', 'Update', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (238, 0, 'en', 'core/acl/users', 'read_image_failed', 'Failed to read the image file', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (239, 0, 'en', 'core/acl/users', 'users', 'Users', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (240, 0, 'en', 'core/acl/users', 'update_avatar_success', 'Update profile image successfully!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (241, 0, 'en', 'core/acl/users', 'info.title', 'User profile', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (242, 0, 'en', 'core/acl/users', 'info.first_name', 'First Name', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (243, 0, 'en', 'core/acl/users', 'info.last_name', 'Last Name', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (244, 0, 'en', 'core/acl/users', 'info.email', 'Email', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (245, 0, 'en', 'core/acl/users', 'info.second_email', 'Secondary Email', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (246, 0, 'en', 'core/acl/users', 'info.address', 'Address', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (247, 0, 'en', 'core/acl/users', 'info.second_address', 'Secondary Address', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (248, 0, 'en', 'core/acl/users', 'info.birth_day', 'Date of birth', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (249, 0, 'en', 'core/acl/users', 'info.job', 'Job Position', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (250, 0, 'en', 'core/acl/users', 'info.mobile_number', 'Mobile Number', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (251, 0, 'en', 'core/acl/users', 'info.second_mobile_number', 'Secondary Phone', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (252, 0, 'en', 'core/acl/users', 'info.interes', 'Interests', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (253, 0, 'en', 'core/acl/users', 'info.about', 'About', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (254, 0, 'en', 'core/acl/users', 'gender.title', 'Gender', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (255, 0, 'en', 'core/acl/users', 'gender.male', 'Male', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (256, 0, 'en', 'core/acl/users', 'gender.female', 'Female', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (257, 0, 'en', 'core/acl/users', 'total_users', 'Total users', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (258, 0, 'en', 'core/acl/users', 'statuses.activated', 'Activated', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (259, 0, 'en', 'core/acl/users', 'statuses.deactivated', 'Deactivated', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (260, 0, 'en', 'core/acl/users', 'make_super', 'Make super', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (261, 0, 'en', 'core/acl/users', 'remove_super', 'Remove super', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (262, 0, 'en', 'core/acl/users', 'is_super', 'Is super?', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (263, 0, 'en', 'core/acl/users', 'email_placeholder', 'Ex: example@gmail.com', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (264, 0, 'en', 'core/acl/users', 'password_confirmation', 'Re-type password', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (265, 0, 'en', 'core/acl/users', 'select_role', 'Select role', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (266, 0, 'en', 'core/acl/users', 'create_new_user', 'Create a new user', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (267, 0, 'en', 'core/acl/users', 'cannot_delete_super_user', 'Permission denied. Cannot delete a super user!', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (268, 0, 'en', 'core/acl/users', 'assigned_role', 'Assigned Role', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (269, 0, 'en', 'core/acl/users', 'no_role_assigned', 'No role assigned', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (270, 0, 'en', 'core/acl/users', 'view_user_profile', 'View user\'s profile', '2021-09-14 05:22:40', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (433, 0, 'en', 'core/base/base', 'yes', 'Yes', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (434, 0, 'en', 'core/base/base', 'no', 'No', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (435, 0, 'en', 'core/base/base', 'is_default', 'Is default?', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (436, 0, 'en', 'core/base/base', 'proc_close_disabled_error', 'Function proc_close() is disabled. Please contact your hosting provider to enable\n    it. Or you can add to .env: CAN_EXECUTE_COMMAND=false to disable this feature.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (437, 0, 'en', 'core/base/base', 'email_template.header', 'Email template header', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (438, 0, 'en', 'core/base/base', 'email_template.footer', 'Email template footer', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (439, 0, 'en', 'core/base/base', 'email_template.site_title', 'Site title', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (440, 0, 'en', 'core/base/base', 'email_template.site_url', 'Site URL', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (441, 0, 'en', 'core/base/base', 'email_template.site_logo', 'Site Logo', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (442, 0, 'en', 'core/base/base', 'email_template.date_time', 'Current date time', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (443, 0, 'en', 'core/base/base', 'email_template.date_year', 'Current year', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (444, 0, 'en', 'core/base/base', 'email_template.site_admin_email', 'Site admin email', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (445, 0, 'en', 'core/base/base', 'change_image', 'Change image', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (446, 0, 'en', 'core/base/base', 'delete_image', 'Delete image', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (447, 0, 'en', 'core/base/base', 'preview_image', 'Preview image', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (448, 0, 'en', 'core/base/base', 'image', 'Image', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (449, 0, 'en', 'core/base/base', 'using_button', 'Using button', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (450, 0, 'en', 'core/base/base', 'select_image', 'Select image', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (451, 0, 'en', 'core/base/base', 'to_add_more_image', 'to add more images', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (452, 0, 'en', 'core/base/base', 'add_image', 'Add image', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (453, 0, 'en', 'core/base/base', 'tools', 'Tools', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (454, 0, 'en', 'core/base/cache', 'cache_management', 'Cache management', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (455, 0, 'en', 'core/base/cache', 'cache_commands', 'Clear cache commands', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (456, 0, 'en', 'core/base/cache', 'commands.clear_cms_cache.title', 'Clear all CMS cache', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (457, 0, 'en', 'core/base/cache', 'commands.clear_cms_cache.description', 'Clear CMS caching: database caching, static blocks... Run this command when you don\'t see the changes after updating data.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (458, 0, 'en', 'core/base/cache', 'commands.clear_cms_cache.success_msg', 'Cache cleaned', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (459, 0, 'en', 'core/base/cache', 'commands.refresh_compiled_views.title', 'Refresh compiled views', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (460, 0, 'en', 'core/base/cache', 'commands.refresh_compiled_views.description', 'Clear compiled views to make views up to date.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (461, 0, 'en', 'core/base/cache', 'commands.refresh_compiled_views.success_msg', 'Cache view refreshed', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (462, 0, 'en', 'core/base/cache', 'commands.clear_config_cache.title', 'Clear config cache', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (463, 0, 'en', 'core/base/cache', 'commands.clear_config_cache.description', 'You might need to refresh the config caching when you change something on production environment.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (464, 0, 'en', 'core/base/cache', 'commands.clear_config_cache.success_msg', 'Config cache cleaned', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (465, 0, 'en', 'core/base/cache', 'commands.clear_route_cache.title', 'Clear route cache', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (466, 0, 'en', 'core/base/cache', 'commands.clear_route_cache.description', 'Clear cache routing.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (467, 0, 'en', 'core/base/cache', 'commands.clear_route_cache.success_msg', 'The route cache has been cleaned', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (468, 0, 'en', 'core/base/cache', 'commands.clear_log.title', 'Clear log', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (469, 0, 'en', 'core/base/cache', 'commands.clear_log.description', 'Clear system log files', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (470, 0, 'en', 'core/base/cache', 'commands.clear_log.success_msg', 'The system log has been cleaned', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (471, 0, 'en', 'core/base/enums', 'statuses.draft', 'Draft', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (472, 0, 'en', 'core/base/enums', 'statuses.pending', 'Pending', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (473, 0, 'en', 'core/base/enums', 'statuses.published', 'Published', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (474, 0, 'en', 'core/base/errors', '401_title', 'Permission Denied', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (475, 0, 'en', 'core/base/errors', '401_msg', '<li>You have not been granted access to the section by the administrator.</li>\n	                <li>You may have the wrong account type.</li>\n	                <li>You are not authorized to view the requested resource.</li>\n	                <li>Your subscription may have expired.</li>', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (476, 0, 'en', 'core/base/errors', '404_title', 'Page could not be found', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (477, 0, 'en', 'core/base/errors', '404_msg', '<li>The page you requested does not exist.</li>\n	                <li>The link you clicked is no longer.</li>\n	                <li>The page may have moved to a new location.</li>\n	                <li>An error may have occurred.</li>\n	                <li>You are not authorized to view the requested resource.</li>', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (478, 0, 'en', 'core/base/errors', '500_title', 'Page could not be loaded', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (479, 0, 'en', 'core/base/errors', '500_msg', '<li>The page you requested does not exist.</li>\n	                <li>The link you clicked is no longer.</li>\n	                <li>The page may have moved to a new location.</li>\n	                <li>An error may have occurred.</li>\n	                <li>You are not authorized to view the requested resource.</li>', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (480, 0, 'en', 'core/base/errors', 'reasons', 'This may have occurred because of several reasons', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (481, 0, 'en', 'core/base/errors', 'try_again', 'Please try again in a few minutes, or alternatively return to the homepage by <a href=\"http://botble.local/admin\">clicking here</a>.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (482, 0, 'en', 'core/base/errors', 'not_found', 'Not Found', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (483, 0, 'en', 'core/base/forms', 'choose_image', 'Choose image', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (484, 0, 'en', 'core/base/forms', 'actions', 'Actions', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (485, 0, 'en', 'core/base/forms', 'save', 'Save', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (486, 0, 'en', 'core/base/forms', 'save_and_continue', 'Save & Edit', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (487, 0, 'en', 'core/base/forms', 'image', 'Image', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (488, 0, 'en', 'core/base/forms', 'image_placeholder', 'Insert path of image or click upload button', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (489, 0, 'en', 'core/base/forms', 'create', 'Create', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (490, 0, 'en', 'core/base/forms', 'edit', 'Edit', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (491, 0, 'en', 'core/base/forms', 'permalink', 'Permalink', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (492, 0, 'en', 'core/base/forms', 'ok', 'OK', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (493, 0, 'en', 'core/base/forms', 'cancel', 'Cancel', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (494, 0, 'en', 'core/base/forms', 'character_remain', 'character(s) remain', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (495, 0, 'en', 'core/base/forms', 'template', 'Template', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (496, 0, 'en', 'core/base/forms', 'choose_file', 'Choose file', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (497, 0, 'en', 'core/base/forms', 'file', 'File', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (498, 0, 'en', 'core/base/forms', 'content', 'Content', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (499, 0, 'en', 'core/base/forms', 'description', 'Description', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (500, 0, 'en', 'core/base/forms', 'name', 'Name', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (501, 0, 'en', 'core/base/forms', 'slug', 'Slug', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (502, 0, 'en', 'core/base/forms', 'title', 'Title', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (503, 0, 'en', 'core/base/forms', 'value', 'Value', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (504, 0, 'en', 'core/base/forms', 'name_placeholder', 'Name', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (505, 0, 'en', 'core/base/forms', 'alias_placeholder', 'Alias', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (506, 0, 'en', 'core/base/forms', 'description_placeholder', 'Short description', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (507, 0, 'en', 'core/base/forms', 'parent', 'Parent', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (508, 0, 'en', 'core/base/forms', 'icon', 'Icon', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (509, 0, 'en', 'core/base/forms', 'icon_placeholder', 'Ex: fa fa-home', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (510, 0, 'en', 'core/base/forms', 'order_by', 'Order by', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (511, 0, 'en', 'core/base/forms', 'order_by_placeholder', 'Order by', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (512, 0, 'en', 'core/base/forms', 'is_featured', 'Is featured?', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (513, 0, 'en', 'core/base/forms', 'is_default', 'Is default?', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (514, 0, 'en', 'core/base/forms', 'update', 'Update', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (515, 0, 'en', 'core/base/forms', 'publish', 'Publish', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (516, 0, 'en', 'core/base/forms', 'remove_image', 'Remove image', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (517, 0, 'en', 'core/base/forms', 'remove_file', 'Remove file', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (518, 0, 'en', 'core/base/forms', 'order', 'Order', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (519, 0, 'en', 'core/base/forms', 'alias', 'Alias', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (520, 0, 'en', 'core/base/forms', 'basic_information', 'Basic information', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (521, 0, 'en', 'core/base/forms', 'short_code', 'Shortcode', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (522, 0, 'en', 'core/base/forms', 'add_short_code', 'Add a shortcode', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (523, 0, 'en', 'core/base/forms', 'add', 'Add', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (524, 0, 'en', 'core/base/forms', 'link', 'Link', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (525, 0, 'en', 'core/base/forms', 'show_hide_editor', 'Show/Hide Editor', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (526, 0, 'en', 'core/base/forms', 'basic_info_title', 'Basic information', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (527, 0, 'en', 'core/base/forms', 'expand_all', 'Expand all', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (528, 0, 'en', 'core/base/forms', 'collapse_all', 'Collapse all', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (529, 0, 'en', 'core/base/forms', 'view_new_tab', 'Open in new tab', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (530, 0, 'en', 'core/base/layouts', 'platform_admin', 'Platform Administration', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (531, 0, 'en', 'core/base/layouts', 'dashboard', 'Dashboard', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (532, 0, 'en', 'core/base/layouts', 'widgets', 'Widgets', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (533, 0, 'en', 'core/base/layouts', 'plugins', 'Plugins', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (534, 0, 'en', 'core/base/layouts', 'settings', 'Settings', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (535, 0, 'en', 'core/base/layouts', 'setting_general', 'General', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (536, 0, 'en', 'core/base/layouts', 'setting_email', 'Email', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (537, 0, 'en', 'core/base/layouts', 'system_information', 'System information', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (538, 0, 'en', 'core/base/layouts', 'theme', 'Theme', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (539, 0, 'en', 'core/base/layouts', 'copyright', 'Copyright :year &copy; :company. Version: <span>:version</span>', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (540, 0, 'en', 'core/base/layouts', 'profile', 'Profile', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (541, 0, 'en', 'core/base/layouts', 'logout', 'Logout', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (542, 0, 'en', 'core/base/layouts', 'no_search_result', 'No results found, please try with different keywords.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (543, 0, 'en', 'core/base/layouts', 'home', 'Home', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (544, 0, 'en', 'core/base/layouts', 'search', 'Search', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (545, 0, 'en', 'core/base/layouts', 'add_new', 'Add new', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (546, 0, 'en', 'core/base/layouts', 'n_a', 'N/A', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (547, 0, 'en', 'core/base/layouts', 'page_loaded_time', 'Page loaded in', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (548, 0, 'en', 'core/base/layouts', 'view_website', 'View website', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (549, 0, 'en', 'core/base/notices', 'create_success_message', 'Created successfully', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (550, 0, 'en', 'core/base/notices', 'update_success_message', 'Updated successfully', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (551, 0, 'en', 'core/base/notices', 'delete_success_message', 'Deleted successfully', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (552, 0, 'en', 'core/base/notices', 'success_header', 'Success!', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (553, 0, 'en', 'core/base/notices', 'error_header', 'Error!', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (554, 0, 'en', 'core/base/notices', 'no_select', 'Please select at least one record to perform this action!', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (555, 0, 'en', 'core/base/notices', 'processing_request', 'We are processing your request.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (556, 0, 'en', 'core/base/notices', 'error', 'Error!', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (557, 0, 'en', 'core/base/notices', 'success', 'Success!', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (558, 0, 'en', 'core/base/notices', 'info', 'Info!', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (559, 0, 'en', 'core/base/notices', 'enum.validate_message', 'The :attribute value you have entered is invalid.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (560, 0, 'en', 'core/base/system', 'no_select', 'Please select at least one record to take this action!', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (561, 0, 'en', 'core/base/system', 'cannot_find_user', 'Unable to find specified user', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (562, 0, 'en', 'core/base/system', 'supper_revoked', 'Super user access revoked', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (563, 0, 'en', 'core/base/system', 'cannot_revoke_yourself', 'Can not revoke supper user access permission yourself!', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (564, 0, 'en', 'core/base/system', 'cant_remove_supper', 'You don\'t has permission to remove this super user', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (565, 0, 'en', 'core/base/system', 'cant_find_user_with_email', 'Unable to find user with specified email address', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (566, 0, 'en', 'core/base/system', 'supper_granted', 'Super user access granted', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (567, 0, 'en', 'core/base/system', 'delete_log_success', 'Delete log file successfully!', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (568, 0, 'en', 'core/base/system', 'get_member_success', 'Member list retrieved successfully', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (569, 0, 'en', 'core/base/system', 'error_occur', 'The following errors occurred', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (570, 0, 'en', 'core/base/system', 'user_management', 'User Management', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (571, 0, 'en', 'core/base/system', 'user_management_description', 'Manage users.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (572, 0, 'en', 'core/base/system', 'role_and_permission', 'Roles and Permissions', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (573, 0, 'en', 'core/base/system', 'role_and_permission_description', 'Manage the available roles.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (574, 0, 'en', 'core/base/system', 'user.list_super', 'List Super Users', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (575, 0, 'en', 'core/base/system', 'user.email', 'Email', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (576, 0, 'en', 'core/base/system', 'user.last_login', 'Last Login', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (577, 0, 'en', 'core/base/system', 'user.username', 'Username', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (578, 0, 'en', 'core/base/system', 'user.add_user', 'Add Super User', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (579, 0, 'en', 'core/base/system', 'user.cancel', 'Cancel', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (580, 0, 'en', 'core/base/system', 'user.create', 'Create', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (581, 0, 'en', 'core/base/system', 'options.features', 'Feature Access Control', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (582, 0, 'en', 'core/base/system', 'options.feature_description', 'Manage the available.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (583, 0, 'en', 'core/base/system', 'options.manage_super', 'Super User Management', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (584, 0, 'en', 'core/base/system', 'options.manage_super_description', 'Add/remove super users.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (585, 0, 'en', 'core/base/system', 'options.info', 'System information', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (586, 0, 'en', 'core/base/system', 'options.info_description', 'All information about current system configuration.', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (587, 0, 'en', 'core/base/system', 'info.title', 'System information', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (588, 0, 'en', 'core/base/system', 'info.cache', 'Cache', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (589, 0, 'en', 'core/base/system', 'info.locale', 'Active locale', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (590, 0, 'en', 'core/base/system', 'info.environment', 'Environment', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (591, 0, 'en', 'core/base/system', 'disabled_in_demo_mode', 'You cannot do it in demo mode!', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (592, 0, 'en', 'core/base/system', 'report_description', 'Please share this information for troubleshooting', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (593, 0, 'en', 'core/base/system', 'get_system_report', 'Get System Report', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (594, 0, 'en', 'core/base/system', 'system_environment', 'System Environment', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (595, 0, 'en', 'core/base/system', 'framework_version', 'Framework Version', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (596, 0, 'en', 'core/base/system', 'timezone', 'Timezone', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (597, 0, 'en', 'core/base/system', 'debug_mode', 'Debug Mode', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (598, 0, 'en', 'core/base/system', 'storage_dir_writable', 'Storage Dir Writable', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (599, 0, 'en', 'core/base/system', 'cache_dir_writable', 'Cache Dir Writable', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (600, 0, 'en', 'core/base/system', 'app_size', 'App Size', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (601, 0, 'en', 'core/base/system', 'server_environment', 'Server Environment', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (602, 0, 'en', 'core/base/system', 'php_version', 'PHP Version', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (603, 0, 'en', 'core/base/system', 'php_version_error', 'PHP must be >= :version', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (604, 0, 'en', 'core/base/system', 'server_software', 'Server Software', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (605, 0, 'en', 'core/base/system', 'server_os', 'Server OS', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (606, 0, 'en', 'core/base/system', 'database', 'Database', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (607, 0, 'en', 'core/base/system', 'ssl_installed', 'SSL Installed', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (608, 0, 'en', 'core/base/system', 'cache_driver', 'Cache Driver', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (609, 0, 'en', 'core/base/system', 'session_driver', 'Session Driver', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (610, 0, 'en', 'core/base/system', 'queue_connection', 'Queue Connection', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (611, 0, 'en', 'core/base/system', 'mbstring_ext', 'Mbstring Ext', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (612, 0, 'en', 'core/base/system', 'openssl_ext', 'OpenSSL Ext', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (613, 0, 'en', 'core/base/system', 'pdo_ext', 'PDO Ext', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (614, 0, 'en', 'core/base/system', 'curl_ext', 'CURL Ext', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (615, 0, 'en', 'core/base/system', 'exif_ext', 'Exif Ext', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (616, 0, 'en', 'core/base/system', 'file_info_ext', 'File info Ext', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (617, 0, 'en', 'core/base/system', 'tokenizer_ext', 'Tokenizer Ext', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (618, 0, 'en', 'core/base/system', 'extra_stats', 'Extra Stats', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (619, 0, 'en', 'core/base/system', 'installed_packages', 'Installed packages and their version numbers', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (620, 0, 'en', 'core/base/system', 'extra_information', 'Extra Information', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (621, 0, 'en', 'core/base/system', 'copy_report', 'Copy Report', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (622, 0, 'en', 'core/base/system', 'package_name', 'Package Name', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (623, 0, 'en', 'core/base/system', 'dependency_name', 'Dependency Name', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (624, 0, 'en', 'core/base/system', 'version', 'Version', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (625, 0, 'en', 'core/base/system', 'cms_version', 'CMS Version', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (626, 0, 'en', 'core/base/system', 'imagick_or_gd_ext', 'Imagick/GD Ext', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (627, 0, 'en', 'core/base/tables', 'id', 'ID', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (628, 0, 'en', 'core/base/tables', 'name', 'Name', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (629, 0, 'en', 'core/base/tables', 'slug', 'Slug', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (630, 0, 'en', 'core/base/tables', 'title', 'Title', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (631, 0, 'en', 'core/base/tables', 'order_by', 'Order By', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (632, 0, 'en', 'core/base/tables', 'order', 'Order', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (633, 0, 'en', 'core/base/tables', 'status', 'Status', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (634, 0, 'en', 'core/base/tables', 'created_at', 'Created At', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (635, 0, 'en', 'core/base/tables', 'updated_at', 'Updated At', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (636, 0, 'en', 'core/base/tables', 'description', 'Description', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (637, 0, 'en', 'core/base/tables', 'operations', 'Operations', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (638, 0, 'en', 'core/base/tables', 'url', 'URL', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (639, 0, 'en', 'core/base/tables', 'author', 'Author', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (640, 0, 'en', 'core/base/tables', 'notes', 'Notes', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (641, 0, 'en', 'core/base/tables', 'column', 'Column', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (642, 0, 'en', 'core/base/tables', 'origin', 'Origin', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (643, 0, 'en', 'core/base/tables', 'after_change', 'After changes', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (644, 0, 'en', 'core/base/tables', 'views', 'Views', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (645, 0, 'en', 'core/base/tables', 'browser', 'Browser', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (646, 0, 'en', 'core/base/tables', 'session', 'Session', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (647, 0, 'en', 'core/base/tables', 'activated', 'activated', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (648, 0, 'en', 'core/base/tables', 'deactivated', 'deactivated', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (649, 0, 'en', 'core/base/tables', 'is_featured', 'Is featured', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (650, 0, 'en', 'core/base/tables', 'edit', 'Edit', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (651, 0, 'en', 'core/base/tables', 'delete', 'Delete', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (652, 0, 'en', 'core/base/tables', 'restore', 'Restore', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (653, 0, 'en', 'core/base/tables', 'activate', 'Activate', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (654, 0, 'en', 'core/base/tables', 'deactivate', 'Deactivate', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (655, 0, 'en', 'core/base/tables', 'excel', 'Excel', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (656, 0, 'en', 'core/base/tables', 'export', 'Export', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (657, 0, 'en', 'core/base/tables', 'csv', 'CSV', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (658, 0, 'en', 'core/base/tables', 'pdf', 'PDF', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (659, 0, 'en', 'core/base/tables', 'print', 'Print', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (660, 0, 'en', 'core/base/tables', 'reset', 'Reset', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (661, 0, 'en', 'core/base/tables', 'reload', 'Reload', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (662, 0, 'en', 'core/base/tables', 'display', 'Display', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (663, 0, 'en', 'core/base/tables', 'all', 'All', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (664, 0, 'en', 'core/base/tables', 'add_new', 'Add New', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (665, 0, 'en', 'core/base/tables', 'action', 'Actions', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (666, 0, 'en', 'core/base/tables', 'delete_entry', 'Delete', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (667, 0, 'en', 'core/base/tables', 'view', 'View Detail', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (668, 0, 'en', 'core/base/tables', 'save', 'Save', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (669, 0, 'en', 'core/base/tables', 'show_from', 'Show from', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (670, 0, 'en', 'core/base/tables', 'to', 'to', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (671, 0, 'en', 'core/base/tables', 'in', 'in', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (672, 0, 'en', 'core/base/tables', 'records', 'records', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (673, 0, 'en', 'core/base/tables', 'no_data', 'No data to display', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (674, 0, 'en', 'core/base/tables', 'no_record', 'No record', '2021-09-14 05:22:41', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (675, 0, 'en', 'core/base/tables', 'confirm_delete', 'Confirm delete', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (676, 0, 'en', 'core/base/tables', 'confirm_delete_msg', 'Do you really want to delete this record?', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (677, 0, 'en', 'core/base/tables', 'confirm_delete_many_msg', 'Do you really want to delete selected record(s)?', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (678, 0, 'en', 'core/base/tables', 'cancel', 'Cancel', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (679, 0, 'en', 'core/base/tables', 'template', 'Template', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (680, 0, 'en', 'core/base/tables', 'email', 'Email', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (681, 0, 'en', 'core/base/tables', 'last_login', 'Last login', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (682, 0, 'en', 'core/base/tables', 'shortcode', 'Shortcode', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (683, 0, 'en', 'core/base/tables', 'image', 'Image', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (684, 0, 'en', 'core/base/tables', 'bulk_changes', 'Bulk changes', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (685, 0, 'en', 'core/base/tables', 'submit', 'Submit', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (686, 0, 'en', 'core/base/tables', 'please_select_record', 'Please select at least one record to perform this action!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (687, 0, 'en', 'core/base/tabs', 'detail', 'Detail', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (688, 0, 'en', 'core/base/tabs', 'file', 'Files', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (689, 0, 'en', 'core/base/tabs', 'record_note', 'Record Note', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (690, 0, 'en', 'core/base/tabs', 'revision', 'Revision History', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (944, 0, 'en', 'core/dashboard/dashboard', 'update_position_success', 'Update widget position successfully!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (945, 0, 'en', 'core/dashboard/dashboard', 'hide_success', 'Update widget successfully!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (946, 0, 'en', 'core/dashboard/dashboard', 'confirm_hide', 'Are you sure?', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (947, 0, 'en', 'core/dashboard/dashboard', 'hide_message', 'Do you really want to hide this widget? It will be disappear on Dashboard!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (948, 0, 'en', 'core/dashboard/dashboard', 'confirm_hide_btn', 'Yes, hide this widget', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (949, 0, 'en', 'core/dashboard/dashboard', 'cancel_hide_btn', 'Cancel', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (950, 0, 'en', 'core/dashboard/dashboard', 'collapse_expand', 'Collapse/Expand', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (951, 0, 'en', 'core/dashboard/dashboard', 'hide', 'Hide', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (952, 0, 'en', 'core/dashboard/dashboard', 'reload', 'Reload', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (953, 0, 'en', 'core/dashboard/dashboard', 'save_setting_success', 'Save widget settings successfully!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (954, 0, 'en', 'core/dashboard/dashboard', 'widget_not_exists', 'Widget is not exits!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (955, 0, 'en', 'core/dashboard/dashboard', 'manage_widgets', 'Manage Widgets', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (956, 0, 'en', 'core/dashboard/dashboard', 'fullscreen', 'Full screen', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (957, 0, 'en', 'core/dashboard/dashboard', 'title', 'Dashboard', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (972, 0, 'en', 'core/media/media', 'filter', 'Filter', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (973, 0, 'en', 'core/media/media', 'everything', 'Everything', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (974, 0, 'en', 'core/media/media', 'image', 'Image', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (975, 0, 'en', 'core/media/media', 'video', 'Video', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (976, 0, 'en', 'core/media/media', 'document', 'Document', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (977, 0, 'en', 'core/media/media', 'view_in', 'View in', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (978, 0, 'en', 'core/media/media', 'all_media', 'All media', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (979, 0, 'en', 'core/media/media', 'trash', 'Trash', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (980, 0, 'en', 'core/media/media', 'recent', 'Recent', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (981, 0, 'en', 'core/media/media', 'favorites', 'Favorites', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (982, 0, 'en', 'core/media/media', 'upload', 'Upload', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (983, 0, 'en', 'core/media/media', 'create_folder', 'Create folder', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (984, 0, 'en', 'core/media/media', 'refresh', 'Refresh', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (985, 0, 'en', 'core/media/media', 'empty_trash', 'Empty trash', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (986, 0, 'en', 'core/media/media', 'search_file_and_folder', 'Search file and folder', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (987, 0, 'en', 'core/media/media', 'sort', 'Sort', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (988, 0, 'en', 'core/media/media', 'file_name_asc', 'File name - ASC', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (989, 0, 'en', 'core/media/media', 'file_name_desc', 'File name - DESC', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (990, 0, 'en', 'core/media/media', 'uploaded_date_asc', 'Uploaded date - ASC', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (991, 0, 'en', 'core/media/media', 'uploaded_date_desc', 'Uploaded date - DESC', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (992, 0, 'en', 'core/media/media', 'size_asc', 'Size - ASC', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (993, 0, 'en', 'core/media/media', 'size_desc', 'Size - DESC', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (994, 0, 'en', 'core/media/media', 'actions', 'Actions', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (995, 0, 'en', 'core/media/media', 'nothing_is_selected', 'Nothing is selected', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (996, 0, 'en', 'core/media/media', 'insert', 'Insert', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (997, 0, 'en', 'core/media/media', 'folder_name', 'Folder name', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (998, 0, 'en', 'core/media/media', 'create', 'Create', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (999, 0, 'en', 'core/media/media', 'rename', 'Rename', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1000, 0, 'en', 'core/media/media', 'close', 'Close', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1001, 0, 'en', 'core/media/media', 'save_changes', 'Save changes', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1002, 0, 'en', 'core/media/media', 'move_to_trash', 'Move items to trash', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1003, 0, 'en', 'core/media/media', 'confirm_trash', 'Are you sure you want to move these items to trash?', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1004, 0, 'en', 'core/media/media', 'confirm', 'Confirm', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1005, 0, 'en', 'core/media/media', 'confirm_delete', 'Delete item(s)', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1006, 0, 'en', 'core/media/media', 'confirm_delete_description', 'Your request cannot rollback. Are you sure you wanna delete these items?', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1007, 0, 'en', 'core/media/media', 'empty_trash_title', 'Empty trash', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1008, 0, 'en', 'core/media/media', 'empty_trash_description', 'Your request cannot rollback. Are you sure you wanna remove all items in trash?', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1009, 0, 'en', 'core/media/media', 'up_level', 'Up one level', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1010, 0, 'en', 'core/media/media', 'upload_progress', 'Upload progress', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1011, 0, 'en', 'core/media/media', 'folder_created', 'Folder is created successfully!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1012, 0, 'en', 'core/media/media', 'gallery', 'Media gallery', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1013, 0, 'en', 'core/media/media', 'trash_error', 'Error when delete selected item(s)', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1014, 0, 'en', 'core/media/media', 'trash_success', 'Moved selected item(s) to trash successfully!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1015, 0, 'en', 'core/media/media', 'restore_error', 'Error when restore selected item(s)', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1016, 0, 'en', 'core/media/media', 'restore_success', 'Restore selected item(s) successfully!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1017, 0, 'en', 'core/media/media', 'copy_success', 'Copied selected item(s) successfully!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1018, 0, 'en', 'core/media/media', 'delete_success', 'Deleted selected item(s) successfully!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1019, 0, 'en', 'core/media/media', 'favorite_success', 'Favorite selected item(s) successfully!', '2021-09-14 05:22:42', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1020, 0, 'en', 'core/media/media', 'remove_favorite_success', 'Remove selected item(s) from favorites successfully!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1021, 0, 'en', 'core/media/media', 'rename_error', 'Error when rename item(s)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1022, 0, 'en', 'core/media/media', 'rename_success', 'Rename selected item(s) successfully!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1023, 0, 'en', 'core/media/media', 'empty_trash_success', 'Empty trash successfully!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1024, 0, 'en', 'core/media/media', 'invalid_action', 'Invalid action!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1025, 0, 'en', 'core/media/media', 'file_not_exists', 'File is not exists!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1026, 0, 'en', 'core/media/media', 'download_file_error', 'Error when downloading files!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1027, 0, 'en', 'core/media/media', 'missing_zip_archive_extension', 'Please enable ZipArchive extension to download file!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1028, 0, 'en', 'core/media/media', 'can_not_download_file', 'Can not download this file!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1029, 0, 'en', 'core/media/media', 'invalid_request', 'Invalid request!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1030, 0, 'en', 'core/media/media', 'add_success', 'Add item successfully!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1031, 0, 'en', 'core/media/media', 'file_too_big', 'File too big. Max file upload is :size bytes', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1032, 0, 'en', 'core/media/media', 'can_not_detect_file_type', 'File type is not allowed or can not detect file type!', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1033, 0, 'en', 'core/media/media', 'upload_failed', 'The file is NOT uploaded completely. The server allows max upload file size is :size . Please check your file size OR try to upload again in case of having network errors', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1034, 0, 'en', 'core/media/media', 'menu_name', 'Media', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1035, 0, 'en', 'core/media/media', 'add', 'Add media', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1036, 0, 'en', 'core/media/media', 'javascript.name', 'Name', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1037, 0, 'en', 'core/media/media', 'javascript.url', 'URL', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1038, 0, 'en', 'core/media/media', 'javascript.full_url', 'Full URL', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1039, 0, 'en', 'core/media/media', 'javascript.size', 'Size', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1040, 0, 'en', 'core/media/media', 'javascript.mime_type', 'Type', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1041, 0, 'en', 'core/media/media', 'javascript.created_at', 'Uploaded at', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1042, 0, 'en', 'core/media/media', 'javascript.updated_at', 'Modified at', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1043, 0, 'en', 'core/media/media', 'javascript.nothing_selected', 'Nothing is selected', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1044, 0, 'en', 'core/media/media', 'javascript.visit_link', 'Open link', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1045, 0, 'en', 'core/media/media', 'javascript.no_item.all_media.icon', 'fas fa-cloud-upload-alt', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1046, 0, 'en', 'core/media/media', 'javascript.no_item.all_media.title', 'Drop files and folders here', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1047, 0, 'en', 'core/media/media', 'javascript.no_item.all_media.message', 'Or use the upload button above', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1048, 0, 'en', 'core/media/media', 'javascript.no_item.trash.icon', 'fas fa-trash-alt', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1049, 0, 'en', 'core/media/media', 'javascript.no_item.trash.title', 'There is nothing in your trash currently', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1050, 0, 'en', 'core/media/media', 'javascript.no_item.trash.message', 'Delete files to move them to trash automatically. Delete files from trash to remove them permanently', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1051, 0, 'en', 'core/media/media', 'javascript.no_item.favorites.icon', 'fas fa-star', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1052, 0, 'en', 'core/media/media', 'javascript.no_item.favorites.title', 'You have not added anything to your favorites yet', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1053, 0, 'en', 'core/media/media', 'javascript.no_item.favorites.message', 'Add files to favorites to easily find them later', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1054, 0, 'en', 'core/media/media', 'javascript.no_item.recent.icon', 'far fa-clock', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1055, 0, 'en', 'core/media/media', 'javascript.no_item.recent.title', 'You did not opened anything yet', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1056, 0, 'en', 'core/media/media', 'javascript.no_item.recent.message', 'All recent files that you opened will be appeared here', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1057, 0, 'en', 'core/media/media', 'javascript.no_item.default.icon', 'fas fa-sync', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1058, 0, 'en', 'core/media/media', 'javascript.no_item.default.title', 'No items', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1059, 0, 'en', 'core/media/media', 'javascript.no_item.default.message', 'This directory has no item', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1060, 0, 'en', 'core/media/media', 'javascript.clipboard.success', 'These file links has been copied to clipboard', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1061, 0, 'en', 'core/media/media', 'javascript.message.error_header', 'Error', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1062, 0, 'en', 'core/media/media', 'javascript.message.success_header', 'Success', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1063, 0, 'en', 'core/media/media', 'javascript.download.error', 'No files selected or cannot download these files', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1064, 0, 'en', 'core/media/media', 'javascript.actions_list.basic.preview', 'Preview', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1065, 0, 'en', 'core/media/media', 'javascript.actions_list.file.copy_link', 'Copy link', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1066, 0, 'en', 'core/media/media', 'javascript.actions_list.file.rename', 'Rename', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1067, 0, 'en', 'core/media/media', 'javascript.actions_list.file.make_copy', 'Make a copy', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1068, 0, 'en', 'core/media/media', 'javascript.actions_list.user.favorite', 'Add to favorite', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1069, 0, 'en', 'core/media/media', 'javascript.actions_list.user.remove_favorite', 'Remove favorite', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1070, 0, 'en', 'core/media/media', 'javascript.actions_list.other.download', 'Download', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1071, 0, 'en', 'core/media/media', 'javascript.actions_list.other.trash', 'Move to trash', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1072, 0, 'en', 'core/media/media', 'javascript.actions_list.other.delete', 'Delete permanently', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1073, 0, 'en', 'core/media/media', 'javascript.actions_list.other.restore', 'Restore', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1074, 0, 'en', 'core/media/media', 'name_invalid', 'The folder name has invalid character(s).', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1075, 0, 'en', 'core/media/media', 'url_invalid', 'Please provide a valid URL', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1076, 0, 'en', 'core/media/media', 'path_invalid', 'Please provide a valid path', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1077, 0, 'en', 'core/media/media', 'download_link', 'Download', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1078, 0, 'en', 'core/media/media', 'url', 'URL', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1079, 0, 'en', 'core/media/media', 'download_explain', 'Enter one URL per line.', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1080, 0, 'en', 'core/media/media', 'downloading', 'Downloading...', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1186, 0, 'en', 'core/setting/setting', 'title', 'Settings', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1187, 0, 'en', 'core/setting/setting', 'email_setting_title', 'Email settings', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1188, 0, 'en', 'core/setting/setting', 'general.theme', 'Theme', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1189, 0, 'en', 'core/setting/setting', 'general.description', 'Setting site information', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1190, 0, 'en', 'core/setting/setting', 'general.title', 'General', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1191, 0, 'en', 'core/setting/setting', 'general.general_block', 'General Information', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1192, 0, 'en', 'core/setting/setting', 'general.rich_editor', 'Rich Editor', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1193, 0, 'en', 'core/setting/setting', 'general.site_title', 'Site title', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1194, 0, 'en', 'core/setting/setting', 'general.admin_email', 'Admin Email', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1195, 0, 'en', 'core/setting/setting', 'general.seo_block', 'SEO Configuration', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1196, 0, 'en', 'core/setting/setting', 'general.seo_title', 'SEO Title', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1197, 0, 'en', 'core/setting/setting', 'general.seo_description', 'SEO Description', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1198, 0, 'en', 'core/setting/setting', 'general.webmaster_tools_block', 'Google Webmaster Tools', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1199, 0, 'en', 'core/setting/setting', 'general.google_site_verification', 'Google site verification', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1200, 0, 'en', 'core/setting/setting', 'general.placeholder.site_title', 'Site Title (maximum 120 characters)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1201, 0, 'en', 'core/setting/setting', 'general.placeholder.admin_email', 'Admin Email', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1202, 0, 'en', 'core/setting/setting', 'general.placeholder.seo_title', 'SEO Title (maximum 120 characters)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1203, 0, 'en', 'core/setting/setting', 'general.placeholder.seo_description', 'SEO Description (maximum 120 characters)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1204, 0, 'en', 'core/setting/setting', 'general.placeholder.google_analytics', 'Google Analytics', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1205, 0, 'en', 'core/setting/setting', 'general.placeholder.google_site_verification', 'Google Site Verification', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1206, 0, 'en', 'core/setting/setting', 'general.cache_admin_menu', 'Cache admin menu?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1207, 0, 'en', 'core/setting/setting', 'general.enable_send_error_reporting_via_email', 'Enable to send error reporting via email?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1208, 0, 'en', 'core/setting/setting', 'general.time_zone', 'Timezone', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1209, 0, 'en', 'core/setting/setting', 'general.default_admin_theme', 'Default admin theme', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1210, 0, 'en', 'core/setting/setting', 'general.enable_change_admin_theme', 'Enable change admin theme?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1211, 0, 'en', 'core/setting/setting', 'general.enable', 'Enable', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1212, 0, 'en', 'core/setting/setting', 'general.disable', 'Disable', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1213, 0, 'en', 'core/setting/setting', 'general.enable_cache', 'Enable cache?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1214, 0, 'en', 'core/setting/setting', 'general.cache_time', 'Cache time (minutes)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1215, 0, 'en', 'core/setting/setting', 'general.cache_time_site_map', 'Cache Time Site map', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1216, 0, 'en', 'core/setting/setting', 'general.admin_logo', 'Admin logo', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1217, 0, 'en', 'core/setting/setting', 'general.admin_favicon', 'Admin favicon', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1218, 0, 'en', 'core/setting/setting', 'general.admin_title', 'Admin title', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1219, 0, 'en', 'core/setting/setting', 'general.admin_title_placeholder', 'Title show to tab of browser', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1220, 0, 'en', 'core/setting/setting', 'general.cache_block', 'Cache', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1221, 0, 'en', 'core/setting/setting', 'general.admin_appearance_title', 'Admin appearance', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1222, 0, 'en', 'core/setting/setting', 'general.admin_appearance_description', 'Setting admin appearance such as editor, language...', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1223, 0, 'en', 'core/setting/setting', 'general.seo_block_description', 'Setting site title, site meta description, site keyword for optimize SEO', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1224, 0, 'en', 'core/setting/setting', 'general.webmaster_tools_description', 'Google Webmaster Tools (GWT) is free software that helps you manage the technical side of your website', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1225, 0, 'en', 'core/setting/setting', 'general.cache_description', 'Config cache for system for optimize speed', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1226, 0, 'en', 'core/setting/setting', 'general.yes', 'Yes', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1227, 0, 'en', 'core/setting/setting', 'general.no', 'No', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1228, 0, 'en', 'core/setting/setting', 'general.show_on_front', 'Your homepage displays', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1229, 0, 'en', 'core/setting/setting', 'general.select', '— Select —', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1230, 0, 'en', 'core/setting/setting', 'general.show_site_name', 'Show site name after page title, separate with \"-\"?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1231, 0, 'en', 'core/setting/setting', 'general.locale', 'Site language', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1232, 0, 'en', 'core/setting/setting', 'general.locale_direction', 'Front site language direction', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1233, 0, 'en', 'core/setting/setting', 'general.admin_locale_direction', 'Admin language direction', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1234, 0, 'en', 'core/setting/setting', 'general.admin_login_screen_backgrounds', 'Login screen backgrounds (~1366x768)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1235, 0, 'en', 'core/setting/setting', 'email.subject', 'Subject', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1236, 0, 'en', 'core/setting/setting', 'email.content', 'Content', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1237, 0, 'en', 'core/setting/setting', 'email.title', 'Setting for email template', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1238, 0, 'en', 'core/setting/setting', 'email.description', 'Email template using HTML & system variables.', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1239, 0, 'en', 'core/setting/setting', 'email.reset_to_default', 'Reset to default', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1240, 0, 'en', 'core/setting/setting', 'email.back', 'Back to settings', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1241, 0, 'en', 'core/setting/setting', 'email.reset_success', 'Reset back to default successfully', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1242, 0, 'en', 'core/setting/setting', 'email.confirm_reset', 'Confirm reset email template?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1243, 0, 'en', 'core/setting/setting', 'email.confirm_message', 'Do you really want to reset this email template to default?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1244, 0, 'en', 'core/setting/setting', 'email.continue', 'Continue', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1245, 0, 'en', 'core/setting/setting', 'email.sender_name', 'Sender name', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1246, 0, 'en', 'core/setting/setting', 'email.sender_name_placeholder', 'Name', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1247, 0, 'en', 'core/setting/setting', 'email.sender_email', 'Sender email', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1248, 0, 'en', 'core/setting/setting', 'email.mailer', 'Mailer', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1249, 0, 'en', 'core/setting/setting', 'email.port', 'Port', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1250, 0, 'en', 'core/setting/setting', 'email.port_placeholder', 'Ex: 587', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1251, 0, 'en', 'core/setting/setting', 'email.host', 'Host', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1252, 0, 'en', 'core/setting/setting', 'email.host_placeholder', 'Ex: smtp.gmail.com', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1253, 0, 'en', 'core/setting/setting', 'email.username', 'Username', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1254, 0, 'en', 'core/setting/setting', 'email.username_placeholder', 'Username to login to mail server', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1255, 0, 'en', 'core/setting/setting', 'email.password', 'Password', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1256, 0, 'en', 'core/setting/setting', 'email.password_placeholder', 'Password to login to mail server', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1257, 0, 'en', 'core/setting/setting', 'email.encryption', 'Encryption', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1258, 0, 'en', 'core/setting/setting', 'email.mail_gun_domain', 'Domain', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1259, 0, 'en', 'core/setting/setting', 'email.mail_gun_domain_placeholder', 'Domain', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1260, 0, 'en', 'core/setting/setting', 'email.mail_gun_secret', 'Secret', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1261, 0, 'en', 'core/setting/setting', 'email.mail_gun_secret_placeholder', 'Secret', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1262, 0, 'en', 'core/setting/setting', 'email.mail_gun_endpoint', 'Endpoint', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1263, 0, 'en', 'core/setting/setting', 'email.mail_gun_endpoint_placeholder', 'Endpoint', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1264, 0, 'en', 'core/setting/setting', 'email.log_channel', 'Log channel', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1265, 0, 'en', 'core/setting/setting', 'email.sendmail_path', 'Sendmail Path', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1266, 0, 'en', 'core/setting/setting', 'email.encryption_placeholder', 'Encryption: ssl or tls', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1267, 0, 'en', 'core/setting/setting', 'email.ses_key', 'Key', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1268, 0, 'en', 'core/setting/setting', 'email.ses_key_placeholder', 'Key', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1269, 0, 'en', 'core/setting/setting', 'email.ses_secret', 'Secret', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1270, 0, 'en', 'core/setting/setting', 'email.ses_secret_placeholder', 'Secret', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1271, 0, 'en', 'core/setting/setting', 'email.ses_region', 'Region', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1272, 0, 'en', 'core/setting/setting', 'email.ses_region_placeholder', 'Region', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1273, 0, 'en', 'core/setting/setting', 'email.postmark_token', 'Token', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1274, 0, 'en', 'core/setting/setting', 'email.postmark_token_placeholder', 'Token', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1275, 0, 'en', 'core/setting/setting', 'email.template_title', 'Email templates', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1276, 0, 'en', 'core/setting/setting', 'email.template_description', 'Base templates for all emails', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1277, 0, 'en', 'core/setting/setting', 'email.template_header', 'Email template header', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1278, 0, 'en', 'core/setting/setting', 'email.template_header_description', 'Template for header of emails', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1279, 0, 'en', 'core/setting/setting', 'email.template_footer', 'Email template footer', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1280, 0, 'en', 'core/setting/setting', 'email.template_footer_description', 'Template for footer of emails', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1281, 0, 'en', 'core/setting/setting', 'email.default', 'Default', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1282, 0, 'en', 'core/setting/setting', 'email.using_queue_to_send_mail', 'Using queue job to send emails (Must to setup Queue first https://laravel.com/docs/queues#supervisor-configuration)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1283, 0, 'en', 'core/setting/setting', 'media.title', 'Media', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1284, 0, 'en', 'core/setting/setting', 'media.driver', 'Driver', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1285, 0, 'en', 'core/setting/setting', 'media.description', 'Settings for media', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1286, 0, 'en', 'core/setting/setting', 'media.aws_access_key_id', 'AWS Access Key ID', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1287, 0, 'en', 'core/setting/setting', 'media.aws_secret_key', 'AWS Secret Key', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1288, 0, 'en', 'core/setting/setting', 'media.aws_default_region', 'AWS Default Region', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1289, 0, 'en', 'core/setting/setting', 'media.aws_bucket', 'AWS Bucket', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1290, 0, 'en', 'core/setting/setting', 'media.aws_url', 'AWS URL', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1291, 0, 'en', 'core/setting/setting', 'media.do_spaces_access_key_id', 'DO Spaces Access Key ID', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1292, 0, 'en', 'core/setting/setting', 'media.do_spaces_secret_key', 'DO Spaces Secret Key', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1293, 0, 'en', 'core/setting/setting', 'media.do_spaces_default_region', 'DO Spaces Default Region', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1294, 0, 'en', 'core/setting/setting', 'media.do_spaces_bucket', 'DO Spaces Bucket', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1295, 0, 'en', 'core/setting/setting', 'media.do_spaces_endpoint', 'DO Spaces Endpoint', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1296, 0, 'en', 'core/setting/setting', 'media.do_spaces_cdn_enabled', 'Is DO Spaces CDN enabled?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1297, 0, 'en', 'core/setting/setting', 'media.media_do_spaces_cdn_custom_domain', 'Do Spaces CDN custom domain', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1298, 0, 'en', 'core/setting/setting', 'media.media_do_spaces_cdn_custom_domain_placeholder', 'https://your-custom-domain.com', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1299, 0, 'en', 'core/setting/setting', 'media.wasabi_access_key_id', 'Wasabi Access Key ID', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1300, 0, 'en', 'core/setting/setting', 'media.wasabi_secret_key', 'Wasabi Secret Key', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1301, 0, 'en', 'core/setting/setting', 'media.wasabi_default_region', 'Wasabi Default Region', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1302, 0, 'en', 'core/setting/setting', 'media.wasabi_bucket', 'Wasabi Bucket', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1303, 0, 'en', 'core/setting/setting', 'media.wasabi_root', 'Wasabi Root', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1304, 0, 'en', 'core/setting/setting', 'media.default_placeholder_image', 'Default placeholder image', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1305, 0, 'en', 'core/setting/setting', 'media.enable_chunk', 'Enable chunk size upload?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1306, 0, 'en', 'core/setting/setting', 'media.chunk_size', 'Chunk size (Bytes)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1307, 0, 'en', 'core/setting/setting', 'media.chunk_size_placeholder', 'Default: 1048576 ~ 1MB', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1308, 0, 'en', 'core/setting/setting', 'media.max_file_size', 'Chunk max file size (MB)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1309, 0, 'en', 'core/setting/setting', 'media.max_file_size_placeholder', 'Default: 1048576 ~ 1GB', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1310, 0, 'en', 'core/setting/setting', 'media.enable_watermark', 'Enable watermark?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1311, 0, 'en', 'core/setting/setting', 'media.watermark_source', 'Watermark image', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1312, 0, 'en', 'core/setting/setting', 'media.watermark_size', 'Size of watermark (%)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1313, 0, 'en', 'core/setting/setting', 'media.watermark_size_placeholder', 'Default: 10 (%)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1314, 0, 'en', 'core/setting/setting', 'media.watermark_opacity', 'Watermark Opacity (%)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1315, 0, 'en', 'core/setting/setting', 'media.watermark_opacity_placeholder', 'Default: 70 (%)', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1316, 0, 'en', 'core/setting/setting', 'media.watermark_position', 'Watermark position', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1317, 0, 'en', 'core/setting/setting', 'media.watermark_position_x', 'Watermark position X', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1318, 0, 'en', 'core/setting/setting', 'media.watermark_position_y', 'Watermark position Y', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1319, 0, 'en', 'core/setting/setting', 'media.watermark_position_top_left', 'Top left', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1320, 0, 'en', 'core/setting/setting', 'media.watermark_position_top_right', 'Top right', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1321, 0, 'en', 'core/setting/setting', 'media.watermark_position_bottom_left', 'Bottom left', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1322, 0, 'en', 'core/setting/setting', 'media.watermark_position_bottom_right', 'Bottom right', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1323, 0, 'en', 'core/setting/setting', 'media.watermark_position_center', 'Center', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1324, 0, 'en', 'core/setting/setting', 'media.turn_off_automatic_url_translation_into_latin', 'Turn off automatic URL translation into Latin?', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1325, 0, 'en', 'core/setting/setting', 'license.purchase_code', 'Purchase code', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1326, 0, 'en', 'core/setting/setting', 'license.buyer', 'Buyer', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1327, 0, 'en', 'core/setting/setting', 'field_type_not_exists', 'This field type does not exist', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1328, 0, 'en', 'core/setting/setting', 'save_settings', 'Save settings', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1329, 0, 'en', 'core/setting/setting', 'template', 'Template', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1330, 0, 'en', 'core/setting/setting', 'description', 'Description', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1331, 0, 'en', 'core/setting/setting', 'enable', 'Enable', '2021-09-14 05:22:43', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1332, 0, 'en', 'core/setting/setting', 'send', 'Send', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1333, 0, 'en', 'core/setting/setting', 'test_email_description', 'To send test email, please make sure you are updated configuration to send mail!', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1334, 0, 'en', 'core/setting/setting', 'test_email_input_placeholder', 'Enter the email which you want to send test email.', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1335, 0, 'en', 'core/setting/setting', 'test_email_modal_title', 'Send a test email', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1336, 0, 'en', 'core/setting/setting', 'test_send_mail', 'Send test mail', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1337, 0, 'en', 'core/setting/setting', 'test_email_send_success', 'Send email successfully!', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1338, 0, 'en', 'core/setting/setting', 'locale_direction_ltr', 'Left to Right', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1339, 0, 'en', 'core/setting/setting', 'locale_direction_rtl', 'Right to Left', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1340, 0, 'en', 'core/setting/setting', 'saving', 'Saving...', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1341, 0, 'en', 'core/setting/setting', 'emails_warning', 'You can add up to :count emails', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1342, 0, 'en', 'core/setting/setting', 'email_add_more', 'Add more', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1489, 0, 'en', 'core/table/table', 'operations', 'Operations', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1490, 0, 'en', 'core/table/table', 'loading_data', 'Loading data from server', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1491, 0, 'en', 'core/table/table', 'display', 'Display', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1492, 0, 'en', 'core/table/table', 'all', 'All', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1493, 0, 'en', 'core/table/table', 'edit_entry', 'Edit', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1494, 0, 'en', 'core/table/table', 'delete_entry', 'Delete', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1495, 0, 'en', 'core/table/table', 'show_from', 'Showing from', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1496, 0, 'en', 'core/table/table', 'to', 'to', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1497, 0, 'en', 'core/table/table', 'in', 'in', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1498, 0, 'en', 'core/table/table', 'records', 'records', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1499, 0, 'en', 'core/table/table', 'no_data', 'No data to display', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1500, 0, 'en', 'core/table/table', 'no_record', 'No record', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1501, 0, 'en', 'core/table/table', 'loading', 'Loading data from server', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1502, 0, 'en', 'core/table/table', 'confirm_delete', 'Confirm delete', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1503, 0, 'en', 'core/table/table', 'confirm_delete_msg', 'Do you really want to delete this record?', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1504, 0, 'en', 'core/table/table', 'cancel', 'Cancel', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1505, 0, 'en', 'core/table/table', 'delete', 'Delete', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1506, 0, 'en', 'core/table/table', 'close', 'Close', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1507, 0, 'en', 'core/table/table', 'contains', 'Contains', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1508, 0, 'en', 'core/table/table', 'is_equal_to', 'Is equal to', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1509, 0, 'en', 'core/table/table', 'greater_than', 'Greater than', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1510, 0, 'en', 'core/table/table', 'less_than', 'Less than', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1511, 0, 'en', 'core/table/table', 'value', 'Value', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1512, 0, 'en', 'core/table/table', 'select_field', 'Select field', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1513, 0, 'en', 'core/table/table', 'reset', 'Reset', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1514, 0, 'en', 'core/table/table', 'add_additional_filter', 'Add additional filter', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1515, 0, 'en', 'core/table/table', 'apply', 'Apply', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1516, 0, 'en', 'core/table/table', 'filters', 'Filters', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1517, 0, 'en', 'core/table/table', 'bulk_change', 'Bulk changes', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1518, 0, 'en', 'core/table/table', 'select_option', 'Select option', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1519, 0, 'en', 'core/table/table', 'bulk_actions', 'Bulk Actions', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1520, 0, 'en', 'core/table/table', 'save_bulk_change_success', 'Update data for selected record(s) successfully!', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1521, 0, 'en', 'core/table/table', 'please_select_record', 'Please select at least one record to perform this action!', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1522, 0, 'en', 'core/table/table', 'filtered', '(filtered from _MAX_ total records)', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1523, 0, 'en', 'core/table/table', 'search', 'Search...', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1559, 0, 'en', 'packages/menu/menu', 'name', 'Menus', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1560, 0, 'en', 'packages/menu/menu', 'key_name', 'Menu name (key: :key)', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1561, 0, 'en', 'packages/menu/menu', 'basic_info', 'Basic information', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1562, 0, 'en', 'packages/menu/menu', 'add_to_menu', 'Add to menu', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1563, 0, 'en', 'packages/menu/menu', 'custom_link', 'Custom link', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1564, 0, 'en', 'packages/menu/menu', 'add_link', 'Add link', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1565, 0, 'en', 'packages/menu/menu', 'structure', 'Menu structure', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1566, 0, 'en', 'packages/menu/menu', 'remove', 'Remove', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1567, 0, 'en', 'packages/menu/menu', 'cancel', 'Cancel', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1568, 0, 'en', 'packages/menu/menu', 'title', 'Title', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1569, 0, 'en', 'packages/menu/menu', 'icon', 'Icon', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1570, 0, 'en', 'packages/menu/menu', 'url', 'URL', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1571, 0, 'en', 'packages/menu/menu', 'target', 'Target', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1572, 0, 'en', 'packages/menu/menu', 'css_class', 'CSS class', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1573, 0, 'en', 'packages/menu/menu', 'self_open_link', 'Open link directly', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1574, 0, 'en', 'packages/menu/menu', 'blank_open_link', 'Open link in new tab', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1575, 0, 'en', 'packages/menu/menu', 'create', 'Create menu', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1576, 0, 'en', 'packages/menu/menu', 'edit', 'Edit menu', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1577, 0, 'en', 'packages/menu/menu', 'menu_settings', 'Menu settings', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1578, 0, 'en', 'packages/menu/menu', 'display_location', 'Display location', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1599, 0, 'en', 'packages/optimize/optimize', 'settings.title', 'Optimize page speed', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1600, 0, 'en', 'packages/optimize/optimize', 'settings.description', 'Minify HTML output, inline CSS, remove comments...', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1601, 0, 'en', 'packages/optimize/optimize', 'settings.enable', 'Enable optimize page speed?', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1605, 0, 'en', 'packages/page/pages', 'create', 'Create new page', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1606, 0, 'en', 'packages/page/pages', 'edit', 'Edit page', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1607, 0, 'en', 'packages/page/pages', 'form.name', 'Name', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1608, 0, 'en', 'packages/page/pages', 'form.name_placeholder', 'Page\'s name (Maximum 120 characters)', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1609, 0, 'en', 'packages/page/pages', 'form.content', 'Content', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1610, 0, 'en', 'packages/page/pages', 'form.note', 'Note content', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1611, 0, 'en', 'packages/page/pages', 'notices.no_select', 'Please select at least one record to take this action!', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1612, 0, 'en', 'packages/page/pages', 'notices.update_success_message', 'Update successfully', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1613, 0, 'en', 'packages/page/pages', 'cannot_delete', 'Page could not be deleted', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1614, 0, 'en', 'packages/page/pages', 'deleted', 'Page deleted', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1615, 0, 'en', 'packages/page/pages', 'pages', 'Pages', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1616, 0, 'en', 'packages/page/pages', 'menu', 'Pages', '2021-09-14 05:22:44', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1617, 0, 'en', 'packages/page/pages', 'menu_name', 'Pages', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1618, 0, 'en', 'packages/page/pages', 'edit_this_page', 'Edit this page', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1619, 0, 'en', 'packages/page/pages', 'total_pages', 'Total pages', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1620, 0, 'en', 'packages/page/pages', 'settings.show_on_front', 'Your homepage displays', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1621, 0, 'en', 'packages/page/pages', 'settings.select', '— Select —', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1622, 0, 'en', 'packages/page/pages', 'front_page', 'Front Page', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1641, 0, 'en', 'packages/plugin-management/plugin', 'enabled', 'Enabled', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1642, 0, 'en', 'packages/plugin-management/plugin', 'deactivated', 'Deactivated', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1643, 0, 'en', 'packages/plugin-management/plugin', 'activated', 'Activated', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1644, 0, 'en', 'packages/plugin-management/plugin', 'activate', 'Activate', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1645, 0, 'en', 'packages/plugin-management/plugin', 'deactivate', 'Deactivate', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1646, 0, 'en', 'packages/plugin-management/plugin', 'author', 'By', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1647, 0, 'en', 'packages/plugin-management/plugin', 'update_plugin_status_success', 'Update plugin successfully', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1648, 0, 'en', 'packages/plugin-management/plugin', 'plugins', 'Plugins', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1649, 0, 'en', 'packages/plugin-management/plugin', 'missing_required_plugins', 'Please activate plugin(s): :plugins before activate this plugin!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1650, 0, 'en', 'packages/plugin-management/plugin', 'remove', 'Remove', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1651, 0, 'en', 'packages/plugin-management/plugin', 'remove_plugin_success', 'Remove plugin successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1652, 0, 'en', 'packages/plugin-management/plugin', 'remove_plugin', 'Remove plugin', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1653, 0, 'en', 'packages/plugin-management/plugin', 'remove_plugin_confirm_message', 'Do you really want to remove this plugin?', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1654, 0, 'en', 'packages/plugin-management/plugin', 'remove_plugin_confirm_yes', 'Yes, remove it!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1655, 0, 'en', 'packages/plugin-management/plugin', 'total_plugins', 'Total plugins', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1656, 0, 'en', 'packages/plugin-management/plugin', 'invalid_plugin', 'This plugin is not a valid plugin, please check it again!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1657, 0, 'en', 'packages/plugin-management/plugin', 'version', 'Version', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1658, 0, 'en', 'packages/plugin-management/plugin', 'invalid_json', 'Invalid plugin.json!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1659, 0, 'en', 'packages/plugin-management/plugin', 'activate_success', 'Activate plugin successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1660, 0, 'en', 'packages/plugin-management/plugin', 'activated_already', 'This plugin is activated already!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1661, 0, 'en', 'packages/plugin-management/plugin', 'plugin_not_exist', 'This plugin is not exists.', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1662, 0, 'en', 'packages/plugin-management/plugin', 'missing_json_file', 'Missing file plugin.json!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1663, 0, 'en', 'packages/plugin-management/plugin', 'plugin_invalid', 'Plugin is valid!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1664, 0, 'en', 'packages/plugin-management/plugin', 'published_assets_success', 'Publish assets for plugin :name successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1665, 0, 'en', 'packages/plugin-management/plugin', 'plugin_removed', 'Plugin has been removed!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1666, 0, 'en', 'packages/plugin-management/plugin', 'deactivated_success', 'Deactivate plugin successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1667, 0, 'en', 'packages/plugin-management/plugin', 'deactivated_already', 'This plugin is deactivated already!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1668, 0, 'en', 'packages/plugin-management/plugin', 'folder_is_not_writeable', 'Cannot write files! Folder :name is not writable. Please chmod to make it writable!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1669, 0, 'en', 'packages/plugin-management/plugin', 'plugin_is_not_ready', 'Plugin :name is not ready to use', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1697, 0, 'en', 'packages/seo-helper/seo-helper', 'meta_box_header', 'Search Engine Optimize', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1698, 0, 'en', 'packages/seo-helper/seo-helper', 'edit_seo_meta', 'Edit SEO meta', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1699, 0, 'en', 'packages/seo-helper/seo-helper', 'default_description', 'Setup meta title & description to make your site easy to discovered on search engines such as Google', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1700, 0, 'en', 'packages/seo-helper/seo-helper', 'seo_title', 'SEO Title', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1701, 0, 'en', 'packages/seo-helper/seo-helper', 'seo_description', 'SEO description', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1707, 0, 'en', 'packages/slug/slug', 'permalink_settings', 'Permalink', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1708, 0, 'en', 'packages/slug/slug', 'settings.title', 'Permalink settings', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1709, 0, 'en', 'packages/slug/slug', 'settings.description', 'Manage permalink for all modules.', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1710, 0, 'en', 'packages/slug/slug', 'settings.preview', 'Preview', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1711, 0, 'en', 'packages/slug/slug', 'settings.turn_off_automatic_url_translation_into_latin', 'Turn off automatic URL translation into Latin?', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1712, 0, 'en', 'packages/slug/slug', 'preview', 'Preview', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1718, 0, 'en', 'packages/theme/theme', 'name', 'Themes', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1719, 0, 'en', 'packages/theme/theme', 'theme', 'Theme', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1720, 0, 'en', 'packages/theme/theme', 'author', 'Author', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1721, 0, 'en', 'packages/theme/theme', 'version', 'Version', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1722, 0, 'en', 'packages/theme/theme', 'description', 'Description', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1723, 0, 'en', 'packages/theme/theme', 'active_success', 'Activate theme :name successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1724, 0, 'en', 'packages/theme/theme', 'active', 'Active', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1725, 0, 'en', 'packages/theme/theme', 'activated', 'Activated', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1726, 0, 'en', 'packages/theme/theme', 'appearance', 'Appearance', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1727, 0, 'en', 'packages/theme/theme', 'theme_options', 'Theme options', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1728, 0, 'en', 'packages/theme/theme', 'save_changes', 'Save Changes', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1729, 0, 'en', 'packages/theme/theme', 'developer_mode', 'Developer Mode Enabled', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1730, 0, 'en', 'packages/theme/theme', 'custom_css', 'Custom CSS', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1731, 0, 'en', 'packages/theme/theme', 'custom_js', 'Custom JS', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1732, 0, 'en', 'packages/theme/theme', 'custom_header_js', 'Header JS', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1733, 0, 'en', 'packages/theme/theme', 'custom_header_js_placeholder', 'JS in header of page, wrap it inside &#x3C;script&#x3E;&#x3C;/script&#x3E;', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1734, 0, 'en', 'packages/theme/theme', 'custom_body_js', 'Body JS', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1735, 0, 'en', 'packages/theme/theme', 'custom_body_js_placeholder', 'JS in body of page, wrap it inside &#x3C;script&#x3E;&#x3C;/script&#x3E;', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1736, 0, 'en', 'packages/theme/theme', 'custom_footer_js', 'Footer JS', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1737, 0, 'en', 'packages/theme/theme', 'custom_footer_js_placeholder', 'JS in footer of page, wrap it inside &#x3C;script&#x3E;&#x3C;/script&#x3E;', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1738, 0, 'en', 'packages/theme/theme', 'remove_theme_success', 'Remove theme successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1739, 0, 'en', 'packages/theme/theme', 'theme_is_not_existed', 'This theme is not existed!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1740, 0, 'en', 'packages/theme/theme', 'remove', 'Remove', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1741, 0, 'en', 'packages/theme/theme', 'remove_theme', 'Remove theme', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1742, 0, 'en', 'packages/theme/theme', 'remove_theme_confirm_message', 'Do you really want to remove this theme?', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1743, 0, 'en', 'packages/theme/theme', 'remove_theme_confirm_yes', 'Yes, remove it!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1744, 0, 'en', 'packages/theme/theme', 'total_themes', 'Total themes', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1745, 0, 'en', 'packages/theme/theme', 'show_admin_bar', 'Show admin bar (When admin logged in, still show admin bar in website)?', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1746, 0, 'en', 'packages/theme/theme', 'settings.title', 'Theme', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1747, 0, 'en', 'packages/theme/theme', 'settings.description', 'Setting for theme', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1748, 0, 'en', 'packages/theme/theme', 'add_new', 'Add new', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1749, 0, 'en', 'packages/theme/theme', 'theme_activated_already', 'Theme \":name\" is activated already!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1750, 0, 'en', 'packages/theme/theme', 'missing_json_file', 'Missing file theme.json!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1751, 0, 'en', 'packages/theme/theme', 'theme_invalid', 'Theme is valid!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1752, 0, 'en', 'packages/theme/theme', 'published_assets_success', 'Publish assets for :themes successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1753, 0, 'en', 'packages/theme/theme', 'cannot_remove_theme', 'Cannot remove activated theme, please activate another theme before removing \":name\"!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1754, 0, 'en', 'packages/theme/theme', 'theme_deleted', 'Theme \":name\" has been destroyed.', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1755, 0, 'en', 'packages/theme/theme', 'removed_assets', 'Remove assets of a theme :name successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1756, 0, 'en', 'packages/theme/theme', 'update_custom_css_success', 'Update custom CSS successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1757, 0, 'en', 'packages/theme/theme', 'update_custom_js_success', 'Update custom JS successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1758, 0, 'en', 'packages/theme/theme', 'go_to_dashboard', 'Go to dashboard', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1759, 0, 'en', 'packages/theme/theme', 'custom_css_placeholder', 'Using Ctrl + Space to autocomplete.', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1760, 0, 'en', 'packages/theme/theme', 'theme_option_general', 'General', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1761, 0, 'en', 'packages/theme/theme', 'theme_option_general_description', 'General settings', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1762, 0, 'en', 'packages/theme/theme', 'theme_option_seo_open_graph_image', 'SEO default Open Graph image', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1763, 0, 'en', 'packages/theme/theme', 'theme_option_logo', 'Logo', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1764, 0, 'en', 'packages/theme/theme', 'theme_option_favicon', 'Favicon', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1765, 0, 'en', 'packages/theme/theme', 'folder_is_not_writeable', 'Cannot write files! Folder :name is not writable. Please chmod to make it writable!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1789, 0, 'en', 'packages/widget/widget', 'name', 'Widgets', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1790, 0, 'en', 'packages/widget/widget', 'create', 'New widget', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1791, 0, 'en', 'packages/widget/widget', 'edit', 'Edit widget', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1792, 0, 'en', 'packages/widget/widget', 'delete', 'Delete', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1793, 0, 'en', 'packages/widget/widget', 'available', 'Available Widgets', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1794, 0, 'en', 'packages/widget/widget', 'instruction', 'To activate a widget drag it to a sidebar or click on it. To deactivate a widget and delete its settings, drag it back.', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1795, 0, 'en', 'packages/widget/widget', 'number_tag_display', 'Number tags will be display', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1796, 0, 'en', 'packages/widget/widget', 'number_post_display', 'Number posts will be display', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1797, 0, 'en', 'packages/widget/widget', 'select_menu', 'Select Menu', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1798, 0, 'en', 'packages/widget/widget', 'widget_text', 'Text', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1799, 0, 'en', 'packages/widget/widget', 'widget_text_description', 'Arbitrary text or HTML.', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1800, 0, 'en', 'packages/widget/widget', 'widget_recent_post', 'Recent Posts', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1801, 0, 'en', 'packages/widget/widget', 'widget_recent_post_description', 'Recent posts widget.', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1802, 0, 'en', 'packages/widget/widget', 'widget_custom_menu', 'Custom Menu', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1803, 0, 'en', 'packages/widget/widget', 'widget_custom_menu_description', 'Add a custom menu to your widget area.', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1804, 0, 'en', 'packages/widget/widget', 'widget_tag', 'Tags', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1805, 0, 'en', 'packages/widget/widget', 'widget_tag_description', 'Popular tags', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1806, 0, 'en', 'packages/widget/widget', 'save_success', 'Save widget successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1807, 0, 'en', 'packages/widget/widget', 'delete_success', 'Delete widget successfully!', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1808, 0, 'en', 'packages/widget/widget', 'primary_sidebar_name', 'Primary sidebar', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1809, 0, 'en', 'packages/widget/widget', 'primary_sidebar_description', 'Primary sidebar section', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1829, 0, 'en', 'plugins/analytics/analytics', 'sessions', 'Sessions', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1830, 0, 'en', 'plugins/analytics/analytics', 'visitors', 'Visitors', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1831, 0, 'en', 'plugins/analytics/analytics', 'pageviews', 'Pageviews', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1832, 0, 'en', 'plugins/analytics/analytics', 'bounce_rate', 'Bounce Rate', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1833, 0, 'en', 'plugins/analytics/analytics', 'page_session', 'Pages/Session', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1834, 0, 'en', 'plugins/analytics/analytics', 'avg_duration', 'Avg. Duration', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1835, 0, 'en', 'plugins/analytics/analytics', 'percent_new_session', 'Percent new session', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1836, 0, 'en', 'plugins/analytics/analytics', 'new_users', 'New visitors', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1837, 0, 'en', 'plugins/analytics/analytics', 'visits', 'visits', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1838, 0, 'en', 'plugins/analytics/analytics', 'views', 'views', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1839, 0, 'en', 'plugins/analytics/analytics', 'view_id_not_specified', 'You must provide a valid view id. The document here: <a href=\"https://docs.botble.com/cms/:version/plugin-analytics\" target=\"_blank\">https://docs.botble.com/cms/:version/plugin-analytics</a>', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1840, 0, 'en', 'plugins/analytics/analytics', 'credential_is_not_valid', 'Analytics credentials is not valid. The document here: <a href=\"https://docs.botble.com/cms/:version/plugin-analytics\" target=\"_blank\">https://docs.botble.com/cms/:version/plugin-analytics</a>', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1841, 0, 'en', 'plugins/analytics/analytics', 'start_date_can_not_before_end_date', 'Start date :start_date cannot be after end date :end_date', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1842, 0, 'en', 'plugins/analytics/analytics', 'wrong_configuration', 'To view analytics you\'ll need to get a google analytics client id and add it to your settings. <br /> You also need JSON credential data. <br /> The document here: <a href=\"https://docs.botble.com/cms/:version/plugin-analytics\" target=\"_blank\">https://docs.botble.com/cms/:version/plugin-analytics</a>', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1843, 0, 'en', 'plugins/analytics/analytics', 'settings.title', 'Google Analytics', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1844, 0, 'en', 'plugins/analytics/analytics', 'settings.description', 'Config Credentials for Google Analytics', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1845, 0, 'en', 'plugins/analytics/analytics', 'settings.tracking_code', 'Tracking ID', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1846, 0, 'en', 'plugins/analytics/analytics', 'settings.tracking_code_placeholder', 'Example: GA-12586526-8', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1847, 0, 'en', 'plugins/analytics/analytics', 'settings.view_id', 'View ID', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1848, 0, 'en', 'plugins/analytics/analytics', 'settings.view_id_description', 'Google Analytics View ID', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1849, 0, 'en', 'plugins/analytics/analytics', 'settings.json_credential', 'Service Account Credentials', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1850, 0, 'en', 'plugins/analytics/analytics', 'settings.json_credential_description', 'Service Account Credentials', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1851, 0, 'en', 'plugins/analytics/analytics', 'widget_analytics_page', 'Top Most Visit Pages', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1852, 0, 'en', 'plugins/analytics/analytics', 'widget_analytics_browser', 'Top Browsers', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1853, 0, 'en', 'plugins/analytics/analytics', 'widget_analytics_referrer', 'Top Referrers', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1854, 0, 'en', 'plugins/analytics/analytics', 'widget_analytics_general', 'Site Analytics', '2021-09-14 05:22:45', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1881, 0, 'en', 'plugins/audit-log/history', 'name', 'Activities Logs', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1882, 0, 'en', 'plugins/audit-log/history', 'created', 'created', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1883, 0, 'en', 'plugins/audit-log/history', 'updated', 'updated', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1884, 0, 'en', 'plugins/audit-log/history', 'deleted', 'deleted', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1885, 0, 'en', 'plugins/audit-log/history', 'logged in', 'logged in', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1886, 0, 'en', 'plugins/audit-log/history', 'logged out', 'logged out', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1887, 0, 'en', 'plugins/audit-log/history', 'changed password', 'changed password', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1888, 0, 'en', 'plugins/audit-log/history', 'updated profile', 'updated profile', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1889, 0, 'en', 'plugins/audit-log/history', 'attached', 'attached', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1890, 0, 'en', 'plugins/audit-log/history', 'shared', 'shared', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1891, 0, 'en', 'plugins/audit-log/history', 'to the system', 'to the system', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1892, 0, 'en', 'plugins/audit-log/history', 'of the system', 'of the system', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1893, 0, 'en', 'plugins/audit-log/history', 'menu', 'menu', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1894, 0, 'en', 'plugins/audit-log/history', 'post', 'post', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1895, 0, 'en', 'plugins/audit-log/history', 'page', 'page', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1896, 0, 'en', 'plugins/audit-log/history', 'category', 'category', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1897, 0, 'en', 'plugins/audit-log/history', 'tag', 'tag', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1898, 0, 'en', 'plugins/audit-log/history', 'user', 'user', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1899, 0, 'en', 'plugins/audit-log/history', 'contact', 'contact', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1900, 0, 'en', 'plugins/audit-log/history', 'backup', 'backup', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1901, 0, 'en', 'plugins/audit-log/history', 'custom-field', 'custom field', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1902, 0, 'en', 'plugins/audit-log/history', 'widget_audit_logs', 'Activities Logs', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1903, 0, 'en', 'plugins/audit-log/history', 'action', 'Action', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1904, 0, 'en', 'plugins/audit-log/history', 'user_agent', 'User Agent', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1905, 0, 'en', 'plugins/audit-log/history', 'system', 'System', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1906, 0, 'en', 'plugins/audit-log/history', 'delete_all', 'Delete all records', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1933, 0, 'en', 'plugins/backup/backup', 'name', 'Backup', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1934, 0, 'en', 'plugins/backup/backup', 'backup_description', 'Backup database and uploads folder.', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1935, 0, 'en', 'plugins/backup/backup', 'create_backup_success', 'Created backup successfully!', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1936, 0, 'en', 'plugins/backup/backup', 'delete_backup_success', 'Delete backup successfully!', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1937, 0, 'en', 'plugins/backup/backup', 'restore_backup_success', 'Restore backup successfully!', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1938, 0, 'en', 'plugins/backup/backup', 'generate_btn', 'Generate backup', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1939, 0, 'en', 'plugins/backup/backup', 'create', 'Create a backup', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1940, 0, 'en', 'plugins/backup/backup', 'restore', 'Restore a backup', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1941, 0, 'en', 'plugins/backup/backup', 'create_btn', 'Create', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1942, 0, 'en', 'plugins/backup/backup', 'restore_btn', 'Restore', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1943, 0, 'en', 'plugins/backup/backup', 'restore_confirm_msg', 'Do you really want to restore this revision?', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1944, 0, 'en', 'plugins/backup/backup', 'download_uploads_folder', 'Download backup of uploads folder', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1945, 0, 'en', 'plugins/backup/backup', 'download_database', 'Download backup of database', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1946, 0, 'en', 'plugins/backup/backup', 'restore_tooltip', 'Restore this backup', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1947, 0, 'en', 'plugins/backup/backup', 'demo_alert', 'Hi guest, if you see demo site is destroyed, please help me <a href=\":link\">go here</a> and restore demo site to the latest revision! Thank you so much!', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1948, 0, 'en', 'plugins/backup/backup', 'menu_name', 'Backups', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1949, 0, 'en', 'plugins/backup/backup', 'size', 'Size', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1950, 0, 'en', 'plugins/backup/backup', 'no_backups', 'There is no backup now!', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1951, 0, 'en', 'plugins/backup/backup', 'proc_open_disabled_error', 'Function <strong>proc_open()</strong> has been disabled so the system cannot backup the database. Please contact your hosting provider to enable it.', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1952, 0, 'en', 'plugins/backup/backup', 'database_backup_not_existed', 'Backup database is not existed!', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1953, 0, 'en', 'plugins/backup/backup', 'uploads_folder_backup_not_existed', 'Backup uploads folder is not existed!', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1954, 0, 'en', 'plugins/backup/backup', 'important_message1', 'This is a simple backup feature, it is a solution for you if your site has < 1GB data and can be used for quickly backup your site.', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1955, 0, 'en', 'plugins/backup/backup', 'important_message2', 'If you have more than 1GB images/files in local storage, you should use backup feature of your hosting or VPS.', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1956, 0, 'en', 'plugins/backup/backup', 'important_message3', 'To backup your database, function <strong>proc_open()</strong> or <strong>system()</strong> must be enabled. Contact your hosting provider to enable it if it is disabled.', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1973, 0, 'en', 'plugins/block/block', 'name', 'Block', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1974, 0, 'en', 'plugins/block/block', 'create', 'New block', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1975, 0, 'en', 'plugins/block/block', 'edit', 'Edit block', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1976, 0, 'en', 'plugins/block/block', 'menu', 'Static Blocks', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1977, 0, 'en', 'plugins/block/block', 'static_block_short_code_name', 'Static Block', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1978, 0, 'en', 'plugins/block/block', 'static_block_short_code_description', 'Add a custom static block', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1979, 0, 'en', 'plugins/block/block', 'alias', 'Alias', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1984, 0, 'en', 'plugins/blog/base', 'menu_name', 'Blog', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1985, 0, 'en', 'plugins/blog/base', 'blog_page', 'Blog Page', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1986, 0, 'en', 'plugins/blog/base', 'select', '-- Select --', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1987, 0, 'en', 'plugins/blog/base', 'blog_page_id', 'Blog page', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1988, 0, 'en', 'plugins/blog/base', 'number_posts_per_page', 'Number posts per page', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1989, 0, 'en', 'plugins/blog/base', 'write_some_tags', 'Write some tags', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1990, 0, 'en', 'plugins/blog/base', 'short_code_name', 'Blog posts', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1991, 0, 'en', 'plugins/blog/base', 'short_code_description', 'Add blog posts', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1992, 0, 'en', 'plugins/blog/base', 'number_posts_per_page_in_category', 'Number of posts per page in a category', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1993, 0, 'en', 'plugins/blog/base', 'number_posts_per_page_in_tag', 'Number of posts per page in a tag', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1994, 0, 'en', 'plugins/blog/categories', 'create', 'Create new category', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1995, 0, 'en', 'plugins/blog/categories', 'edit', 'Edit category', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1996, 0, 'en', 'plugins/blog/categories', 'menu', 'Categories', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1997, 0, 'en', 'plugins/blog/categories', 'edit_this_category', 'Edit this category', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1998, 0, 'en', 'plugins/blog/categories', 'menu_name', 'Categories', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (1999, 0, 'en', 'plugins/blog/categories', 'none', 'None', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2000, 0, 'en', 'plugins/blog/categories', 'total_posts', 'Total posts: :total', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2001, 0, 'en', 'plugins/blog/member', 'dob', 'Born :date', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2002, 0, 'en', 'plugins/blog/member', 'draft_posts', 'Draft Posts', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2003, 0, 'en', 'plugins/blog/member', 'pending_posts', 'Pending Posts', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2004, 0, 'en', 'plugins/blog/member', 'published_posts', 'Published Posts', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2005, 0, 'en', 'plugins/blog/member', 'posts', 'Posts', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2006, 0, 'en', 'plugins/blog/member', 'write_post', 'Write a post', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2007, 0, 'en', 'plugins/blog/posts', 'create', 'Create new post', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2008, 0, 'en', 'plugins/blog/posts', 'edit', 'Edit post', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2009, 0, 'en', 'plugins/blog/posts', 'form.name', 'Name', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2010, 0, 'en', 'plugins/blog/posts', 'form.name_placeholder', 'Post\'s name (Maximum :c characters)', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2011, 0, 'en', 'plugins/blog/posts', 'form.description', 'Description', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2012, 0, 'en', 'plugins/blog/posts', 'form.description_placeholder', 'Short description for post (Maximum :c characters)', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2013, 0, 'en', 'plugins/blog/posts', 'form.categories', 'Categories', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2014, 0, 'en', 'plugins/blog/posts', 'form.tags', 'Tags', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2015, 0, 'en', 'plugins/blog/posts', 'form.tags_placeholder', 'Tags', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2016, 0, 'en', 'plugins/blog/posts', 'form.content', 'Content', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2017, 0, 'en', 'plugins/blog/posts', 'form.is_featured', 'Is featured?', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2018, 0, 'en', 'plugins/blog/posts', 'form.note', 'Note content', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2019, 0, 'en', 'plugins/blog/posts', 'form.format_type', 'Format', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2020, 0, 'en', 'plugins/blog/posts', 'cannot_delete', 'Post could not be deleted', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2021, 0, 'en', 'plugins/blog/posts', 'post_deleted', 'Post deleted', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2022, 0, 'en', 'plugins/blog/posts', 'posts', 'Posts', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2023, 0, 'en', 'plugins/blog/posts', 'post', 'Post', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2024, 0, 'en', 'plugins/blog/posts', 'edit_this_post', 'Edit this post', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2025, 0, 'en', 'plugins/blog/posts', 'no_new_post_now', 'There is no new post now!', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2026, 0, 'en', 'plugins/blog/posts', 'menu_name', 'Posts', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2027, 0, 'en', 'plugins/blog/posts', 'widget_posts_recent', 'Recent Posts', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2028, 0, 'en', 'plugins/blog/posts', 'categories', 'Categories', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2029, 0, 'en', 'plugins/blog/posts', 'category', 'Category', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2030, 0, 'en', 'plugins/blog/posts', 'author', 'Author', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2031, 0, 'en', 'plugins/blog/tags', 'form.name', 'Name', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2032, 0, 'en', 'plugins/blog/tags', 'form.name_placeholder', 'Tag\'s name (Maximum 120 characters)', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2033, 0, 'en', 'plugins/blog/tags', 'form.description', 'Description', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2034, 0, 'en', 'plugins/blog/tags', 'form.description_placeholder', 'Short description for tag (Maximum 400 characters)', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2035, 0, 'en', 'plugins/blog/tags', 'form.categories', 'Categories', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2036, 0, 'en', 'plugins/blog/tags', 'notices.no_select', 'Please select at least one tag to take this action!', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2037, 0, 'en', 'plugins/blog/tags', 'create', 'Create new tag', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2038, 0, 'en', 'plugins/blog/tags', 'edit', 'Edit tag', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2039, 0, 'en', 'plugins/blog/tags', 'cannot_delete', 'Tag could not be deleted', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2040, 0, 'en', 'plugins/blog/tags', 'deleted', 'Tag deleted', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2041, 0, 'en', 'plugins/blog/tags', 'menu', 'Tags', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2042, 0, 'en', 'plugins/blog/tags', 'edit_this_tag', 'Edit this tag', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2043, 0, 'en', 'plugins/blog/tags', 'menu_name', 'Tags', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2097, 0, 'en', 'plugins/captcha/captcha', 'settings.title', 'Captcha', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2098, 0, 'en', 'plugins/captcha/captcha', 'settings.description', 'Settings for Google Captcha', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2099, 0, 'en', 'plugins/captcha/captcha', 'settings.captcha_site_key', 'Captcha Site Key', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2100, 0, 'en', 'plugins/captcha/captcha', 'settings.captcha_secret', 'Captcha Secret', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2101, 0, 'en', 'plugins/captcha/captcha', 'settings.enable_captcha', 'Enable Captcha?', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2102, 0, 'en', 'plugins/captcha/captcha', 'settings.helper', 'Go here to get credentials https://www.google.com/recaptcha/admin#list.', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2103, 0, 'en', 'plugins/captcha/captcha', 'settings.hide_badge', 'Hide recaptcha badge (for v3)', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2104, 0, 'en', 'plugins/captcha/captcha', 'settings.type', 'Type', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2105, 0, 'en', 'plugins/captcha/captcha', 'settings.v2_description', 'V2 (Verify requests with a challenge)', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2106, 0, 'en', 'plugins/captcha/captcha', 'settings.v3_description', 'V3 (Verify requests with a score)', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2107, 0, 'en', 'plugins/captcha/captcha', 'failed_validate', 'Failed to validate the captcha.', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2114, 0, 'en', 'plugins/contact/contact', 'menu', 'Contact', '2021-09-14 05:22:46', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2115, 0, 'en', 'plugins/contact/contact', 'edit', 'View contact', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2116, 0, 'en', 'plugins/contact/contact', 'tables.phone', 'Phone', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2117, 0, 'en', 'plugins/contact/contact', 'tables.email', 'Email', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2118, 0, 'en', 'plugins/contact/contact', 'tables.full_name', 'Full Name', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2119, 0, 'en', 'plugins/contact/contact', 'tables.time', 'Time', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2120, 0, 'en', 'plugins/contact/contact', 'tables.address', 'Address', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2121, 0, 'en', 'plugins/contact/contact', 'tables.subject', 'Subject', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2122, 0, 'en', 'plugins/contact/contact', 'tables.content', 'Content', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2123, 0, 'en', 'plugins/contact/contact', 'contact_information', 'Contact information', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2124, 0, 'en', 'plugins/contact/contact', 'replies', 'Replies', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2125, 0, 'en', 'plugins/contact/contact', 'email.header', 'Email', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2126, 0, 'en', 'plugins/contact/contact', 'email.title', 'New contact from your site', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2127, 0, 'en', 'plugins/contact/contact', 'form.name.required', 'Name is required', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2128, 0, 'en', 'plugins/contact/contact', 'form.email.required', 'Email is required', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2129, 0, 'en', 'plugins/contact/contact', 'form.email.email', 'The email address is not valid', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2130, 0, 'en', 'plugins/contact/contact', 'form.content.required', 'Content is required', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2131, 0, 'en', 'plugins/contact/contact', 'contact_sent_from', 'This contact information sent from', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2132, 0, 'en', 'plugins/contact/contact', 'sender', 'Sender', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2133, 0, 'en', 'plugins/contact/contact', 'sender_email', 'Email', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2134, 0, 'en', 'plugins/contact/contact', 'sender_address', 'Address', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2135, 0, 'en', 'plugins/contact/contact', 'sender_phone', 'Phone', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2136, 0, 'en', 'plugins/contact/contact', 'message_content', 'Message content', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2137, 0, 'en', 'plugins/contact/contact', 'sent_from', 'Email sent from', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2138, 0, 'en', 'plugins/contact/contact', 'form_name', 'Name', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2139, 0, 'en', 'plugins/contact/contact', 'form_email', 'Email', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2140, 0, 'en', 'plugins/contact/contact', 'form_address', 'Address', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2141, 0, 'en', 'plugins/contact/contact', 'form_subject', 'Subject', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2142, 0, 'en', 'plugins/contact/contact', 'form_phone', 'Phone', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2143, 0, 'en', 'plugins/contact/contact', 'form_message', 'Message', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2144, 0, 'en', 'plugins/contact/contact', 'required_field', 'The field with (<span style=\"color: red\">*</span>) is required.', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2145, 0, 'en', 'plugins/contact/contact', 'send_btn', 'Send message', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2146, 0, 'en', 'plugins/contact/contact', 'new_msg_notice', 'You have <span class=\"bold\">:count</span> New Messages', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2147, 0, 'en', 'plugins/contact/contact', 'view_all', 'View all', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2148, 0, 'en', 'plugins/contact/contact', 'statuses.read', 'Read', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2149, 0, 'en', 'plugins/contact/contact', 'statuses.unread', 'Unread', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2150, 0, 'en', 'plugins/contact/contact', 'phone', 'Phone', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2151, 0, 'en', 'plugins/contact/contact', 'address', 'Address', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2152, 0, 'en', 'plugins/contact/contact', 'message', 'Message', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2153, 0, 'en', 'plugins/contact/contact', 'settings.email.title', 'Contact', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2154, 0, 'en', 'plugins/contact/contact', 'settings.email.description', 'Contact email configuration', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2155, 0, 'en', 'plugins/contact/contact', 'settings.email.templates.notice_title', 'Send notice to administrator', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2156, 0, 'en', 'plugins/contact/contact', 'settings.email.templates.notice_description', 'Email template to send notice to administrator when system get new contact', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2157, 0, 'en', 'plugins/contact/contact', 'no_reply', 'No reply yet!', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2158, 0, 'en', 'plugins/contact/contact', 'reply', 'Reply', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2159, 0, 'en', 'plugins/contact/contact', 'send', 'Send', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2160, 0, 'en', 'plugins/contact/contact', 'shortcode_name', 'Contact form', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2161, 0, 'en', 'plugins/contact/contact', 'shortcode_description', 'Add a contact form', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2162, 0, 'en', 'plugins/contact/contact', 'shortcode_content_description', 'Add shortcode [contact-form][/contact-form] to editor?', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2163, 0, 'en', 'plugins/contact/contact', 'message_sent_success', 'Message sent successfully!', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2207, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'theme_options.name', 'Cookie Consent', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2208, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'theme_options.description', 'Cookie consent settings', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2209, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'theme_options.enable', 'Enable cookie consent?', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2210, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'theme_options.message', 'Message', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2211, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'theme_options.button_text', 'Button text', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2212, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'theme_options.max_width', 'Max width (px)', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2213, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'theme_options.background_color', 'Background color', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2214, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'theme_options.text_color', 'Text color', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2215, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'theme_options.learn_more_url', 'Learn more URL', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2216, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'theme_options.learn_more_text', 'Learn more text', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2217, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'message', 'Your experience on this site will be improved by allowing cookies.', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2218, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'button_text', 'Allow cookies', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2219, 0, 'en', 'plugins/custom-field/base', 'admin_menu.title', 'Custom Fields', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2220, 0, 'en', 'plugins/custom-field/base', 'page_title', 'Custom Fields', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2221, 0, 'en', 'plugins/custom-field/base', 'all_field_groups', 'All field groups', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2222, 0, 'en', 'plugins/custom-field/base', 'form.create_field_group', 'Create field group', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2223, 0, 'en', 'plugins/custom-field/base', 'form.edit_field_group', 'Edit field group', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2224, 0, 'en', 'plugins/custom-field/base', 'form.field_items_information', 'Field items information', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2225, 0, 'en', 'plugins/custom-field/base', 'form.repeater_fields', 'Repeater', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2226, 0, 'en', 'plugins/custom-field/base', 'form.add_field', 'Add field', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2227, 0, 'en', 'plugins/custom-field/base', 'form.remove_field', 'Remove field', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2228, 0, 'en', 'plugins/custom-field/base', 'form.close_field', 'Close field', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2229, 0, 'en', 'plugins/custom-field/base', 'form.field_label', 'Label', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2230, 0, 'en', 'plugins/custom-field/base', 'form.field_label_helper', 'This is the title of field item. It will be shown on edit pages.', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2231, 0, 'en', 'plugins/custom-field/base', 'form.field_name', 'Field name', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2232, 0, 'en', 'plugins/custom-field/base', 'form.field_name_helper', 'The alias of field item. Accepted numbers, characters and underscore.', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2233, 0, 'en', 'plugins/custom-field/base', 'form.field_type', 'Field type', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2234, 0, 'en', 'plugins/custom-field/base', 'form.field_type_helper', 'Please select the type of this field.', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2235, 0, 'en', 'plugins/custom-field/base', 'form.field_instructions', 'Field instructions', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2236, 0, 'en', 'plugins/custom-field/base', 'form.field_instructions_helper', 'The instructions guide for user easier know what they need to input.', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2237, 0, 'en', 'plugins/custom-field/base', 'form.default_value', 'Default value', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2238, 0, 'en', 'plugins/custom-field/base', 'form.default_value_helper', 'The default value of field when leave it blank', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2239, 0, 'en', 'plugins/custom-field/base', 'form.placeholder', 'Placeholder', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2240, 0, 'en', 'plugins/custom-field/base', 'form.placeholder_helper', 'Placeholder text', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2241, 0, 'en', 'plugins/custom-field/base', 'form.rows', 'Rows', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2242, 0, 'en', 'plugins/custom-field/base', 'form.rows_helper', 'Rows of this textarea', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2243, 0, 'en', 'plugins/custom-field/base', 'form.choices', 'Choices', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2244, 0, 'en', 'plugins/custom-field/base', 'form.choices_helper', 'Enter each choice on a new line.<br>For more control, you may specify both a value and label like this:<br>red: Red<br>blue: Blue', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2245, 0, 'en', 'plugins/custom-field/base', 'form.button_label', 'Button for repeater', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2246, 0, 'en', 'plugins/custom-field/base', 'form.groups.basic', 'Basic', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2247, 0, 'en', 'plugins/custom-field/base', 'form.groups.content', 'Content', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2248, 0, 'en', 'plugins/custom-field/base', 'form.groups.choice', 'Choices', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2249, 0, 'en', 'plugins/custom-field/base', 'form.groups.other', 'Other', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2250, 0, 'en', 'plugins/custom-field/base', 'form.types.text', 'Text field', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2251, 0, 'en', 'plugins/custom-field/base', 'form.types.textarea', 'Textarea', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2252, 0, 'en', 'plugins/custom-field/base', 'form.types.number', 'Number', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2253, 0, 'en', 'plugins/custom-field/base', 'form.types.email', 'Email', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2254, 0, 'en', 'plugins/custom-field/base', 'form.types.password', 'Password', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2255, 0, 'en', 'plugins/custom-field/base', 'form.types.wysiwyg', 'WYSIWYG editor', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2256, 0, 'en', 'plugins/custom-field/base', 'form.types.image', 'Image', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2257, 0, 'en', 'plugins/custom-field/base', 'form.types.file', 'File', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2258, 0, 'en', 'plugins/custom-field/base', 'form.types.select', 'Select', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2259, 0, 'en', 'plugins/custom-field/base', 'form.types.checkbox', 'Checkbox', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2260, 0, 'en', 'plugins/custom-field/base', 'form.types.radio', 'Radio', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2261, 0, 'en', 'plugins/custom-field/base', 'form.types.repeater', 'Repeater', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2262, 0, 'en', 'plugins/custom-field/base', 'form.rules.rules', 'Display rules', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2263, 0, 'en', 'plugins/custom-field/base', 'form.rules.rules_helper', 'Show this field group if', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2264, 0, 'en', 'plugins/custom-field/base', 'form.rules.add_rule_group', 'Add rule group', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2265, 0, 'en', 'plugins/custom-field/base', 'form.rules.is_equal_to', 'Equal to', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2266, 0, 'en', 'plugins/custom-field/base', 'form.rules.is_not_equal_to', 'Not equal to', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2267, 0, 'en', 'plugins/custom-field/base', 'form.rules.and', 'And', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2268, 0, 'en', 'plugins/custom-field/base', 'form.rules.or', 'Or', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2269, 0, 'en', 'plugins/custom-field/base', 'import', 'Import', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2270, 0, 'en', 'plugins/custom-field/base', 'export', 'Export', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2271, 0, 'en', 'plugins/custom-field/base', 'publish', 'Publish', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2272, 0, 'en', 'plugins/custom-field/base', 'remove_this_line', 'Remove this line', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2273, 0, 'en', 'plugins/custom-field/base', 'collapse_this_line', 'Collapse this line', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2274, 0, 'en', 'plugins/custom-field/base', 'error_occurred', 'Error occurred', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2275, 0, 'en', 'plugins/custom-field/base', 'request_completed', 'Request completed', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2276, 0, 'en', 'plugins/custom-field/base', 'item_not_existed', 'Item is not exists', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2277, 0, 'en', 'plugins/custom-field/rules', 'groups.basic', 'Basic', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2278, 0, 'en', 'plugins/custom-field/rules', 'groups.other', 'Other', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2279, 0, 'en', 'plugins/custom-field/rules', 'groups.blog', 'Blog', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2280, 0, 'en', 'plugins/custom-field/rules', 'logged_in_user', 'Logged in user', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2281, 0, 'en', 'plugins/custom-field/rules', 'logged_in_user_has_role', 'Logged in has role', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2282, 0, 'en', 'plugins/custom-field/rules', 'page_template', 'Page template', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2283, 0, 'en', 'plugins/custom-field/rules', 'page', 'Page', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2284, 0, 'en', 'plugins/custom-field/rules', 'model_name', 'Model name', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2285, 0, 'en', 'plugins/custom-field/rules', 'model_name_page', 'Page', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2286, 0, 'en', 'plugins/custom-field/rules', 'category', 'Category', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2287, 0, 'en', 'plugins/custom-field/rules', 'post_with_related_category', 'Post with related category', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2288, 0, 'en', 'plugins/custom-field/rules', 'model_name_post', 'Post (blog)', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2289, 0, 'en', 'plugins/custom-field/rules', 'model_name_category', 'Category (blog)', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2290, 0, 'en', 'plugins/custom-field/rules', 'post_format', 'Post format', '2021-09-14 05:22:47', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2361, 0, 'en', 'plugins/gallery/gallery', 'create', 'Create new gallery', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2362, 0, 'en', 'plugins/gallery/gallery', 'edit', 'Edit gallery', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2363, 0, 'en', 'plugins/gallery/gallery', 'galleries', 'Galleries', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2364, 0, 'en', 'plugins/gallery/gallery', 'item', 'Gallery item', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2365, 0, 'en', 'plugins/gallery/gallery', 'select_image', 'Select images', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2366, 0, 'en', 'plugins/gallery/gallery', 'reset', 'Reset gallery', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2367, 0, 'en', 'plugins/gallery/gallery', 'update_photo_description', 'Update photo\'s description', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2368, 0, 'en', 'plugins/gallery/gallery', 'update_photo_description_placeholder', 'Photo\'s description...', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2369, 0, 'en', 'plugins/gallery/gallery', 'delete_photo', 'Delete this photo', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2370, 0, 'en', 'plugins/gallery/gallery', 'gallery_box', 'Gallery images', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2371, 0, 'en', 'plugins/gallery/gallery', 'by', 'By', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2372, 0, 'en', 'plugins/gallery/gallery', 'menu_name', 'Galleries', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2373, 0, 'en', 'plugins/gallery/gallery', 'gallery_images', 'Gallery images', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2374, 0, 'en', 'plugins/gallery/gallery', 'add_gallery_short_code', 'Add a gallery', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2375, 0, 'en', 'plugins/gallery/gallery', 'shortcode_name', 'Gallery images', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2376, 0, 'en', 'plugins/gallery/gallery', 'limit_display', 'Limit number display', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2391, 0, 'en', 'plugins/language/language', 'name', 'Languages', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2392, 0, 'en', 'plugins/language/language', 'choose_language', 'Choose a language', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2393, 0, 'en', 'plugins/language/language', 'select_language', 'Select language', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2394, 0, 'en', 'plugins/language/language', 'choose_language_helper', 'You can choose a language in the list or directly edit it below.', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2395, 0, 'en', 'plugins/language/language', 'language_name', 'Language name', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2396, 0, 'en', 'plugins/language/language', 'language_name_helper', 'The name is how it is displayed on your site (for example: English).', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2397, 0, 'en', 'plugins/language/language', 'locale', 'Locale', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2398, 0, 'en', 'plugins/language/language', 'locale_helper', 'Laravel Locale for the language (for example: <code>en</code>).', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2399, 0, 'en', 'plugins/language/language', 'language_code', 'Language code', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2400, 0, 'en', 'plugins/language/language', 'language_code_helper', 'Language code - preferably 2-letters ISO 639-1 (for example: en)', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2401, 0, 'en', 'plugins/language/language', 'text_direction', 'Text direction', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2402, 0, 'en', 'plugins/language/language', 'text_direction_helper', 'Choose the text direction for the language', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2403, 0, 'en', 'plugins/language/language', 'left_to_right', 'Left to right', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2404, 0, 'en', 'plugins/language/language', 'right_to_left', 'Right to left', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2405, 0, 'en', 'plugins/language/language', 'flag', 'Flag', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2406, 0, 'en', 'plugins/language/language', 'flag_helper', 'Choose a flag for the language.', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2407, 0, 'en', 'plugins/language/language', 'order', 'Order', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2408, 0, 'en', 'plugins/language/language', 'order_helper', 'Position of the language in the language switcher', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2409, 0, 'en', 'plugins/language/language', 'add_new_language', 'Add new language', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2410, 0, 'en', 'plugins/language/language', 'code', 'Code', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2411, 0, 'en', 'plugins/language/language', 'default_language', 'Is default?', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2412, 0, 'en', 'plugins/language/language', 'actions', 'Actions', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2413, 0, 'en', 'plugins/language/language', 'translations', 'Translations', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2414, 0, 'en', 'plugins/language/language', 'edit', 'Edit', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2415, 0, 'en', 'plugins/language/language', 'edit_tooltip', 'Edit this language', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2416, 0, 'en', 'plugins/language/language', 'delete', 'Delete', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2417, 0, 'en', 'plugins/language/language', 'delete_tooltip', 'Delete this language and all its associated data', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2418, 0, 'en', 'plugins/language/language', 'choose_default_language', 'Choose :language as default language', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2419, 0, 'en', 'plugins/language/language', 'current_language', 'Current record\'s language', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2420, 0, 'en', 'plugins/language/language', 'edit_related', 'Edit related language for this record', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2421, 0, 'en', 'plugins/language/language', 'add_language_for_item', 'Add other language version for this record', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2422, 0, 'en', 'plugins/language/language', 'settings', 'Settings', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2423, 0, 'en', 'plugins/language/language', 'language_hide_default', 'Hide default language from URL?', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2424, 0, 'en', 'plugins/language/language', 'language_display_flag_only', 'Flag only', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2425, 0, 'en', 'plugins/language/language', 'language_display_name_only', 'Name only', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2426, 0, 'en', 'plugins/language/language', 'language_display_all', 'Display all flag and name', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2427, 0, 'en', 'plugins/language/language', 'language_display', 'Language display', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2428, 0, 'en', 'plugins/language/language', 'switcher_display', 'Switcher language display', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2429, 0, 'en', 'plugins/language/language', 'language_switcher_display_dropdown', 'Dropdown', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2430, 0, 'en', 'plugins/language/language', 'language_switcher_display_list', 'List', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2431, 0, 'en', 'plugins/language/language', 'current_language_edit_notification', 'You are editing \"<strong class=\"current_language_text\">:language</strong>\" version', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2432, 0, 'en', 'plugins/language/language', 'confirm-change-language', 'Confirm change language', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2433, 0, 'en', 'plugins/language/language', 'confirm-change-language-message', 'Do you really want to change language to \"<strong class=\"change_to_language_text\"></strong>\" ? This action will be override \"<strong class=\"change_to_language_text\"></strong>\" version if it\'s existed!', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2434, 0, 'en', 'plugins/language/language', 'confirm-change-language-btn', 'Confirm change', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2435, 0, 'en', 'plugins/language/language', 'hide_languages', 'Hide languages', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2436, 0, 'en', 'plugins/language/language', 'hide_languages_description', 'You can completely hide content in specific languages from visitors and search engines, but still view it yourself. This allows reviewing translations that are in progress.', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2437, 0, 'en', 'plugins/language/language', 'hide_languages_helper_display_hidden', '{0} All languages are currently displayed.|{1} :language is currently hidden to visitors.|[2, Inf] :language are currently hidden to visitors.', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2438, 0, 'en', 'plugins/language/language', 'show_all', 'Show all', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2439, 0, 'en', 'plugins/language/language', 'change_language', 'Language', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2440, 0, 'en', 'plugins/language/language', 'language_show_default_item_if_current_version_not_existed', 'Show item in default language if it is not existed in current language', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2441, 0, 'en', 'plugins/language/language', 'select_flag', 'Select a flag...', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2442, 0, 'en', 'plugins/language/language', 'delete_confirmation_message', 'Do you really want to delete this language? It also deletes all items in this language and cannot rollback!', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2443, 0, 'en', 'plugins/language/language', 'added_already', 'This language was added already!', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2494, 0, 'en', 'plugins/member/dashboard', 'joined_on', 'Joined :date', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2495, 0, 'en', 'plugins/member/dashboard', 'dob', 'Born :date', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2496, 0, 'en', 'plugins/member/dashboard', 'email', 'Email', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2497, 0, 'en', 'plugins/member/dashboard', 'password', 'Password', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2498, 0, 'en', 'plugins/member/dashboard', 'password-confirmation', 'Confirm Password', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2499, 0, 'en', 'plugins/member/dashboard', 'remember-me', 'Remember Me', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2500, 0, 'en', 'plugins/member/dashboard', 'login-title', 'Login', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2501, 0, 'en', 'plugins/member/dashboard', 'login-cta', 'Login', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2502, 0, 'en', 'plugins/member/dashboard', 'register-title', 'Register', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2503, 0, 'en', 'plugins/member/dashboard', 'register-cta', 'Register', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2504, 0, 'en', 'plugins/member/dashboard', 'forgot-password-cta', 'Forgot Your Password?', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2505, 0, 'en', 'plugins/member/dashboard', 'name', 'Name', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2506, 0, 'en', 'plugins/member/dashboard', 'reset-password-title', 'Reset Password', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2507, 0, 'en', 'plugins/member/dashboard', 'reset-password-cta', 'Reset Password', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2508, 0, 'en', 'plugins/member/dashboard', 'cancel-link', 'Cancel', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2509, 0, 'en', 'plugins/member/dashboard', 'logout-cta', 'Logout', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2510, 0, 'en', 'plugins/member/dashboard', 'header_profile_link', 'Profile', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2511, 0, 'en', 'plugins/member/dashboard', 'header_settings_link', 'Settings', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2512, 0, 'en', 'plugins/member/dashboard', 'header_logout_link', 'Logout', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2513, 0, 'en', 'plugins/member/dashboard', 'unknown_state', 'Unknown', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2514, 0, 'en', 'plugins/member/dashboard', 'close', 'Close', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2515, 0, 'en', 'plugins/member/dashboard', 'save', 'Save', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2516, 0, 'en', 'plugins/member/dashboard', 'loading', 'Loading...', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2517, 0, 'en', 'plugins/member/dashboard', 'new_image', 'New image', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2518, 0, 'en', 'plugins/member/dashboard', 'change_profile_image', 'Change avatar', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2519, 0, 'en', 'plugins/member/dashboard', 'save_cropped_image_failed', 'Save cropped image failed!', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2520, 0, 'en', 'plugins/member/dashboard', 'failed_to_crop_image', 'Failed to crop image', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2521, 0, 'en', 'plugins/member/dashboard', 'failed_to_load_data', 'Failed to load data', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2522, 0, 'en', 'plugins/member/dashboard', 'read_image_failed', 'Read image failed', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2523, 0, 'en', 'plugins/member/dashboard', 'update_avatar_success', 'Update avatar successfully!', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2524, 0, 'en', 'plugins/member/dashboard', 'change_avatar_description', 'Click on image to change avatar', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2525, 0, 'en', 'plugins/member/dashboard', 'notices.error', 'Error!', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2526, 0, 'en', 'plugins/member/dashboard', 'notices.success', 'Success!', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2527, 0, 'en', 'plugins/member/dashboard', 'sidebar_title', 'Personal settings', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2528, 0, 'en', 'plugins/member/dashboard', 'sidebar_information', 'Account Information', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2529, 0, 'en', 'plugins/member/dashboard', 'sidebar_security', 'Security', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2530, 0, 'en', 'plugins/member/dashboard', 'account_field_title', 'Account Information', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2531, 0, 'en', 'plugins/member/dashboard', 'profile-picture', 'Profile picture', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2532, 0, 'en', 'plugins/member/dashboard', 'uploading', 'Uploading...', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2533, 0, 'en', 'plugins/member/dashboard', 'phone', 'Phone', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2534, 0, 'en', 'plugins/member/dashboard', 'first_name', 'First name', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2535, 0, 'en', 'plugins/member/dashboard', 'last_name', 'Last name', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2536, 0, 'en', 'plugins/member/dashboard', 'description', 'Short description', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2537, 0, 'en', 'plugins/member/dashboard', 'description_placeholder', 'Tell something about yourself...', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2538, 0, 'en', 'plugins/member/dashboard', 'verified', 'Verified', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2539, 0, 'en', 'plugins/member/dashboard', 'verify_require_desc', 'Please verify email by link we sent to you.', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2540, 0, 'en', 'plugins/member/dashboard', 'birthday', 'Birthday', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2541, 0, 'en', 'plugins/member/dashboard', 'year_lc', 'year', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2542, 0, 'en', 'plugins/member/dashboard', 'month_lc', 'month', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2543, 0, 'en', 'plugins/member/dashboard', 'day_lc', 'day', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2544, 0, 'en', 'plugins/member/dashboard', 'gender', 'Gender', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2545, 0, 'en', 'plugins/member/dashboard', 'gender_male', 'Male', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2546, 0, 'en', 'plugins/member/dashboard', 'gender_female', 'Female', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2547, 0, 'en', 'plugins/member/dashboard', 'gender_other', 'Other', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2548, 0, 'en', 'plugins/member/dashboard', 'security_title', 'Security', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2549, 0, 'en', 'plugins/member/dashboard', 'current_password', 'Current password', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2550, 0, 'en', 'plugins/member/dashboard', 'password_new', 'New password', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2551, 0, 'en', 'plugins/member/dashboard', 'password_new_confirmation', 'Confirmation password', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2552, 0, 'en', 'plugins/member/dashboard', 'password_update_btn', 'Update password', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2553, 0, 'en', 'plugins/member/dashboard', 'activity_logs', 'Activity Logs', '2021-09-14 05:22:48', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2554, 0, 'en', 'plugins/member/dashboard', 'oops', 'Oops!', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2555, 0, 'en', 'plugins/member/dashboard', 'no_activity_logs', 'You have no activity logs yet', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2556, 0, 'en', 'plugins/member/dashboard', 'actions.create_post', 'You have created post \":name\"', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2557, 0, 'en', 'plugins/member/dashboard', 'actions.update_post', 'You have updated post \":name\"', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2558, 0, 'en', 'plugins/member/dashboard', 'actions.delete_post', 'You have deleted post \":name\"', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2559, 0, 'en', 'plugins/member/dashboard', 'actions.update_setting', 'You have updated your settings', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2560, 0, 'en', 'plugins/member/dashboard', 'actions.update_security', 'You have updated your security settings', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2561, 0, 'en', 'plugins/member/dashboard', 'actions.your_post_updated_by_admin', 'Your post \":name\" is updated by administrator', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2562, 0, 'en', 'plugins/member/dashboard', 'actions.changed_avatar', 'You have changed your avatar', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2563, 0, 'en', 'plugins/member/dashboard', 'load_more', 'Load more', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2564, 0, 'en', 'plugins/member/dashboard', 'loading_more', 'Loading...', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2565, 0, 'en', 'plugins/member/dashboard', 'back-to-login', 'Back to login page', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2566, 0, 'en', 'plugins/member/member', 'create', 'New member', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2567, 0, 'en', 'plugins/member/member', 'edit', 'Edit member', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2568, 0, 'en', 'plugins/member/member', 'menu_name', 'Members', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2569, 0, 'en', 'plugins/member/member', 'confirmation_subject', 'Email verification', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2570, 0, 'en', 'plugins/member/member', 'confirmation_subject_title', 'Verify your email', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2571, 0, 'en', 'plugins/member/member', 'not_confirmed', 'The given email address has not been confirmed. <a href=\":resend_link\">Resend confirmation link.</a>', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2572, 0, 'en', 'plugins/member/member', 'confirmation_successful', 'You successfully confirmed your email address.', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2573, 0, 'en', 'plugins/member/member', 'confirmation_info', 'Please confirm your email address.', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2574, 0, 'en', 'plugins/member/member', 'confirmation_resent', 'We sent you another confirmation email. You should receive it shortly.', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2575, 0, 'en', 'plugins/member/member', 'form.email', 'Email', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2576, 0, 'en', 'plugins/member/member', 'form.password', 'Password', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2577, 0, 'en', 'plugins/member/member', 'form.password_confirmation', 'Password confirmation', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2578, 0, 'en', 'plugins/member/member', 'form.change_password', 'Change password?', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2579, 0, 'en', 'plugins/member/member', 'forgot_password', 'Forgot password', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2580, 0, 'en', 'plugins/member/member', 'login', 'Login', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2581, 0, 'en', 'plugins/member/member', 'settings.email.title', 'Member', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2582, 0, 'en', 'plugins/member/member', 'settings.email.description', 'Member email configuration', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2583, 0, 'en', 'plugins/member/member', 'first_name', 'First Name', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2584, 0, 'en', 'plugins/member/member', 'last_name', 'Last Name', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2585, 0, 'en', 'plugins/member/member', 'email_placeholder', 'Ex: example@gmail.com', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2586, 0, 'en', 'plugins/member/member', 'write_a_post', 'Write a post', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2587, 0, 'en', 'plugins/member/member', 'phone', 'Phone', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2588, 0, 'en', 'plugins/member/member', 'phone_placeholder', 'Phone', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2589, 0, 'en', 'plugins/member/member', 'confirmed_at', 'Confirmed at', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2590, 0, 'en', 'plugins/member/member', 'avatar', 'Avatar', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2591, 0, 'en', 'plugins/member/member', 'dob', 'Date of birth', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2592, 0, 'en', 'plugins/member/settings', 'title', 'Member', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2593, 0, 'en', 'plugins/member/settings', 'description', 'Settings for members', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2594, 0, 'en', 'plugins/member/settings', 'verify_account_email', 'Verify account\'s email?', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2595, 0, 'en', 'plugins/member/settings', 'verify_account_email_description', 'Need to config email in Admin -> Settings -> Email to send email verification.', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2596, 0, 'en', 'plugins/request-log/request-log', 'name', 'Request Logs', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2597, 0, 'en', 'plugins/request-log/request-log', 'status_code', 'Status Code', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2598, 0, 'en', 'plugins/request-log/request-log', 'no_request_error', 'No request error now!', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2599, 0, 'en', 'plugins/request-log/request-log', 'widget_request_errors', 'Request Errors', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2600, 0, 'en', 'plugins/request-log/request-log', 'count', 'Count', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2601, 0, 'en', 'plugins/request-log/request-log', 'delete_all', 'Delete all records', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2607, 0, 'en', 'plugins/social-login/social-login', 'settings.title', 'Social Login settings', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2608, 0, 'en', 'plugins/social-login/social-login', 'settings.description', 'Configure social login options', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2609, 0, 'en', 'plugins/social-login/social-login', 'settings.facebook.title', 'Facebook login settings', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2610, 0, 'en', 'plugins/social-login/social-login', 'settings.facebook.description', 'Enable/disable & configure app credentials for Facebook login', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2611, 0, 'en', 'plugins/social-login/social-login', 'settings.facebook.app_id', 'App ID', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2612, 0, 'en', 'plugins/social-login/social-login', 'settings.facebook.app_secret', 'App Secret', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2613, 0, 'en', 'plugins/social-login/social-login', 'settings.facebook.helper', 'Please go to https://developers.facebook.com to create new app update App ID, App Secret. A Valid OAuth Redirect URIs is :callback', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2614, 0, 'en', 'plugins/social-login/social-login', 'settings.google.title', 'Google login settings', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2615, 0, 'en', 'plugins/social-login/social-login', 'settings.google.description', 'Enable/disable & configure app credentials for Google login', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2616, 0, 'en', 'plugins/social-login/social-login', 'settings.google.app_id', 'App ID', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2617, 0, 'en', 'plugins/social-login/social-login', 'settings.google.app_secret', 'App Secret', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2618, 0, 'en', 'plugins/social-login/social-login', 'settings.google.helper', 'Please go to https://console.developers.google.com/apis/dashboard to create new app update App ID, App Secret. Callback URL is :callback', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2619, 0, 'en', 'plugins/social-login/social-login', 'settings.github.title', 'Github login settings', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2620, 0, 'en', 'plugins/social-login/social-login', 'settings.github.description', 'Enable/disable & configure app credentials for Github login', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2621, 0, 'en', 'plugins/social-login/social-login', 'settings.github.app_id', 'App ID', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2622, 0, 'en', 'plugins/social-login/social-login', 'settings.github.app_secret', 'App Secret', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2623, 0, 'en', 'plugins/social-login/social-login', 'settings.github.helper', 'Please go to https://github.com/settings/developers to create new app update App ID, App Secret. Callback URL is :callback', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2624, 0, 'en', 'plugins/social-login/social-login', 'settings.linkedin.title', 'Linkedin login settings', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2625, 0, 'en', 'plugins/social-login/social-login', 'settings.linkedin.description', 'Enable/disable & configure app credentials for Linkedin login', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2626, 0, 'en', 'plugins/social-login/social-login', 'settings.linkedin.app_id', 'App ID', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2627, 0, 'en', 'plugins/social-login/social-login', 'settings.linkedin.app_secret', 'App Secret', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2628, 0, 'en', 'plugins/social-login/social-login', 'settings.linkedin.helper', 'Please go to https://www.linkedin.com/developers/apps/new to create new app update App ID, App Secret. Callback URL is :callback', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2629, 0, 'en', 'plugins/social-login/social-login', 'settings.enable', 'Enable?', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2630, 0, 'en', 'plugins/social-login/social-login', 'menu', 'Social Login', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2655, 0, 'en', 'plugins/translation/translation', 'translations', 'Translations', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2656, 0, 'en', 'plugins/translation/translation', 'translations_description', 'Translate all words in system.', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2657, 0, 'en', 'plugins/translation/translation', 'export_warning', 'Warning, translations are not visible until they are exported back to the resources/lang file, using \'php artisan cms:translations:export\' command or publish button.', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2658, 0, 'en', 'plugins/translation/translation', 'import_done', 'Done importing, processed :counter items! Reload this page to refresh the groups!', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2659, 0, 'en', 'plugins/translation/translation', 'translation_manager', 'Translations Manager', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2660, 0, 'en', 'plugins/translation/translation', 'done_publishing', 'Done publishing the translations for group', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2661, 0, 'en', 'plugins/translation/translation', 'append_translation', 'Append new translations', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2662, 0, 'en', 'plugins/translation/translation', 'replace_translation', 'Replace existing translations', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2663, 0, 'en', 'plugins/translation/translation', 'import_group', 'Import group', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2664, 0, 'en', 'plugins/translation/translation', 'confirm_publish_group', 'Are you sure you want to publish the translations group \":group\"? This will overwrite existing language files.', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2665, 0, 'en', 'plugins/translation/translation', 'publish_translations', 'Publish translations', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2666, 0, 'en', 'plugins/translation/translation', 'back', 'Back', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2667, 0, 'en', 'plugins/translation/translation', 'edit_title', 'Enter translation', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2668, 0, 'en', 'plugins/translation/translation', 'choose_group_msg', 'Choose a group to display the group translations. If no groups are visible, make sure you have imported the translations.', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2669, 0, 'en', 'plugins/translation/translation', 'choose_a_group', 'Choose a group', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2670, 0, 'en', 'plugins/translation/translation', 'locales', 'Locales', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2671, 0, 'en', 'plugins/translation/translation', 'theme-translations', 'Theme translations', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2672, 0, 'en', 'plugins/translation/translation', 'admin-translations', 'Other translations', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2673, 0, 'en', 'plugins/translation/translation', 'translate_from', 'Translate from', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2674, 0, 'en', 'plugins/translation/translation', 'to', 'to', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2675, 0, 'en', 'plugins/translation/translation', 'no_other_languages', 'No other language to translate!', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2676, 0, 'en', 'plugins/translation/translation', 'edit', 'Edit', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2677, 0, 'en', 'plugins/translation/translation', 'locale', 'Locale', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2678, 0, 'en', 'plugins/translation/translation', 'locale_placeholder', 'Ex: en', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2679, 0, 'en', 'plugins/translation/translation', 'name', 'Name', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2680, 0, 'en', 'plugins/translation/translation', 'default_locale', 'Default locale?', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2681, 0, 'en', 'plugins/translation/translation', 'actions', 'Actions', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2682, 0, 'en', 'plugins/translation/translation', 'choose_language', 'Choose language', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2683, 0, 'en', 'plugins/translation/translation', 'add_new_language', 'Add new language', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2684, 0, 'en', 'plugins/translation/translation', 'select_language', 'Select language', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2685, 0, 'en', 'plugins/translation/translation', 'flag', 'Flag', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2686, 0, 'en', 'plugins/translation/translation', 'folder_is_not_writeable', 'Cannot write files! Folder /resources/lang is not writable. Please chmod to make it writable!', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2687, 0, 'en', 'plugins/translation/translation', 'delete', 'Delete', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2688, 0, 'en', 'plugins/translation/translation', 'confirm_delete_message', 'Do you really want to delete this locale? It will delete all files/folders for this local in /resources/lang!', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2689, 0, 'en', 'plugins/translation/translation', 'download', 'Download', '2021-09-14 05:22:49', '2021-09-14 05:23:02');
INSERT INTO `translations` VALUES (2690, 0, 'en', 'plugins/translation/translation', 'select_locale', 'Select locale', '2021-09-14 05:22:49', '2021-09-14 05:23:02');

-- ----------------------------
-- Table structure for user_meta
-- ----------------------------
DROP TABLE IF EXISTS `user_meta`;
CREATE TABLE `user_meta`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_meta_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `username` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `avatar_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `super_user` tinyint(1) NOT NULL DEFAULT 0,
  `manage_supers` tinyint(1) NOT NULL DEFAULT 0,
  `permissions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_login` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'hoganmarry@gmail.com', NULL, '$2y$10$NYdW.VoL8D7JbotBwl8/I.8zWthKa9s6AsJmistX5f8gdhmXdt7sC', NULL, '2022-04-08 14:17:24', '2022-04-08 14:17:24', '系统管理员', 'Admin', 'admin', NULL, 1, 1, NULL, NULL);

-- ----------------------------
-- Table structure for widgets
-- ----------------------------
DROP TABLE IF EXISTS `widgets`;
CREATE TABLE `widgets`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `widget_id` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sidebar_id` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of widgets
-- ----------------------------
INSERT INTO `widgets` VALUES (1, 'RecentPostsWidget', 'footer_sidebar', 'wpcmf', 0, '{\"id\":\"RecentPostsWidget\",\"name\":\"\\u6700\\u65b0\\u5185\\u5bb9\",\"number_display\":5}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (2, 'RecentPostsWidget', 'top_sidebar', 'wpcmf', 0, '{\"id\":\"RecentPostsWidget\",\"name\":\"\\u6700\\u65b0\\u5185\\u5bb9\",\"number_display\":5}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (3, 'TagsWidget', 'primary_sidebar', 'wpcmf', 0, '{\"id\":\"TagsWidget\",\"name\":\"\\u6807\\u7b7e\",\"number_display\":5}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (4, 'CustomMenuWidget', 'primary_sidebar', 'wpcmf', 1, '{\"id\":\"CustomMenuWidget\",\"name\":\"\\u5206\\u7c7b\",\"menu_id\":\"featured-categories\"}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (5, 'CustomMenuWidget', 'primary_sidebar', 'wpcmf', 2, '{\"id\":\"CustomMenuWidget\",\"name\":\"\\u793e\\u4ea4\",\"menu_id\":\"social\"}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (6, 'CustomMenuWidget', 'footer_sidebar', 'wpcmf', 1, '{\"id\":\"CustomMenuWidget\",\"name\":\"\\u7f51\\u7ad9\\u6536\\u85cf\",\"menu_id\":\"favorite-websites\"}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (7, 'CustomMenuWidget', 'footer_sidebar', 'wpcmf', 2, '{\"id\":\"CustomMenuWidget\",\"name\":\"\\u6211\\u7684\\u94fe\\u63a5\",\"menu_id\":\"my-links\"}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (8, 'RecentPostsWidget', 'footer_sidebar', 'wpcmf-en_US', 0, '{\"id\":\"RecentPostsWidget\",\"name\":\"Recent Posts\",\"number_display\":5}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (9, 'RecentPostsWidget', 'top_sidebar', 'wpcmf-en_US', 0, '{\"id\":\"RecentPostsWidget\",\"name\":\"Recent Posts\",\"number_display\":5}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (10, 'TagsWidget', 'primary_sidebar', 'wpcmf-en_US', 0, '{\"id\":\"TagsWidget\",\"name\":\"Tags\",\"number_display\":5}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (11, 'CustomMenuWidget', 'primary_sidebar', 'wpcmf-en_US', 1, '{\"id\":\"CustomMenuWidget\",\"name\":\"Categories\",\"menu_id\":\"featured-categories\"}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (12, 'CustomMenuWidget', 'primary_sidebar', 'wpcmf-en_US', 2, '{\"id\":\"CustomMenuWidget\",\"name\":\"Social\",\"menu_id\":\"social\"}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (13, 'CustomMenuWidget', 'footer_sidebar', 'wpcmf-en_US', 1, '{\"id\":\"CustomMenuWidget\",\"name\":\"Favorite Websites\",\"menu_id\":\"favorite-websites\"}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');
INSERT INTO `widgets` VALUES (14, 'CustomMenuWidget', 'footer_sidebar', 'wpcmf-en_US', 2, '{\"id\":\"CustomMenuWidget\",\"name\":\"My Links\",\"menu_id\":\"my-links\"}', '2022-04-08 14:17:40', '2022-04-08 14:17:40');

SET FOREIGN_KEY_CHECKS = 1;
